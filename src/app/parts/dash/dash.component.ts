import { Router } from "@angular/router";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "@pluritech/auth-service";
import { OrglistComponent } from "./../../component/orglist/orglist.component";
import { RolelistComponent } from "./../../component/rolelist/rolelist.component";
import { MatDialog } from "@angular/material";
import { CommonService } from "./../../_services/common.service";
@Component({
  selector: "app-dash",
  templateUrl: "./dash.component.html",
  styleUrls: ["./dash.component.scss"]
})


export class DashComponent implements OnInit, OnDestroy {
  organisationid: any;
  toggleVal: boolean = true;
  orgimg:any
  orgname:any
  adminactive: boolean = true
  provideractive: boolean = true
  frontdesk:boolean = true
  activerole:any
  totalcount
  activelabel:any
  today_count=0
  provider_unavl_count:any = 0;
  ongoing_count=0
  isDoctor:boolean = false;
  isFrontdesk:boolean = false;
  constructor(
    private router: Router,
    private authService: AuthService,
    public dialog: MatDialog,
    private commonservice:CommonService,
  ) { }

  public async logout(): Promise<void> {
    localStorage.clear();
    this.router.navigate(["/login"]);
  }

  ngOnInit() {
    this.totalcount=0
    let currentUser = JSON.parse(localStorage.getItem("orguserrole"));
    this.isDoctor = currentUser.doctor;
    this.isFrontdesk = currentUser.frontdesk;
    this.organisationid = currentUser.orgid;
    let actorganistion = JSON.parse(localStorage.getItem("actorganistion"));
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole

    if(this.activerole=="admin"){
      this.adminactive=true
    }else{
      this.adminactive=false
    }

    if(this.activerole=="l1"){
      this.provideractive=true
    }else{
      this.provideractive=false
    }

    if(this.activerole=="l2"){
      this.frontdesk=true
    }else{
      this.frontdesk=false
    }

    this.commonservice.getLoginUserDetails().subscribe((response) => {
      if(response.status_code=="200"){
        if(this.activerole=="admin"){
          this.activelabel=response.Data.roles[0].label
        }else if(this.activerole=="l1"){
          this.activelabel=response.Data.roles[1].label
        }else if(this.activerole=="l2"){
          this.activelabel=response.Data.roles[2].label
        }
        if(response.Data.org_details.org_logo!=""){
          var orgactive={
            "org_name":response.Data.org_details[0].org_name,
            "org_image":response.Data.org_details[0].org_logo_url,
            "cityname":response.Data.org_details[0].cityname
          }
          localStorage.setItem('actorganistion', JSON.stringify(orgactive));
          this.orgname = response.Data.org_details[0].org_name
          this.orgimg = response.Data.org_details[0].org_logo_url
        }else{
          let existdata = JSON.parse(localStorage.getItem('orguserrole'));
          let imgorg=existdata.orgimg
          var orgactivedata={
            "org_name":response.Data.org_details[0].org_name,
            "org_image":"",
            "cityname":response.Data.org_details[0].cityname
          }
          localStorage.setItem('actorganistion', JSON.stringify(orgactive));
          this.orgname = response.Data.org_details[0].org_name
          this.orgimg = response.Data.org_details[0].org_logo_url
        }

        localStorage.setItem('orguserrole', "")
        var orguserrole ={
          "adminrole": response.Data.roles[0].isAssigned,
          "frontdesk": response.Data.roles[1].isAssigned,
          "doctor": response.Data.roles[2].isAssigned,
          "orgname": response.Data.org_details[0].org_name,
          "orgid": response.Data.org_details[0].org_id,
          "orgimg": response.Data.org_details[0].org_logo_url,
          "org_type":response.Data.org_details[0].org_type
        }
        localStorage.setItem('orguserrole', JSON.stringify(orguserrole));
      }
    });

    this.commonservice.getQrmDashboardDetails().subscribe((response) => {
      var coundata =  Number(response.Data.new_request_count)+Number(response.Data.reschedule_request_count)
      if(coundata>0){
        this.totalcount=coundata
      }else{
        this.totalcount=0
      }
      this.today_count=response.Data.today_count
      this.ongoing_count=response.Data.ongoing_count
      this.provider_unavl_count=response.Data.provider_unavl_count
      this.commonservice.setNotificationCount({
        notification_count: response.Data.notification_count,
      });
    })
  }

  changeorganisation() {
    this.dialog.open(OrglistComponent, { disableClose: true });
  }

  changerole() {
    this.dialog.open(RolelistComponent, { disableClose: true });
  }

  ngOnDestroy() {
  }

  changeroledata(role) {
    var actas = {
      "activerole": role
    }
    localStorage.setItem('activeas', JSON.stringify(actas));
  }
}
