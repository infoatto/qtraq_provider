import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";
interface responseHeader{
    body:any;
}

@Injectable()
export class responseInterceptors implements HttpInterceptor {
    constructor(private router: Router) {

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap((response:any) => {
                if(response.status == "402"){
                    localStorage.removeItem('Adminuser');
                    localStorage.removeItem('orguserrole');
                    this.router.navigate(["/login"]);
                }
            })
        );
    }
}