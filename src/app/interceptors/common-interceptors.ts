import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class commonInterceptor implements HttpInterceptor {
    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let roletypedetails = JSON.parse(localStorage.getItem('activerole'));
        let currentUser  = JSON.parse(localStorage.getItem('Adminuser'));
        let orguserlist  = JSON.parse(localStorage.getItem('orguserrole')); 
        if(currentUser != null && currentUser != 0  && orguserlist != null && orguserlist != 0  && roletypedetails != null && roletypedetails != 0 ){    
            let usermasterid = currentUser.user_master_id;
            let utoken       = currentUser.utoken; 
            let orgid        = orguserlist.orgid;
            let roletype     = roletypedetails.activerole;
            const commonReq = req.clone({
                headers: req.headers.set('utoken',atob(utoken)).set('loginuserid',atob(usermasterid)).set("orgid",orgid).set("roletype",roletype).set("usertype","P").set("apptype","W")
            });
            return next.handle(commonReq);
        }else{
            return next.handle(req);
        }
    }
}