
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { DashComponent } from './parts/dash/dash.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{AuthGuard} from './auth.guard';
import { AddeditorganisationComponent } from './component/addeditorganisation/addeditorganisation.component'
import { AddeditproviderComponent } from './component/addeditprovider/addeditprovider.component'
import { QuesetupMasterComponent } from './component/quesetup/quesetup-master.component'
import { AddeditQueMasterComponent } from './component/addedit-que-master/addedit-que-master.component'
import {AddSystemroleMasterComponent} from './component/add-systemrole-master/add-systemrole-master.component'//
//import { PatienthistoryComponent } from './component/patienthistory/patienthistory.component'
import { SiteComponent } from './component/sitelayout/site.component'
import{ AvailabilityComponent } from './component/availability/availability.component'
import{ DescriptionComponent } from './component/description/description.component'
import { DiagnosisandprescriptionComponent } from './component/diagnosisandprescription/diagnosisandprescription.component';
import { NotificationComponent } from './component/notification/notification.component';



const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'forgot',
    component: ForgotPasswordComponent,
  },
  //Site routes goes here 
  { 
      path: '', 
      component: SiteComponent,
      children: [
        { path: '', component: DashComponent, canActivate: [AuthGuard], pathMatch: 'full'},
        { path: 'dashboard', component: DashComponent, canActivate: [AuthGuard], pathMatch: 'full'},
        { path: 'addeditorganisation',component: AddeditorganisationComponent,canActivate: [AuthGuard],},
        { path: 'addeditorganisation/:orgid', component: AddeditorganisationComponent, canActivate: [AuthGuard],},
        { path: 'providersList', loadChildren: "./component/providersmaster/providersmaster.module#ProvidersmasterModule"},
        { path: 'description', component: DescriptionComponent, canActivate: [AuthGuard],},
        { path: 'editprofile', loadChildren: "./component/editprofile/editprofile.module#EditprofileModule",canActivate: [AuthGuard],},
        { path: 'providersList/addeditprovider', component: AddeditproviderComponent, canActivate: [AuthGuard],},
        { path: 'providersList/addeditprovider/:providerid', component: AddeditproviderComponent, canActivate: [AuthGuard],},
        {
          path: 'quesetup',
          component: QuesetupMasterComponent,
          canActivate: [AuthGuard],
        },
        
       
        {
          path: 'availabilty',
          component: AvailabilityComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'queuerenewal',
          loadChildren: "./component/qtraqrenewals/qtraqrenewals-master.module#QtraqrenewalsMasterModule",
         canActivate: [AuthGuard],
        },
        
        {
          path: 'changepassword',
          loadChildren: "./component/changePassword/changePassword.module#ChangePasswordModule",
        canActivate: [AuthGuard],
        },
        
        {
          path: 'quesetup/addqueue/:providerid',
          component: AddeditQueMasterComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'quesetup/addeditque/:providerid/:queue_id',
          component: AddeditQueMasterComponent,
          canActivate: [AuthGuard],
        },
        {
          path: 'systemroles',
          loadChildren: "./component/addedit-system-rolesmaster/addedit-system-rolesmaster.module#AddeditSystemRolesMasterModule",
          canActivate: [AuthGuard],
       },
        {
          path: 'systemroles/addsystemroles',
          component: AddSystemroleMasterComponent,
          canActivate: [AuthGuard],
        },
        { 
          path: 'queueaccessprolist',
           loadChildren: "./component/queueaccessprolistmaster/queueaccessprolist.module#QueueaccessprolistModule",
        canActivate: [AuthGuard],
        },
        {

 path: 'requestsreceived',
 loadChildren: "./component/requestsreceived/requestsreceived.module#RequestsreceivedModule",
 canActivate: [AuthGuard],
        },
        {
          path: 'searchtokens',
          loadChildren: "./component/searchtokens/searchtoken.module#SearchtokenModule",
         canActivate: [AuthGuard],
        },
        {
          path: 'doctoravailability',
         loadChildren: "./component/doctoravailability/doctoravailability.module#DoctoravailabilityModule",
          canActivate: [AuthGuard],
        },
        {
          path: 'assignnewtoken',
         loadChildren: "./component/assignnewtoken/assignnewtoken.module#AssignnewtokenModule",
        canActivate: [AuthGuard],
      
      },
        {
          path: "todaysqueue",
          loadChildren: "./component/todaysqueue/todaysqueue.module#TodaysqueueModule",
          canActivate: [AuthGuard],
        
    },

{
path: "providerongoing",
loadChildren: "./component/todaysqueue/todaysqueue.module#TodaysqueueModule",
canActivate: [AuthGuard],
},
{
  path: "ongoingqueues",
  loadChildren: "./component/todaysqueue/todaysqueue.module#TodaysqueueModule",
  canActivate: [AuthGuard],
  data: { "somedata": "somedata" },
},
    {
          path: 'weeklydashboard',
          loadChildren: "./component/weeklydashboard/weeklydashboard.module#WeeklydashboardModule",
        canActivate: [AuthGuard],
        },
        {
          path: 'patienthistory',
          loadChildren: "./component/patienthistory/patienthistory.module#PatienthistoryModule",
          canActivate: [AuthGuard],
        },
        {
          path: 'overridemasterqueues',
          loadChildren: "./component/overridemasterqueues/overridemasterqueues.module#OverridemasterqueuesModule",
   
          //component: OverridemasterqueuesComponent,
          canActivate: [AuthGuard],
        },

        {
          path: "futurepastqueues",
          loadChildren: "./component/futurepastqueues/futurepastqueues.module#FuturepastqueuesModule",
            canActivate: [AuthGuard],
        },
        {
          path: "diagnosisandprescription",
          component:DiagnosisandprescriptionComponent,
          canActivate: [AuthGuard],
        },
        {
          path: "notifications",
          component:NotificationComponent,
          canActivate: [AuthGuard],
        },
  
  
      ]
  },
]



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
