/*
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class MessagingService {

  private messaging = firebase.messaging()

  private messageSource = new Subject()
  currentMessage = this.messageSource.asObservable()

  constructor(private afs: AngularFirestore) {

  }

  

  // get permission to send messages
  getPermission(user) {
    this.messaging.requestPermission()
    .then(() => {
      return this.messaging.getToken()
    })
    .then(token => {

      
      this.saveToken(user, token)
    })
    .catch((err) => {

    });
  }


  // Listen for token refresh
  monitorRefresh(user) {
    this.messaging.onTokenRefresh(() => {
      this.messaging.getToken()
      .then(refreshedToken => {
        this.saveToken(user, refreshedToken)
      })
      .catch( err => console.log(err, 'Unable to retrieve new token') )
    });
  }

  

  // used to show message when app is open
  receiveMessages() {



    this.messaging.onMessage(payload => {

alert(payload.notification.body)

     this.messageSource.next(payload)
   });

  }


gettokendata()
{

var constdata=""
this.messaging.getToken().then(token => {
//return token


constdata=token
return constdata
  })

}
  private saveToken(user, token): void {
const currentTokens = user.fcmTokens || { }
localStorage.removeItem('currentTokens')
localStorage.setItem('currentTokens',token);
      if (!currentTokens[token]) {
        const userRef = this.afs.collection('users').doc(user.uid)
        const tokens = { currentTokens, [token]: true }
        userRef.update({ fcmTokens: tokens })
      }
  }



}
*/