import { Component,ViewChild,OnInit } from '@angular/core';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { FormBuilder,FormControl,FormGroup,FormArray,Validators,NgForm } from '@angular/forms';
import { TodaysqueueService } from './../../_services/todaysqueue.service';
import { INgxMyDpOptions,IMyDateModel } from 'ngx-mydatepicker';
import { MatSnackBar } from '@angular/material';
import { NgProgress } from 'ngx-progressbar';
import { SearchtokensService } from './../../_services/searchtokens.service';
import { MessagequelistComponent } from './../../component/messagequelist/messagequelist.component'
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import { TimeFormatPipe } from './../../pipes/time.pipe'
import { PatienthistoryService } from './../../_services/patienthistory.service';
import { RequestreceivedService } from './../../_services/requestsreceived.service';
import { DatePipe } from '@angular/common';
import { MatDialogConfig } from '@angular/material'
import { DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component'
import { MatDialog,MatDialogRef,MAT_DIALOG_DATA } from '@angular/material';
@Component({
	selector: 'ongoingqueues',
	templateUrl: './ongoingqueues.component.html',
	styleUrls: ['./ongoingqueues.component.scss']
})
export class OngoingqueuesComponent implements OnInit {
	dropdownSettings = {};
	myOptions: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
	};
	dropdownSettingsnewdata = {};
	searchtokenallshow: Boolean = true
	checktoday = 0
	token_provider_id: any
	usermasterid: any
	searchtokendatadetail: any
	tokentype: any
	changeseqtokenid: any
	searchtokendatadetailshowdata: Boolean = false
	alldata: any
	providerlist: Boolean = true
	ongoingqueuelistdata: Boolean = true
	ongoingtab: Boolean = false
	broadcast: Boolean = false
	broadcasttoday: Boolean = false
	is_doctor_arrived: any
	pending: any
	ongoing: any
	noshow: any
	completedorcancelled: any
	profileimage_path: any
	first_name: any
	user_master_id: any
	selectedindex = 0;
	ongoingqueuelist: any
	todayongoingqueuelist: any
	progressbar: any
	queuelist: Boolean = false
	allTokensCount: any
	transactionQueueTokens: any
	errorMessageongoing: any
	doctoravailabilityfilter: any = {
		month: "",
		year: ""
	};
	adddoctoravailability: any = {
		unavailabilitydaytype: ''
	}
	errorMessage: string;
	orgname: any
	orgimage: any
	totalRecords: any
	ongoingqueuedata: any
	queue_details: any
	queue_detailstoday: any
	queue_tokens: any
	avg_consulting_time: any
	avg_waiting_time: any
	cancelCompletedTokensCount: any
	checkup_total_amount: any
	noshowTokensCount: any
	ongoingTokensCount: any
	pendingTokensCount: any
	queue_transaction_id: any
	ongoingestimated_end_time
	ongoingqueue_date
	ongoingprovider_name
	ongoingqueue_pic_url
	ongoingqueue_name
	broadcastlisting: any
	broadcastlistingpage = 1
	broadcasterror: any
	addbroadcastmessage: Boolean = false
	broadcastmsgerror: any
	page = 0;
	size = 10;
	ongoinglist: any
	broadcastmesseglist: any
	getTransactionTokenList: any
	completedorcancelledpage = 0
	noshowpage = 0
	ongoingpage = 0
	pendingpage = 0
	allpage = 0
	modelFlag: Boolean = false
	cancelmodel: Boolean = false
	latertoday: Boolean = false
	total_tokens: any
	ongoingqueuelistdatashow: Boolean = false
	transactionqueueid: any
	quelistdatareshadule: any
	requestreceived: Boolean = false
	getTokenDetailsreshadule: any
	pipe: any
	token_idres: any
	token_noconres: any
	token_datereq: any
	token_timereq: any
	queue_namereqexisting: any
	newrequested_date: any
	reshaduledate: any
	queue_reqque: any
	queue_namereq: any
	token_nores: any
	token_timeres: any
	restoken_time: any
	customfield: any
	token_id: any
	hiddenall: Boolean = true
	todaystoken: Boolean = false
	dialogRef: any
	onassign_custom_form_values: any
	oncomplete_custom_form_values: any
	concatarrayforresult: any
	//dignosis start 
	dignosispriscription: Boolean = false
	patienthistorydata: any
	token_details: any
	diagnosis_details: any
	prescription_details: any
	test_details: any
	prescription_images: any
	report_images: any
	//dignosis end
	//assign start
	queue_id: any
	token_no: any
	token_time: any
	token_apitime: any
	quelistdata: any
	queselecteddate: any
	myFormattedDate: any
	assignnewtokenform: any
	selectedproid: any
	mobilesearchmasterid: any
	selfname: any
	tokenrequestorno: any
	bookedfor: any
	bookedforself: Boolean = false
	bookingtypeselectedtype: any
	token_date: any
	searchfirst_name: any
	regno: any
	usergendermale: any
	searchmobileage: any
	bookingusername: any
	searchfield: any
	assignsearchdiv: any
	userprofileimg: any
	usergenderfemale: any
	usergenderOther: any
	familymemeberidfordetail: any
	bookedother: any
	searchbookinguserid: any
	cancelmodelforassigndat: Boolean = false
	assignbuttonshow: any
	selfnumber: any
	selfimage: any
	bookingfordropdownvis: any
	publicorgname: any
	familymember: any
	bookingtype: any
	familymembershow: Boolean = false
	//assign end
	currentdate = new Date();
	//provider id selected
	provideridselected: any
	//provider end
	todayongoingmsg: Boolean = false
	preascription_title: any
	/*   prescription start 6-20-2019    */
	public totalfiles: Array < File > = [];
	patienthistoryaddmore: Boolean = false
	showformfield: Boolean = false
	image_name: any
	submitdate: any
	public lengthCheckToaddMore = 0;

	model: any = {
		date: {
			year: 2018,
			month: 10,
			day: 9
		}
	};
	/*priscription end  6-20-2019*/
	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private ongoingqueues: TodaysqueueService,
		private snackbar: MatSnackBar,
		public ngProgress: NgProgress,
		private searchtoken: SearchtokensService,
		private requestrceived: RequestreceivedService,
		public dialog: MatDialog,
		private assignnewtoken: AssignnewtokenService,
		private patientservice: PatienthistoryService
	) {}
	@ViewChild(NgForm) userlogin: NgForm;
	public documentGrp: FormGroup;
	public doctoravunailability: FormGroup;
	public addunavailability: FormGroup;
	familymemberselfshow: boolean = false
	provider_full_name: any
	profile_pic_url: any
	keys: any;
	months: any;
	years = [];
	public totalFileName = [];
  
  	ngOnInit() {
		this.documentGrp = this.formBuilder.group({
      documentFile: new FormControl(File),
      items: this.formBuilder.array([this.createUploadDocuments(this.preascription_title, this.image_name)]),
    });
		this.dignosispriscription = false
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'custom_field_value_id',
			textField: 'custom_list_text',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};
		this.dropdownSettingsnewdata = {
			singleSelection: false,
			idField: 'custom_field_value_id',
			textField: 'custom_list_text',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};

		this.familymemberselfshow = true
		let actorganistion = JSON.parse(localStorage.getItem('orguserrole'));
		this.publicorgname = actorganistion.orgname
		this.dignosispriscription = false
		this.requestreceived = false
		this.hiddenall = true
		this.ongoingqueuelistdatashow = true
		this.latertoday = true
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let user_master_id = atob(currentUser.user_master_id);
		let profileimage_path = atob(currentUser.profileimage_path);
		let first_name = atob(currentUser.profileimage_path);
		this.getongoingqueue(profileimage_path, first_name, user_master_id)
		this.pipe = new DatePipe('en-US');
		this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
		this.queselecteddate = this.myFormattedDate
		this.submitdate = ""
		this.bookingtype = "New"
	}

	createUploadDocuments(preascription_title, image_name): FormGroup {
		return this.formBuilder.group({
			preascription_title: preascription_title,
			image_name: image_name,
		});
	}

	addPrescription(): void {
		this.items.insert(0, this.createUploadDocuments(this.preascription_title, this.image_name))
		this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
	}

	get items(): FormArray {
		return this.documentGrp.get('items') as FormArray;
	};

	getongoingqueue(profileimage_path, first_name, user_master_id) {
		this.profileimage_path = profileimage_path
		this.first_name = first_name
		this.user_master_id = user_master_id
		this.ongoingqueues.getongoingqueue(user_master_id).subscribe((response) => {
        if (response.status_code == '200') {
          this.providerlist = false
          this.ongoingqueuelist = response.Data.todaysongoing
          this.todayongoingqueuelist = response.Data.todayslater
        } else if (response.status_code == '402') {
          localStorage.clear();
          this.router.navigate(['/login']);
        } else {
          this.errorMessageongoing = response.Metadata.Message
        }
      },(err: any) => console.log("error", err),
      () =>console.log("error in ongoing queue")
    );
	}

	getongoingqueuetoken(queue_transaction_id) {
		this.router.navigate(['/todaysqueue/'], {
			queryParams: {
				"ongoingqueue": "ongoingqueue",
				"ongoing_queue_transaction_id": queue_transaction_id
			},
			skipLocationChange: true
		})
	}
}