import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OngoingqueuesComponent } from './ongoingqueues.component';

describe('OngoingqueuesComponent', () => {
  let component: OngoingqueuesComponent;
  let fixture: ComponentFixture<OngoingqueuesComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngoingqueuesComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(OngoingqueuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
