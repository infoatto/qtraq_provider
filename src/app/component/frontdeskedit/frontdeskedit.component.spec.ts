import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontdeskeditComponent } from './frontdeskedit.component';

describe('FrontdeskeditComponent', () => {
  let component: FrontdeskeditComponent;
  let fixture: ComponentFixture<FrontdeskeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontdeskeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontdeskeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
