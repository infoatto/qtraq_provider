import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagequelistComponent } from './messagequelist.component';

describe('MessagequelistComponent', () => {
  let component: MessagequelistComponent;
  let fixture: ComponentFixture<MessagequelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagequelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagequelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
