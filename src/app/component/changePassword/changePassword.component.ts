import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule } from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';

//import { NotificationService } from './../../_services/notification.service';
import { MatSnackBar } from '@angular/material';
import{ ChangePasswordService } from './../../_services/changePassword.service';
import { CommonService } from "./../../_services/common.service";

@Component({
  selector: 'changePassword',
  templateUrl: './changePassword.component.html',
  styleUrls: ['./changePassword.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  toggleVal:boolean = false;
  public addeditque: FormGroup;

  errorMessage: string;
  submitted = false;
  passwordvalidatio:string;
  publicorgname:any

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private cs:CommonService,
    private snackbar: MatSnackBar,
    private changepassword:ChangePasswordService) 
  { 


  }


  ngOnInit() 
  {

    let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name

this.addeditque = this.formBuilder.group({
  currentpassword:'',
  newpassword:'',
  confirmPassword:''
})
}

//OnqueSubmit(formValue: any)
onSubmit(formValue: any) 
{



if(formValue.newpassword.length>=8)
{
//return false

  if(formValue.confirmPassword==formValue.newpassword)
{
  
  //currentpassword
  this.changepassword.getchangepassword(formValue.currentpassword,formValue.confirmPassword)
    .subscribe(
       data => {
       if(data.status_code==200)
       {

        this.snackbar.open(data.Metadata.Message, '', {
          duration: 2000,
        });

this.router.navigate(['/']);
       // this.passwordvalidatio=" confirmPassword not match with password"

       }
       else
       {



        //this.errorMessage=data
//this.errorMessage=data.Metadata.Message
this.snackbar.open(data.Metadata.Message, '', {
  duration: 2000,
});

       }
      })

}
else
{

  this.snackbar.open("confirmPassword not match with password", '', {
    duration: 2000,
});

//this.passwordvalidatio=" confirmPassword not match with password"
}
}
else{
  this.snackbar.open("password should have minimum 8 characters length", '', {
    duration: 2000,
});


}



}

toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}



backtohome()
{
this.cs.backtohome()
}





}
