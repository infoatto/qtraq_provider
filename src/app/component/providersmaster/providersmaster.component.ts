import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { ProvidersService } from "./../../_services/providers.service";
import { FormsModule } from "@angular/forms";
import {MatSnackBar} from '@angular/material';

import { CommonService } from "./../../_services/common.service";


@Component({
  selector: "app-organisationmaster",
  templateUrl: "./providersmaster.component.html",
  styleUrls: ["./organisationmaster.component.css"]
})

export class ProvidersmasterComponent implements OnInit {
  prolist: any;
  totalRecords = "";
  page = 0;
  publicorgname:any
  toggleVal:boolean = false;
  organisationfilter: any = {
    orgname: "",
    orgstatus: ""
  };
providername:any
providernamedata:any
activerole:any


  size = 10;

  constructor(
    private providerserv: ProvidersService,
    private elementRef: ElementRef,
    private snackbar:MatSnackBar,
    private router:Router,
    private cs:CommonService
  ) {}
  ngOnInit() {



    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole

    let currentorg = JSON.parse(localStorage.getItem('orguserrole'));

let orgname = currentorg.orgname

    if(this.activerole=="l2")
    {


      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      let usermasterid=currentUser.user_master_id;
      let first_name=currentUser.first_name; 
      let middle_name=currentUser.middle_name; 
      let last_name=currentUser.last_name; 
      //atob(usermasterid

      this.providername = 'Dr. '+atob(first_name)+" "+atob(last_name)

      //this.router.navigate(['/providersList/addeditprovider/'+response.Data[0].user_master_id]);
      
      this.router.navigate(['/providersList/addeditprovider/'+atob(usermasterid)],{
        queryParams:{"providernamedata":this.providername}, skipLocationChange: true
        });
      



    }else{

 this.providerserv
.checkOrganisationslist()
.subscribe(
response => {
if(response.status_code=="200")
{
if(response.Data.length>1)
{
 this.getorglist(1, this.organisationfilter);
}
else
{
this.providername = 'Dr. '+response.Data[0].first_name+" "+response.Data[0].last_name

//this.router.navigate(['/providersList/addeditprovider/'+response.Data[0].user_master_id]);

this.router.navigate(['/providersList/addeditprovider/'+response.Data[0].user_master_id],{
  queryParams:{"providernamedata":this.providername}, skipLocationChange: true
  });

}
}
else
{
  this.router.navigate(['/']);
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
})
let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
    }

  


}

divclick(user_master_id,lastname,firstname)
{
 this.providernamedata = 'Dr. '+firstname+" "+lastname

 this.router.navigate(['/providersList/addeditprovider/'+user_master_id],{
queryParams:{"providernamedata":this.providernamedata}, skipLocationChange: true
});

}



  getorglist(page: number, orgfilter) {
    this.providerserv
      .getOrganisationslist(page - 1, this.size, orgfilter)
      .subscribe(
        response => {

          if(response.status_code=="200")
          {
          this.prolist = response.Data;
          this.totalRecords = response.total_count;
          this.page = page;
          }
          else
          {
            this.router.navigate(['/']);

            this.snackbar.open(response.Metadata.Message,'', {
              duration: 2000,
              });
            
          }
        
        },
        (err: any) => console.log("error", err),
        () =>
          console.log("getCustomersPage() retrieved customers for page:", +page)
      );
  }

onSubmit() {
this.getorglist(1, this.organisationfilter);
}

clearfilter() {
this.organisationfilter.orgname = "";
this.organisationfilter.orgstatus = "";
this.getorglist(1, this.organisationfilter);
}

pageChanged(event) {
this.getorglist(event, this.organisationfilter);
}

statuschange(testid, status) {
this.providerserv.updatestatus(testid, status).subscribe(data => {
if (data.status_code == 200) {
      } else {
      }
    });
  }


  backtohome()
  {
    this.cs.backtohome()
  }

}
