import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//import {  PostsComponent} from './posts.component'
import { ProvidersmasterComponent } from './providersmaster.component'
//import { HomeComponent } from './home/home.component';
//import { DatanewComponent } from './datanew/datanew.component';
const routes: Routes = [
   // { path: "po123", component: HomeComponent},
    { path: "", component: ProvidersmasterComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [
        RouterModule.forChild(routes)
    
    ]
})
export class ProvidersmasterRoutingModule { }