import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrglistComponent } from './orglist.component';

describe('OrglistComponent', () => {
  let component: OrglistComponent;
  let fixture: ComponentFixture<OrglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
