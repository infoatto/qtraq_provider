import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { CommonService } from './../../_services';
import { ActivatedRoute, Router } from '@angular/router';
import { SidebarToggleService } from 'app/_services/sidebar-toggle.service';
import {MatSnackBar} from '@angular/material';
import { ErrorsnackbarComponent } from './../../pages/errorsnackbar/errorsnackbar.component'

@Component({
  selector: 'app-orglist',
  templateUrl: './orglist.component.html',
  styleUrls: ['./orglist.component.scss']
})
export class OrglistComponent implements OnInit {

  organisationlist: any

  constructor(
    private router:Router,
    private orgservice: CommonService,
    public dialogRef: MatDialogRef<OrglistComponent>,
    private sideToggleService:SidebarToggleService,
    private snackbar:MatSnackBar,
     ) {}

  ngOnInit() {
    this.orgservice.getorganisationlist()
    .subscribe(
      (response) =>  {
          if(response.status_code==200)
          {
            this.organisationlist=response.Data

           
          }
          else
          {
     this.dialogRef.close();

     this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      })


          }
        
        }
        
        )


  
  }




onNoClick(orgname,orgid,rolesadmin,frontdesk,doctor,org_logo,org_type): void {

  
  localStorage.setItem('orguserrole',"")






    var orguserrole=
    {
    "adminrole":rolesadmin,
    "frontdesk":frontdesk,
    "doctor":doctor,
    "orgname":orgname,
    "orgid":orgid,
    "orgimg": org_logo,
    "org_type":org_type
  }
localStorage.setItem('orguserrole', JSON.stringify(orguserrole));


localStorage.setItem('actorganistion',"")
var orgactive=
{
  "org_name":orgname,
  "org_image":org_logo,
}
localStorage.setItem('actorganistion', JSON.stringify(orgactive));



var actas=null

if(doctor==true)
{
   actas={
    "activerole":'l2'
      }
}

else if(frontdesk==true)
{
   actas={
    "activerole":"l1"
      }
}
else if(rolesadmin==true)
{
   actas={
    "activerole":"admin"
      }
}
this.dialogRef.close();
localStorage.removeItem('activerole');
localStorage.setItem('activerole',JSON.stringify(actas));
//window.location.reload()
if(actas==null)
{

localStorage.clear();
this.router.navigate(["/login"]); 

/*
this.snackbar.open("System role not available",'', {
  duration: 2000,
  });
*/
this.snackbar.openFromComponent(ErrorsnackbarComponent, {
  duration: 2000,
  } 
)
  // window.location.reload()
}
else
{
 // window.location.reload()
  this.router.navigate(["/"]); 
}

}












closebox()
{
this.dialogRef.close();
}

}
