import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsreceivedComponent } from './requestsreceived.component';

describe('RequestsreceivedComponent', () => {
  let component: RequestsreceivedComponent;
  let fixture: ComponentFixture<RequestsreceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsreceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsreceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
