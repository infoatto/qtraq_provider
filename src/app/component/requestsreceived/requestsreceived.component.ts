import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import { RequestreceivedService } from './../../_services/requestsreceived.service';
import {MatSnackBar} from '@angular/material';
import { MatDialogConfig } from '@angular/material'
//import { DateTimeFormatPipe } from './../../pipes/datetime.pipe'
import { DatePipe } from '@angular/common';
import {MatDialog} from '@angular/material';
import{ DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component'
import{ MessagequelistComponent } from './../../component/messagequelist/messagequelist.component'
import {INgxMyDpOptions} from 'ngx-mydatepicker';
import { CommonService } from "./../../_services/common.service";
import { ConfirmationDialogService } from './../../component/confirmation-dialog/confirmation-dialog.service';
//import * as moment from 'moment';

@Component({
  selector: 'app-requestsreceived',
  templateUrl: './requestsreceived.component.html',
  styleUrls: ['./requestsreceived.component.scss']
})
export class RequestsreceivedComponent implements OnInit {
  concatarrayforresult:any
  opt1_requested_queue_name:any
  concatarrayforresultexist:any
  quepresent:Boolean=false
  is_buffer_token_started:Boolean=false
  pipe:any
  requestsreceived:any;
  totalRecords = "";
  page=0;
  pagecanel=0;
  pagereschedule=0;
  getprolist="";
  dropdownSettings = {};
  dropdownSettingsnew = {};
  dropdownSettingsnewdata = {};
  tokenproviderid=""
  canceltokenproviderid=""
  rescheduletokenid=""
  quelistdata:any
  rescheduletokenidcount=""
  showalltabs:Boolean=true
  getTokenDetails:any
  getTokenDetailsreshadule:any
  getprovidertokendetail:any
  TokenDetails:Boolean=false

  commonpartdetail:any

  commonpartdetailshow:Boolean=false

  provider_name:any

  canceledtoken:Boolean=false
  opt1_requested_queue:any
  token_id:any
  token_no:any
  token_time:any
  token_date:any

  tokendate_changeformat:any

  customfield:any
  dialogRef:any
  queue_id:any
  defaultselected:any
  token_nocon:any
  requestreceived:any
  queue_namereqexisting:any
  //reshadule start
  token_idres:any
  token_noconres:any
  token_datereq:any
  token_timereq:any
  queue_namereq:any
  newrequested_date:any
  //queue_namereq
  queue_reqque:any
  reshaduledate:any
  quelistdatareshadule:any
  //reshadule end
  token_timeres:any
  restoken_time:any
  newtoken_time:any
  getcanceltokendetail:any
  caneltoken_nocon:any
  canceltoken_date:any
  canceltoken_timereq:any
  cancelqueue_name:any
  canceltoken_id:any
  selectedIndex:any
  futurepastprofileimage_path:any
  futurepastorg_location:any
  reshaduleprovider:any
  providerdate:any
  providerid:any
  totalRecordscancel:any
  totalRecordscancellist:any
  totalreschedulelist:any
  token_nores:any
  token_noexist:any
  token_no_token_conveted_timeexist:any
  showlinkingback:boolean=true
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate()-1;
  currentMonth: any = this.mytime.getUTCMonth() + 1;
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableUntil:{year: this.currentYear, month: this.currentMonth, day: this.currentDate}
  };
  myOptionscustom: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };
  size = 10;
  //transaction start
  progressbar:any
  queuelist:Boolean=false
  total_tokens:any
  transactionQueueTokens:any
  queue_detailstoday:any
  queue_reqquedata:any
  showalldata:Boolean=true
  tokenidrequestreshadule:any
  checktodaytoken:any
  orgname:any
  token_status:any;
  //transaction end
  /*   start transacsation back start    */
  tokenidfortransactionback:any
  requesttypeback:any

  /* start transacsation back end  */
  myFormattedDatenew:any
  getscreenname:any
  futurepastmasterid:any
  futurepasttodate:any
  futurepastfromdate:any
  futurepastlastname:any
  futurepastfirstname:any
  futurepastmiddle_name:any
  futurepastprofileimage_pathf:any
  callfromotherscreen:Boolean=false
  queue_transaction_id:any
  customfieldexist:any
  //searchtoken start
  fromdatesearch:any
  patientsearch:any
  todatesearch:any
  tokentypesearch:any
  searchpage:any
  tokenidrequestreshadulesearch:any
  //searchtoken end
  //new code 11-1-2019 start
  getenumvalues:any
  getenumdefaultvalues:any
  //new code end 11-1-2019 end
  requestBack:any = "";
  constructor(
    private requestrceived:RequestreceivedService,
    private assignnewtoken:AssignnewtokenService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbar:MatSnackBar,
    public dialog: MatDialog,
    private cs: CommonService,
    private confirmationDialogService: ConfirmationDialogService
  ) {
    // this.requesttype = this.route.snapshot.queryParamMap.get('newrequest')
  }

  ngOnInit() {
    this.showlinkingback=true
    this.callfromotherscreen=false
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    this.orgname=orguserlist.orgname
    this.requestreceived=false
    this.TokenDetails=false
    this.canceledtoken=false
    this.dropdownSettingsnew = {
      singleSelection: true,
      idField: 'custom_field_value_id',
      textField: 'custom_list_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettingsnewdata = {
      singleSelection: false,
      idField: 'custom_field_value_id',
      textField: 'custom_list_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'user_master_id',
      textField: 'provider_name',
      selectAllText: 'Select All',
      enableCheckAll:false,
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      defaultOpen:false,
      allowSearchFilter: true
    };
    this.pipe = new DatePipe('en-US');
    this.reshaduledate= this.pipe.transform(new Date(), 'dd-MM-yyyy');
    // this.route.snapshot.queryParamMap.get('queselecteddate')
    if((this.route.snapshot.queryParamMap.get('requestsreceivedreshadule'))!=null){
      //var tokenid  =this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
      //tokenidrequestreshadule start
      this.getscreenname = this.route.snapshot.queryParamMap.get('screenname')
      if(this.getscreenname=="Future Past Token"){
        var tokenid  =this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
        this.checktodaytoken =this.route.snapshot.queryParamMap.get('checktodaytokenid')
        this.queue_transaction_id=this.route.snapshot.queryParamMap.get('queue_transaction_id')
        this.showlinkingback=false
        this.callfromotherscreen=true
        this.tokenidrequestreshadule=tokenid
        this.futurepastmasterid =this.route.snapshot.queryParamMap.get('futurepastmasterid')
        this.futurepasttodate =this.route.snapshot.queryParamMap.get('futurepasttodate')
        this.futurepastfromdate =this.route.snapshot.queryParamMap.get('futurepastfromdate')
        this.futurepastlastname =this.route.snapshot.queryParamMap.get('futurepastlastname')
        this.futurepastfirstname =this.route.snapshot.queryParamMap.get('futurepastfirstname')
        this.futurepastmiddle_name =this.route.snapshot.queryParamMap.get('futurepastmiddle_name')
        this.futurepastprofileimage_pathf =this.route.snapshot.queryParamMap.get('futurepastprofileimage_path')
        this.reschedulelistsclick(tokenid)
      }else if(this.getscreenname=="Search Tokens"){
        this.showlinkingback=false
        this.fromdatesearch=this.route.snapshot.queryParamMap.get('fromdate')
        this.patientsearch=this.route.snapshot.queryParamMap.get('patient')
        this.todatesearch=this.route.snapshot.queryParamMap.get('todate')
        this.tokentypesearch=this.route.snapshot.queryParamMap.get('tokentypesearch')
        this.searchpage=this.route.snapshot.queryParamMap.get('searchpage')
        this.tokenidrequestreshadule=this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
        this.reschedulelistsclick(this.tokenidrequestreshadule)
      }else{
        var tokenid  =this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
        this.checktodaytoken =this.route.snapshot.queryParamMap.get('checktodaytokenid')
        this.queue_transaction_id=this.route.snapshot.queryParamMap.get('queue_transaction_id')
        this.showlinkingback=false
        this.callfromotherscreen=true
        this.tokenidrequestreshadule=tokenid
        this.reschedulelistsclick(tokenid)
      }
      //tokenidrequestreshadule end
    }else if((this.route.snapshot.queryParamMap.get('todaysnewrequestsreceived'))!=null){
      this.requesttypeback=this.route.snapshot.queryParamMap.get('requesttype')
      if(this.requesttypeback=="newrequest"){
        this.newtokenlist(1,this.tokenproviderid);
        this.cancelledtokens(1,this.canceltokenproviderid);
        this.rescheduletokens(1,this.rescheduletokenid)
        this.providerid  =this.route.snapshot.queryParamMap.get('selectedproid')
        var tokenidfortransactionback  = this.route.snapshot.queryParamMap.get('tokenidfortransactionback')
        this.token_date =this.route.snapshot.queryParamMap.get('queselecteddate')
        this.opt1_requested_queue  =this.route.snapshot.queryParamMap.get('queue_id')
        this.requestreceivedclick(tokenidfortransactionback)
      }else if(this.requesttypeback=="newrequestreshadule"){
        this.newtokenlist(1,this.tokenproviderid);
        this.cancelledtokens(1,this.canceltokenproviderid);
        this.rescheduletokens(1,this.rescheduletokenid)
        this.reshaduleprovider  =this.route.snapshot.queryParamMap.get('selectedproid')
        var tokenidfortransactionback  =this.route.snapshot.queryParamMap.get('tokenidfortransactionback')
        //this.newrequested_date =this.route.snapshot.queryParamMap.get('queselecteddate')
        this.queue_reqque  =this.route.snapshot.queryParamMap.get('queue_id')
        this.reshaduledate=this.route.snapshot.queryParamMap.get('queselecteddate')
        this.reschedulelistsclick(tokenidfortransactionback)
      }
    }else if((this.route.snapshot.queryParamMap.get('callforrequestreceived'))!=null){
      var newtoken_id=this.route.snapshot.queryParamMap.get('newtoken_id')
      this.requestreceivedclick(newtoken_id)
    }else if((this.route.snapshot.queryParamMap.get('tokendetaildatareshadule'))!=null){
      var newtoken_id=this.route.snapshot.queryParamMap.get('newtoken_id')
      this.reschedulelistsclick(newtoken_id)
    }else{
      this.showlinkingback=true
      this.newtokenlist(1,this.tokenproviderid);
      this.cancelledtokens(1,this.canceltokenproviderid);
      this.rescheduletokens(1,this.rescheduletokenid)
      this.getProvidersList();
      //cancelledtokens
      this.showalltabs=true
      if(this.route.snapshot.queryParamMap.get('backfrom') == "reschedule"){
        this.selectedIndex='1'
      }else if(this.route.snapshot.queryParamMap.get('backfrom') == "request"){
        this.selectedIndex='0'
      }
    }
  }

  getProvidersList(){
    this.requestrceived.getProvidersList().subscribe((response) => {
      this.getprolist=response.Data
    })
  }

  onItemSelect(item: any){
    this.tokenproviderid=item.user_master_id
    this.newtokenlist(1,this.tokenproviderid)
  }

  onItemDeSelect(item: any){
    this.tokenproviderid=""
    this.newtokenlist(1,this.tokenproviderid)
  }

  public openConfirmationDialog() {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to ... ?').then((confirmed) => console.log('User confirmed:', confirmed)).catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  newtokenlist(page: number,tokenproviderdata){
    this.requestrceived.getrequestnewtokenlist((page - 1) ,this.size,tokenproviderdata).subscribe((response) =>{
      if(response.status_code=='200'){
        this.requestsreceived=response.Data
        this.totalRecords=response.total_count
        this.page=page
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.requestsreceived=[]
        this.totalRecords="0"
        this.page=1
      }
    })
  }

  cancelledtokens(page: number,canceltokenproviderid){
    this.requestrceived.getrequestcancelledllist((page - 1) ,this.size,canceltokenproviderid).subscribe((response) => {
      if(response.status_code=='200'){
        // this.requestBack = 'cancel';
        this.totalRecordscancellist=response.Data
        this.totalRecordscancel=response.total_count
        this.pagecanel=page
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.totalRecordscancellist=[]
        this.totalRecordscancel="0"
        this.page=1
      }
    })
  }

  //Reschedule token start
  rescheduletokens(page: number,rescheduletokenproviderid){
    this.requestrceived.rescheduletokens((page - 1) ,this.size,rescheduletokenproviderid).subscribe((response) => {
      if(response.status_code=='200'){
        this.requestBack = 'reschedule';
        this.totalreschedulelist=response.Data
        this.rescheduletokenidcount=response.total_count
        this.pagereschedule=page
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.totalreschedulelist=[]
        this.rescheduletokenidcount="0"
        this.pagereschedule=1
      }
    })
  }

  //Reschedule token end
  onoganisationclick(org){
    if(org.index==1){
    }
  }

  pageChanged(event){
    this.newtokenlist(event,this.tokenproviderid)
    //this.getnotificationlist(event,this.notificationfilter)
  }

  //cancel function start
  pageChangedcancel(event){
    this.cancelledtokens(event,this.canceltokenproviderid)
  }

  oncancelItemSelect(item: any){
    this.canceltokenproviderid=item.user_master_id
    this.cancelledtokens(1,this.canceltokenproviderid)
  }

  oncancelItemDeSelect(item: any){
    this.canceltokenproviderid=""
    this.cancelledtokens(1,this.canceltokenproviderid)
  }
  //cancel function end

  //reshadule start
  pageChangedreshadule(event){
    this.rescheduletokens(event,this.rescheduletokenid)
  }

  onreshaduleItemSelect(item: any) {
    this.rescheduletokenid=item.user_master_id
    this.rescheduletokens(1,this.rescheduletokenid)
  }

  onreshaduleItemDeSelect(item: any) {
    this.rescheduletokenid=""
    this.rescheduletokens(1,this.rescheduletokenid)
  }

  requestreceivedclick(tokenid){
    this.requestrceived.getEnumValues().subscribe((response) => {
      if(response.status_code=="200"){
        this.getenumvalues=response.Data
        this.getenumdefaultvalues=response.Data[0]
      }else{
        this.getenumdefaultvalues=""
      }
    })
    this.showalltabs=false
    this.tokenidfortransactionback=tokenid
    this.requestrceived.getTokenDetails(tokenid).subscribe((response) => {
      if(response.status_code=='200'){
        this.requestBack = "request";
        this.TokenDetails=true
        this.commonpartdetailshow=true
        this.commonpartdetail=null
        this.commonpartdetail=response.Data
        this.getTokenDetails=response.Data
        //this.opt1_requested_queue_name=response.Data[0].opt1_requested_queue_name
        this.opt1_requested_queue_name=response.Data[0].booking_for_name
        var requested_on_date =  response.Data[0].opt1_requested_date
        var token_provider_id =  response.Data[0].token_provider_id
        this.pipe = new DatePipe('en-US');
        const myFormattedDate = this.pipe.transform(requested_on_date,'dd-MM-yyyy');
        //this.token_date=response.Data[0].opt1_requested_date
        if(response.Data[0].opt1_requested_date!=null){
        //this.tokendate_changeformat = this.pipe.transform(response.Data[0].opt1_requested_date, 'dd-MM-yyyy');
          if((this.requesttypeback!=" ")&&(this.requesttypeback!=undefined)&&(this.requesttypeback=="newrequest")){
            this.tokendate_changeformat = this.token_date;
            //this.opt1_requested_queue=this.queue_id
            this.queue_id=this.opt1_requested_queue
            this.providerdate=this.token_date
          }else{
            this.tokendate_changeformat = response.Data[0].web_opt1_requested_date;
            this.opt1_requested_queue=response.Data[0].opt1_requested_queue
            this.queue_id=response.Data[0].opt1_requested_queue
            this.providerdate=myFormattedDate
          }
        }else{
          this.tokendate_changeformat=""
        }
        this.providerid=token_provider_id
        this.token_id = response.Data[0].token_id
        this.provider_name=response.Data[0].provider_details[0].provider_name
        this.futurepastprofileimage_path=response.Data[0].provider_details[0].provider_profile_pic
        this.futurepastorg_location=response.Data[0].provider_details[0].area
        if(response.Data[0].requested_queue_status=="Active"){
          this.token_nocon=response.Data[0].token_no
          //this.onqueselect(response.Data[0].opt1_requested_queue)
          //this.onqueselectcustom(response.Data[0].opt1_requested_queue)
        }
        //this.onqueselect(response.Data[0].opt1_requested_queue)
        //this.onqueselectcustom(this.opt1_requested_queue)
        if((response.Data[0].oncomplete_custom_form_values!=null)&&(response.Data[0].onassign_custom_form_values!=null)){
          this.concatarrayforresult=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values);
          this.concatarrayforresultexist=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values);
        }else if(response.Data[0].oncomplete_custom_form_values!=null){
          this.concatarrayforresult=response.Data[0].oncomplete_custom_form_values
          this.concatarrayforresultexist=response.Data[0].oncomplete_custom_form_values
        }else if(response.Data[0].onassign_custom_form_values!=null){
          this.concatarrayforresult=response.Data[0].onassign_custom_form_values
          this.concatarrayforresultexist=response.Data[0].onassign_custom_form_values
        }else{
          this.concatarrayforresult=""
          this.concatarrayforresultexist=""
        }
        this.gettokenlist(this.providerdate,token_provider_id)
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
        });
      }
    })
  }

  onqueselectcustom(event){
    this.requestrceived.getmonitorqueQueues(event,this.providerdate).subscribe((response) => {
      this.queue_id=event
      if(response.status_code=="200"){
        if(response.Data.last_token==null){
          this.token_no='0'
          this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {duration: 1000,});
        }else{
          this.token_no=response.Data.last_token.token_no
          this.token_status=response.Data.last_token.token_status
          this.token_noexist=this.token_no
        }
        this.token_time=response.Data.last_token.token_conveted_time
        this.newtoken_time=response.Data.last_token.token_time
        this.token_date=this.providerdate
        this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
        this.token_no_token_conveted_timeexist=this.token_time
        //this.concatarrayforresult=null
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
    this.requestrceived.getcustomlist(event).subscribe((response) => {
      if(response.status_code == "200"){
        response.Data.map((elem:any)=>{
          var temp = this.concatarrayforresult.find(e => (e.custom_label === elem.actual_custom_label || e.custom_label == elem.custom_label));
          if(temp){
            elem.custom_field_value = temp.custom_field_value;
            elem.custom_field_date_value = temp.custom_field_date_value;
            elem.amount_type = temp.amount_type;
            if(elem.custom_field_type =="List Multi Select"){
              if(temp.custom_field_options != null){
                temp.custom_field_options.map((opt:any)=>{
                  var matchedOpt = elem.custom_field_options.find(o => (o.custom_list_text === opt.custom_list_text));
                  if(matchedOpt){
                    opt.custom_field_value_id  = matchedOpt.custom_field_value_id;
                  }
                  return opt;
                })
              }
            }
            elem.custom_field_options_value = temp.custom_field_options;
            elem.custom_list_text_value = temp.custom_list_text;
          }else{
            elem.custom_field_value = "";
            elem.custom_field_date_value = "";
            elem.amount_type = "";
            elem.custom_field_options_value = [];
            elem.custom_list_text_value = "";
          }
          return elem;
        });
        this.customfield = response.Data
      }else{
        this.customfield = null
      }
    },(err: any) => console.log("error",err),() =>
      console.log("error in assign new token response")
    );
  }

  onqueselect(event){
    this.requestrceived.getmonitorqueQueues(event,this.providerdate).subscribe((response) => {
      this.queue_id=event
      if(response.status_code=="200") {
        if(response.Data.last_token==null){
          this.token_no='0'
          this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {duration: 1000,});
        }else{
          this.token_no=response.Data.last_token.token_no
          this.token_status=response.Data.last_token.token_status
          this.token_time=response.Data.last_token.token_conveted_time
          this.newtoken_time=response.Data.last_token.token_time
          this.token_date=response.Data.last_token.token_date
        }
        // this.token_date=this.providerdate
        this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
        // this.concatarrayforresult=null
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
        });
        this.queue_id=""
      }
    })
    this.requestrceived.getcustomlist(event).subscribe((response) => {
      if(response.status_code == "200"){
        response.Data.map((elem:any)=>{
          var temp = this.concatarrayforresult.find(e => (e.custom_label === elem.actual_custom_label || e.custom_label == elem.custom_label));
          if(temp){
            elem.custom_field_value = temp.custom_field_value;
            elem.custom_field_date_value = temp.custom_field_date_value;
            elem.amount_type = temp.amount_type;
            if(elem.custom_field_type =="List Multi Select"){
              if(temp.custom_field_options != null){
                temp.custom_field_options.map((opt:any)=>{
                  var matchedOpt = elem.custom_field_options.find(o => (o.custom_list_text === opt.custom_list_text));
                  if(matchedOpt){
                    opt.custom_field_value_id  = matchedOpt.custom_field_value_id;
                  }
                  return opt;
                })
              }
            }
            elem.custom_field_options_value = temp.custom_field_options;
            elem.custom_list_text_value = temp.custom_list_text;
          }else{
            elem.custom_field_value = "";
            elem.custom_field_date_value = "";
            elem.amount_type = "";
            elem.custom_field_options_value = [];
            elem.custom_list_text_value = "";
          }
          return elem;
        });
        this.customfield = response.Data
      }else{
        this.customfield = null
      }
    },(err: any) => console.log("error",err),() =>
      console.log("error in assign new token response")
    );
  }

  gettokenlist(reqdate,token_provider_id){
    var dateex = reqdate.split("-");
    var newdate = new Date("" + dateex[1] + "/" + dateex[0] + "/" + dateex[2] + "");
    var todayDate = new Date();
    if(newdate.getTime() < todayDate.getTime()){
      this.providerdate = todayDate.getDate() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getFullYear();
      reqdate = this.providerdate;
    }else{
      this.providerdate = reqdate;
    }
    this.requestrceived.gettokenlist(reqdate,token_provider_id).subscribe((response) => {
      if(response.status_code=='200'){
        this.quelistdata=response.Data
        this.onqueselectcustom(this.opt1_requested_queue)
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else if(response.status_code=='432'){
        this.showalltabs=true
        this.TokenDetails=false
        this.commonpartdetailshow=false
        this.router.navigate(['/requestsreceived']);
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.quelistdata=null
        this.token_time=null
        this.newtoken_time = null
        this.token_no=null
        this.customfield=null
      }
    })
  }

  onAssignDateChanged(event){
    if(event.formatted!=''){
      this.requestrceived.getmonitorqueQueuesassign(this.providerid,event.formatted).subscribe((response) => {
        if(response.status_code=="200"){
          this.quelistdata=response.Data
          this.providerdate=event.formatted
          var l="0"
          var queid="0"
          for(var i=0;i<this.quelistdata.length;i++){
            if(this.quelistdata[i]['queue_id']==this.opt1_requested_queue){
              //queid=
              l="1"
            }
          }
          if(l=="1"){
            this.onqueselectcustom(this.opt1_requested_queue)
            // this.customfield=this.customfieldexist
            this.concatarrayforresult=this.concatarrayforresultexist
            //this.token_no=this.token_noexist
            //this.token_time=this.token_no_token_conveted_timeexist
          }else{
            this.customfield=null
            this.concatarrayforresult=null
            this.token_no=""
            this.token_time=""
            this.newtoken_time = ""
          }
          // this.concatarrayforresult=null
          this.token_date=event.formatted
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.quelistdata=null
          this.customfield=null
          this.concatarrayforresult=null
          this.token_no=""
          this.token_time=""
          this.newtoken_time = ""
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      })
    }else{
      this.snackbar.open("Please select Date",'', {
        duration: 1000,
      });
    }
  }

  onAssignresDateChanged(event){
    if(event.formatted!=''){
      // console.log(event.formatted);
      // event.formatted
      this.requestrceived.getmonitorqueQueuesassign(this.reshaduleprovider,this.reshaduledate).subscribe((response) => {
        //this.queue_id=event
        if(response.status_code=="200"){
          this.quepresent=true
          this.quelistdatareshadule=response.Data
          var l="0"
          for(var i=0;i<this.quelistdatareshadule.length;i++){
            if(this.quelistdatareshadule[i]['queue_id']==this.queue_reqque){
              l="1"
            }
          }
          if(l=="1"){
            this.customfield=this.customfieldexist
            this.concatarrayforresult=this.concatarrayforresultexist
            this.token_nores=this.token_noexist
            this.token_timeres=this.token_no_token_conveted_timeexist
          }else{
            this.customfield=null
            this.concatarrayforresult=null
            this.token_nores=""
            this.token_timeres=""
          }
          this.concatarrayforresult=null
          this.reshaduledate=event.formatted
          this.onqueselectreshadulecustom(this.queue_reqque)

        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.quelistdatareshadule=null
          this.customfield=null
          this.concatarrayforresult=null
          this.token_nores=""
          this.token_timeres=""
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      })
    }else{
      this.snackbar.open("Please select Date",'', {duration: 1000,});
    }
  }

  //reshadule dete change end
  onqueclick() {
    const config = new MatDialogConfig();
    this.assignnewtoken.getProviderQueues(this.providerdate, this.providerid).subscribe((response) => {
      if (response.status_code == "200") {
        this.quelistdata = response.Data;
        config.data = {
          quelistdata: this.quelistdata,
          tokendate: this.providerdate,
          queue_id: this.queue_id,
          token_noexist:this.token_no,
          selectedproid: this.providerid
        };
        this.dialogRef = this.dialog.open(DialogmonitorqueComponent, config);
        this.dialogRef.afterClosed().subscribe((value:any) => {
          if(value != undefined){
            if(value.token_no == null) {
              this.token_no = '0';
              this.snackbar.open("This Queue is Full for the day. Select another queue.", '', {
                duration: 1000,
              });
            } else {
              if(value.token_no != '0'){
                this.token_no = value.token_no;
                this.queue_id = value.queue_id;
                this.queue_reqque = value.selectedqueue;
                this.opt1_requested_queue = value.selectedqueue;
                this.token_time = value.token_conveted_time;
                this.newtoken_time = value.token_time;
                this.is_buffer_token_started = value.is_buffer_token_started;
                this.token_status = value.token_status;
                this.tokendate_changeformat = value.selecteddate;
                this.providerdate = value.selecteddate;
                this.token_date = value.selecteddate;
                this.reshaduledate = value.selecteddate;
              }
            }
            // this.onqueselect(this.queue_id);
          }
        });
      }
    })
  }

  /*onqueclick(){
  const config = new MatDialogConfig();
  config.data = { tokendate: this.providerdate,queue_id: this.queue_id,token_noexist:this.token_no};
  this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
  this.dialogRef.afterClosed().subscribe((value:any) => {
    console.log("value",value);
    if(value.token_no==null){
      this.token_no='0'
      this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {
        duration: 1000,
      });
    }else{
      this.token_no=value.token_no
      this.token_status=value.token_status
    }
    this.token_time=value.token_conveted_time
    this.newtoken_time=value.token_time
    this.is_buffer_token_started=value.is_buffer_token_started
  });
}*/

  reshaduleonqueclick(){
    this.assignnewtoken.getProviderQueues(this.reshaduledate, this.providerid).subscribe((response) => {
      if (response.status_code == "200") {
        const config = new MatDialogConfig();
        this.quelistdata = response.Data;
        config.data = {
          quelistdata: this.quelistdata,
          tokendate: this.reshaduledate,
          queue_id: this.queue_reqque,
          token_noexist:this.token_nores,
          selectedproid: this.providerid,
          providername:this.provider_name
        };
        this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
        this.dialogRef.afterClosed().subscribe((value:any) => {
          if(value.token_no > 0){
            this.token_nores=value.token_no;
            this.queue_id = value.queue_id;
            this.token_timeres=value.token_conveted_time
            this.restoken_time=value.token_time
            this.newtoken_time = value.token_time
            this.queue_reqque = value.queue_id;
            this.is_buffer_token_started=value.is_buffer_token_started
            this.token_status=value.token_status
            this.token_date = value.selecteddate;
            this.reshaduledate = value.selecteddate;
            this.onqueselect(this.queue_id);
          }else{
            this.token_nores='0'
            this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {
              duration: 1000,
            });
          }
        });
      }
    });
  }

  onSubmit(data){
    if(this.queue_id!=undefined){
      const formData = new FormData();
      if(this.customfield!=undefined){
        let counte=this.customfield.length
        if(this.customfield!=null){
          let counte=this.customfield.length
          for(let i=0;i<this.customfield.length;i++){
            var datalast=this.customfield[i]['custom_field_id']
            var datalastlist=(data[this.customfield[i]['custom_field_id']])
            var newStr
            if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true)){
              if(datalastlist==""){
                this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {
                  duration: 2000,
                });
                return false;
              }
            }
            // if(datalastlist != undefined){
              // if(datalastlist){
                var multileat="";
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  if(datalastlist != undefined){
                    for(let j=0;j<datalastlist.length;j++){
                      multileat+=datalastlist[j].custom_field_value_id+","
                    }
                  }
                  newStr = multileat.substring(0, multileat.length-1);
                }
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  if(newStr!=undefined){
                    formData.append('custom_field_text_value['+i+']',newStr);
                  }
                }else if(this.customfield[i]['custom_field_type']=="Date"){
                  var formattedVal:any = "";
                  if(datalastlist != undefined){
                    formattedVal = datalastlist.formatted;
                  }
                  formData.append('custom_field_text_value['+i+']',formattedVal);
                }else{
                  if(datalastlist!=undefined){
                      formData.append('custom_field_text_value['+i+']',datalastlist);
                  }
                }

                if(this.customfield[i]['custom_field_type']=="Date"){
                  if(datalastlist != undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                  }
                }else if(this.customfield[i]['custom_field_type']=="Amount"){
                  if(datalastlist!=undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                    formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
                  }
                }else{
                  if(datalastlist!=undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                  }
                }
              // }
            // }
          }
        }
      }
      formData.append('tokenmsg',data.tokenmessage);
      formData.append('tokenno',this.token_no);
      formData.append('tokendate',this.reshaduledate);
      formData.append('tokentime',this.newtoken_time);
      formData.append('requesttype',"newtoken");
      // console.log(formData);return false;
      if (this.token_status == "BUFFER") {
        this.confirmationDialogService.confirm('BUFFER TOKEN',  'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.token_date + ' to ' + data.firstname + ' ?','Yes - Book as Confirmed','No','Yes - Book as RAC','md').then((confirmed:any) => {
          if(confirmed == "rac"){
            formData.append('apt_status', "RAC");
            confirmed = true;
          }
          if (confirmed) {
            this.requestrceived.confirmtoken(formData,this.token_no,this.newtoken_time,this.token_date,this.token_id,this.queue_id,data.tokenmessage).subscribe((response) => {
              if(response.status_code=="200"){
                this.snackbar.open(response.Metadata.Message,'', {
                  duration: 2000,
                });
                this.router.navigate(['/']);
              }else if(response.status_code=='402'){
                localStorage.clear();
                this.router.navigate(['/login']);
              }else{
                this.snackbar.open(response.Metadata.Message,'', {
                  duration: 2000,
                });
              }
            })
          }
        })
      }else{
        this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to assign Token no. '+this.token_no+' for '+this.token_date+' to '+this.opt1_requested_queue_name+' ?').then((confirmed) => {
          if(confirmed){
            this.requestrceived.confirmtoken(formData,this.token_no,this.newtoken_time,this.token_date,this.token_id,this.queue_id,data.tokenmessage).subscribe((response) => {
              if(response.status_code=="200"){
                this.snackbar.open(response.Metadata.Message,'', {
                  duration: 2000,
                });
                this.router.navigate(['/']);
              }else if(response.status_code=='402'){
                localStorage.clear();
                this.router.navigate(['/login']);
              }else{
                this.snackbar.open(response.Metadata.Message,'', {
                  duration: 2000,
                });
              }
            })
          }
        })
      }
    }else{
      this.snackbar.open("please select queue",'', {duration: 2000,});
    }
  }

  onreshaduleSubmit(data){
    if(this.queue_reqque!=undefined){
      const formData = new FormData();
      if(this.customfield!=undefined){
        let counte=this.customfield.length
        if(this.customfield!=null){
          let counte=this.customfield.length
          for(let i=0;i<this.customfield.length;i++){
            var datalast=this.customfield[i]['custom_field_id']
            var datalastlist=(data[this.customfield[i]['custom_field_id']])
            var newStr
            if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true)){
              if(datalastlist==""){
                this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {duration: 2000,});
                return false
              }
            }

            var multileat="";
            if(this.customfield[i]['custom_field_type']=="List Multi Select"){
              if(datalastlist != undefined){
                for(let j=0;j<datalastlist.length;j++){
                multileat+=datalastlist[j].custom_field_value_id+","
                }
              }
              newStr = multileat.substring(0, multileat.length-1);
            }
            if(this.customfield[i]['custom_field_type']=="List Multi Select"){
              if(newStr!=undefined){
                formData.append('custom_field_text_value['+i+']',newStr);
              }
            }else if(this.customfield[i]['custom_field_type']=="Date"){
            var formattedVal:any = "";
              if(datalastlist != undefined){
                formattedVal = datalastlist.formatted;
              }
              formData.append('custom_field_text_value['+i+']',formattedVal);
            }else{
              if(datalastlist!=undefined){
                formData.append('custom_field_text_value['+i+']',datalastlist);
              }
            }

            if(this.customfield[i]['custom_field_type']=="Date"){
              if(datalastlist != undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
              }
            }else if(this.customfield[i]['custom_field_type']=="Amount"){
              if(datalastlist!=undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
              }
            }else{
              if(datalastlist!=undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
              }
            }
          }
        }
      }
      formData.append('tokenmsg',data.tokenmessageres);
      formData.append('tokenno',this.token_nores);
      formData.append('tokendate',this.reshaduledate);
      formData.append('tokentime',this.newtoken_time);
      formData.append('requesttype','reschedule');
      if (this.token_status == "BUFFER") {
        this.confirmationDialogService.confirm('BUFFER TOKEN',  'Are you sure you wish to assign Token no. ' + this.token_nores + ' for ' + this.reshaduledate + ' to ' + this.opt1_requested_queue_name + ' ?','Yes - Book as Confirmed','No','Yes - Book as RAC','md').then((confirmed:any) => {
          if(confirmed == "rac"){
            formData.append('apt_status', "RAC");
            confirmed = true;
          }
          if(confirmed){
            this.requestrceived.confirmtokenres(formData,this.token_nores,this.restoken_time,this.reshaduledate,this.token_idres,this.queue_reqque,data.tokenmessageres).subscribe((response) => {
              if(response.status_code=="200"){
                this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
                if(this.showlinkingback==true){
                  this.router.navigate(['/']);
                }else{
                  this.backfromreshadule()
                }
              }else if(response.status_code=='402'){
                localStorage.clear();
                this.router.navigate(['/login']);
              }else{
                this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
              }
            })
          }
        })
      }else{
        this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to assign Token no. '+this.token_nores+' for '+this.reshaduledate+' to '+this.opt1_requested_queue_name+' ?').then((confirmed) => {
          if(confirmed){
            this.requestrceived.confirmtokenres(formData,this.token_nores,this.restoken_time,this.reshaduledate,this.token_idres,this.queue_reqque,data.tokenmessageres).subscribe((response) => {
              if(response.status_code=="200"){
                this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
                //this.router.navigate(['/']);
                if(this.showlinkingback==true){
                  this.router.navigate(['/']);
                }else{
                  this.backfromreshadule()
                }
              }else if(response.status_code=='402'){
                localStorage.clear();
                this.router.navigate(['/login']);
              }else{
                this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
              }
            })
          }
        })
      }
    }else{
      this.snackbar.open("please select Queue",'', {duration: 2000,});
    }
  }

  //reshadule token end 4-4-2019
  returntoken(tokenmessage){
    if(tokenmessage.value!=""){
      this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to Decline this token booking request ?').then((confirmed) => {
        if(confirmed){
          this.requestrceived.providerReturnToken(tokenmessage.value,"newtoken",this.token_id).subscribe((response) => {
            if(response.status_code=="200"){
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              });
              this.router.navigate(['/']);
            }else if(response.status_code=='402'){
              localStorage.clear();
              this.router.navigate(['/login']);
            }else{
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              });
            }
          })
        }
      })
    }else{
      this.snackbar.open("Please Enter Message",'', {
        duration: 2000,
      });
    }
  }

  returnreshaduletoken(tokenmessage){
    if(tokenmessage.value!=""){
      this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to Decline this token booking request ?').then((confirmed) => {
        if(confirmed){
          this.requestrceived.providerReturnToken(tokenmessage.value,"reschedule",this.token_idres).subscribe((response) => {
            if(response.status_code=="200"){
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              });
              this.router.navigate(['/']);
            }else if(response.status_code=='402'){
              localStorage.clear();
              this.router.navigate(['/login']);
            }else{
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              });
            }
          })
        }
      })
    }else{
      this.snackbar.open("please enter message",'', {
        duration: 2000,
      });
    }
  }

  //resheduling code start
  reschedulelistsclick(token_id){
    this.showalltabs=false
    this.tokenidfortransactionback=token_id
    this.requestrceived.getTokenDetails(token_id).subscribe((response) => {
      if(response.status_code=='200'){
        this.requestreceived=true
        this.getTokenDetailsreshadule=response.Data
        this.commonpartdetailshow=true
        this.commonpartdetail=null
        this.commonpartdetail=response.Data
        this.provider_name=response.Data[0].provider_name
        var requested_on_date =  response.Data[0].opt1_rescheduling_requested_date
        this.opt1_requested_queue_name=response.Data[0].booking_for_name
        var token_provider_id =  response.Data[0].token_provider_id
        this.pipe = new DatePipe('en-US');
        //const myFormattedDatenew = this.pipe.transform(requested_on_date, 'dd-MM-yyyy');
        this.token_idres = response.Data[0].token_id
        this.token_noconres=response.Data[0].token_no
        this.pipe = new DatePipe('en-US');
        this.token_datereq=response.Data[0].web_token_date
        this.token_timereq=response.Data[0].web_token_time
        this.requestrceived.getEnumValues().subscribe((response) => {
          if(response.status_code=="200"){
            this.getenumvalues=response.Data
            this.getenumdefaultvalues=response.Data[0]
          }else{
            this.getenumdefaultvalues=""
          }
        })
        if((this.requesttypeback!=" ")&&(this.requesttypeback!=undefined)&&(this.requesttypeback=="newrequestreshadule")){
          this.myFormattedDatenew=this.reshaduledate
        }else{
          this.queue_namereq=response.Data[0].opt1_reshedule_queue_name
          this.queue_namereqexisting=response.Data[0].queue_name
          this.reshaduleprovider = response.Data[0].token_provider_id
          this.queue_reqque=response.Data[0].opt1_rescheduling_requested_queue
          this.myFormattedDatenew=this.pipe.transform(requested_on_date, 'dd-MM-yyyy');
          // this.reshaduledate=response.Data[0].web_opt1_rescheduling_requested_date
        }
        if(response.Data[0].requested_queue_status=="Active"){
          this.token_noconres=response.Data[0].opt1_queuename
        }
        this.newrequested_date=response.Data[0].opt1_rescheduling_requested_date
        //reshaduleprovider
        //this.onqueselectcustomres(this.queue_reqque)
        this.onqueselectreshadulecustom(this.queue_reqque)
        this.concatarrayforresult=null
        if((response.Data[0].oncomplete_custom_form_values!=null)&&(response.Data[0].onassign_custom_form_values!=null)){
          this.concatarrayforresult=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values);
          this.concatarrayforresultexist=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values);
        }else if(response.Data[0].oncomplete_custom_form_values!=null){
          this.concatarrayforresult=response.Data[0].oncomplete_custom_form_values
          this.concatarrayforresultexist=response.Data[0].oncomplete_custom_form_values
        }else if(response.Data[0].onassign_custom_form_values!=null){
          this.concatarrayforresult=response.Data[0].onassign_custom_form_values
          this.concatarrayforresultexist=response.Data[0].onassign_custom_form_values
        }else{
          this.concatarrayforresult=""
          this.concatarrayforresultexist=""
        }
        this.providerid=token_provider_id
        this.token_id = response.Data[0].token_id
        this.provider_name=response.Data[0].provider_details[0].provider_name
        this.futurepastprofileimage_path=response.Data[0].provider_details[0].provider_profile_pic
        this.futurepastorg_location=response.Data[0].provider_details[0].area
        this.gettokenlistreshadule(this.myFormattedDatenew,token_provider_id)
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
  }

  confirmtoken(tokenid){
    this.showalltabs=false
    this.requestrceived.getTokenDetails(tokenid).subscribe((response) => {
      if(response.status_code=='200'){
        this.TokenDetails=true
        this.getprovidertokendetail=response.Data
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
  }

  confirmtokenreshadule(tokenid){
    this.showalltabs=false
    this.requestrceived.getTokenDetails(tokenid).subscribe((response) => {
      if(response.status_code=='200'){
        this.TokenDetails=true
        this.getprovidertokendetail=response.Data
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
  }

  onqueselectreshadule(event){
    this.requestrceived.getmonitorqueQueues(event,this.reshaduledate).subscribe((response) =>{
      // this.concatarrayforresult=null
      this.queue_reqque=event
      //this.queue_id=event
      if(response.status_code=="200"){
        if(response.Data.last_token==null){
          this.token_no='0'
          this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {
            duration: 1000,
          });
        }else{
          this.token_nores=response.Data.last_token.token_no
        }
        this.token_timeres=response.Data.last_token.token_conveted_time
        this.restoken_time=response.Data.last_token.token_time;
        this.newtoken_time = response.Data.last_token.token_time;
        this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
        // this.concatarrayforresult=null
        this.requestrceived.getcustomlist(event).subscribe((response) => {
          if(response.status_code == "200"){
            response.Data.map((elem:any)=>{
              var temp = this.concatarrayforresult.find(e => (e.custom_label === elem.actual_custom_label || e.custom_label == elem.custom_label));
              if(temp){
                elem.custom_field_value = temp.custom_field_value;
                elem.custom_field_date_value = temp.custom_field_date_value;
                elem.amount_type = temp.amount_type;
                if(elem.custom_field_type =="List Multi Select"){
                  if(temp.custom_field_options != null){
                    temp.custom_field_options.map((opt:any)=>{
                      var matchedOpt = elem.custom_field_options.find(o => (o.custom_list_text === opt.custom_list_text));
                      if(matchedOpt){
                        opt.custom_field_value_id  = matchedOpt.custom_field_value_id;
                      }
                      return opt;
                    })
                  }
                }
                elem.custom_field_options_value = temp.custom_field_options;
                elem.custom_list_text_value = temp.custom_list_text;
              }else{
                elem.custom_field_value = "";
                elem.custom_field_date_value = "";
                elem.amount_type = "";
                elem.custom_field_options_value = [];
                elem.custom_list_text_value = "";
              }
              return elem;
            });
            this.customfield = response.Data
          }else{
            this.customfield = null
          }
        },(err: any) => console.log("error",err),() =>
          console.log("error in assign new token response")
        );
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
  }

  onqueselectreshadulecustom(event){
    this.requestrceived.getmonitorqueQueues(event,this.reshaduledate).subscribe((response) =>{
      // this.concatarrayforresult=null
      this.queue_reqque=event
      //this.queue_id=event
      if(response.status_code=="200"){
        if(response.Data.last_token==null){
          this.token_nores='0'
          this.snackbar.open("This Queue is Full for the day. Select another queue.",'', {duration: 1000,});
        }else{
          this.token_nores=response.Data.last_token.token_no
        }
        this.token_timeres=response.Data.last_token.token_conveted_time
        this.restoken_time=response.Data.last_token.token_time
        this.newtoken_time = response.Data.last_token.token_time;
        this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
        // this.concatarrayforresult=null
        this.token_noexist=response.Data.last_token.token_no
        this.token_no_token_conveted_timeexist=response.Data.last_token.token_conveted_time
        //this.token_noexist=response.Data.last_token.token_no
        this.requestrceived.getcustomlist(event).subscribe((response) => {
          if(response.status_code == "200"){
            response.Data.map((elem:any)=>{
              var temp = this.concatarrayforresult.find(e => (e.custom_label === elem.actual_custom_label || e.custom_label == elem.custom_label));
              if(temp){
                elem.custom_field_value = temp.custom_field_value;
                elem.custom_field_date_value = temp.custom_field_date_value;
                elem.amount_type = temp.amount_type;
                if(elem.custom_field_type =="List Multi Select"){
                  if(temp.custom_field_options != null){
                    temp.custom_field_options.map((opt:any)=>{
                      var matchedOpt = elem.custom_field_options.find(o => (o.custom_list_text === opt.custom_list_text));
                      if(matchedOpt){
                        opt.custom_field_value_id  = matchedOpt.custom_field_value_id;
                      }
                      return opt;
                    })
                  }
                }
                elem.custom_field_options_value = temp.custom_field_options;
                elem.custom_list_text_value = temp.custom_list_text;
              }else{
                elem.custom_field_value = "";
                elem.custom_field_date_value = "";
                elem.amount_type = "";
                elem.custom_field_options_value = [];
                elem.custom_list_text_value = "";
              }
              return elem;
            });
            this.customfield = response.Data
          }else{
            this.customfield = null
          }
        },(err: any) => console.log("error",err),() =>
          console.log("error in assign new token response")
        );
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
  }


  //get token list start
  gettokenlistreshadule(reqdate,token_provider_id){
    var dateex = reqdate.split("-");
    var newdate = new Date("" + dateex[1] + "/" + dateex[0] + "/" + dateex[2] + "");
    var todayDate = new Date();
    if(newdate.getTime() < todayDate.getTime()){
      this.reshaduledate = todayDate.getDate() + "-" + (todayDate.getMonth() + 1) + "-" + todayDate.getFullYear();
      reqdate = this.reshaduledate;
    }else{
      this.reshaduledate = reqdate;
    }
    this.requestrceived.gettokenlist(reqdate,token_provider_id).subscribe((response) => {
      if(response.status_code=='200'){
        this.quelistdatareshadule=response.Data
        this.quepresent=true
      }else if(response.status_code=='432'){
        this.showalltabs=true
        this.requestreceived=false
        this.commonpartdetailshow=false
        this.router.navigate(['/requestsreceived']);
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.quelistdatareshadule=null
        this.token_timeres=null
        this.token_nores=null
        this.customfield=null
        this.quepresent=false
      }
    })
  }

  canceltokenclick(tokenid){
    this.showalltabs=false
    this.requestrceived.getTokenDetails(tokenid).subscribe((response) => {
      if(response.status_code=='200'){
        this.canceledtoken=true
        this.getcanceltokendetail=response.Data
        this.commonpartdetailshow=true
        this.commonpartdetail=null
        this.commonpartdetail=response.Data
        this.canceltoken_id=response.Data[0].token_id
        this.caneltoken_nocon=response.Data[0].token_no
        this.canceltoken_date=response.Data[0].token_date
        this.canceltoken_timereq=response.Data[0].token_time
        this.cancelqueue_name=response.Data[0].queue_name
      }
    })
  }

  cancelmessageclick(){
    const config = new MatDialogConfig();
    config.data = { tokenid: this.canceltoken_id};
    this.dialogRef=this.dialog.open(MessagequelistComponent,config);
    this.dialogRef.afterClosed().subscribe(value => {})
  }

  reshadulemessageclick(){
    const config = new MatDialogConfig();
    config.data = { tokenid: this.token_idres};
    this.dialogRef=this.dialog.open(MessagequelistComponent,config);
    this.dialogRef.afterClosed().subscribe(value => {
    })
  }

  newreqmessageclick(){
    const config = new MatDialogConfig();

    config.data = { tokenid: this.token_id};
    this.dialogRef=this.dialog.open(MessagequelistComponent,config);
    this.dialogRef.afterClosed().subscribe(value => {
    })
  }

  //transaction start 6-10-2019
  peopleicon(){

  }

  getTransactionQueueDatas(){
    if((this.queue_id!=undefined)&&(this.queue_id!=" ")&&(this.token_date!=" ")&&(this.token_date!=undefined)){
      this.assignnewtoken.getTransactionQueueDatas(this.queue_id,this.token_date,this.providerid).subscribe((response) => {
        if(response.status_code=='200'){
          this.router.navigate(['/todaysqueue/'],{
            queryParams:{
              "newrequestsreceived":"newrequestsreceived",
              "newrequest":"newrequest",
              "tokenidfortransactionback":this.tokenidfortransactionback,
              "opt1_requested_queue":this.queue_id,
              "token_date":this.token_date,
              "providerid":this.providerid,
            },skipLocationChange: true
          })
        }else{
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
    }else{
      this.snackbar.open("Please Select Queue ",'', {
        duration: 2000,
      });
    }
  }

  backtoassign(){
    this.showalldata=true
    this.queuelist=false
  }

  getTransactionQueueDatasreshadule(){
    var datavalue = this.route.snapshot.queryParamMap.get('requestsreceivedreshadule')
    if(datavalue==null){
      this.getscreenname="default"
    }
    if(this.queue_reqque!=undefined){
      if((this.queue_reqque!=undefined)&&(this.queue_reqque!=" ")){
        this.assignnewtoken.getTransactionQueueDatas(this.queue_reqque,this.reshaduledate,this.reshaduleprovider).subscribe((response) => {
          if(response.status_code=='200'){
            this.router.navigate(['/todaysqueue/'],{
              queryParams:{
              "newrequestsreceived":"newrequestsreceived",
              "newrequest":"newrequestreshadule",
              "futurepastmasterid":this.futurepastmasterid,
              "futurepasttodate":this.futurepasttodate,
              "futurepastfromdate":this.futurepastfromdate,
              "futurepastlastname":this.futurepastlastname,
              "futurepastfirstname":this.futurepastfirstname,
              "futurepastmiddle_name":this.futurepastmiddle_name,
              "futurepastprofileimage_path":this.futurepastprofileimage_pathf,
              "tokenidfortransactionback":this.tokenidfortransactionback,
              "opt1_requested_queue":this.queue_reqque,
              "token_date":this.reshaduledate,
              "providerid":this.reshaduleprovider,
              "screenname":this.getscreenname,
              "provider_name":this.provider_name,
              "queue_namereqexisting":this.queue_namereqexisting,
              "tokenid":this.tokenidrequestreshadule
            },
            skipLocationChange: true
          })
        }else{
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
    }else{
      this.snackbar.open("Please Select Queue ",'', {
        duration: 2000,
      });
    }
  }else{
    this.snackbar.open("Please Select Queue ",'', {
      duration: 2000,
    });
  }
}
//transaction end 6-10-2019


  //breadcram start 6-19-2019 start
  backtohome(){
    this.cs.backtohome()
  }
//breadcram start 6-19-2019 end

  backtocancel()
  {

  }

  requestreceivedback(){
    this.commonpartdetailshow=false
    this.selectedIndex='2'
    //this.newtokenlist(1,this.tokenproviderid);
    this.cancelledtokens(1,this.canceltokenproviderid);
    //this.rescheduletokens(1,this.rescheduletokenid)
    this.getProvidersList();
    //cancelledtokens
    this.showalltabs=true
    this.canceledtoken=false
    //this.router.navigate(['/requestsreceived/'])
  }

  backtoreshadule(){
    this.commonpartdetailshow=false
    this.selectedIndex='1'
    this.newtokenlist(1,this.tokenproviderid);
    this.cancelledtokens(1,this.canceltokenproviderid);
    this.rescheduletokens(1,this.rescheduletokenid)
    this.getProvidersList();
    //cancelledtokens
    this.showalltabs=true
    this.requestreceived=false
    //this.router.navigate(['/requestsreceived/'])
  }

  backtoNewtoken(){
    this.commonpartdetailshow=false
    this.selectedIndex='0'
    this.newtokenlist(1,this.tokenproviderid);
    this.cancelledtokens(1,this.canceltokenproviderid);
    this.rescheduletokens(1,this.rescheduletokenid)
    this.getProvidersList();
    //cancelledtokens
    this.showalltabs=true
    this.TokenDetails=false
    //this.router.navigate(['/requestsreceived/'])
  }



  backfromreshadule(){
    this.router.navigate(['/todaysqueue/'],{
      queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
        "tokenidrequestreshadule":this.tokenidrequestreshadule,
        "queue_transaction_id":this.queue_transaction_id,
        "provider_name":this.provider_name,
        "queue_namereqexisting":this.queue_namereqexisting,
        "callfrom":"reshadule",
        'screennameback':this.getscreenname,
        "checktodayid":this.checktodaytoken,
        "futurepastmasterid":this.futurepastmasterid,
        "futurepasttodate":this.futurepasttodate,
        "futurepastfromdate":this.futurepastfromdate,
        "futurepastlastname":this.futurepastlastname,
        "futurepastfirstname":this.futurepastfirstname,
        "futurepastmiddle_name":this.futurepastmiddle_name,
        "futurepastprofileimage_path":this.futurepastprofileimage_pathf,
        "fromdatesearch":this.fromdatesearch,
        "patientsearch":this.patientsearch,
        "todatesearch":this.todatesearch,
        "tokentypesearch":this.tokentypesearch,
        "searchpage":this.searchpage,
      }, skipLocationChange: true
    });
  }

  backtopage(){
    if(this.getscreenname=='Todays Queues'){
      this.router.navigate(['/todaysqueue/'],{})
    }
  }

  callfromprovider(){
    this.router.navigate(['/todaysqueue/'],{

    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "callfrom":"provider",
    'screennameback':this.getscreenname,
    "tokenidrequestreshadule":this.tokenidrequestreshadule,
    "queue_transaction_id":this.queue_transaction_id,
    "checktodayid":this.checktodaytoken
    }, skipLocationChange: true

  });


}

home()
{

  this.router.navigate(['/']);

}


fastfuturtoken(){
  this.router.navigate(['/futurepastqueues/'],{
    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "newreq":"newreq",
    "providerid":this.providerid,
    "token_id":this.token_id,
    "provider_name":this.provider_name,
    "futurepastprofileimage_path":this.futurepastprofileimage_path,
    "futurepastorg_location":this.futurepastorg_location,

    }, skipLocationChange: true

  });

}


fastfuturtokentrshadule(){
  this.router.navigate(['/futurepastqueues/'],{
    queryParams:{
      "requestsreceivedreshadule":"requestsreceivedreshadule",
      "newreq":"reshadule",
      "queue_namereqexisting":this.queue_namereqexisting,
      "providerid":this.providerid,
      'screennameback':this.getscreenname,
      'todaytokenid':this.tokenidrequestreshadule,
      'checktodaytoken':this.checktodaytoken,
      'queue_transaction_id':this.queue_transaction_id,
      "token_id":this.token_id,
      "provider_name":this.provider_name,
      "futurepastprofileimage_path":this.futurepastprofileimage_path,
      "futurepastorg_location":this.futurepastorg_location,
      "futurepastmasterid":this.futurepastmasterid,
      "futurepasttodate":this.futurepasttodate,
      "futurepastfromdate":this.futurepastfromdate,
      "futurepastlastname":this.futurepastlastname,
      "futurepastfirstname":this.futurepastfirstname,
      "futurepastmiddle_name":this.futurepastmiddle_name,
      "futurepastprofileimage_pathfuture":this.futurepastprofileimage_pathf,
      "fromdatesearch":this.fromdatesearch,
      "patientsearch":this.patientsearch,
      "todatesearch":this.todatesearch,
      "tokentypesearch":this.tokentypesearch,
      "searchpage":this.searchpage,
    }, skipLocationChange: true

  });

}





fastfuturtokenwithdate()
{


/*
  this.router.navigate(['/futurepastqueues/'],{
    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "newreq":"newreq",
    "providerid":this.providerid,
    "token_id":this.token_id,
    "provider_name":this.provider_name,
    "futurepastprofileimage_path":this.futurepastprofileimage_path,
    "futurepastorg_location":this.futurepastorg_location,
    }, skipLocationChange: true

  });
*/

this.router.navigate(['/futurepastqueues/'],{

  queryParams:{"futurepasttokenback":"futurepasttokenback",
  "tokenidrequestreshadule":this.tokenidrequestreshadule,
  "queue_transaction_id":this.queue_transaction_id,
  "provider_name":this.provider_name,
  "queue_namereqexisting":this.queue_namereqexisting,
  "callfrom":"futurepastqueues",
  'screennameback':this.getscreenname,
  "checktodayid":this.checktodaytoken,
  "futurepastmasterid":this.futurepastmasterid,
  "futurepasttodate":this.futurepasttodate,
  "futurepastfromdate":this.futurepastfromdate,
  "futurepastlastname":this.futurepastlastname,
  "futurepastfirstname":this.futurepastfirstname,
  "futurepastmiddle_name":this.futurepastmiddle_name,
  "futurepastprofileimage_path":this.futurepastprofileimage_pathf,
  }, skipLocationChange: true

});


}




backfromsearch()
{
  this.router.navigate(['/searchtokens/'],{
    queryParams:{"searchtoken":"searchtoken",
    "fromdate":this.fromdatesearch,
    "patient":this.patientsearch,
    "todate":this.todatesearch,
    "tokentypesearch":this.tokentypesearch,
    "searchpage":this.searchpage}, skipLocationChange: true

  });

}
backtorequestreceivedfirstscreen() {

  if(this.requestBack != ""){
    this.router.navigate(['/requestsreceived/'],{
      queryParams:{"backfrom":this.requestBack},
      skipLocationChange: true
    });
  }else{
    this. backfromreshadule()
  }
}

onTimeChange(changedTime:any){
  this.newtoken_time = changedTime;
}



}
