import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { NgModule } from "@angular/core";
//import { CommonModule } from "@angular/common";
import { FuturepastqueuesComponent } from './futurepastqueues.component'
import {MatTabsModule} from '@angular/material/tabs';
import {FuturepastqueuesRoutingModule  } from "./futurepastqueues-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
import{ TimeFormatPipe } from '../../pipes/time.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
//import { FormsModule } from '@angular/forms';
import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { FuturepastqueuesRoutingModule } from './futurepastqueues-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FuturepastqueuesRoutingModule,
    CommonModule,
    FuturepastqueuesRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     FormsModule,
    
     ReactiveFormsModule
  ],
  declarations: [FuturepastqueuesComponent, TimeFormatPipe]
})
export class FuturepastqueuesModule { }
