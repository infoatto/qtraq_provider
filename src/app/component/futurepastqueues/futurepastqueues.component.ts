import { Component, OnInit,ViewChild } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
//import { QuesetupService } from './../../_services/quesetup.service';
import {MatSnackBar} from '@angular/material';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';
import { FuturepastqueuesService } from './../../_services/futurepastqueues.service'
import { TodaysqueueService } from './../../_services/todaysqueue.service'; 
import { NgProgress } from 'ngx-progressbar';
import { RequestreceivedService } from './../../_services/requestsreceived.service';
import { SearchtokensService } from './../../_services/searchtokens.service';
import{ MessagequelistComponent } from './../../component/messagequelist/messagequelist.component'
import{ DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDialogConfig } from '@angular/material'
import { of } from 'rxjs';
import { TransactionqueuesummaryComponent } from '../transactionqueuesummary/transactionqueuesummary.component';
@Component({
  selector: 'futurepastqueues',
  templateUrl: './futurepastqueues.component.html',
  styleUrls: ['./futurepastqueues.component.scss']
})
export class FuturepastqueuesComponent implements OnInit {
  //future past start
  @ViewChild(NgForm) userlogin: NgForm;
  quesetupdata:any
  providerdetail:any
  queuser_name:any
  org_location:any
  public futurepastorder: FormGroup;
  getfuturepastlist:any
  futurepastlist:Boolean=false
  //future past end
  provideridselected:any
  bookingtype:any
  dropdownSettings = {};
  dropdownSettingsnewdata = {};
  submitdate:any
  myOptions: INgxMyDpOptions = {
  dateFormat: 'dd-mm-yyyy',
  };
  dialogRef:any
  todaystoken:Boolean=false
  checktoday=0
  productLoading:Boolean=false
  token_provider_id:any
  usermasterid:any
  searchtokendatadetail:any
  tokentype:any
  changeseqtokenid:any
  searchtokendatadetailshowdata:Boolean=false
  alldata:any
  futurepastqueues:Boolean=true
  providerlist:Boolean=false
  providerlistdata:Boolean=false
  ongoingqueuelistdata:Boolean=true
  ongoingtab:Boolean=false
  broadcast:Boolean=false
  broadcasttoday:Boolean=false
  is_doctor_arrived:any
  pending:any
  ongoing:any
  noshow:any
  completedorcancelled:any
  profileimage_path:any
  first_name:any
  middle_name:any
  user_master_id:any
  selectedindex=0;
  ongoingqueuelist:any
  todayongoingqueuelist:any
  progressbar:any
  queuelist:Boolean=false
  allTokensCount:any
  transactionQueueTokens:any
  errorMessageongoing:any
  doctoravailabilityfilter: any = {
    month:"",
    year:""
  };
  adddoctoravailability:any={
    unavailabilitydaytype:''
  }
  errorMessage: string;
  orgname:any
  orgimage:any
  totalRecords:any
  ongoingqueuedata:any
  queue_details:any
  queue_detailstoday:any
  queue_tokens:any
  avg_consulting_time:any
  avg_waiting_time:any
  cancelCompletedTokensCount:any
  checkup_total_amount:any
  noshowTokensCount:any
  ongoingTokensCount:any
  pendingTokensCount:any
  queue_transaction_id:any
  ongoingestimated_end_time
  ongoingqueue_date
  ongoingprovider_name
  ongoingqueue_pic_url
  ongoingqueue_name
  broadcastlisting:any
  broadcastlistingpage=1
  broadcasterror:any
  addbroadcastmessage:Boolean=false
  broadcastmsgerror:any
  page=0;
  size = 10;
  ongoinglist:any
  broadcastmesseglist:any   
  getTransactionTokenList:any
  completedorcancelledpage=0
  noshowpage=0
  ongoingpage=0
  pendingpage=0
  allpage=0
  modelFlag:Boolean=false
  cancelmodel:Boolean=false
  latertoday:Boolean=false
  total_tokens:any
  ongoingqueuelistdatashow:Boolean=false

  transactionqueueid:any
  //4-22-2019 start
  quelistdatareshadule:any
  requestreceived:Boolean=false
  getTokenDetailsreshadule:any
  pipe:any
  token_idres:any
  token_noconres:any
  token_datereq:any
  token_timereq:any
  queue_namereqexisting:any
  newrequested_date:any
  reshaduledate:any
  queue_reqque:any
  queue_namereq:any
  token_nores:any
  token_timeres:any
  restoken_time:any
  customfield:any
  token_id:any
  hiddenall:Boolean=true
  //4-22-2019 end
  onassign_custom_form_values:any
  oncomplete_custom_form_values:any
  concatarrayforresult:any
  //dignosis start 
  dignosispriscription:Boolean=false
  patienthistorydata:any
  token_details:any
  diagnosis_details:any
  prescription_details:any
  test_details:any
  prescription_images:any
  report_images:any
  //dignosis end
  //assign start
  queue_id:any
  token_no:any
  token_time:any
  token_apitime:any
  quelistdata:any
  queselecteddate:any
  myFormattedDate:any
  assignnewtokenform:any
  selectedproid:any
  mobilesearchmasterid:any
  selfname:any
  tokenrequestorno:any
  bookedfor:any
  bookedforself:Boolean=false
  bookingtypeselectedtype:any
  token_date:any
  searchfirst_name:any
  regno:any
  usergendermale:any
  searchmobileage:any
  bookingusername:any
  searchfield:any
  assignsearchdiv:any
  userprofileimg:any
  usergenderfemale:any
  usergenderOther:any
  familymemeberidfordetail:any
  bookedother:any
  searchbookinguserid:any
  cancelmodelforassigndat:Boolean=false
  assignbuttonshow:any
  selfnumber:any
  selfimage:any
  bookingfordropdownvis:any
  publicorgname:any
  familymember:any
  familymembershow:Boolean=false 
  //assign end
  queue_namereqexistingreshadule:any
  currentdate = new Date();
  startdate = new Date();
  enddate = new Date()
  checktodaytoken:any
  todaytokenid:any
  futurepastmasterid:any
  futurepasttodate:any
  futurepastfromdate:any
  futurepastlastname:any
  futurepastfirstname:any
  futurepastmiddle_name:any
  futurepastprofileimage_pathf:any
  futurepastprofileimage_path:any
  //start search code  9-26-2019
  fromdatesearch:any
  patientsearch:any
  todatesearch:any
  tokentypesearch:any
  searchpage:any
  //end search code  9-26-2019
  public doctoravunailability: FormGroup;
  public addunavailability: FormGroup;
  familymemberselfshow:boolean=false
  keys:any;
  months:any ;
  years = [];
  fromdate:any
  todate:any
  fromdatefromtoday:any
  todatefromtoday:any
  showbrd:Boolean=false
  //new requeest start
  newproviderid:any
  newtoken_id:any
  newprovider_name:any
  newfuturepastprofileimage_path:any
  newfuturepastorg_location:any
  requestsreceivedreshadule:Boolean=false
  newreqtype:any
  screenname:any
constructor(private futurepastqueue:FuturepastqueuesService, 
  private router:Router,
  private snackbar:MatSnackBar,
  private searchtoken:SearchtokensService, 
  private formBuilder: FormBuilder,public ngProgress: NgProgress,
  public dialog: MatDialog,
  private assignnewtoken:AssignnewtokenService,
  private route: ActivatedRoute,
  private ongoingqueues:TodaysqueueService,
  private requestrceived:RequestreceivedService
) {

 }


ngOnInit() {
  var DateObj = new Date();
  this.enddate.setMonth(this.enddate.getMonth()+3)
  this.providerlistdata=true
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'custom_field_value_id',
      textField: 'custom_list_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettingsnewdata = {
      singleSelection: false,
      idField: 'custom_field_value_id',
      textField: 'custom_list_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.bookingtype="New"
    this.familymemberselfshow=true
    let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name
    this.dignosispriscription=false
    this.requestreceived=false
    this.hiddenall=true
    this.ongoingqueuelistdatashow=true
    this.latertoday=true
    //this.quesetuplist(1);
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let user_master_id=atob(currentUser.user_master_id);
    let profileimage_path=atob(currentUser.profileimage_path);
    let first_name=atob(currentUser.profileimage_path);
    //profileimage_path,first_name,user_master_id
    //this.getongoingqueue(profileimage_path,first_name,user_master_id)
    this.pipe = new DatePipe('en-US');
    this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
    this.queselecteddate=this.myFormattedDate
    /* futur past start*/
    this.futurepastorder = this.formBuilder.group({
      fromdata:"",
      todate:""
    })
    /* futur past end*/
    this.fromdatefromtoday=""
    this.todatefromtoday=""
    if((this.route.snapshot.queryParamMap.get('futurepasttokenback'))!=null){
      var fromdate=this.route.snapshot.queryParamMap.get('futurepastfromdate')
      var todate=this.route.snapshot.queryParamMap.get('futurepasttodate')
      this.fromdatefromtoday=fromdate
      this.todatefromtoday=todate
      var fromdatedata=fromdate.split('-')
      var todatedata=todate.split('-')
      this.user_master_id=this.route.snapshot.queryParamMap.get('futurepastmasterid')
      this.futurepastorder = this.formBuilder.group({
        fromdata:{ 
          date: { 
            year: fromdatedata[2], 
            month: Number(fromdatedata[1]), 
            day: Number(fromdatedata[0]) 
          },
          formatted:'' 
        },todate:{ 
          date: { 
            year: todatedata[2], 
            month: Number(todatedata[1]), 
            day: Number(todatedata[0]) 
          },
          formatted:''
        }
      })
      this.providerlistdata=false
      this.onSubmitfuturepast(this.futurepastorder)
      this.providerdetail=true
      this.queuser_name=this.route.snapshot.queryParamMap.get('futurepastlastname')
      this.first_name=this.route.snapshot.queryParamMap.get('futurepastfirstname')
      this.middle_name=this.route.snapshot.queryParamMap.get('futurepastmiddle_name')
      this.profileimage_path=this.route.snapshot.queryParamMap.get('futurepastprofileimage_path')
      this.org_location=this.route.snapshot.queryParamMap.get('futurepastorg_location')
      this.futurepastqueue.getorgque(0,this.size).subscribe((response) => {
        if(response.Data.length>1){
          this.showbrd=true
        }else{
          this.showbrd=false
        }
      })
      this.quesetuplistback(1);
      this.providerlistdata=false
      this.quesetupdata=false
    }else if((this.route.snapshot.queryParamMap.get('requestsreceivedreshadule'))!=null){
      this.screenname=this.route.snapshot.queryParamMap.get('screennameback')
      this.queue_namereqexisting=this.route.snapshot.queryParamMap.get('queue_namereqexisting')
      this.checktodaytoken=this.route.snapshot.queryParamMap.get('checktodaytoken')
      this.queue_transaction_id=this.route.snapshot.queryParamMap.get('queue_transaction_id')
      this.todaytokenid=this.route.snapshot.queryParamMap.get('todaytokenid')
      this.newreqtype=this.route.snapshot.queryParamMap.get('newreq')
      this.user_master_id=this.route.snapshot.queryParamMap.get('providerid')
      this.newtoken_id = this.route.snapshot.queryParamMap.get('token_id')
      this.newprovider_name=this.route.snapshot.queryParamMap.get('provider_name')
      this.newfuturepastprofileimage_path=this.route.snapshot.queryParamMap.get('futurepastprofileimage_path')
      this.newfuturepastorg_location=this.route.snapshot.queryParamMap.get('futurepastorg_location')
      this.requestsreceivedreshadule=true
      this.divclicknew(this.newprovider_name,this.newfuturepastprofileimage_path, this.newfuturepastorg_location)
      //future past start
      this.futurepastmasterid=this.route.snapshot.queryParamMap.get('futurepastmasterid')
      this.futurepasttodate=this.route.snapshot.queryParamMap.get('futurepasttodate')
      this.futurepastfromdate=this.route.snapshot.queryParamMap.get('futurepastfromdate')
      this.futurepastlastname=this.route.snapshot.queryParamMap.get('futurepastlastname')
      this.futurepastfirstname=this.route.snapshot.queryParamMap.get('futurepastfirstname')
      this.futurepastmiddle_name=this.route.snapshot.queryParamMap.get('futurepastmiddle_name')
      this.futurepastprofileimage_path=this.route.snapshot.queryParamMap.get('futurepastprofileimage_pathfuture')
      //future past end
      //search  start 9-26-2019
      this.fromdatesearch=this.route.snapshot.queryParamMap.get('fromdatesearch')
      this.patientsearch=this.route.snapshot.queryParamMap.get('patientsearch')
      this.todatesearch=this.route.snapshot.queryParamMap.get('todatesearch')
      this.tokentypesearch=this.route.snapshot.queryParamMap.get('tokentypesearch')
      this.searchpage=this.route.snapshot.queryParamMap.get('searchpage')
      //search  end 9-26-2019
    }else
{


this.futurepastqueue.getorgque(0,this.size)
.subscribe(
(response) => 
{



if(response.Data.length>1)
{
this.showbrd=true
this.quesetuplist(1);
}
else
{

this.showbrd=false
this.divclick(response.Data[0].user_master_id,
  response.Data[0].last_name,
  response.Data[0].first_name,
  response.Data[0].middle_name,
  response.Data[0].profileimage_path,
  response.Data[0].org_location)

}


})



//this.quesetuplist(1);

}
this.submitdate=""
}



quesetuplistback(page: number)
  {

    this.productLoading=true 

  this.futurepastqueue.getorgque((page - 1),this.size)
  .subscribe(
  (response) => 
  {

    this.productLoading=false 
    //this.providerlistdata=true
  this.quesetupdata=response.Data
  this.totalRecords=response.total_count
  this.page=page
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);


}
  




quesetuplist(page: number)
  {

    this.productLoading=true 

  this.futurepastqueue.getorgque((page - 1),this.size)
  .subscribe(
  (response) => 
  {

    this.productLoading=false 
    this.providerlistdata=true
  this.quesetupdata=response.Data
  this.totalRecords=response.total_count
  this.page=page
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);


}
  

divclick(user_master_id,last_name,first_name,middle_name,profileimage_path,org_location)
{
  this.providerlistdata=false
  this.providerdetail=true
  this.user_master_id=user_master_id
  this.queuser_name=last_name
  this.first_name=first_name
  this.middle_name=middle_name
  this.profileimage_path=profileimage_path
  this.org_location=org_location
  this.hiddenall=true
  var DateObj = new Date();
  this.futurepastorder = this.formBuilder.group({
    fromdata:{ date: { year: DateObj.getFullYear(), month: DateObj.getMonth()+1, day: DateObj.getDate()-1 },formatted:'' },
    todate:{ date: { year: this.enddate.getFullYear(), month: this.enddate.getMonth()+1, day: this.enddate.getDate() },formatted:''}
  })
  var frommont = Number(DateObj.getMonth())+1
  var todata = Number(this.enddate.getMonth())+1
  this.fromdatefromtoday=(DateObj.getDate()-1)+"-"+frommont+"-"+DateObj.getFullYear()
  this.todatefromtoday=this.enddate.getDate()+"-"+todata+"-"+this.enddate.getFullYear()
  this.onSubmitfuturepast(this.futurepastorder)
}

  differensequeue(queue_type,queue_transaction_id){
    if(this.requestsreceivedreshadule==true){
    }else{
      this.router.navigate(['/todaysqueue/'],
        {
        queryParams:{"futurepasttoken":"futurepasttoken",
        "futurepastqueuetype":queue_type,
        "futurepastqueue_transaction_id":queue_transaction_id,
        "futurepastmasterid":this.user_master_id,
        "futurepastfromdate":this.fromdate,
        "futurepasttodate":this.todate,
        "last_name":this.queuser_name,
        "first_name":this.first_name,
        "middle_name":this.middle_name,
        "profileimage_path":this.profileimage_path,
        "org_location":this.org_location,
        },skipLocationChange: true
      })
    }
  }

  pageChanged(event){
    this.quesetuplist(event)
  }

  submitdateChanged(event){
    if(event.formatted!=''){
      this.submitdate=event.formatted
    }
  }

  onSubmitfuturepast(formValue: any){
    var fromdate 
    // if((formValue.todate!=undefined)&&(formValue.todate!="")&&(formValue.todate.formatted!=""))
    if((formValue.fromdata!=undefined)&&(formValue.fromdata!="")&&(formValue.fromdata.formatted!="")){
      fromdate=formValue.fromdata.formatted
      this.fromdate=fromdate
    }else{
      fromdate=this.fromdatefromtoday
      this.fromdate=this.fromdatefromtoday
    }
    var todate
    if((formValue.todate!=undefined)&&(formValue.todate!="")&&(formValue.todate.formatted!="")){
      todate=formValue.todate.formatted
      this.todate=todate
    }else{
      todate=this.todatefromtoday
      this.todate=this.todatefromtoday
    }
    this.futurepastqueue.getfuturepast(this.user_master_id,fromdate,todate).subscribe((response) => {
      if(response.status_code=='200'){
        this.futurepastlist=true
        this.getfuturepastlist=response.Data
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
      })
    }
    })
  }
  
  getongoingqueue(profileimage_path,first_name,user_master_id){
    this.profileimage_path=profileimage_path
    this.first_name=first_name
    this.user_master_id=user_master_id
    this.ongoingqueues.getongoingqueue(user_master_id).subscribe((response) => {
      if(response.status_code=='200'){
        //this.providerlist=false
        this.ongoingqueuelist=response.Data.todaysongoing
        this.todayongoingqueuelist=response.Data.todayslater
        this.selectedproid = response.Data.queue_details[0]['provider_id']
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.errorMessageongoing=response.Metadata.Message
      }
    });
  }

  getongoingqueuetoken(queue_transaction_id){
    this.checktoday=1
    this.queue_transaction_id=queue_transaction_id
    this.ongoingqueues.getongoingtransactionqueue(queue_transaction_id).subscribe((response) => {
      if(response.status_code=='200'){
        this.futurepastlist=false
        this.providerdetail=false
        this.latertoday=false
        this.ongoingqueuelist=false
        this.ongoingtab=true
        this.queue_details=response.Data.queue_details
        this.is_doctor_arrived=response.Data.queue_details[0].is_doctor_arrived
        this.queue_id=response.Data.queue_details[0].queue_id
        this.ongoingestimated_end_time=response.Data.queue_details[0]['ongoingestimated_end_tim']
        this.ongoingqueue_date=response.Data.queue_details[0]['queue_date']
        this.ongoingprovider_name=response.Data.queue_details[0]['provider_name']
        this.ongoingqueue_pic_url=response.Data.queue_details[0]['profileimage_path']
        this.ongoingqueue_name=response.Data.queue_details[0]['queue_name']
        this.queue_reqque=response.Data.queue_details[0].queue_id
        //this.onqueselect(response.Data.queue_details[0].queue_id)
        this.selectedproid = response.Data.queue_details[0].provider_id
        this.avg_consulting_time=response.Data.avg_consulting_time
        this.avg_waiting_time=response.Data.avg_waiting_time
        this.cancelCompletedTokensCount=response.Data.cancelCompletedTokensCount
        this.checkup_total_amount=response.Data.checkup_total_amount
        this.noshowTokensCount=response.Data.noshowTokensCount
        this.ongoingTokensCount=response.Data.ongoingTokensCount
        this.pendingTokensCount=response.Data.pendingTokensCount
        this.allTokensCount=response.Data.allTokensCount
        this.onoganisationclick(4)
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
      }
    });
  }

  //assign new token start 6-3-2019 
  onbookingchangedata(event){
    this.bookingtype = event
  }

  onoganisationclick(tokentype){
    var tokentypedata
    if(tokentype.index=='0'){
      //tokentypedata="all"
      tokentypedata="completedorcancelled"
      if(this.completedorcancelledpage==0){
        this.page=1
      }else{
        this.page=this.completedorcancelledpage
      }
    }else if(tokentype.index=='1'){
      tokentypedata="noshow"
      if(this.completedorcancelledpage==0){
        this.page=1
        //this.page=this.noshowpage
      }else{
        //  this.page=1
        this.page=this.noshowpage
      }
    }else if(tokentype.index=='2'){
      tokentypedata="ongoing"
      //ongoing
      if(this.ongoingpage==0){
        this.page=1
      }else{
        this.page=this.ongoingpage
      }
    }else if(tokentype.index=='3'){
      tokentypedata="pending"
      //ongoing
      if(this.pendingpage==0){
        this.page=1
      }else{
        this.page=this.pendingpage
      }
    }else if(tokentype.index=='4'){
      //tokentypedata="completedorcancelled"
      tokentypedata="all"
      //ongoing
      if(this.allpage==0){
        this.page=1
      }else{
        this.page=this.allpage
      }
    }else{
      tokentypedata="all"
      if(this.allpage==0){
        this.page=1
      }else{
        this.page=this.allpage
      }
    }
    this.ongoingqueues.getongoingtransactiontype(tokentypedata,this.queue_transaction_id,this.page-1).subscribe((response) => {
      if(response.status_code=='200'){
        if(tokentypedata=="noshow"){
          this.noshow=response.Data.queue_tokens
        }else if(tokentypedata=="ongoing"){
          this.ongoing=response.Data.queue_tokens
        }else if(tokentypedata=="pending"){
          //this.selectedindex=3
          this.pending=response.Data.queue_tokens
        }else if(tokentypedata=="completedorcancelled"){
          //this.selectedindex=4
          this.completedorcancelled=response.Data.queue_tokens
        }else if(tokentypedata=="all"){
          //this.selectedindex=3
          this.alldata=response.Data.queue_tokens
        }else{
          this.alldata=response.Data.queue_tokens
        }
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        //  this.errorMessageongoing=response.Metadata.Message
        //this.error=response.Metadata.Message
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
      }
    });
  }

  checkboxchanged(event,transaction_id){
    if(event.target.checked==true){
      this.ongoingqueues.markDrArrived(transaction_id).subscribe((response) => {
        if(response.status_code=='200'){
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.errorMessageongoing=response.Metadata.Message
        }
      });
    }
  }





  
broadcastmesseg()
{

this.ongoingqueues.transactionQueueMessages((this.broadcastlistingpage - 1),
this.queue_transaction_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
this.ongoingtab=false
this.ongoingqueuelist=false
//this.broadcast=true
this.broadcasttoday=true
this.broadcastmesseglist=response.Data.queue_messages
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.broadcasterror=response.Metadata.Message
}
});
}



broadcastmessegtodays()
{

this.ongoingqueues.transactionQueueMessages((this.broadcastlistingpage - 1),
this.queue_transaction_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
this.ongoingtab=false
this.ongoingqueuelist=false
this.queuelist=false

this.broadcasttoday=true
this.broadcastmesseglist=response.Data.queue_messages
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.broadcasterror=response.Metadata.Message
}
});
}




broadcastbuttontapped()
{
  this.broadcast=false
  this.ongoingtab=true
  this.ongoingqueuelist=true
  this.getongoingqueuetoken(this.queue_transaction_id)
}


broadcastbuttontappedtoday()
{
  this.broadcasttoday=false

  /*
  this.ongoingtab=false
  this.ongoingqueuelist=false
 this.queuelist=true
*/

this.gettransactionqueuetoken(this.queue_transaction_id)

  //this.getongoingqueuetoken(this.queue_transaction_id)
}




backbuttontappedtoday()
{
  this.assignsearchdiv=false
  this.hiddenall=true

  this.gettransactionqueuetoken(this.queue_transaction_id)


}

backbuttontapped()
{

this.assignsearchdiv=false

this.hiddenall=true
this.getongoingqueuetoken(this.queue_transaction_id)
}


backbuttontappedreshadule()
{
  this.hiddenall=true

  this.requestreceived=false
}





backbuttontabbed()
{

if(this.checktoday==1)
{
  this.searchtokendatadetailshowdata=false
  this.getongoingqueuetoken(this.queue_transaction_id)
}
else if(this.checktoday==2)
{
this.searchtokendatadetailshowdata=false
this.gettransactionqueuetoken(this.queue_transaction_id)

}



}


addbroadcast()
{
this.addbroadcastmessage=true
this.broadcast=false
this.broadcasttoday=false
}

addmessagesms(messege)
{
this.ongoingqueues.addmessagesms(messege.value,this.queue_transaction_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
//this.broadcast=true  
this.addbroadcastmessage=false
this.broadcastmesseg()
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.broadcastmsgerror=response.Metadata.Message
}
});
}


gettransactionqueuetoken(queue_transaction_id)
{


this.checktoday=2

  this.queue_transaction_id=queue_transaction_id


  this.ngProgress.start();
  this.ngProgress.set(0)
  
this.ongoingqueuelistdatashow=false
  //this.providerdetail=false
  //this.queuelist=true
  this.ongoingqueues.transactionQueueTokens(queue_transaction_id)
  .subscribe(
  (response) => 
  {
  if(response.status_code=='200')
  {
    this.providerdetail=false
this.futurepastlist=false


this.selectedproid = response.Data.queue_details[0].provider_id

 // this.providerlist=false
 this.latertoday=false
 this.ongoingtab=false
  this.queuelist=true
this.queue_id=response.Data.queue_details[0].queue_id
this.queue_reqque=response.Data.queue_details[0].queue_id
this.onqueselect(response.Data.queue_details[0].queue_id)
this.progressbar =((response.Data.total_tokens)/(response.Data.queue_allowed_tokens))
this.total_tokens=response.Data.total_tokens
this.transactionQueueTokens=response.Data.queue_tokens
this.queue_detailstoday=response.Data.queue_details
this.ongoingprovider_name=response.Data.queue_details[0]['provider_name']
  }
  else if(response.status_code=='402')
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
{

  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
  })

 
//  this.error=response.Metadata.Message
  
}
})
}

onoganisationpagechange(page,tokentype)
{
var tokentypedata
if(tokentype=='completedorcancelled')
{
this.completedorcancelledpage=page
}
else if(tokentype=='noshow')
{
this.noshowpage=page
}
else if(tokentype=='ongoing')
{
this.ongoingpage=page
}
else if(tokentype=='pending')
{
this.pendingpage=page
}
else if(tokentype=='all')
{
this.allpage=page
}
this.ongoingqueues.getongoingtransactiontype(tokentype,this.queue_transaction_id,page-1)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
if(tokentype=="noshow")
{
this.noshow=response.Data.queue_tokens
}
else if(tokentype=="ongoing")
{
this.ongoing=response.Data.queue_tokens
}
else if(tokentype=="pending")
{
//this.selectedindex=3
this.pending=response.Data.queue_tokens
}
else if(tokentype=="completedorcancelled")
{
//this.selectedindex=4
this.completedorcancelled=response.Data.queue_tokens
}
else if(tokentype=="all")
{
//this.selectedindex=3
this.alldata=response.Data.queue_tokens
} 

}
    else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
  //  this.errorMessageongoing=response.Metadata.Message
  //this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
  
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);
}



/*
pageChanged(event)
{
this.page=event
this.quesetuplist(this.page)
}
*/
/*new code 4-17-2019 */





tokendetail(token_id)
{




this.searchtoken.gettokendetail("",token_id)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{

this.queuelist=false

this.token_id=token_id

this.transactionqueueid=response.Data[0].queue_transaction_id
this.token_provider_id=response.Data[0].token_provider_id

this.onassign_custom_form_values=response.Data[0].onassign_custom_form_values
this.oncomplete_custom_form_values=response.Data[0].oncomplete_custom_form_values




if((response.Data[0].oncomplete_custom_form_values!=null)&&(response.Data[0].oncomplete_custom_form_values!=null))
{
this.concatarrayforresult=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values)
}







this.ongoingtab=false
this.searchtokendatadetailshowdata=true
this.searchtokendatadetail=response.Data
this.searchtoken.getcustomlist(response.Data[0].queue_id)
.subscribe(
(response) => 
{
  if(response.status_code=="200")
  {


 this.customfield=response.Data

 
  }
  else if(response.status_code=='402')
{
     localStorage.clear();
     this.router.navigate(['/login']);
}
  else 
{
  this.customfield=null
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
  })
}

},
  (err: any) => console.log("error",err),
  () =>
  console.log("error in assign new token response")
);
}
else if(response.status_code=='402')
{
 localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
}
)


}





onSubmitdata(data){
  const formData = new FormData();
  if(this.customfield!=undefined){
    let counte=this.customfield.length
    if(this.customfield!=null){
      let counte=this.customfield.length
      for(let i=0;i<this.customfield.length;i++){
        var datalast=this.customfield[i]['custom_field_id']
        var datalastlist=(data[this.customfield[i]['custom_field_id']])
        var newStr
        // if(datalastlist != undefined){
          // if(datalastlist){
            var multileat="";
            if(this.customfield[i]['custom_field_type']=="List Multi Select"){
              if(datalastlist != undefined){
                for(let j=0;j<datalastlist.length;j++){
                  multileat+=datalastlist[j].custom_field_value_id+","
                }
              }
              newStr = multileat.substring(0, multileat.length-1);
            }
            if(this.customfield[i]['custom_field_type']=="List Multi Select"){
              if(newStr!=undefined){
                formData.append('custom_field_text_value['+i+']',newStr);  
              }
            }else if(this.customfield[i]['custom_field_type']=="Date"){
              var formattedVal:any = "";
              if(datalastlist != undefined){
                formattedVal = datalastlist.formatted;
              }
              formData.append('custom_field_text_value['+i+']',formattedVal);  
            }else{
              if(datalastlist!=undefined){
                  formData.append('custom_field_text_value['+i+']',datalastlist);  
              }
            }

            if(this.customfield[i]['custom_field_type']=="Date"){
              if(datalastlist != undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
              }
            }else if(this.customfield[i]['custom_field_type']=="Amount"){
              if(datalastlist!=undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
                formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);  
              }
            }else{
              if(datalastlist!=undefined){
                formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
              }
            }
          // }
        // }
      }
    }
  }
  if(data.remark!=""){
    formData.append('token_remark',data.remark);  
  }
  if(this.submitdate!=""){
  //this.submitdate
    formData.append('followup_date',this.submitdate);  
  }
  formData.append('booking_type',this.bookingtype);
  this.searchtoken.tokenSaveOnCompleteDetails(this.token_id,this.transactionqueueid,formData).subscribe((response) =>{
    if(response.status_code=="200"){
      this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
    }else if(response.status_code=='402'){
      localStorage.clear();
      this.router.navigate(['/login']);
    }else{
      //this.error=response.Metadata.Message
      this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
    }
  })
}

/*
onSubmitdata()
{
}
*/







  //tokenChangeSequence


cancelbooking()
{
  this.cancelmodelToggle("open")
}


cancelsubmit(reasonforcancel,token_id,queue_transaction_id)
{
  this.searchtoken.cancelsubmit(reasonforcancel,token_id,queue_transaction_id,this.changeseqtokenid)
  .subscribe(
  (response) => 
  {
  
  if(response.status_code=="200")
  {
  //  this.getTransactionTokenList=response.Data
  
  this.cancelmodelToggle("close")
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })
    this.tokendetail(token_id)
  }
  else if(response.status_code=='402')
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
  {
  //this.error=response.Metadata.Message
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  }
  })
  

}







modelToggle(action:string){
  if(action == "open"){
  this.modelFlag = true;
  }else{
  this.modelFlag = false;
  }
  }

  cancelmodelToggle(action:string){
    if(action == "open"){
    this.cancelmodel = true;
    }else{
    this.cancelmodel = false;
    }
    }
  



ontokenChange(tokenid)
{
this.changeseqtokenid=tokenid

}

changesequencesubmit(reson,token_id,transactionid)
{
  let position:any = "";
this.searchtoken.changesequencesubmit(reson,token_id,transactionid,this.changeseqtokenid,position)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{
//  this.getTransactionTokenList=response.Data

this.modelToggle("close")
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  this.tokendetail(token_id)
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
//this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})
}

/*new code 4-17-2019 */



/* new code 4-22-2019*/



reschedulelistsclick(token_id)
{




//this.showalltabs=false
this.requestrceived.getTokenDetails(token_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
  
this.hiddenall=false
this.customfield=null
this.requestreceived=true
this.getTokenDetailsreshadule=response.Data
var requested_on_date =  response.Data[0].opt1_rescheduling_requested_date
var token_provider_id =  response.Data[0].token_provider_id
this.pipe = new DatePipe('en-US');

this.token_idres = response.Data[0].token_id
this.token_noconres=response.Data[0].token_no



this.provideridselected =  response.Data[0].token_provider_id
this.token_datereq=response.Data[0].token_date
this.token_timereq=response.Data[0].token_time
this.queue_namereqexisting=response.Data[0].queue_name
this.newrequested_date=response.Data[0].opt1_rescheduling_requested_date
//const myFormattedDatenew = this.pipe.transform(requested_on_date, 'dd-MM-yyyy');
//this.reshaduledate=myFormattedDatenew
this.queue_reqque=response.Data[0].opt1_rescheduling_requested_queue
this.queue_namereq=response.Data[0].opt1_reshedule_queue_name
this.token_noconres=response.Data[0].opt1_queuename
//this.gettokenlistreshadule(myFormattedDatenew,token_provider_id)
//this.onqueselectreshadule(this.queue_reqque)

}
 else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);

}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}


gettokenlistreshadule(reqdate,token_provider_id)
{


this.requestrceived.gettokenlist(reqdate,token_provider_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{



this.quelistdatareshadule=response.Data

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}




onqueselectreshadule(event)
{

this.requestrceived.getmonitorqueQueues(event,this.reshaduledate)
.subscribe(
(response) => 
{

this.queue_reqque=event
this.concatarrayforresult=null







 //this.queue_id=event
if(response.status_code=="200")
{
this.token_nores=response.Data.last_token.token_no
this.token_timeres=response.Data.last_token.token_conveted_time
this.restoken_time=response.Data.last_token.token_time



this.requestrceived.getcustomlist(event)
.subscribe(
(response) => 
{

this.customfield=response.Data
},
(err: any) => console.log("error",err),
() =>
console.log("error in assign new token response")
);




}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})

}



onAssignresDateChanged(event)
{
if(event.formatted!='')
{
  this.requestrceived.getmonitorqueQueues(event.formatted,this.provideridselected)
  .subscribe(
  (response) => 
  {
 //this.queue_id=event
  if(response.status_code=="200")
 {
 

 this.reshaduledate=event.formatted

 //this.token_noconres=response.Data[0].token_no
 //this.token_datereq=response.Data[0].token_date
 //this.token_timereq=response.Data[0].token_time
 //this.token_nores=response.Data.last_token.token_no
 //this.token_timeres=response.Data.last_token.token_conveted_time
 //this.restoken_time=response.Data.last_token.token_time

 this.gettokenlistreshadule(this.reshaduledate,this.token_provider_id)

 //this.token_no=response.Data.last_token.token_no
 //this.token_time=response.Data.last_token.token_conveted_time
 //this.token_date=event.formatted
 }
 else if(response.status_code=='402')
 {
      localStorage.clear();
      this.router.navigate(['/login']);
 }
 else
 {
 
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 
 }
  })
}
else
{
  this.snackbar.open("Please select Date",'', {
    duration: 1000,
    });
}
}



reshaduleonqueclick()
{

const config = new MatDialogConfig();
config.data = { tokendate: this.reshaduledate,queue_id: this.queue_reqque};
this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
this.dialogRef.afterClosed().subscribe(value => {
this.token_nores=value.token_no
this.token_timeres=value.token_conveted_time
this.restoken_time=value.token_time
});
}



reshadulemessageclick()
{

  const config = new MatDialogConfig();

  

  config.data = { tokenid: this.token_idres};


  this.dialogRef=this.dialog.open(MessagequelistComponent,config);

  this.dialogRef.afterClosed().subscribe(value => {
  
  })

}
newreqmessageclick()
{

  const config = new MatDialogConfig();

  

  config.data = { tokenid: this.token_id};


  this.dialogRef=this.dialog.open(MessagequelistComponent,config);

  this.dialogRef.afterClosed().subscribe(value => {
  
  })

}



diegnosisprescription(tokenid)
{



this.hiddenall=false
this.dignosispriscription=true


this.requestrceived.getTokenDPRDetails(tokenid)
  .subscribe(
  (response) => 
  {

    



if(response.status_code=="200")
{

//this.patienthistorydata=response.diagnosis_details



this.token_details=response.Data.token_details[0]
this.diagnosis_details=response.Data.diagnosis_details





this.prescription_details=response.Data.prescription_details
this.test_details=response.Data.test_details




this.prescription_images=response.Data.prescription_images
this.report_images=response.Data.report_images
}
    else if(response.status_code=='402')
 {
      localStorage.clear();
      this.router.navigate(['/login']);
 }
 else
 {
 
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 
 }


})
}


/* assign new token*/

assigntoken(type)
{

  if(type=="ongoing")
  {
  this.todaystoken=false
  }
  else
  {
    this.todaystoken=true
  }
  this.cancelmodelforassign("open")
  //cancelmodelforassign


}


cancelmodelforassign(action:string){
  if(action == "open"){
  this.cancelmodelforassigndat = true;
  }else{
  this.cancelmodelforassigndat = false;
  }
  }



/*
  onSubmit(data)
  {
  
if(data.firstname!=undefined)
{

const formData = new FormData();

if(this.customfield!=undefined)
{

 
  let counte=this.customfield.length



if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr



if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}


}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}





if(this.queue_id!=undefined)
{
  this.assignnewtoken.submitassignquecustomforunknown(
    formData,this.queue_id,this.selectedproid,
    this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
    this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
    data.firstname,data.regno,this.usermasterid,data.gender,
    data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
  )
   .subscribe(
   (response) => 
   {




  this.queue_id=event




   if(response.status_code=="200")
   {
  
  this.router.navigate(['/']);
  
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
  
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  })
}
else
{
this.snackbar.open("please select queue",'', {
duration: 2000,
})
}

}
else
{




const formData = new FormData();

if(this.customfield!=undefined)
{

 
  let counte=this.customfield.length



if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr



if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}
}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}




if(this.queue_id!=undefined)
{

  
this.assignnewtoken.submitassignquecustom(
  formData,this.queue_id,this.selectedproid,
  this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
  this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
  this.searchfirst_name,data.regno,this.usermasterid,this.usergendermale,
  data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
)
 .subscribe(
 (response) => 
 {
this.queue_id=event




 if(response.status_code=="200")
 {

this.router.navigate(['/']);

this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });

}
else if(response.status_code=='402')
{
     localStorage.clear();
     this.router.navigate(['/login']);
}
else if(response.status_code=="400")
{
this.assignsearchdiv=true
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })

}

else 
{



  this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})


}
})




}
else
{
  this.snackbar.open("please select queue",'', {
    duration: 2000,
    })

}
}
}
*/


onSubmit(data)
{

if(data.firstname!=undefined)
{

const formData = new FormData();

if(this.customfield!=undefined)
{

 
  let counte=this.customfield.length



if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr



if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true))
{

  

if(datalastlist==""){

this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {
  duration: 2000,
  });
return false
}
}



if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}


}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}







if(this.queue_id!=undefined)
{
  this.assignnewtoken.submitassignquecustomforunknown(
    formData,this.queue_id,this.selectedproid,
    this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
    this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
    data.firstname,data.regno,this.usermasterid,data.gender,
    data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
  )
   .subscribe(
   (response) => 
   {




  this.queue_id=event




   if(response.status_code=="200")
   {
  
  this.router.navigate(['/']);
  
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
  
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  })
}
else
{
this.snackbar.open("please select queue",'', {
duration: 2000,
})
}

}
else
{




const formData = new FormData();

if(this.customfield!=undefined)
{

 
let counte=this.customfield.length












if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr


if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true))
{

  

if(datalastlist==""){

this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {
  duration: 2000,
  });
return false
}
}


if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{



/*
if((this.customfield[i]['is_mandatory']==true)&&(newStr==undefined))
{

  this.snackbar.open("please select multiselect field",'', {
    duration: 2000,
    });

}
*/
//else{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
//}

}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}
}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}




if(this.queue_id!=undefined)
{
  
this.assignnewtoken.submitassignquecustom(
  formData,this.queue_id,this.selectedproid,
  this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
  this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
  this.searchfirst_name,data.regno,this.usermasterid,this.usergendermale,
  data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
)
 .subscribe(
 (response) => 
 {
this.queue_id=event




 if(response.status_code=="200")
 {

this.router.navigate(['/']);

this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });

}
else if(response.status_code=='402')
{
     localStorage.clear();
     this.router.navigate(['/login']);
}
else if(response.status_code=="400")
{
this.assignsearchdiv=true
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })
}
else 
{


  this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})




}
else
{
  this.snackbar.open("please select queue",'', {
    duration: 2000,
    })

}
}

}







  
  

  onqueselect(event)
  {
  this.assignnewtoken.getmonitorqueQueues(this.myFormattedDate,event)
   .subscribe(
   (response) => 
   {
  this.queue_id=event
   if(response.status_code=="200")
  {
  
  
  
  this.token_no=response.Data.last_token.token_no
  
  
  
  //this.token_no=response.Data.last_token.token_no
  this.token_time=response.Data.last_token.token_conveted_time
  this.token_apitime=response.Data.last_token.token_time
  
  //this.token_time=response.Data.last_token.token_no_converted_slot
  this.token_date=this.queselecteddate
  
  
  
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  
  }
  })
  //custom field list start
  
  this.providerlist=false
  this.assignnewtokenform=true
  this.assignnewtoken.getcustomlist(event)
  .subscribe(
  (response) => 
  {
  
    if(response.status_code=="200")
    {
    
  
  this.customfield=response.Data
    }
    else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
    else 
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
    })
  }
  
  
  },
    (err: any) => console.log("error",err),
    () =>
    console.log("error in assign new token response")
  );
  }
  
   
onAssignDateChanged(event)
{
if(event.formatted!='')
{
this.queselecteddate=event.formatted

this.myFormattedDate=event.formatted

this.assignnewtoken.getProviderQueues(event.formatted,this.selectedproid)
.subscribe(
(response) => 
{



if(response.status_code=="200")
{
 // this.quelistdata=response.Data
this.queue_reqque=""
 this.quelistdata=""
  this.token_no=null
  this.token_time=null
  this.token_apitime=null
  this.token_date=null
  this.queue_id=""
  this.customfield = null
this.quelistdata=response.Data

this.userlogin.resetForm();


}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{

  this.quelistdata=[]
this.token_time=""
this.token_no=""
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
}
else
{
  this.snackbar.open("Please select Date",'', {
    duration: 1000,
    });
}
}

  

onqueclick()
{
const config = new MatDialogConfig();
config.data = { tokendate: this.queselecteddate,queue_id: this.queue_id};
this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
this.dialogRef.afterClosed().subscribe(value => {
this.token_no=value.token_no
this.token_time=value.token_conveted_time
this.token_apitime=value.token_time


});


}


assignnow(mobilenumber)
{


this.router.navigate(['/assignnewtoken/'],{
queryParams:{"futurepastmobile":mobilenumber,
"bookingtypeselectedtypesearch":this.bookingtypeselectedtype}, skipLocationChange: true
});
  



/*
this.bookingusername=mobilenumber

if(this.bookingtypeselectedtype=="Self")
{
  this.bookedfor="self"
  this.bookedforself=true


  this.assignnewtoken.getQTRAQUserDetails(mobilenumber,this.selectedproid)
  .subscribe(
  (response) => 
  {
  if(response.status_code=='200')
  {
  this.searchfield=true
  this.assignsearchdiv=true
  this.hiddenall=false
  this.cancelmodelforassign("close")
  this.providerlist=false
  this.assignsearchdiv=true

this.regno=response.Data[0]['reg_no']
this.usermasterid=response.Data[0]['user_master_id']
this.searchmobileage=response.Data[0]['age']
this.searchfirst_name=response.Data[0]['first_name']
this.usergendermale=response.Data[0]['gender']
this.userprofileimg=response.Data[0]['profile_pic_path']

this.datedata(this.myFormattedDate)
this.onqueselect(this.queue_id)

  }
  else if(response.status_code=='402')
  {
      localStorage.clear();
      this.router.navigate(['/login']);
  }
  else
  {
    this.searchmobileage=""
    this.searchfirst_name=""
    this.usergendermale=false
    this.usergenderfemale=false
    this.usergenderOther=false
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      });
  }
  },
    (err: any) => console.log("error",err),
    () =>
    console.log("error in assign new token response")
  );



}
else if(this.bookingtypeselectedtype=="Family")
{
this.assignnewtoken.getQTRAQfamilymemberuserdetail(this.mobilesearchmasterid,
  this.familymemeberidfordetail)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
this.bookedfor=response.Data[0]['member_type']
this.regno=response.Data[0]['reg_no']
if(response.Data[0]['member_type']=="family_member_without_qtraqid")
{
  this.usermasterid=response.Data[0]['family_member_id']
}
else
{
this.usermasterid=response.Data[0]['family_member_user_id']
}



this.hiddenall=false
this.searchfirst_name=response.Data[0]['member_first_name']
this.usergendermale=response.Data[0]['member_gender']
this.userprofileimg=response.Data[0]['member_pic_path']
this.searchbookinguserid = response.Data[0]['family_member_id']
this.cancelmodelforassign("close")
this.assignsearchdiv=true
this.providerlist=false
this.assignsearchdiv=true
this.searchfield=true

this.datedata(this.myFormattedDate)

}


else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{
  this.bookingusername=""
  this.searchmobileage=""
  this.searchfirst_name=""
  this.usergendermale=false
  this.usergenderfemale=false
  this.usergenderOther=false
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("error in assign new token response")
);
}
else if(this.bookingtypeselectedtype=="other")
{


this.bookedother=true
this.bookedfor="self"
this.bookedforself=true
this.cancelmodelToggle("close")
this.assignsearchdiv=true
this.assignsearchdiv=true
this.searchfield=true
this.datedata(this.myFormattedDate)
}
*/


}





datedata(event)
{
//if(event.formatted!='')
//{
//this.queselecteddate=event.formatted
this.assignnewtoken.getProviderQueues(event,this.selectedproid)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{
  this.quelistdata=response.Data



}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
}



cancelsubmitdata(mobilenumber)
{

  this.assignnewtoken.getQTRAQUserDetails(mobilenumber,this.selectedproid)
  .subscribe(
  (response) => 
  {





  if(response.status_code=='200')
  {


this.assignbuttonshow=true

this.bookingtypeselectedtype="Self"

this.tokenrequestorno=mobilenumber





this.mobilesearchmasterid=response.Data[0].user_master_id
this.selfname=response.Data[0].first_name
 this.selfnumber=response.Data[0].user_name
  this.selfimage=response.Data[0].profile_pic_path
  this.bookingfordropdownvis=true

  }
else if(response.status_code=='400')
{
  this.tokenrequestorno=mobilenumber
  this.bookingtypeselectedtype="other"
}
})
}

onbookingchange(event)
{
if(event=="Family")
{
this.assignnewtoken.getQTRAQfamilymember(this.mobilesearchmasterid)
.subscribe(
(response) => 
{


this.bookingtypeselectedtype="Family"

if(response.status_code=='200')
{
this.familymember=response.Data

this.familymemberselfshow=false

this.familymembershow=true

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
   });
}
})
}
else if(event=="Self")
{
this.familymembershow=false
this.familymemberselfshow=true
this.bookingtypeselectedtype="Self"


}

}


familymemberchackboxcheck(masterid)
{



this.familymemeberidfordetail=masterid
}




onreshaduleSubmit(data)
{

  
  if(this.queue_reqque!=undefined)
  {
  const formData = new FormData();
  
  
  
  if(this.customfield!=undefined)
  {
  
   
    let counte=this.customfield.length
  
  
  
  if(this.customfield!=null)
  {
  let counte=this.customfield.length
  
  
  
  for(let i=0;i<this.customfield.length;i++)
  {
  var datalast=this.customfield[i]['custom_field_id']
  var datalastlist=(data[this.customfield[i]['custom_field_id']])
  var newStr
  
  
  
  if(datalastlist != undefined)
  {
  if(datalastlist)
  {
  var multileat=""
  if(this.customfield[i]['custom_field_type']=="List Multi Select")
  {
  for(let j=0;j<datalastlist.length;j++)
  {
    multileat+=datalastlist[j].custom_field_value_id+","
  }
  newStr = multileat.substring(0, multileat.length-1);
  }
  if(this.customfield[i]['custom_field_type']=="List Multi Select")
  {
  if(newStr!=undefined){
  formData.append('custom_field_text_value[]',newStr);  
  }
  }
  else if(this.customfield[i]['custom_field_type']=="Date")
  {
  
  
  if(datalastlist.formatted!=undefined){
  formData.append('custom_field_text_value[]',datalastlist.formatted);  
  }
  }
  else
  {
    if(datalastlist!=undefined){
  formData.append('custom_field_text_value[]',datalastlist);  
    }
  }
  
  if(this.customfield[i]['custom_field_type']=="Date")
  {
  if(datalastlist.formatted!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }
  }
  else{
    if(datalastlist!=undefined)
    {
    formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
    formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
    }
  
  }
  
  
  }
  }
  }
  }
  }

  formData.append('tokenmsg',data.tokenmessageres);  


  formData.append('tokenno',this.token_nores);  
  formData.append('tokendate',this.reshaduledate);  
  formData.append('tokentime',this.restoken_time);  
  
  formData.append('requesttype','reschedule');  

this.requestrceived.confirmtokenres(formData,this.token_nores,this.restoken_time,this.reshaduledate,this.token_idres,this.queue_reqque,data.tokenmessageres)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{

this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
});
this.router.navigate(['/']);

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}
else
{
  this.snackbar.open("please select Queue",'', {
    duration: 2000,
    });
}

}



backtohome()
 {
   this.router.navigate(['/'])
 }

 backtohomeseleactdoctor()
 {
  this.providerdetail=false
  this.hiddenall=false
  this.providerlistdata=true
  this.getfuturepastlist=null
   this.router.navigate(['/futurepastqueues'])
 }



/*assign new token end*/


/*new code end 4-22-2019*/



//new requeueest call start

divclicknew(newprovider_name,newfuturepastprofileimage_path,newfuturepastorg_location)
{
  this.providerlistdata=false
  this.providerdetail=true
  this.hiddenall=true
  var DateObj = new Date();
  this.futurepastorder = this.formBuilder.group({
    fromdata:{ date: { year: DateObj.getFullYear(), month: DateObj.getMonth()+1, day: DateObj.getDate() },formatted:'' },
    todate:{ date: { year: this.enddate.getFullYear(), month: this.enddate.getMonth()+1, day: this.enddate.getDate() },formatted:''}
  })
  var frommont = Number(DateObj.getMonth())+1
  var todata = Number(this.enddate.getMonth())+1
  this.fromdatefromtoday=DateObj.getDate()+"-"+frommont+"-"+DateObj.getFullYear()
  this.todatefromtoday=this.enddate.getDate()+"-"+todata+"-"+this.enddate.getFullYear()
  this.onSubmitfuturepast(this.futurepastorder)
}


tokendetaildata()
{


  this.router.navigate(['/requestsreceived/'],{
    queryParams:{"callforrequestreceived":"callforrequestreceived",
    "newtoken_id":this.newtoken_id}
    , skipLocationChange: true
    });

}


tokendetaildatareshadule()
{


  this.router.navigate(['/requestsreceived/'],{
    queryParams:{"tokendetaildatareshadule":"tokendetaildatareshadule",
    "newtoken_id":this.newtoken_id}
    , skipLocationChange: true
    });

}





newrequeuest()
{
  this.router.navigate(['/requestsreceived/'])

}



searchbacktodoctor()
{

this.hiddenall=false
 this.providerdetail=false
this.futurepastqueue.getorgque(0,this.size)
.subscribe(
(response) => 
{



if(response.Data.length>1)
{
this.showbrd=true
this.quesetuplist(1);
}
else
{

this.showbrd=false
this.divclick(response.Data[0].user_master_id,
  response.Data[0].last_name,
  response.Data[0].first_name,
  response.Data[0].middle_name,
  response.Data[0].profileimage_path,
  response.Data[0].org_location)

}


})






}


//new requeueest call end

reschedulelistsclicktransaction(data)
{




if(data=='today')
{
this.router.navigate(['/requestsreceived/'],{
  queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
  "screenname":'Todays Queues',
  "type":'Todays Queues',
  "tokenidrequestreshadule":this.todaytokenid,"checktodaytokenid":this.checktoday,
  "queue_transaction_id":this.queue_transaction_id
  }, skipLocationChange: true
  
});

}
else if(data=='ongoing')
{

  this.router.navigate(['/requestsreceived/'],{
    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "screenname":'Ongoing Queues',
    "type":'Ongoing Queues',
    "tokenidrequestreshadule":this.todaytokenid,"checktodaytokenid":this.checktoday,
    "queue_transaction_id":this.queue_transaction_id
    }, skipLocationChange: true
  });
  
}

else if(data=='futurepast'){







  this.router.navigate(['/requestsreceived/'],{
    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "screenname":'Future Past Token',
    "type":'Future Past Token',
    "tokenidrequestreshadule":this.todaytokenid,
    "checktodaytokenid":this.checktoday,
    "queue_transaction_id":this.queue_transaction_id,
    "futurepastmasterid":this.futurepastmasterid,
    "futurepasttodate":this.futurepasttodate,
    "futurepastfromdate":this.futurepastfromdate,
    "futurepastlastname":this.futurepastlastname,
    "futurepastfirstname":this.futurepastfirstname,
    "futurepastmiddle_name":this.futurepastmiddle_name,
    "futurepastprofileimage_path":this.futurepastprofileimage_path,
    }, skipLocationChange: true
  });
 
}


else{



}

}




backfromreshadule(){

      this.router.navigate(['/todaysqueue/'],{
  
        queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
        "tokenidrequestreshadule":this.todaytokenid,
        "queue_transaction_id":this.queue_transaction_id,
        "provider_name":this.newprovider_name,
        "queue_namereqexisting":this.queue_namereqexisting,
        "callfrom":"reshadule",
        'screennameback':this.screenname,
        "checktodayid":this.checktodaytoken,
    
      }, skipLocationChange: true
        
      });
     }






     futurepastback(){

      this.router.navigate(['/todaysqueue/'],{
  
        queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
        "tokenidrequestreshadule":this.todaytokenid,
        "queue_transaction_id":this.queue_transaction_id,
        "provider_name":this.newprovider_name,
        "queue_namereqexisting":this.queue_namereqexisting,
        "callfrom":"reshadule",
        'screennameback':this.screenname,
        "checktodayid":this.checktodaytoken,

        "futurepastmasterid":this.futurepastmasterid,
        "futurepasttodate":this.futurepasttodate,
        "futurepastfromdate":this.futurepastfromdate,
        "futurepastlastname":this.futurepastlastname,
        "futurepastfirstname":this.futurepastfirstname,
        "futurepastmiddle_name":this.futurepastmiddle_name,
        "futurepastprofileimage_path":this.futurepastprofileimage_path,
        
      }, skipLocationChange: true
        
      });
     }

















     callfromprovider()
     {
     
       this.router.navigate(['/todaysqueue/'],{
     
         queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
         "callfrom":"provider",
         'screennameback':this.screenname,
         "tokenidrequestreshadule":this.todaytokenid,
         "queue_transaction_id":this.queue_transaction_id,
         "checktodayid":this.checktodaytoken,
         
         "futurepastmasterid":this.futurepastmasterid,
        "futurepasttodate":this.futurepasttodate,
        "futurepastfromdate":this.futurepastfromdate,
        "futurepastlastname":this.futurepastlastname,
        "futurepastfirstname":this.futurepastfirstname,
        "futurepastmiddle_name":this.futurepastmiddle_name,
        "futurepastprofileimage_path":this.futurepastprofileimage_path,
       
        
        }, skipLocationChange: true
         
       });
     
     
     }
     

     backtopage(){
        this.router.navigate(['/todaysqueue/'],{
        })
      }

      ongoingback(){
      this.router.navigate(['/ongoingqueues'])
      }











      backbuttontabbedforfuturepast()
      {
      





var fromdate=this.futurepastfromdate
var todate=this.futurepasttodate
this.fromdatefromtoday=fromdate
this.todatefromtoday=todate
var fromdatedata=fromdate.split('-')
var todatedata=todate.split('-')
this.user_master_id=this.route.snapshot.queryParamMap.get('futurepastmasterid')
this.futurepastorder = this.formBuilder.group({

fromdata:{ date: { year: fromdatedata[2], month: Number(fromdatedata[1]), day: Number(fromdatedata[0]) },formatted:'' },
todate:{ date: { year: todatedata[2], month: Number(todatedata[1]), day: Number(todatedata[0]) },formatted:''}
})
this.providerlistdata=false
this.onSubmitfuturepast(this.futurepastorder)
this.providerdetail=true
this.queuser_name=this.futurepastlastname
this.first_name=this.futurepastfirstname,
this.middle_name=this.futurepastmiddle_name,
this.profileimage_path=this.futurepastprofileimage_path

this.futurepastqueue.getorgque(0,this.size)
.subscribe(
(response) => 
{

  if(response.Data.length>1)
{
this.showbrd=true
}
else
{
this.showbrd=false
}
})
this.quesetuplistback(1);
this.providerlistdata=false
this.quesetupdata=false

this.queuelist=false

this.requestsreceivedreshadule=false











/*
      
        this.router.navigate(['/futurepastqueues/'],{
          queryParams:{"futurepasttokenback":"futurepasttokenback",
         "futurepastmasterid":this.futurepastmasterid,
        "futurepastfromdate":this.futurepastfromdate,
        "futurepasttodate":this.futurepasttodate,
        "futurepastlastname":this.futurepastlastname,
        "futurepastfirstname":this.futurepastfirstname,
        "futurepastmiddle_name":this.futurepastmiddle_name,
        "futurepastprofileimage_path":this.futurepastprofileimage_path
       },
          skipLocationChange: true
      })
*/

}
      

searchresadule()
{

  this.router.navigate(['/requestsreceived/'],{

   
    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "fromdate":this.fromdatesearch,
    "patient":this.patientsearch,
    "tokenidrequestreshadule":this.todaytokenid,
    "todate":this.todatesearch,
    "screenname":this.screenname,
    "tokentypesearch":this.tokentypesearch,
    "searchpage":this.searchpage}, skipLocationChange: true
    
  });


}




backfromreshadulefuture(){







  this.router.navigate(['/todaysqueue/'],{

    queryParams:{"requestsreceivedreshadule":"requestsreceivedreshadule",
    "fromdatesearch":this.fromdatesearch,
    "patientsearch":this.patientsearch,
    "tokenidrequestreshadule":this.todaytokenid,
    "todatesearch":this.todatesearch,
    "screenname":this.screenname,
    "tokentypesearch":this.tokentypesearch,
    "searchpage":this.searchpage,
    'screennameback':this.screenname,
    "checktodayid":this.checktodaytoken,

  }, skipLocationChange: true
    
  });
 }



  backtosearchmain(){
    this.router.navigate(['/searchtokens/'],{
      queryParams:{"searchtoken":"searchtoken",
      "fromdate":this.fromdatesearch,
      "patient":this.patientsearch,
      "todate":this.todatesearch,
      "tokentypesearch":this.tokentypesearch,
      "searchpage":this.searchpage}, skipLocationChange: true
    });
  }

  singleQueueSummary(queue_transaction_id:any,queue_name:any,queue_date:any) {
    const config = new MatDialogConfig();
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
		config.data = { 
      callfrom:"singlequeue",
			queue_transaction_id: queue_transaction_id,
			queuename: queue_name,
			queuedate: queue_date
    }
    config.width = "450px";
		this.dialogRef = this.dialog.open(TransactionqueuesummaryComponent, config);
		this.dialogRef.afterClosed().subscribe(value => {})
  }

  transactionQueueSummaryByDate(providerid:any,queue_start_date:any,queue_end_date:any,formated_date:any) {
    const config = new MatDialogConfig();
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
		config.data = { 
      callfrom:"daterange",
      providerid: providerid,
			providerorgid: orgid,
      queue_start_date: queue_start_date,
      queue_end_date:queue_end_date,
      formated_date:formated_date,
    }
    config.width = "450px";
		this.dialogRef = this.dialog.open(TransactionqueuesummaryComponent, config);
		this.dialogRef.afterClosed().subscribe(value => {})
  }

  transactionQueueSummaryByDateRange(){
    var Rawfromdate = this.futurepastorder.controls['fromdata'].value.date
    var fromdate = Rawfromdate.year+"-"+Rawfromdate.month+"-01";
    // formating date
    var date = new Date(fromdate), 
    y = date.getFullYear(), 
    m = date.getMonth();
    var lastDay = new Date(y, m + 1, 0);
    var todate = lastDay.getFullYear()+"-"+(lastDay.getMonth() + 1)+"-"+lastDay.getDate();
    const config = new MatDialogConfig();
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
		config.data = { 
      callfrom:"daterange",
      providerid: this.user_master_id,
			providerorgid: orgid,
      queue_start_date:fromdate,
      queue_end_date:todate,
      formated_date:"",
    }
    config.width = "450px";
		this.dialogRef = this.dialog.open(TransactionqueuesummaryComponent, config);
		this.dialogRef.afterClosed().subscribe(value => {})
  }

   
  
      
}





