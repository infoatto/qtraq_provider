import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuturepastqueuesComponent } from './futurepastqueues.component';

describe('FuturepastqueuesComponent', () => {
  let component: FuturepastqueuesComponent;
  let fixture: ComponentFixture<FuturepastqueuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuturepastqueuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuturepastqueuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
