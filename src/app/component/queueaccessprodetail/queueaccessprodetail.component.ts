import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl,FormArray } from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import {MatSnackBar} from '@angular/material';
import { SystemroleService } from './../../_services/systemrole.service';

@Component({
  selector: 'queueaccessprodetail',
  templateUrl: './queueaccessprodetail.component.html',
  styleUrls: ['./queueaccessprodetail.component.scss']
})

//queueaccessprodetail

export class QueueaccessprodetailComponent implements OnInit {

  errorMessage: string;
  submitted = false;
  public documentGrp: FormGroup;

  user_id:"";
  is_frontdesk:"";
  is_doctordesk:"";
  is_admindesk:"";
  profileimage_path:"";
  user_name:"";
  first_name:"";

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar:MatSnackBar,
    private systemsroleervice:SystemroleService) 
  { 
  }
  ngOnInit() 
  {

    this.documentGrp = this.formBuilder.group({
      items: this.formBuilder.array([this.createUploadDocuments(this.user_id,this.is_frontdesk,
      this.is_doctordesk,this.is_admindesk,this.profileimage_path,this.user_name,this.first_name,"editcall")]),
      });

    this.systemsroleervice.getsystemrole()
    .subscribe(
     data => {
    

      if(data.status_code==200)
      {
    
    //this.router.navigate(['/notification']);
     
    for(var i=0;i<(data.Data.length);i++)
    {
    this.items.insert(i, this.createUploadDocuments(
    data.Data[i].user_master_id,
    data.Data[i].is_l1,
    data.Data[i].is_l2,
    data.Data[i].is_org_admin,
    data.Data[i].profileimage_path,
    data.Data[i].user_name,
    data.Data[i].first_name,
    ""
    ))
    }

  }
      else
      {
      this.errorMessage=data.Metadata.Message
      
    
    }
   
 })


  
  }



  createUploadDocuments(user_id,is_frontdesk,is_doctordesk,is_admindesk,profileimage_path,user_name,first_name,editcall): FormGroup {
    return this.formBuilder.group({
      profileimage_path:profileimage_path,
      first_name:first_name,
      user_name:user_name,
      user_id: user_id,
      is_frontdesk: is_frontdesk,
      is_doctordesk : is_doctordesk,
      is_admindesk:is_admindesk,
      editcall:editcall
    });
    }
    
    
    
    get items(): FormArray {
      return this.documentGrp.get('items') as FormArray;
    };




onsystemrolesubmit(formValue: any)
{


  
  let main_form: FormData = new FormData();
  for (let j = 0; j < formValue.items.length-1; j++) 
{
main_form.append("user_id[]", formValue.items[j]['user_id'])
if((formValue.items[j]['is_frontdesk']==true)||(formValue.items[j]['is_frontdesk']=="Yes"))
{
main_form.append("is_frontdesk[]", "Yes")
}
else
{
main_form.append("is_frontdesk[]", "No")
}
if((formValue.items[j]['is_doctordesk']==true)||(formValue.items[j]['is_doctordesk']=="Yes"))
{
main_form.append("is_doctordesk[]", "Yes")
}
else
{
main_form.append("is_doctordesk[]", "No")
}
if((formValue.items[j]['is_admindesk']==true)||(formValue.items[j]['is_admindesk']=="Yes"))
{
main_form.append("is_admindesk[]", "Yes")
}
else
{
main_form.append("is_admindesk[]", "No")
}
//main_form.append("user_id[]", formValue.items[j]['user_id'])
//main_form.append("user_id[]", formValue.items[j]['user_id'])
}


this.systemsroleervice.submitsystemrole(main_form)
 .subscribe(
    data => {
    if(data.status_code==200)
    {
    //this.router.navigate(['/notification']);
   this.snackbar.open(data.Metadata.Message,'', {
    duration: 2000,
    });
  }
    else
    {
  //  this.errorMessage=data.Metadata.Message
  this.snackbar.open(data.Metadata.Message,'', {
    duration: 2000,
    });

}
    })
}



addsystemrole()
{


}

/*

  onSubmit() 
  {

    this.submitted = true;
this.notificationservice.createnotification(this.notification)
 .subscribe(
    data => {
    if(data.status_code==200)
    {
    this.router.navigate(['/notification']);
    }
    else
    {
    this.errorMessage=data.Metadata.Message
    }
    })
}
*/


}
