import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueaccessprodetailComponent } from './queueaccessprodetail.component';
describe('QueueaccessprodetailComponent', () => {
  let component: QueueaccessprodetailComponent;
  let fixture: ComponentFixture<QueueaccessprodetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueaccessprodetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueaccessprodetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
