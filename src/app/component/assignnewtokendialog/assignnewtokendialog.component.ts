import { Component, OnInit, Inject, ViewChild, Directive, Input, ElementRef } from '@angular/core';
import { NotificationService } from '../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ AssignnewtokenComponent } from '../assignnewtoken/assignnewtoken.component'
import { AssignnewtokenService } from '../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators,FormArray,ReactiveFormsModule,FormControl } from '@angular/forms';

@Component({
  selector: 'assignnewtokendialog',
  templateUrl: './assignnewtokendialog.component.html',
  styleUrls: ['./assignnewtokendialog.component.scss']
})
export class AssignnewtokendialogComponent implements OnInit {
  notification:any;
  totalRecords = "";
  page=0;
  queuelist:any
  token_no:any
  tokennumberexist:any
  tokendate:any
  queue_id:any
  selectedproid:any
  size = 10;


  assignbuttonshow:Boolean=false
  bookingtypeselectedtype:any
  bookingtypeselected:Boolean=false

  mobilenumbertext:Boolean=false
  textfieldshow:Boolean=false

  tokenrequestorno:any
  mobilesearchmasterid:any
  familymemberid:any
  selfname:any
  selfnumber:any
  selfimage:any
  bookingfordropdownvis:any
  familymember:any
  familymemberselfshow:Boolean=false
  familymembershow:Boolean=false
  familymemeberidfordetail:any
  bookingtypeselfselected:Boolean=false

  firstname:any
  middle_name:any
  lastname:any
  qtraqid:any

  orgname:any
  public searchform: FormGroup;

  //this.doctoraunavailability=response.Data
  assigndatalist:any
  showlist:Boolean=false

  searchradio="name"

  //constructor(private notificationservice:NotificationService) { }
  constructor(
    public dialogRef: MatDialogRef<AssignnewtokenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private assignnewtoken:AssignnewtokenService,
    private snackbar:MatSnackBar,private router: Router,private formBuilder: FormBuilder,
    private hostElement: ElementRef
  ){
    this.selectedproid=this.data.selectedproid
    this.firstname=this.data.firstname
    this.middle_name=this.data.middle_name
    this.lastname=this.data.lastname
  }

  ngOnInit(){
    this.bookingtypeselected=true
    let currentUser = JSON.parse(localStorage.getItem('actorganistion'));
    this.orgname = currentUser.org_name;
    this.mobilenumbertext=true
    this.searchform = this.formBuilder.group({
      fname:"",
      lname:"",
      mobilenumber:"",
      qtraqid:""
    })
    this.showlist=false
    // this.fname.nativeElement.focus();
  }


  assignsubmit(qtraqid,fname,lname,mobilenumber){
    if(this.searchradio=='name'){
      if((fname=="")&&(lname=="")) {
        this.snackbar.open("Please enter first name OR last name",'', {
          duration: 2000,
        });
        return false
      }
    }else if(this.searchradio=='phoneno'){
      if((mobilenumber=="")){
        this.snackbar.open("Please enter Phone No.",'', {
          duration: 2000,
        });
        return false
      }
    }else if(this.searchradio=='both'){
      if(((fname=="")&&(lname==""))||(mobilenumber=="")){
        this.snackbar.open("Phone and name required",'', {
          duration: 2000,
          });
          return false
      }
    }else if(this.searchradio == "id"){
      if(qtraqid == ""){
        this.snackbar.open("please enter QTRAQ ID ",'', {
          duration: 2000,
          });
          return false
      }
    }

  //this.searchradio
  /*new code start*/
    const formData = new FormData();
    if(this.searchradio == "id"){
      formData.append('searchingby','id')
      formData.append('searchingforfirstname',"");
      formData.append('searchingforlastname',"");
      formData.append('searchingforID',qtraqid);
    }else if(this.searchradio=='name'){
      formData.append('searchingby','name')
      formData.append('searchingforfirstname',fname)
      formData.append('searchingforlastname',lname)
    }else if(this.searchradio=='phoneno'){
      formData.append('searchingby','phone')
      formData.append('searchingforphone',mobilenumber)
    }else if(this.searchradio=='both'){
      formData.append('searchingby','both')
      formData.append('searchingforfirstname',fname)
      formData.append('searchingforlastname',lname)
      formData.append('searchingforphone',mobilenumber)
    }

    this.assignnewtoken.getQTRAQUserDetailsnew(formData,this.selectedproid).subscribe((response) =>{
      if(response.status_code=='200'){
        this.showlist=true
        this.assigndatalist=response.Data
      }else if(response.status_code=='400'){
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
  }

  onbookingchange(event){
    if(event=="Family"){
      this.assignnewtoken.getQTRAQfamilymember(this.mobilesearchmasterid).subscribe((response) =>{
        this.bookingtypeselectedtype="Family"
        if(response.status_code=='200'){
          this.familymember=response.Data
          this.familymemberselfshow=true
          this.familymembershow=true
          if(this.familymember==null){
            this.bookingtypeselected=false
            this.bookingtypeselfselected=true
            this.bookingtypeselectedtype="Self"
          }
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.bookingtypeselected=false
          this.bookingtypeselfselected=true
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
    }else if(event=="Self"){
      this.bookingtypeselfselected=true
      this.familymembershow=false
      this.familymemberselfshow=true
      this.bookingtypeselectedtype="Self"
    }
  }


  familymemberchackboxcheck(masterid){
    this.familymemberid=masterid
  }

  assignnownew(member_type,reg_no,age,member_name,profile_pic,member_id,user_name,user_master_id,gender,fm_user_master_id,last_visited){
    // alert(member_type);
    let data={
      'fullname':member_name,
      'mobilenumber':user_name,
      "bookingtypeselectedtype":member_type,
      "mobilesearchmasterid":user_master_id,
      "selectedproid":this.selectedproid,
      "familymemberid":member_id,
      "profileimg":profile_pic,
      "reg_no":reg_no,
      "age":age,
      "gender":gender,
      "last_visited":last_visited
    }

    if(((this.selectedproid==user_master_id)) && ((this.selectedproid==fm_user_master_id)||(fm_user_master_id=="")||(fm_user_master_id==0))){
      this.snackbar.open('Cannot select this user','', {
        duration: 2000,
      });
    }else{
      this.dialogRef.close(data);
    }
  }

  patientnotfound(qtraqid:any,fname:any,lname:any,mobilenumber:any){
    let data:any;
    mobilenumber = (mobilenumber)?mobilenumber:0;
    var fullname = (fname != "" && lname != "")?fname+" "+lname:(fname != "")?fname:(lname != "")?lname:"";
    // alert(fullname);return false;
    if(this.searchradio=='id'){
      data={
        'fullname':fullname,
        'mobilenumber':mobilenumber,
        "textvalue":qtraqid,
        "mobilenum":"",
        "serby":this.searchradio
      }
    }else if(this.searchradio=='name'){
      data={
        'fullname':fullname,
        'mobilenumber':mobilenumber,
        "textvalue":fname+" "+lname,
        "mobilenum":mobilenumber,
        "serby":this.searchradio
      }
    }else if(this.searchradio=='phoneno'){
      data={
        'fullname':fullname,
        'mobilenumber':mobilenumber,
        "textvalue":mobilenumber,
        "mobilenum":mobilenumber,
        "serby":this.searchradio
      }
    }else if(this.searchradio=='both'){
      data={
        'fullname':fullname,
        'mobilenumber':mobilenumber,
        "textvalue":fname+" "+lname,
        "mobilenum":mobilenumber,
        "serby":this.searchradio
      }
    }
    //let data={'fullname':"",'mobilenumber':"0","textvalue":searchtext,"serby":serby}
    this.dialogRef.close(data);
  }

  assignnow(mobilenumber){
    if((this.bookingtypeselectedtype=='Family')&&(this.familymemberid==undefined)){
      this.snackbar.open('please select Family Members','', {
        duration: 2000,
      });
    }else{
      let data={
        'firstname':this.firstname,
        'middle_name':this.middle_name,
        'lastname':this.lastname,
        'mobilenumber':mobilenumber,
        "bookingtypeselectedtype":this.bookingtypeselectedtype,
        "mobilesearchmasterid":this.mobilesearchmasterid,
        "selectedproid":this.selectedproid,
        "familymemberid":this.familymemberid
      }
      this.dialogRef.close(data);
    }
  }

  close(){
    this.dialogRef.close()
  }

  searchtextchange(textvalue){
    this.searchradio=textvalue
    var firstname:any = this.searchform.controls['fname'].value;
    let lastname = this.searchform.controls['lname'].value;
    let mobilenumber = this.searchform.controls['mobilenumber'].value;
    let qtraqid = this.searchform.controls['qtraqid'].value;
    this.assignsubmit(qtraqid,firstname,lastname,mobilenumber);
  }
}
