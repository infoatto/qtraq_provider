

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignnewtokendialogComponent } from './assignnewtokendialog.component';

describe('AssignnewtokendialogComponent', () => {
  let component: AssignnewtokendialogComponent;
  let fixture: ComponentFixture<AssignnewtokendialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignnewtokendialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignnewtokendialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

