import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import { DoctoravailabilityService } from './../../_services/doctoravailability.service';

import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'providerdoctoravailability',
  templateUrl: './providerdoctoravailability.component.html',
  styleUrls: ['./providerdoctoravailability.component.scss']
})
export class ProviderdoctoravailabilityComponent implements OnInit {


  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
   };

 /*
  notification: any =
  {
  utoken:localStorage.getItem("utoken"),
  usermasterid:localStorage.getItem("user_master_id"),
  nid: 0,
  notificationtitle: '',
  content:'',
  status: 'Active'
  };
*/

showdoctorlist:Boolean=true
showendtodate:Boolean=false
leavetimetype:Boolean=false
addunavailabilitydata:Boolean=false

broadcasttoalldata:Boolean=false

providerlistdata:any

providerlistshow:Boolean=false

doctoravailabilityfilter: any = {
  month:"",
  year:""
};

adddoctoravailability:any={
  unavailabilitydaytype:''
}


 errorMessage: string;
 doctoraunavailability:any
 orgname:any
 orgimage:any
 totalRecords:any
 providerid:any

viewimpact:any


  constructor(private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private router: Router,
  private doctoravailability:DoctoravailabilityService,
  private snackbar:MatSnackBar
) 
  { 
  }

  public doctoravunailability: FormGroup;

  public addunavailability: FormGroup;


  keys:any;
  months:any ;
  years = [];

  getDates() {
    var date = new Date();
    var currentYear = date.getFullYear()-5;

    //set values for year dropdown
    for (var i = 0; i <= 100; i++) {
      this.years.push(currentYear + i);
    }

 this.months = [
 {"id":"1","value":"Jan"},
 {"id":"2","value":"Feb"},
 {"id":"3","value":"Mar"},
 {"id":"4","value":"Apr"},
 {"id":"5","value":"May"},
 {"id":"6","value":"Jun"},
 {"id":"7","value":"Jul"},
 {"id":"8","value":"Aug"},
 {"id":"9","value":"Sep"},
 {"id":"10","value":"Oct"},
 {"id":"11","value":"Nov"},
 {"id":"12","value":"Dec"}
 ]
 }
 page=0;
    size = 10;
  ngOnInit() 
  {
    //addunavailabilitydata
this.providerlistshow=false

    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
   
this.orgname=orguserlist.orgname
this.orgimage=orguserlist.orgimg



this.addunavailability = this.formBuilder.group({
  unavailabilitydaytype:"",
  fromdata:"",
  fullday:"",
  broadcastedmessage:"",
  todate:"",
  leavetime:"",
  leavetimetype:"",
  unavailabilitytype:"",
  canceltokens:"",
  broadcasttoallqueue:"",
  unavailabilityreason:"",
  applytoall:""
})

this.doctoravunailability= this.formBuilder.group({
  month:'',
  year:''
})
this.getDates()
}


paginglist(page: number,formdata)
{
this.doctoravailability.doctoravailability((page - 1) ,this.size,formdata)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{
  this.doctoraunavailability=response.Data
  this.totalRecords=response.total_count
  this.page=page
  this.errorMessage=""
}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else 
{
//this.errorMessage=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}


OnSubmit(formValue: any)
{

  
  this.doctoravailabilityfilter.month=formValue.month
  this.doctoravailabilityfilter.year=formValue.year
  this.paginglist(1,formValue)

}


pageChanged(event)
{
  this.paginglist(event,this.doctoravailabilityfilter)
}

clearfilter()
{
   this.doctoravailabilityfilter.month=""
  this.doctoravailabilityfilter.year=""
  this.paginglist(1,this.doctoravailabilityfilter)
}


providerlist()
{


//this.getproviderlist(1)
}



/*
getproviderlist(page: number)
{
  
  
  this.doctoravailability.getorgque((page - 1),this.size)
  .subscribe(
  (response) => 
  {
this.showdoctorlist=false
this.providerlistshow=true
this.providerlistdata=response.Data
this.totalRecords=response.total_count
this.page=page
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
*/



adddoctoraunavailability()
{

this.providerlistshow=false
this.showdoctorlist=false

//this.providerid=providerid

this.addunavailabilitydata=true



}

canceldoctorunavailability()
{
this.showdoctorlist=true
this.addunavailabilitydata=false
}


viewimpactclick(formValue: any)
{



this.doctoravailability.viewimpact(formValue,this.providerid)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{



this.viewimpact=response.Data
//this.showdoctorlist=true
//this.addunavailabilitydata=false
//this.paginglist(1,this.doctoravailabilityfilter)
}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:")
);

}

/*
onNextClick(formValue: any)
{

}
*/

OnunavailabilitySubmit(formValue: any)
{


this.doctoravailability.addproviderdoctoravailability(formValue)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{
this.showdoctorlist=true
this.addunavailabilitydata=false
this.paginglist(1,this.doctoravailabilityfilter)
}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:")
);


}


fulldaychange(event)
{




  
  if(event=="Yes")
  {
  this.leavetimetype=false
  }
  else
  {
    this.leavetimetype=true
  }
  

}

//broadcasttoall

broadcasttoall(event)
{

  if(event=="Yes")
  {
  this.broadcasttoalldata=true
  }
  else
  {
    this.broadcasttoalldata=false
  }
}



unavailabilitydaytypechange(event)
{
  if(event=="Single")
  {
  this.showendtodate=false
  }
  else
  {
    this.showendtodate=true
  }
}




}
