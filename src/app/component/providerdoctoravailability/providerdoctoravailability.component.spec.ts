import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProviderdoctoravailabilityComponent } from './providerdoctoravailability.component';

describe('ProviderdoctoravailabilityComponent', () => {
  let component: ProviderdoctoravailabilityComponent;
  let fixture: ComponentFixture<ProviderdoctoravailabilityComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderdoctoravailabilityComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderdoctoravailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
