import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AssignnewtokenComponent } from './assignnewtoken.component'
import {MatTabsModule} from '@angular/material/tabs';
import { AssignnewtokenRoutingModule  } from "./assignnewtoken-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
//import{ TimeFormatPipe } from './pipes/time.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { SidebarComponent } from '../sidebar/sidebar.component'
@NgModule({
  imports: [CommonModule,
    FormsModule,
    AssignnewtokenRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     ReactiveFormsModule
    ],
  declarations: [AssignnewtokenComponent]
})
export class AssignnewtokenModule {}