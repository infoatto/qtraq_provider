import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssignnewtokenComponent } from './assignnewtoken.component';

describe('AssignnewtokenComponent', () => {
  let component: AssignnewtokenComponent;
  let fixture: ComponentFixture<AssignnewtokenComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignnewtokenComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AssignnewtokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
