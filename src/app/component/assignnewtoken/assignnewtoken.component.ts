import { Component,ViewChild, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import {MatSnackBar} from '@angular/material';
import { DatePipe } from '@angular/common';
import { MatDialogConfig } from '@angular/material'
import{ DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component'
import{ AssignnewtokendialogComponent } from './../../component/assignnewtokendialog/assignnewtokendialog.component'
import{ FamilymemberComponent } from './../../component/familymember/familymember.component'
import {MatDialog } from '@angular/material';
import { CommonService } from "./../../_services/common.service";

@Component({
  selector: 'assignnewtoken',
  templateUrl: './assignnewtoken.component.html',
  styleUrls: ['./assignnewtoken.component.scss']
})
export class AssignnewtokenComponent implements OnInit {
  modelsdata:Array<String>
  customfield:any
  assignnewtokenform:Boolean=false
  assignsearchdiv:Boolean=false
  providerlist:Boolean=true
  assigntokenprovider:any
  selectedproid:any
  quelistdata:any
  totalRecords:any
  queselecteddate:any
  onbookingforchangedata:any
  bookingtypeselectedtype:any
  bookingtypeselected:Boolean=true
  familymember:any
  cancelmodel:Boolean=false
  bookingfordropdownvis:Boolean=false
  selfname:any
  selfnumber:any
  selfimage:any
  familymemberselfshow:Boolean=false
  mobilesearchmasterid:any
  familymembershow:Boolean=false
  providerfirstname:any
  providermiddle_name:any
  providerlastname:any
  regno:any
  userprofileimg:any
  tokenrequestorno:any
  bookedfor:any
  bookedforself:Boolean=false
  bookedother:boolean=false
  usermasterid:any
  familymemeberidfordetail:any
  currentdate = new Date();
  bookingtype:any
  showalldata:Boolean=true
  //transaction start
  progressbar:any
  queuelist:Boolean=false
  queue_reqque:any
  total_tokens:any
  transactionQueueTokens:any
  queue_detailstoday:any
  //transaction end
  queueselected:any
  //assign new token start
  profileimg:any
  regnonew:any
  age:any
  //assign new token end
  serby:any
  textvalue:any
  mobilenum:any
  selecteddate:any
  selectedqueue:any
  last_visited:any

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,private snackbar:MatSnackBar,
    private assignnewtoken:AssignnewtokenService,public dialog: MatDialog,
    private cs: CommonService
  ){}

  @ViewChild(NgForm) userlogin: NgForm;
  public doctoravunailability: FormGroup;
  public addunavailability: FormGroup;
  keys:any;
  months:any ;
  years = [];
  page=0;
  dropdownSettings:any
  size = 10;
  pipe:any
  //mobile search field start
  mobilenumbersearch:any
  bookingusername:any
  searchusername:any
  searchmobileage:""
  searchfirst_name:any
  searchgender:any
  searchbookinguserid:any
  searchfield:Boolean=false
  usergendermale:any
  usergenderfemale:Boolean=false
  usergenderOther:Boolean=false
  //ravi new code end 2-14-2019
  //mobile search field end
  myFormattedDate
  showdate:any
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };
  //new variable
  token_time:any
  token_apitime:any;
  token_no:any
  token_date:any
  queue_id:any
  token_status:any;
  //new variable end
  dialogRef:any
  publicorgname:any
  assignbuttonshow:Boolean=false
  gender:any
  fullnamecheck:any
  //date picker end
  ngOnInit(){
    this.assignbuttonshow=false
    let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name
    this.pipe = new DatePipe('en-US');
    this.showdate=this.pipe.transform(this.currentdate, 'dd-MM-yyyy')
    this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
    this.queselecteddate=this.myFormattedDate
    this.bookingtype="New"
    this.bookingtypeselectedtype="other"
    this.bookingtypeselected=true
    this.bookingfordropdownvis=false
    this.familymembershow = false
    this.familymemberselfshow = true
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'custom_field_value_id',
      textField: 'custom_list_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.assignnewtoken.getorgque(0,this.size).subscribe((response) => {
      if(response.status_code == 200){
        // console.log("testing = ",response.Data);
        if(response.Data.length > 1){
          this.quesetuplist(1);
        }else{
          this.getproviderlist(response.Data[0].user_master_id,response.Data[0].first_name,response.Data[0].middle_name,response.Data[0].last_name);
        }
      }
    })
  }

  quesetuplist(page: number){
    this.assignnewtoken.getorgque((page - 1),this.size).subscribe((response) => {
      if(response.status_code == 200){
        this.assigntokenprovider=response.Data
        this.totalRecords=response.total_count
        this.page=page
      }
    });
  }

  pageChanged(event){
    this.page=event
    this.quesetuplist(this.page)
  }

  getproviderlist(user_master_id,firstname,middle_name,lastname){
    this.tokenrequestorno=""
    this.mobilesearchmasterid=""
    this.selfname=""
    this.selfnumber=""
    this.selfimage=""
    this.bookingfordropdownvis=false
    this.providerfirstname=firstname
    this.providermiddle_name=middle_name
    this.providerlastname=lastname
    this.selectedproid=user_master_id

    this.assignnewtoken.getProviderQueues(this.queselecteddate,this.selectedproid).subscribe((response) => {
      // if(response.status_code=="200"){
        if(response.status_code = 400){
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000});
        }
        this.quelistdata=response.Data
        this.token_no=0
        const config = new MatDialogConfig();
        config.data = {
          quelistdata:this.quelistdata,
          tokendate: this.queselecteddate,
          queue_id: (this.quelistdata != null )?this.quelistdata[0]['queue_id']:0,
          token_noexist:this.token_no,
          selectedproid:this.selectedproid,
          providername:this.providerfirstname+' '+this.providermiddle_name+' '+this.providerlastname
        };
        this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
        this.dialogRef.afterClosed().subscribe((value:any) => {
          if((value!=undefined)&&(value!="")){
            if(value.token_no > 0){
              this.token_no=value.token_no
              this.selecteddate=value.selecteddate
              this.selectedqueue=value.selectedqueue
              this.token_status=value.token_status;
              this.token_time = value.token_conveted_time
              this.token_apitime = value.token_time
              this.checkforqueue(this.myFormattedDate)
            }else{
              this.router.navigate(['/']);
            }
          }else{
            this.router.navigate(['/']);
            // this.queue_id = this.quelistdata[0]['queue_id'];
            // this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
            // this.checkforqueue(this.myFormattedDate)
          }
        })
      // }
    })
  }

  checkforqueue(event:any){
    this.assignnewtoken.getProviderQueues(event,this.selectedproid).subscribe((response) => {
      if(response.status_code=="432"){
        this.cancelmodelToggle("close")
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        this.router.navigate(['/assignnewtoken']);
      }else{
        const config = new MatDialogConfig();
        this.quelistdata = response.Data;
        config.data = {
          quelistdata: this.quelistdata,
          selectedproid: this.selectedproid,
          firstname:this.providerfirstname,
          middle_name:this.providermiddle_name,
          lastname:this.providerlastname
        };
        this.dialogRef=this.dialog.open(AssignnewtokendialogComponent,config);
        this.dialogRef.afterClosed().subscribe((value) => {
          if(value != null && value != undefined){
            this.fullnamecheck=value.fullname;
            this.bookingtypeselectedtype=value.bookingtypeselectedtype;
            var mobilenumber = value.mobilenumber;
            this.selectedproid=this.selectedproid;
            this.familymemeberidfordetail=value.familymemberid;
            this.mobilesearchmasterid=value.mobilesearchmasterid;
            this.profileimg=value.profileimg;
            this.regnonew=value.reg_no;
            this.age=value.age;
            this.gender=value.gender;
            this.last_visited=value.last_visited;
            if(this.fullnamecheck=='0'){
              if(value.serby!=""){
                this.serby=value.serby;
                this.textvalue=value.textvalue;
                this.mobilenum=value.mobilenum;
              }
            }
            if(value.serby!=""){
              this.serby=value.serby;
              this.textvalue=value.textvalue;
              this.mobilenum=value.mobilenum;
            }
            this.assignnow(mobilenumber);
          }
        });
      }
    })
  }

  cancelmodelToggle(action:string){
    if(action == "open"){
      this.cancelmodel = true;
    }else if(action == "closefamily"){
      this.cancelmodel = false;
    }else{
      this.familymember=null;
      this.assignbuttonshow=false;
      this.cancelmodel = false;
      this.token_no=null;
      this.token_time=null;
      this.token_apitime=null;
      this.token_date=null;
      this.regno=null;
      this.usermasterid=null;
      this.searchmobileage=null;
      this.searchfirst_name=null;
      this.usergendermale=null;
      this.userprofileimg=null;
    }
  }

  getproviderlistdata(user_master_id){
    this.providerlist=false;
    this.assignsearchdiv=true;
    this.searchfield=true;
    this.selectedproid=user_master_id;
  }

  onSubmit(data){
    if(data.firstname!=undefined){
      const formData = new FormData();
      if(this.customfield!=undefined){
        let counte=this.customfield.length;
        if(this.customfield!=null){
          let counte=this.customfield.length;
          for(let i=0;i<this.customfield.length;i++){
            var datalast=this.customfield[i]['custom_field_id'];
            var datalastlist=(data[this.customfield[i]['custom_field_id']]);
            var newStr;
            if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true)){
              if(datalastlist==""){
                this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {duration: 2000,});
                return false;
              }
            }

            // if(datalastlist != undefined){
              // if(datalastlist){
                var multileat="";
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  if(datalastlist != undefined){
                    for(let j=0;j<datalastlist.length;j++){
                      multileat+=datalastlist[j].custom_field_value_id+","
                    }
                  }
                  newStr = multileat.substring(0, multileat.length-1);
                }
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  if(newStr!=undefined){
                    formData.append('custom_field_text_value['+i+']',newStr);
                  }
                }else if(this.customfield[i]['custom_field_type']=="Date"){
                  var formattedVal:any = "";
                  if(datalastlist != undefined){
                    formattedVal = datalastlist.formatted;
                  }
                  formData.append('custom_field_text_value['+i+']',formattedVal);
                }else{
                  if(datalastlist!=undefined){
                      formData.append('custom_field_text_value['+i+']',datalastlist);
                  }
                }

                if(this.customfield[i]['custom_field_type']=="Date"){
                  if(datalastlist != undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                  }
                }else if(this.customfield[i]['custom_field_type']=="Amount"){
                  if(datalastlist!=undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                    formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
                  }
                }else{
                  if(datalastlist!=undefined){
                    formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
                  }
                }
              // }
            // }
          }
        }
      }
      if(data.age==0){
        this.snackbar.open("please provide Age",'', {duration: 2000,});
        return false;
      }
      formData.append('tokenproviderid',this.selectedproid);
      formData.append('tokenrequestorid',this.mobilesearchmasterid);
      formData.append('tokenrequestorname',this.selfname);
      formData.append('tokenrequestorno',this.tokenrequestorno);
      formData.append('tokenno',this.token_no);
      formData.append('tokentime',this.token_apitime);
      formData.append('tokendate',this.queselecteddate);
      formData.append('bookingfor',this.bookingtypeselectedtype);
      formData.append('bookingtype',this.bookingtype);
      formData.append('tokenmsg',data.tokenmsg);
      formData.append('userage',data.age);
      formData.append('usergender',data.gender);
      formData.append('patientid',data.regno);
      formData.append('bookingusername',data.firstname);
      formData.append('bookinguserid',this.usermasterid);
      if(this.queue_id!=undefined){
        this.assignnewtoken.submitassignquecustomforunknown(
          formData,this.queue_id,this.selectedproid,
          this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
          this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
          data.firstname,data.regno,this.usermasterid,data.gender,
          data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
        ).subscribe((response) => {
          this.queue_id=event;
          if(response.status_code=="200"){
            this.router.navigate(['/']);
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }else if(response.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }else{
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }
        })
      }else{
        this.snackbar.open("please select queue",'', {duration: 2000,});
      }
    }else{
      const formData = new FormData();
      if(this.customfield!=undefined){
        let counte=this.customfield.length;
        if(this.customfield!=null){
          let counte=this.customfield.length;
          for(let i=0;i<this.customfield.length;i++){
            var datalast=this.customfield[i]['custom_field_id'];
            var datalastlist=(data[this.customfield[i]['custom_field_id']]);
            var newStr;
            if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true)){
              if(datalastlist==""){
                this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {duration: 2000,});
                return false;
              }
            }
            // if(datalastlist != undefined){
              if(datalastlist){
                var multileat="";
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  for(let j=0;j<datalastlist.length;j++){
                    multileat+=datalastlist[j].custom_field_value_id+","
                  }
                  newStr = multileat.substring(0, multileat.length-1);
                }
                if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                  if(newStr!=undefined){
                    formData.append('custom_field_text_value[]',newStr);
                  }
                }else if(this.customfield[i]['custom_field_type']=="Date"){
                  if(datalastlist.formatted!=undefined){
                    formData.append('custom_field_text_value[]',datalastlist.formatted);
                  }
                }else{
                  if(datalastlist!=undefined){
                    formData.append('custom_field_text_value[]',datalastlist);
                  }
                }
                if(this.customfield[i]['custom_field_type']=="Date"){
                  if(datalastlist.formatted!=undefined){
                    formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);
                  }
                }else{
                  if(datalastlist!=undefined){
                    formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);
                    formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);
                  }
                }
              }
            // }
          }
        }
      }
      if(data.age==0){
        this.snackbar.open("please provide Age",'', {duration: 2000,});
        return false;
      }

      formData.append('tokenproviderid',this.selectedproid);
      formData.append('tokenrequestorid',this.mobilesearchmasterid);
      formData.append('tokenrequestorname',this.selfname);
      formData.append('tokenrequestorno',this.tokenrequestorno);
      formData.append('tokenno',this.token_no);
      formData.append('tokentime',this.token_apitime);
      formData.append('tokendate',this.queselecteddate);
      formData.append('bookingfor',this.bookingtypeselectedtype);
      formData.append('bookingtype',this.bookingtype);
      formData.append('tokenmsg',data.tokenmsg);
      formData.append('userage',data.age);
      formData.append('usergender',this.usergendermale);
      formData.append('patientid',data.regno);
      formData.append('bookingusername',this.searchfirst_name);
      formData.append('bookinguserid',this.usermasterid);
      if(this.queue_id!=undefined){
        this.assignnewtoken.submitassignquecustom(
          formData,
          this.queue_id,
          this.selectedproid,
          this.mobilesearchmasterid,
          this.selfname,this.tokenrequestorno,
          this.token_no,
          this.token_apitime,
          this.queselecteddate,
          this.bookedfor,
          this.searchfirst_name,
          data.regno,
          this.usermasterid,
          this.usergendermale,
          data.age,
          this.bookingtype,
          data.tokenmsg,
          this.bookingtypeselectedtype
        ).subscribe((response) => {
          this.queue_id=event;
          if(response.status_code=="200"){
            this.router.navigate(['/']);
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }else if(response.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }else if(response.status_code=="400"){
            this.assignsearchdiv=true;
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }else{
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }
        });
      }else{
        this.snackbar.open("please select queue",'', {duration: 2000,});
      }
    }
  }

  onAssignDateChanged(event){
    if(event.formatted!=''){
      this.queselecteddate=event.formatted;
      this.myFormattedDate=event.formatted;
      this.assignnewtoken.getProviderQueues(event.formatted,this.selectedproid).subscribe((response) => {
        if(response.status_code=="200"){
          this.quelistdata="";
          this.token_no=null;
          this.token_time=null;
          this.token_apitime=null;
          this.token_date=null;
          this.queue_id="";
          this.customfield = null;
          this.quelistdata=response.Data;
          this.queueselected = "";
          this.userlogin.resetForm();
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.quelistdata=null
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      })
    }else{
      this.snackbar.open("Please select Date",'', {duration: 1000,});
    }
  }

  datedata(event){
    this.assignnewtoken.getProviderQueues(event,this.selectedproid).subscribe((response) => {
      if(response.status_code=="200"){
        this.quelistdata=response.Data;
      }else if(response.status_code=="432"){
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        this.router.navigate(['/']);
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
  }

  assignnow(mobilenumber:any){
    var fullname = this.providerfirstname+" "+this.providermiddle_name+" "+this.providerlastname;
    if(mobilenumber == "0" || mobilenumber == undefined){
      if(this.fullnamecheck!=""){
        this.router.navigate(['/todaysqueue/'],{
          queryParams:{
            "assignnowtoken":"assignnowtoken",
            "mobilenumberassignnow":mobilenumber,
            "bookingtypeselectedtype":this.bookingtypeselectedtype,
            "mobilesearchmasterid":this.mobilesearchmasterid,
            "selectedproid":this.selectedproid,
            "familymemberid":this.familymemeberidfordetail,
            "fullname":fullname,
            "fullnamecheck":this.fullnamecheck,
            "profileimg":this.profileimg,
            "regno":this.regnonew,
            "age":this.age,
            "gender":this.gender,
            "tokenno":this.token_no,
            "selecteddate":this.selecteddate,
            "selectedqueue":this.selectedqueue,
            "last_visited":this.last_visited,
            "token_status":this.token_status,
            "token_time":this.token_time,
            "token_apitime":this.token_apitime
          }, skipLocationChange: true
        });
      }else{
        this.router.navigate(['/todaysqueue/'],{
          queryParams:{
            "assignnowtoken":"assignnowtoken",
            "fullname":fullname,
            "mobilenumberassignnow":mobilenumber,
            "fullnamecheck":"0",
            "serby":this.serby,
            "textvalue":this.textvalue,
            "mobilenum":this.mobilenum,
            "selectedproid":this.selectedproid,
            "bookingtypeselectedtype":"non_member",
            "tokenno":this.token_no,
            "selecteddate":this.selecteddate,
            "selectedqueue":this.selectedqueue,
            "last_visited":this.last_visited,
            "token_status":this.token_status,
            "queue_id":this.queue_id,
            "token_time":this.token_time,
            "token_apitime":this.token_apitime
          }, skipLocationChange: true
        });
      }
    }else{
      this.router.navigate(['/todaysqueue/'],{
        queryParams:{
          "assignnowtoken":"assignnowtoken",
          "mobilenumberassignnow":mobilenumber,
          "bookingtypeselectedtype":this.bookingtypeselectedtype,
          "mobilesearchmasterid":this.mobilesearchmasterid,
          "selectedproid":this.selectedproid,
          "familymemberid":this.familymemeberidfordetail,
          "fullname":fullname,
          "fullnamecheck":this.fullnamecheck,
          "profileimg":this.profileimg,
          "regno":this.regnonew,
          "age":this.age,
          "gender":this.gender,
          "tokenno":this.token_no,
          "selecteddate":this.selecteddate,
          "selectedqueue":this.selectedqueue,
          "last_visited":this.last_visited,
          "token_status":this.token_status,
          "token_time":this.token_time,
          "token_apitime":this.token_apitime,
          "queue_id":this.queue_id,
        }, skipLocationChange: true
      });
    }
  }

  onqueselect(event){
    this.assignnewtoken.getmonitorqueQueues(this.myFormattedDate,event).subscribe((response) => {
      this.queue_id=event;
      if(response.status_code=="200"){
        this.token_no=response.Data.last_token.token_no;
        this.token_time=response.Data.last_token.token_conveted_time;
        this.token_apitime=response.Data.last_token.token_time;
        this.token_date=this.queselecteddate;
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.token_no="";
        this.token_time="";
        this.token_apitime="";
        this.token_date="";
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
    //custom field list start
    this.providerlist=false
    this.assignnewtokenform=true
    this.assignnewtoken.getcustomlist(event).subscribe((response) => {
      if(response.status_code=="200"){
        this.customfield=response.Data;
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.customfield=null;
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    },(err: any) => console.log("error",err),() =>
      console.log("error in assign new token response")
    );
  }

  onqueclick(){
    const config = new MatDialogConfig();
    config.data = { tokendate: this.queselecteddate,queue_id: this.queue_id,token_noexist:this.token_no};
    this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
    this.dialogRef.afterClosed().subscribe((value) => {
      if(value.token_no > 0){
        this.token_no=value.token_no;
        this.token_time=value.token_conveted_time;
        this.token_apitime=value.token_time;
        this.token_date = value.selecteddate;
      }else{
        this.router.navigate(['/home']);
      }
    });
  }

  onbookingforchange(event){
    if(event=='self'){
      this.onbookingforchangedata=event
    }else{
      this.onbookingforchangedata=event
    }
  }

  onfamilypersonclick(){
    const config = new MatDialogConfig();
    config.data = { bookingfor: event};
    this.dialogRef=this.dialog.open(FamilymemberComponent,config);
    this.dialogRef.afterClosed().subscribe(value => {
    });
  }

  cancelsubmit(mobilenumber){
    this.assignnewtoken.getQTRAQUserDetails(mobilenumber,this.selectedproid).subscribe((response) => {
      if(response.status_code=='200'){
        this.assignbuttonshow=true;
        this.bookingtypeselectedtype="Self";
        this.tokenrequestorno=mobilenumber;
        this.mobilesearchmasterid=response.Data[0].user_master_id;
        this.selfname=response.Data[0].first_name;
        this.selfnumber=response.Data[0].user_name;
        this.selfimage=response.Data[0].profile_pic_path;
        this.bookingfordropdownvis=true;
      }else if(response.status_code=='432'){
        this.tokenrequestorno=mobilenumber;
        this.bookingtypeselectedtype="other"
        this.assignbuttonshow=true
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }else if(response.status_code=='400'){
        this.tokenrequestorno=mobilenumber;
        this.bookingtypeselectedtype="other";
        this.assignbuttonshow=true;
      }
    });
  }

  onbookingchange(event){
    if(event=="Family"){
      this.assignnewtoken.getQTRAQfamilymember(this.mobilesearchmasterid).subscribe((response) => {
        this.bookingtypeselectedtype="Family";
        if(response.status_code=='200'){
          this.familymember=response.Data;
          this.familymemberselfshow=false;
          this.familymembershow=true;
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      });
    }else if(event=="Self"){
      this.familymembershow=false;
      this.familymemberselfshow=true;
      this.bookingtypeselectedtype="Self";
    }
  }

  familymemberchackboxcheck(masterid){
    this.familymemeberidfordetail=masterid;
    this.bookingtypeselectedtype="Family";
  }

  changeorganisation(){}

  //assign new token start 6-3-2019
  onbookingchangedata(event){
    this.bookingtype = event;
  }

  getTransactionQueueDatas(){
    if(this.queue_id!=undefined){
      this.assignnewtoken.getTransactionQueueDatas(this.queue_id,this.queselecteddate,this.selectedproid).subscribe((response) =>{
        if(response.status_code=='200'){
          let transaction_id = response.Data[0].queue_transaction_id;
          if(transaction_id!=""){
            this.assignnewtoken.transactionQueueTokens(transaction_id).subscribe((response) => {
              this.showalldata=false;
              this.queuelist=true;
              this.queue_id=response.Data.queue_details[0].queue_id;
              this.queue_reqque=response.Data.queue_details[0].queue_id;
              this.progressbar =((response.Data.total_tokens)/(response.Data.queue_allowed_tokens));
              this.total_tokens=response.Data.total_tokens;
              this.transactionQueueTokens=response.Data.queue_tokens;
              this.queue_detailstoday=response.Data.queue_details;
            });
          }else{
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          }
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      });
    }else{
      this.snackbar.open("Please Select Queue ",'', {duration: 2000,});
    }
  }

  backtoassign(){
    this.showalldata=true;
    this.queuelist=false;
    this.queue_id = undefined;
    this.token_time = "" ;
    this.token_apitime = "";
    this.token_no = "";
  }

  //breadcram start 6-19-2019 start
  backtohome(){
    this.cs.backtohome()
  }
  //breadcram start 6-19-2019 end
  doclist(){
    this.assignsearchdiv=false
    this.searchfield=false
    this.providerlist=true
    this.router.navigate(['/assignnewtoken'])
  }

}
