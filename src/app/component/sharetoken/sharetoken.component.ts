import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from './../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
//import{ AssignnewtokenComponent } from './../../component/assignnewtoken/assignnewtoken.component'
import { PatienthistoryService } from './../../_services/patienthistory.service';
import { PatienthistoryComponent } from './../../component/patienthistory/patienthistory.component'
import {MatSnackBar} from '@angular/material';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
@Component({
  selector: 'sharetoken',
  templateUrl: './sharetoken.component.html',
  styleUrls: ['./sharetoken.component.scss']
})
export class SharetokenComponent implements OnInit {


  diagnosis="yes"
  prescription="yes"
  report="yes"
  fromdate:any
  todate:any

  user_master_id:any
  user_name:any
  first_name:any
  last_name:any
  middle_name:any
  profile_pic:any
  profile_pic_path:any
/*
dialogmonitorque
constructor() { }
ngOnInit() {
  }
*/
notification:any;
totalRecords = "";
page=0;
queuelist:any
tokendate:any
queue_id:any
size = 10;
myOptions: INgxMyDpOptions = {
  dateFormat: 'dd-mm-yyyy',
  };
  bookingtype:any
  bookingforid:any
//fromdatepatient:any
//todatepatient:any

  //constructor(private notificationservice:NotificationService) { }
constructor(public dialogRef: MatDialogRef<PatienthistoryComponent>,
@Inject(MAT_DIALOG_DATA) public data: any,
private shartoken:PatienthistoryService,
private snackbar:MatSnackBar,private router: Router
) 
{

//this.tokendate=this.data.tokendate
//this.queue_id=this.data.queue_id
this.fromdate=this.data.fromdate
this.todate=this.data.todate
this.bookingtype=this.data.consumertype
this.bookingforid=this.data.consumer_id


}
ngOnInit() 
{
 
}

/*
getdatafrommobile(mobilenumber)
{
  this.shartoken.getQTRAQUserDetails(mobilenumber)
  .subscribe(
  (response) => 
  {
  if(response.status_code=="200")
  {
  }
  else if(response.status_code=='402')
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
   });
   }
   })
}
*/


getmobiledata(mobilenumber)
{

this.shartoken.getQTRAQUserDetails(mobilenumber)
.subscribe(
(response) => 
{


 this.user_master_id = response.Data[0].user_master_id 
 this.user_name = response.Data[0].user_name 
 this.first_name = response.Data[0].first_name 
 this.last_name = response.Data[0].last_name 
 this.middle_name = response.Data[0].middle_name 
 this.profile_pic = response.Data[0].profile_pic 
 this.profile_pic_path=response.Data[0].profile_pic_path 

},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:")
);

}

ondiagnosischange(event)
{
this.diagnosis=event
}

onprescriptionchange(event)
{
this.prescription=event
}

onreportchange(event)
{
this.report=event
}

/*
onNoClick(token_no,token_status,token_conveted_time)
{
let data={'token_no':token_no,'token_status':token_status,
'token_conveted_time':token_conveted_time}

this.dialogRef.close(data);
}
*/
/*
sharefromdate(eventdate)
{
this.fromdate=eventdate.formatted
}

sharetodate(eventdate)
{
this.todate=eventdate.formatted
}
*/


onshareclick()
{



  let data={'fromdate':this.fromdate,'todate':this.todate,
  'diagnosis':this.diagnosis,'prescription':this.prescription,'report':this.report
  ,'user_master_id':this.user_master_id,'bookingtype':this.bookingtype,'bookingforid':this.bookingforid}
  this.dialogRef.close(data);
}

}
