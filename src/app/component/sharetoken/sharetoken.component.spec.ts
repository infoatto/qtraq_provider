import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharetokenComponent } from './sharetoken.component';

describe('SharetokenComponent', () => {
  let component: SharetokenComponent;
  let fixture: ComponentFixture<SharetokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharetokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharetokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
