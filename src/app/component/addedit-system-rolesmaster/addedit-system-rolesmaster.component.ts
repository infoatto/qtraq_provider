import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl,FormArray } from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import {MatSnackBar} from '@angular/material';
import { SystemroleService } from './../../_services/systemrole.service';
import { CommonService } from "./../../_services/common.service";

@Component({
  selector: 'app-addedit-notification-master',
  templateUrl: './addedit-system-rolesmaster.component.html',
  styleUrls: ['./addedit-system-rolesmaster.component.scss']
})
export class AddeditSystemRolesMasterComponent implements OnInit {
  toggleVal:boolean = false;
  errorMessage: string;
  submitted = false;
  public documentGrp: FormGroup;

  publicorgname:any

  user_id:"";
  is_frontdesk:"";
  is_doctordesk:"";
  is_admindesk:"";
  profileimage_path:"";
  user_name:"";
  first_name:"";
 last_name:"";
 name:""
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar:MatSnackBar,
    private cs:CommonService,
    private systemsroleervice:SystemroleService) 
  { 
  }
  ngOnInit() 
  {

    


let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name



    this.documentGrp = this.formBuilder.group({
      items: this.formBuilder.array([this.createUploadDocuments(
        this.user_id,this.is_frontdesk,
      this.is_doctordesk,this.is_admindesk,this.profileimage_path,
      this.user_name,this.first_name,this.name,"editcall")]),
      });

    this.systemsroleervice.getsystemrole()
    .subscribe(
     data => {



      if(data.status_code==200)
      {
    for(var i=0;i<(data.Data.length);i++)
    {
    this.items.insert(i, this.createUploadDocuments(
    data.Data[i].user_master_id,
    data.Data[i].is_l1,
    data.Data[i].is_l2,
    data.Data[i].is_org_admin,
    data.Data[i].profileimage_path,
    data.Data[i].user_name,
    data.Data[i].first_name,
    data.Data[i].name,
    ""
    ))
    }
  }
  else if(data.status_code=='402')
  {
      localStorage.clear();
      this.router.navigate(['/login']);
  
  }
  else
  {
  this.errorMessage=data.Metadata.Message
  }
   
 })

}



  createUploadDocuments(user_id,is_frontdesk,is_doctordesk,is_admindesk,profileimage_path,user_name,first_name,name,editcall): FormGroup {
    return this.formBuilder.group({
      profileimage_path:profileimage_path,
      first_name:first_name,
      name:name,
      user_name:user_name,
      user_id: user_id,
      is_frontdesk: is_frontdesk,
      is_doctordesk : is_doctordesk,
      is_admindesk:is_admindesk,
      editcall:editcall
    });
    }
    
    
    
    get items(): FormArray {
      return this.documentGrp.get('items') as FormArray;
    };




onsystemrolesubmit(formValue: any)
{


  
  let main_form: FormData = new FormData();
  for (let j = 0; j < formValue.items.length-1; j++) 
{

  let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
  let usermasterid=currentUser.user_master_id;


main_form.append("user_id[]", formValue.items[j]['user_id'])
if((formValue.items[j]['is_frontdesk']==true))
{
main_form.append("is_frontdesk["+j+"]", "Yes")
}
else
{
main_form.append("is_frontdesk["+j+"]", "No")
}
if((formValue.items[j]['is_doctordesk']==true))
{
main_form.append("is_doctordesk["+j+"]", "Yes")
}
else
{
main_form.append("is_doctordesk["+j+"]", "No")
}
if((formValue.items[j]['is_admindesk']==true))
{
main_form.append("is_admindesk["+j+"]", "Yes")
}
else
{
main_form.append("is_admindesk["+j+"]", "No")
}
//main_form.append("user_id[]", formValue.items[j]['user_id'])
//main_form.append("user_id[]", formValue.items[j]['user_id'])
}




this.systemsroleervice.submitsystemrole(main_form)
 .subscribe(
    data => {
if(data.status_code==200)
{
    //this.router.navigate(['/']);
   
    this.snackbar.open(data.Metadata.Message,'', {
      duration: 2000,
      });
      window.location.reload();
  
}
 else if(data.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);

}

    else
    {
      this.snackbar.open(data.Metadata.Message,'', {
        duration: 2000,
        });
  //  this.errorMessage=data.Metadata.Message
    
}
    })
}



addsystemrole()
{


}

toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}

backbutton()
{
  this.router.navigate(['/']);

}

confirmed() {
  
 }
 
 cancelled() {
  
 }



 backtohome()
 {
this.cs.backtohome()
 }

backtosystemrole()
{
this.router.navigate(['/systemroles']);
}

clickonusername(user_id,isdoctor){


var existornot

if(isdoctor==true)
{
  existornot="yes"

}else{

  existornot="no"

}


  this.router.navigate(['/editprofile/'],{
    queryParams:{"addeditsystemrole":"addeditsystemrole",
   "user_id_master_id":user_id,
   "isdoctor":existornot,
  },
    skipLocationChange: true
})
}


}
