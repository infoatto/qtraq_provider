import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditSystemRolesMasterComponent } from './addedit-system-rolesmaster.component';

describe('AddeditSystemRolesMasterComponent', () => {
  let component: AddeditSystemRolesMasterComponent;
  let fixture: ComponentFixture<AddeditSystemRolesMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditSystemRolesMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditSystemRolesMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
