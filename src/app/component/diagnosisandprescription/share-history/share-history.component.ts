import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { PatienthistoryService } from 'app/_services/patienthistory.service';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import {isNumeric} from "rxjs/util/isNumeric"
@Component({
  selector: 'app-share-history',
  templateUrl: './share-history.component.html',
  styleUrls: ['./share-history.component.scss']
})
export class ShareHistoryComponent implements OnInit {
  passedDetails:any = "";
  token_id:any;
  searchedProviderDetails:any;
  searchKey:any;
  sharetype:any = 'single';
  datePipe: any = new DatePipe('en-US');
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };
  currentDateTime: Date = new Date();
  currentYear: any = this.currentDateTime.getUTCFullYear();
	currentDate: any = this.currentDateTime.getUTCDate()+1 ;
	currentMonth: any = this.currentDateTime.getUTCMonth() + 1;
  toDefaultDate: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
		disableSince: {
			year: this.currentYear,
			month: this.currentMonth,
			day: this.currentDate
		}
  }
  fromDefaultDate: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableSince: {
			year: this.currentYear,
			month: this.currentMonth,
			day: this.currentDate
		},
  }
  sharefromdate:any = "";
  sharetodate:any = "";
  shareform:FormGroup;
  shareDiagnosis:boolean = false;
  sharePrescription:boolean = false;
  shareReport:boolean = false;
  tousermasterid:any;
  constructor(
    public dialogRef: MatDialogRef<ShareHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private patientHistoryServices:PatienthistoryService,
    private snackbar:MatSnackBar,
    private formBuilder:FormBuilder,
  ) {
    this.passedDetails = this.data;
    this.token_id = this.data.token_id;
    console.log(this.passedDetails);
  }

  ngOnInit() {

  }

  close(){
    this.dialogRef.close();
  }

  getQTRAQUserDetails(){
    if(isNumeric(this.searchKey) != true || this.searchKey.length != 10){
      this.snackbar.open("Please enter valid mobile number.",'', {
        duration: 2000,
      });
      return false;
    }
    if(this.searchKey !="" ){
      this.searchedProviderDetails = [];
      this.tousermasterid = "";
      this.patientHistoryServices.getQTRAQUserDetails(this.searchKey).subscribe((response) =>{
        if(response.status_code=='200'){
          this.searchedProviderDetails = response.Data;
          this.tousermasterid = this.searchedProviderDetails[0].user_master_id;
        }else if(response.status_code=='400'){
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
    }
  }

  searchDateChanged(type:any,selecteDate:any){
    if(type == "from"){
      this.sharefromdate = selecteDate.date.year+"-"+selecteDate.date.month+"-"+selecteDate.date.day;
    }else{
      this.sharetodate = selecteDate.date.year+"-"+selecteDate.date.month+"-"+selecteDate.date.day;
    }
  }

  shareHistory(){
    if(this.tousermasterid == "" || this.tousermasterid == undefined){
      this.snackbar.open("Please select doctor to share.",'', {
        duration: 2000,
      });
      return false;
    }

    if(this.sharetype == "multiple" && (this.sharefromdate == "" || this.sharetodate == "")){
      this.snackbar.open("Please select date range.",'', {
        duration: 2000,
      });
      return false;
    }
    var sharediagnosis = (this.shareDiagnosis)?"Yes":"No";
    var shareprescription = (this.sharePrescription)?"Yes":"No";
    var sharereport = (this.shareReport)?"Yes":"No";
    if(sharediagnosis == "No" && shareprescription == "No" && sharereport == "No"){
      this.snackbar.open("Please select Items to Share.",'', {
        duration: 2000,
      });
      return false;
    }

    //change from and to date
    if(this.sharetype == "single"){
      var mydate = new Date(this.passedDetails.token_date);
      this.sharefromdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
      this.sharetodate = this.sharefromdate;
    }

    this.patientHistoryServices.shareMultipleTokenHistory(
        this.tousermasterid,
        this.sharefromdate,
        this.sharetodate,
        sharediagnosis,
        shareprescription,
        sharereport,
        this.passedDetails.booking_for_id,
        this.passedDetails.booking_for_name,
        this.passedDetails.bookingfor,
        this.passedDetails.provider_id,
        this.passedDetails.token_id
      ).subscribe((response)=>{
      this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
      });
      setTimeout(() => {
        if(response.status_code == "200"){
          this.close();
        }
      }, 2000);
    });
  }
}
