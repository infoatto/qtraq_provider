import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSuggestionDialogComponent } from './select-suggestion-dialog.component';

describe('SelectSuggestionDialogComponent', () => {
  let component: SelectSuggestionDialogComponent;
  let fixture: ComponentFixture<SelectSuggestionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSuggestionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSuggestionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
