import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-select-suggestion-dialog',
  templateUrl: './select-suggestion-dialog.component.html',
  styleUrls: ['./select-suggestion-dialog.component.scss']
})
export class SelectSuggestionDialogComponent implements OnInit {
  selecLabel:string = "";
  suggestions:Array<any> = [];
  selectedData:Array<any> = [];
  customSuggestion:string = "";
  constructor(
    public dialogRef: MatDialogRef<SelectSuggestionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackbar:MatSnackBar
  ) { 
    if(data != undefined){
      this.selecLabel = data.selecLabel;
      this.selectedData = data.selectedData;
      let tempSelected = data.selectedData;

      if(data.suggestions != undefined){
        data.suggestions = data.suggestions.filter(function (el) {
          return el != null;
        });
        this.suggestions = data.suggestions.map(function(val){ 
          val.checked = false;
          if(tempSelected.length > 0){
            if(tempSelected != undefined && tempSelected.find(e => e === val.name)){
              val.checked = true;
            }
          }
          let temp = {"name":val.name,"checked":val.checked}
          return temp;
        }) 
      }
    }
  }

  ngOnInit() {
  }

  selectSuggestions(checkedValue:any,event:any){
    const targetIndex = this.suggestions.map(item => item.name).indexOf(checkedValue);
    if(event){
      if(this.suggestions[targetIndex].name != ""){
        this.selectedData.push(this.suggestions[targetIndex].name);
      }
    }else{
      const index = this.selectedData.indexOf(checkedValue, 0);
      if(index > -1){
        this.selectedData.splice(index, 1);
      }
    }
    this.suggestions[targetIndex].checked  = event;
  }

  addNewSuggestions(newSuggestion:any = ""){
    if(newSuggestion !== "" && newSuggestion !== undefined){
      this.selectedData.push(newSuggestion);
      this.suggestions.push({"name":newSuggestion,"checked":true});
    }
    let returnData = {"selectedData":this.selectedData,"suggestions":this.suggestions};
    this.dialogRef.close(returnData);
  }

  close(){
    let returnData = {"selectedData":this.selectedData,"suggestions":this.suggestions};
    this.dialogRef.close(returnData);
  }
}
