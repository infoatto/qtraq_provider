import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ProvidersService } from 'app/_services/providers.service';
import { DatePipe } from '@angular/common';
import { MatDialog, MatSnackBar, MatTabChangeEvent } from '@angular/material';
import { PatientSearchComponent } from './patient-search/patient-search.component';
import { SelectSuggestionDialogComponent } from './select-suggestion-dialog/select-suggestion-dialog.component';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { stringify } from 'querystring';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { PatienthistoryService } from 'app/_services/patienthistory.service';
import { Location } from '@angular/common';
import { RepositionScrollStrategy } from '@angular/cdk/overlay';
import { GalleryPreviewComponent } from './gallery-preview/gallery-preview.component';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ShareHistoryComponent } from './share-history/share-history.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
@Component({
  selector: 'app-diagnosisandprescription',
  templateUrl: './diagnosisandprescription.component.html',
  styleUrls: ['./diagnosisandprescription.component.scss']
})
export class DiagnosisandprescriptionComponent implements OnInit {
  @ViewChild('daterangetab') daterangetab: ElementRef;
  isEditable:boolean = true;
  patientList:any;
  providerList:any;
  selectedPatient:any;
  selectedProvider:any;
  selectedFromdate:any;
  selectedTodate:any;
  selectedPatientType:any="";
  consumerImage:any = "";
  searchForm:FormGroup;
  datePipe: any = new DatePipe('en-US');
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };
  currentDateTime: Date = new Date();
  currentYear: any = this.currentDateTime.getUTCFullYear();
	currentDate: any = this.currentDateTime.getUTCDate()+1 ;
	currentMonth: any = this.currentDateTime.getUTCMonth() + 1;
  toDefaultDate: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
		disableSince: {
			year: this.currentYear,
			month: this.currentMonth,
			day: this.currentDate
		}
  }
  fromDefaultDate: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableSince: {
			year: this.currentYear,
			month: this.currentMonth,
			day: this.currentDate
		},
  }
  fromDefaultDateSet:any;
  toDefaultDateSet:any;
  priorDate:Date = new Date(this.currentDateTime.setFullYear(this.currentDateTime.getFullYear() - 1));
  priorDateYear =  this.priorDate.getUTCFullYear();
  priorDateDay =  this.priorDate.getUTCDate() ;
  priorDateMonth =  this.priorDate.getUTCMonth()+1;
  testTypesDetails:Array<any> = [];
  historySuggestions:any;
  selectedHistory:Array<any> = [];
  historyNotes:any = "";
  complaintSuggestions:any;
  selectedComplaint:Array<any> = [];
  complaintNotes:any = "";
  medicinSuggestions:any;
  prescriptionRemarks:any = "";
  prescription_images:Array<any> = [];
  report_images:Array<any> = [];
  dosageSuggestions:any;
  diagnosisNotes:any = "";
  medicinnoteSuggestions:any;
  durationSuggestions:any;
  testnameSuggestions:any;
  historyDateRange:Array<any> = [];
  allPrescriptionDetails:Array<any> = [];
  selectedPrescriptionDetails:any;
  dateHide:boolean = true;
  loading:boolean = false;
  medicinfieldsArray: Array<any> = [];
  medicinNewAttribute: any = {};
  testPrescriptionfieldsArray: Array<any> = [];
  testPresNewAttribute: any = {};
  page:any = 0;
  isFullDataLoaded:boolean = false;
  currentTokenId:any = "";
  booking_for:any = "";
  booking_for_name:any = "";
  booking_for_user_id:any = "";
  provider_id:any = "";
  provider_user_master_id:any = "";
  provider_name:any = "";
  currentTokenDetails:any = "";
  selectedTokenId:any = "";
  isSearchDisable:boolean = false;
  matTabIndex:any = 0;
  selectedTokenDetails:Array<any> = [];
  queue_date:any;
  tokendate:any = "";
  screenname:string = "home";
  tokenId:any= "";
  isDirty = false;
  pipe: any;
  followupdate:any = "";
  formatedFollowupDate:any;
  myOptionsassign:INgxMyDpOptions ={
    dateFormat: 'dd-mm-yyyy',
    disableUntil:{year: this.currentYear, month: this.currentMonth, day: this.currentDate-1}
  }
  statusforrequestreshadule:any;
  ongoingprovider_name:any;
  ongoingqueue_name:any;
  queue_transaction_id:any;
  searchtoken:any;
  fromdate:any;
  patient:any;
  todate:any;
  tokentypesearch:any;
  searchpage:any;

  futurepastmasterid:any;
  futurepastfromdate:any;
  futurepasttodate:any;
  futurepastlastname:any;
  futurepastfirstname:any;
  futurepastmiddle_name:any;
  futurepastprofileimage_path:any;
  futurepastorg_location:any;
  queue_type:any;
  todaysongoingprovidername:any;
  constructor(
    private providerService:ProvidersService,
    private formBuilder:FormBuilder,
    private dialog:MatDialog,
    private snackbar:MatSnackBar,
    private route: ActivatedRoute,
    private patientservice:PatienthistoryService,
    private router : Router,
    private location: Location,
    private confirmationDialogService:ConfirmationDialogService
  ) {
    if(this.route.snapshot.queryParamMap.has('tokenid')){
      this.currentTokenId = this.route.snapshot.queryParamMap.get('tokenid');
      this.tokenId = this.route.snapshot.queryParamMap.get('tokenid');
      this.booking_for = this.route.snapshot.queryParamMap.get('booking_for');
      this.booking_for_name = this.route.snapshot.queryParamMap.get('booking_for_name');
      this.booking_for_user_id = this.route.snapshot.queryParamMap.get('booking_for_user_id');
      this.booking_for_user_id = this.route.snapshot.queryParamMap.get('booking_for_user_id');
      this.provider_id = this.route.snapshot.queryParamMap.get('provider_id');
      this.provider_name = this.route.snapshot.queryParamMap.get('provider_name');
      this.consumerImage = this.route.snapshot.queryParamMap.get('consumer_profile_pic_path');
      this.tokendate = this.route.snapshot.queryParamMap.get('tokendate');
      this.screenname = this.route.snapshot.queryParamMap.get('screenname');
      console.log(this.route.snapshot.queryParamMap);
      if(this.screenname == "Today's Queues"){
        this.statusforrequestreshadule = this.route.snapshot.queryParamMap.get('statusforrequestreshadule');
        this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
        this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
        this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
      }else if(this.screenname == "Ongoing Queues"){
        this.statusforrequestreshadule = this.route.snapshot.queryParamMap.get('statusforrequestreshadule');
        this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
        this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
        this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
      }else if(this.screenname == "Future Past Token"){
        this.statusforrequestreshadule = this.route.snapshot.queryParamMap.get('statusforrequestreshadule');
        this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
        this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
        this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
        this.futurepastmasterid = this.route.snapshot.queryParamMap.get('futurepastmasterid');
        this.futurepastfromdate = this.route.snapshot.queryParamMap.get('futurepastfromdate');
				this.futurepasttodate = this.route.snapshot.queryParamMap.get('futurepasttodate');
				this.futurepastlastname = this.route.snapshot.queryParamMap.get('futurepastlastname');
				this.futurepastfirstname = this.route.snapshot.queryParamMap.get('futurepastfirstname');
				this.futurepastmiddle_name = this.route.snapshot.queryParamMap.get('futurepastmiddle_name');
				this.futurepastprofileimage_path = this.route.snapshot.queryParamMap.get('futurepastprofileimage_path');
				this.futurepastorg_location = this.route.snapshot.queryParamMap.get('futurepastorg_location');
        this.fromdate = this.route.snapshot.queryParamMap.get('fromdate');
        this.patient = this.route.snapshot.queryParamMap.get('patient');
        this.todate = this.route.snapshot.queryParamMap.get('patient');
        this.tokentypesearch = this.route.snapshot.queryParamMap.get('tokentypesearch');
        this.searchpage = this.route.snapshot.queryParamMap.get('searchpage');
        this.queue_type = this.route.snapshot.queryParamMap.get('queue_type');
        this.todaysongoingprovidername = this.route.snapshot.queryParamMap.get('todaysongoingprovidername');
      }
    }
  }
  ngOnInit() {
    if(this.currentTokenId != "" && this.currentTokenId != null){
      this.isSearchDisable = true;
      // this.dateHide = false;
    }else{
      this.isSearchDisable = false;
      // this.dateHide = true;
    }
    if(this.tokendate == ""){
      this.selectedFromdate = this.priorDateYear+"-"+this.priorDateMonth+"-"+this.priorDateDay;
      this.fromDefaultDateSet =  { date: { year: this.priorDateYear, month: this.priorDateMonth, day: this.priorDateDay }}
      this.selectedTodate = this.currentYear+"-"+this.currentMonth+'-'+(this.currentDate-1);
      this.toDefaultDateSet =  { date: { year: this.currentYear, month: this.currentMonth, day: (this.currentDate-1) }}
    }else{
      var tokendateArray = this.tokendate.split("-");
      var mydate = new Date(tokendateArray[2]+"/"+tokendateArray[1]+"/"+tokendateArray[0]);
      this.toDefaultDateSet =  {date: { year: mydate.getFullYear(), month: (mydate.getMonth()+1 ), day: mydate.getDate() }}
      this.selectedTodate = (mydate.getFullYear())+'-'+(mydate.getMonth()+1)+'-'+(mydate.getDate());
      mydate.setFullYear( mydate.getFullYear() - 1 );
      this.selectedFromdate = (mydate.getFullYear())+'-'+(mydate.getMonth()+1)+'-'+(mydate.getDate());
      this.fromDefaultDateSet =  { date: { year: mydate.getFullYear(), month: (mydate.getMonth()+1 ), day: mydate.getDate() }};
    }
    this.searchForm =this.formBuilder.group({
      'patient': ['', Validators.required],
      'provider': ['', Validators.required],
      'fromdate': ['', Validators.required],
      'todate': ['', Validators.required],
    });
    this.searchForm.controls['fromdate'].setValue(this.fromDefaultDateSet);
    this.searchForm.controls['todate'].setValue(this.toDefaultDateSet);
    if(this.currentTokenId != "" && this.booking_for != "" && this.booking_for_name != "" && this.booking_for_user_id  != ""){
      this.providerList = [{"provider_id":this.provider_id,"provider_name":this.provider_name}];
      this.selectedProvider = this.provider_id;
      this.searchForm.controls['patient'].setValue(this.booking_for_name);
      this.selectedPatient = this.booking_for_user_id;
      this.selectedPatientType = this.booking_for;
      this.getProviderTokenHistory();
    }else{
      this.providerService.getShareHistoryProviderAppProviders().subscribe((response)=>{
        if(response.status_code == 200){
          this.providerList  = response.Data;
          this.selectedProvider = response.Data[0].provider_id;
          this.searchForm.controls['provider'].setValue(response.Data[0].provider_id);
          this.providerService.getShareHistoryProviderAppConsumers('0',this.selectedProvider).subscribe((response)=>{
            if(response.status_code == 200){
              this.patientList  = response.Data;
              this.searchForm.controls['patient'].setValue(response.Data[0].consumer_name);
              this.consumerImage = response.Data[0].consumer_image;
              this.selectedPatientType = response.Data[0].relation_type;
              this.selectedPatient = response.Data[0].consumer_id
              this.getProviderTokenHistory();
            }
          });
        }
      })
    }

    //assign suggestions
    this.medicinSuggestions = (localStorage.getItem('medicine_name') != undefined)?JSON.parse(localStorage.getItem('medicine_name')):[];
    this.dosageSuggestions = (localStorage.getItem('medicine_frequency') != undefined)?JSON.parse(localStorage.getItem('medicine_frequency')):[];
    this.medicinnoteSuggestions = (localStorage.getItem('medicine_remark') != undefined)?JSON.parse(localStorage.getItem('medicine_remark')):[];
    this.durationSuggestions = (localStorage.getItem('medicine_duration') != undefined)?JSON.parse(localStorage.getItem('medicine_duration')):[];
    this.testnameSuggestions = (localStorage.getItem('test_name') != undefined)?JSON.parse(localStorage.getItem('test_name')):[];
    this.historySuggestions = (localStorage.getItem('complaint_text') != undefined)?JSON.parse(localStorage.getItem('complaint_text')):[];
    this.complaintSuggestions = (localStorage.getItem('complaint_text') != undefined)?JSON.parse(localStorage.getItem('complaint_text')):[];

    if(this.medicinSuggestions.length < 1 || this.dosageSuggestions.length < 1 || this.medicinnoteSuggestions.length < 1 || this.durationSuggestions.length < 1 || this.testnameSuggestions.length < 1 || this.historySuggestions.length < 1 || this.complaintSuggestions.length < 1){
      this.getSuggestions();
    }
    this.getTestTypes();
  }

  scrollTabs(scrollType:string){
		if(scrollType == "left"){
      this.daterangetab.nativeElement.scrollLeft -= 100;
		}
		if(scrollType == "right"){
      this.daterangetab.nativeElement.scrollLeft += 100;
      if(!this.isFullDataLoaded){
        this.page++;
        this.getProviderTokenHistory();
      }
		}
  }

  patientChange(consumer_id:any){
    if(this.patientList.length>0){
      let selectedDetails = this.patientList.find(x => x.consumer_id === consumer_id);
      if(selectedDetails !== undefined) {
        this.consumerImage = selectedDetails.consumer_image;
        this.selectedPatientType = selectedDetails.relation_type;
        this.selectedPatient = consumer_id;
        this.getProviderTokenHistory();
      }
    }
  }

  providerChange(provider_id:any){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    // let current_providerid = atob(currentUser.user_master_id);
    // if(provider_id != current_providerid){
    //   this.dateHide = false;
    // }else{
    //   this.dateHide = true;
    // }
    this.providerService.getShareHistoryProviderAppConsumers(0,provider_id).subscribe((response)=>{
      if(response.status_code == 200){
        this.patientList  = response.Data;
        this.searchForm.controls['patient'].setValue(response.Data[0].consumer_name);
        this.consumerImage = response.Data[0].consumer_image;
        this.selectedPatientType = response.Data[0].relation_type;
        this.selectedPatient = response.Data[0].consumer_id;
        this.selectedProvider = provider_id;
        this.page = 0;
        this.getProviderTokenHistory();
      }
    });
  }

  searchDateChanged(dateType:any,event:any){
    this.currentTokenId = "";
    if(dateType == "to"){
      var todate = event.formatted.split("-");
      this.selectedTodate = todate[2]+'-'+todate[1]+'-'+todate[0];
    }else{
      var fromdate = event.formatted.split("-");
      this.selectedFromdate = fromdate[2]+'-'+fromdate[1]+'-'+fromdate[0];
    }
    this.getProviderTokenHistory();
  }

  patientSearch(){
    let dialogRef =  this.dialog.open(PatientSearchComponent,{
      data:{"patientList":this.patientList,"providerid":this.selectedProvider},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((selectedDetails) => {
      if(selectedDetails !== undefined) {
        this.consumerImage = selectedDetails.consumer_image;
        this.selectedPatientType = selectedDetails.relation_type;
        this.searchForm.controls['patient'].setValue(selectedDetails.consumer_name);
        this.selectedPatient = selectedDetails.consumer_id;
        this.page = 0;
        this.getProviderTokenHistory();
      }
    })
  }

  getSuggestions(requestFrom:any = ""){
    this.providerService.getSuggestion(this.searchForm.controls['provider'].value).subscribe((response)=>{
      if(response.status_code == 200){
        this.historySuggestions = response.Data.complaint_text.map(function(val){
          let checked = false;
          let temp = {"name":val,"checked":checked}
          return temp;
        })
        this.complaintSuggestions = response.Data.complaint_text.map(function(val){
          let checked = false;
          let temp = {"name":val,"checked":checked}
          return temp;
        })
        localStorage.setItem('complaint_text',JSON.stringify(this.historySuggestions));
        localStorage.setItem('medicine_name',JSON.stringify(response.Data.medicine_name));
        localStorage.setItem('medicine_frequency',JSON.stringify(response.Data.medicine_frequency));
        localStorage.setItem('medicine_remark',JSON.stringify(response.Data.medicine_remark));
        localStorage.setItem('medicine_duration',JSON.stringify(response.Data.medicine_duration));
        localStorage.setItem('test_name',JSON.stringify(response.Data.test_name));

        this.medicinSuggestions = response.Data.medicine_name;
        this.dosageSuggestions = response.Data.medicine_frequency;
        this.medicinnoteSuggestions = response.Data.medicine_remark;
        this.durationSuggestions = response.Data.medicine_duration;
        this.testnameSuggestions = response.Data.test_name ;
        if(requestFrom != ""){
          this.snackbar.open(response.Metadata.Message,"",{duration:2000});
        }
      }
    })
  }

  getTestTypes(){
    this.providerService.getTestTypes().subscribe((response:any)=>{
      if(response.status_code == "200"){
        this.testTypesDetails = response.Data;
      }else{
        this.testTypesDetails = [];
      }
    });
  }

  selectHistory(){
    let dialogRef =  this.dialog.open(SelectSuggestionDialogComponent,{
      width: '100px !important',
      data:{"selectType":"history","selecLabel":"Select History","suggestions":this.historySuggestions,"selectedData":this.selectedHistory},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((response) => {
      if(response != undefined) {
        this.isDirty = true;
        this.selectedHistory = response.selectedData;
        this.historySuggestions = response.suggestions;
      }
    })
  }

  removeHistoryTags(history:any) {
    const index: number = this.selectedHistory.indexOf(history);
    if (index !== -1) {
      this.selectedHistory.splice(index, 1);
    }
  }

  selectComplaint(){
    let dialogRef =  this.dialog.open(SelectSuggestionDialogComponent,{
      width: '100px !important',
      data:{"selectType":"complaint","selecLabel":"Select Current Complaints","suggestions":this.complaintSuggestions,"selectedData":this.selectedComplaint},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((response) => {
      if(response != undefined) {
        this.isDirty = true;
        this.selectedComplaint = response.selectedData;
        this.complaintSuggestions = response.suggestions;
      }
    })
  }

  removeComplaintTags(history:any) {
    const index: number = this.selectedComplaint.indexOf(history);
    if (index !== -1) {
      this.selectedComplaint.splice(index, 1);
    }
  }

  addmoreMedicin(existingValues:any = ""){
    if(existingValues != ""){
      existingValues.forEach((element:any) => {
        if(element.entry_type = "P" && element.sub_entry_type == "M"){
          this.medicinNewAttribute = {
            medicin:element.medicine_name,
            medicin_note:element.d_p_remarks,
            dosage:element.medicine_frequency,
            duration:element.medicine_duration,
            dispanse:(element.medicine_dispense == "No")?false:true,
          }
          this.medicinfieldsArray.push(this.medicinNewAttribute);
          const lastIndex = this.medicinfieldsArray.length-1 ;
          //for medicin
          this.medicinfieldsArray[lastIndex].medicinControl = new FormControl();
          this.medicinfieldsArray[lastIndex].medicinFilteredOptions = this.medicinfieldsArray[lastIndex].medicinControl.valueChanges.pipe(startWith(''),map((value:any) => this.autoCompletefilter("medicin",value)));
          //for medicin
          //for medicin_note
          this.medicinfieldsArray[lastIndex].medicin_noteControl = new FormControl();
          this.medicinfieldsArray[lastIndex].medicin_noteFilteredOptions = this.medicinfieldsArray[lastIndex].medicin_noteControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("medicin_note",value)));
          //for medicin_note
          //for dosage
          this.medicinfieldsArray[lastIndex].dosageControl = new FormControl();
          this.medicinfieldsArray[lastIndex].dosageFilteredOptions = this.medicinfieldsArray[lastIndex].dosageControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("dosage",value)));
          //for dosage
          //for duration
          this.medicinfieldsArray[lastIndex].durationControl = new FormControl();
          this.medicinfieldsArray[lastIndex].durationFilteredOptions = this.medicinfieldsArray[lastIndex].durationControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("duration",value)));
          //for duration

          this.medicinNewAttribute = {};
        }else if(element.entry_type = "P" && element.sub_entry_type == "PN"){
          this.prescriptionRemarks = element.d_p_remarks;
        }
      });
    }else{
      this.medicinNewAttribute = {
        medicin:"",
        medicin_note:"",
        dosage:"",
        duration:"",
        dispanse:false,
      }
      this.medicinfieldsArray.push(this.medicinNewAttribute);
      const lastIndex = this.medicinfieldsArray.length-1 ;
      //for medicin
      this.medicinfieldsArray[lastIndex].medicinControl = new FormControl();
      this.medicinfieldsArray[lastIndex].medicinFilteredOptions = this.medicinfieldsArray[lastIndex].medicinControl.valueChanges.pipe(startWith(''),map((value:any) => this.autoCompletefilter("medicin",value)));
      //for medicin
      //for medicin_note
      this.medicinfieldsArray[lastIndex].medicin_noteControl = new FormControl();
      this.medicinfieldsArray[lastIndex].medicin_noteFilteredOptions = this.medicinfieldsArray[lastIndex].medicin_noteControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("medicin_note",value)));
      //for medicin_note
      //for dosage
      this.medicinfieldsArray[lastIndex].dosageControl = new FormControl();
      this.medicinfieldsArray[lastIndex].dosageFilteredOptions = this.medicinfieldsArray[lastIndex].dosageControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("dosage",value)));
      //for dosage
       //for duration
      this.medicinfieldsArray[lastIndex].durationControl = new FormControl();
      this.medicinfieldsArray[lastIndex].durationFilteredOptions = this.medicinfieldsArray[lastIndex].durationControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("duration",value)));
      //for duration

      this.medicinNewAttribute = {};
    }
  }

  removeMedicin(index:any) {
    this.medicinfieldsArray.splice(index, 1);
  }

  addmoreTestPrescription(existingValues:any = ""){
    if(existingValues != ""){
      existingValues.forEach((element:any) => {
        this.testPresNewAttribute = {
          test_type:(this.isEditable)?element.test_master_id:element.test_type_name,
          test_name:element.test_name
        }
        this.testPrescriptionfieldsArray.push(this.testPresNewAttribute);
        const lastIndex = this.testPrescriptionfieldsArray.length-1 ;
        this.testPrescriptionfieldsArray[lastIndex].testnameControl = new FormControl();
        this.testPrescriptionfieldsArray[lastIndex].testFilteredOptions = this.testPrescriptionfieldsArray[lastIndex].testnameControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("testname",value)));
      });
    }else{
      this.testPresNewAttribute = {
        test_type:"",
        test_name:""
      }
      this.testPrescriptionfieldsArray.push(this.testPresNewAttribute);
      const lastIndex = this.testPrescriptionfieldsArray.length-1 ;
      this.testPrescriptionfieldsArray[lastIndex].testnameControl = new FormControl();
      this.testPrescriptionfieldsArray[lastIndex].testFilteredOptions = this.testPrescriptionfieldsArray[lastIndex].testnameControl.valueChanges.pipe(startWith(''),map((value:any) =>this.autoCompletefilter("testname",value)));

      this.testPresNewAttribute = {};
    }
  }

  removeTestPrescription(index:any) {
    this.testPrescriptionfieldsArray.splice(index, 1);
  }

  autoCompletefilter(suggestionType:any,value: string): string[] {
    const filterValue = value.toString().toLowerCase();
    if(suggestionType == "medicin"){
      return this.medicinSuggestions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }else if(suggestionType == "medicin_note"){
      return this.medicinnoteSuggestions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }else if(suggestionType == "dosage"){
      return this.dosageSuggestions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }else if(suggestionType == "duration"){
      return this.durationSuggestions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }else if(suggestionType == "testname"){
      return this.testnameSuggestions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
  }

  getProviderTokenHistory(tabIndex:any = ""){
    //clear selected first
    this.loading = true;
    if(this.page < 1){
      this.selectedHistory = [];
      this.selectedComplaint = [];
      this.isFullDataLoaded = false;
    }

    this.providerService.getProviderTokenHistory(
      this.selectedPatient,
      this.selectedProvider,
      this.selectedFromdate,
      this.selectedTodate,
      this.selectedPatientType,
      this.page,
      this.currentTokenId
    ).subscribe((response)=>{
      if(response.status_code == 200){
        if(this.page < 1){
          this.historyDateRange = [];
          this.allPrescriptionDetails = response.Data;
          this.allPrescriptionDetails.forEach((element:any) => {
            this.historyDateRange.push({"token_id":element.token_id,"booked_on":element.token_date,"default":false});
          });
          this.loadPrescriptionDetails(this.historyDateRange[0].token_id,tabIndex);
        }else{
          response.Data.forEach((element:any)=>{
            this.allPrescriptionDetails.push(element);
            this.historyDateRange.push({"token_id":element.token_id,"booked_on":element.token_date,"default":false});
          })
        }
      }else{
        if(this.page < 1){
          this.historyDateRange = [];
          this.allPrescriptionDetails = [];
          this.selectedPrescriptionDetails = [];
          this.snackbar.open(response.Metadata.Message, '', {
            duration: 2000,
          });
        }else{
          this.isFullDataLoaded = true;
          this.page = 0;
        }
      }
      this.loading = false;
    })
  }

  loadPrescriptionDetails(selectedId:any,tabIndex:any = ""){
    this.selectedPrescriptionDetails = this.allPrescriptionDetails.find((item:any)=>item.token_id == selectedId);
    //clear prescription data
    this.testPrescriptionfieldsArray = [];
    this.medicinfieldsArray = [];
    this.prescriptionRemarks = "";
    //updatem daterange active
    this.historyDateRange.forEach((element,index) => {
      this.historyDateRange[index].default = false;
    });
    this.historyDateRange.find((item:any)=>item.token_id == selectedId).default = true;

    this.selectedTokenId = selectedId;
    this.selectedHistory = [];
    this.selectedComplaint = [];
    this.historyNotes = "";
    this.complaintNotes = "";
    this.diagnosisNotes = "";
    // deside editable
    if(this.selectedPrescriptionDetails.allow_prescription_edit == "No"){
      this.isEditable = false;
    }else{
      this.isEditable = true;
    }
    //diagnosis data
    let diagnosis_details = this.selectedPrescriptionDetails.diagnosis_details;
    if(diagnosis_details != null){
      diagnosis_details.forEach((items:any) => {
        if(items.sub_entry_type == "H"){
          this.selectedHistory.push(items.complaint_text);
        }
        if(items.sub_entry_type == "C"){
          this.selectedComplaint.push(items.complaint_text);
        }
        if(items.sub_entry_type == "HN"){
          this.historyNotes = items.d_p_remarks;
        }
        if(items.sub_entry_type == "DN"){
          this.diagnosisNotes = items.d_p_remarks;
        }
        if(items.sub_entry_type == "CN"){
          this.complaintNotes = items.d_p_remarks;
        }
      });
    }
    //diagnosis data
    //prescription data
    let prescription_details = this.selectedPrescriptionDetails.prescription_details;
    if(prescription_details != null){
      this.addmoreMedicin(prescription_details);
    }
    this.prescription_images =  this.selectedPrescriptionDetails.prescription_images;
    //prescription data

    //test data
    let test_details = this.selectedPrescriptionDetails.test_details;
    if(test_details != null){
      this.addmoreTestPrescription(test_details);
    }
    //test data

    //get token details
    this.providerService.getTokenDetails(this.selectedTokenId).subscribe(response=>{
      if(response.status_code == 200){
        this.queue_date = response.Data[0].queue_date;
        this.booking_for_name = response.Data[0].booking_for_name;
        this.booking_for_user_id = response.Data[0].booking_for_user_id;
        this.provider_name = response.Data[0].provider_name;
        this.provider_id = response.Data[0].provider_details[0].provider_detail_id;
        this.provider_user_master_id = response.Data[0].provider_details[0].user_master_id;
        if(response.Data[0].followup_date != '' && response.Data[0].followup_date != undefined){
          this.pipe = new DatePipe('en-US');
          let selecteddate = response.Data[0].followup_date.split("-");
          let selectedYear = selecteddate[0];
          let selectedDay = selecteddate[2];
          let selectedDate: Date = new Date(response.Data[0].followup_date);
          let dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][selectedDate.getDay()];
          let monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][selectedDate.getUTCMonth()];
          // this.followupdate = dayName+", "+selectedDay+" "+monthName+" "+selectedYear;
          this.followupdate = selectedDay+" "+monthName+" "+selectedYear;
          this.formatedFollowupDate = this.pipe.transform(this.followupdate, 'dd-MM-yyyy');
        }
      }
    })

    //report details
    this.report_images =  this.selectedPrescriptionDetails.report_images;
    //report details
    this.matTabIndex = 0;
    if(tabIndex != ""){
      this.matTabIndex = tabIndex;
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.matTabIndex = tabChangeEvent.index
  }

  saveDPR(){
    this.tokenAddEditDiagnosisPrescription();
    /* if(this.matTabIndex == 0){
      this.tokenAddEditDiagnosis();
    }else if(this.matTabIndex == 1){
      this.tokenAddEditPrescription();
    } */
    // var diagnosisReturn:any = "";
    // diagnosisReturn = this.tokenAddEditDiagnosis();
    // var prescriptionReturn:any = "";
    // prescriptionReturn = this.tokenAddEditPrescription();
    // if(diagnosisReturn != "" && prescriptionReturn !=""){
    //   this.getProviderTokenHistory();
    // }
  }

  tokenAddEditDiagnosis(){
    const formData = new FormData();
    formData.append("tokenid",this.selectedTokenId);
    formData.append("consumerid",this.selectedPatient);
    formData.append("tokendate",this.queue_date);
    formData.append("tokentime",this.selectedPrescriptionDetails.token_time);
    //form history values
    var d_index:any = 0;
    if(this.selectedHistory.length > 0){
      this.selectedHistory.forEach((value)=>{
        formData.append("entry_type["+d_index+"]","D");
        formData.append("sub_entry_type["+d_index+"]","H");
        formData.append("remarks["+d_index+"]","");
        formData.append("complaint_text["+d_index+"]",value);
        d_index++;
      })
    }
    //for history notes
    formData.append("entry_type["+d_index+"]","D");
    formData.append("sub_entry_type["+d_index+"]","HN");
    formData.append("remarks["+d_index+"]",this.historyNotes);
    formData.append("complaint_text["+d_index+"]","");

    // form complaint values
    if(this.selectedComplaint.length > 0){
      d_index++;
      this.selectedComplaint.forEach((value)=>{
        formData.append("entry_type["+d_index+"]","D");
        formData.append("sub_entry_type["+d_index+"]","C");
        formData.append("remarks["+d_index+"]","");
        formData.append("complaint_text["+d_index+"]",value);
        d_index++;
      })
    }
    //for complaint notes
    formData.append("entry_type["+d_index+"]","D");
    formData.append("sub_entry_type["+d_index+"]","CN");
    formData.append("remarks["+d_index+"]",this.complaintNotes);
    formData.append("complaint_text["+d_index+"]","");
    //diagnosis notes
    d_index++;
    formData.append("entry_type["+d_index+"]","D");
    formData.append("sub_entry_type["+d_index+"]","DN");
    formData.append("remarks["+d_index+"]",this.diagnosisNotes);
    formData.append("complaint_text["+d_index+"]","");

    //save diagnosis
    this.providerService.tokenAddEditDiagnosis(formData).subscribe(response=>{
      this.snackbar.open(response.Metadata.Message,'',{duration:2000});
    });
  }

  tokenAddEditPrescription(){
    const formData = new FormData();
    formData.append("tokenid",this.selectedTokenId);
    formData.append("consumerid",this.selectedPatient);
    formData.append("tokendate",this.queue_date);
    formData.append("tokentime",this.selectedPrescriptionDetails.token_time);
    formData.append("remarks",this.prescriptionRemarks);
    //form medicin data
    if(this.medicinfieldsArray.length > 0){
      this.medicinfieldsArray.forEach((value,index)=>{
        formData.append("medicine_name["+index+"]",value.medicin);
        formData.append("medicine_qty["+index+"]","");
        formData.append("medicine_duration["+index+"]",value.duration);
        formData.append("medicine_frequency["+index+"]",value.dosage);
        formData.append("medicine_dispense["+index+"]",(value.dispanse)?'Yes':'No');
        formData.append("medicine_remark["+index+"]",value.medicin_note);
      });
    }

    //form test data
    if(this.testPrescriptionfieldsArray.length > 0){
      this.testPrescriptionfieldsArray.forEach((value,index)=>{
        formData.append("test_master_id["+index+"]",value.test_type);
        formData.append("test_name["+index+"]",value.test_name);
      });
    }

    this.providerService.tokenAddEditPrescription(formData).subscribe(response=>{
      this.snackbar.open(response.Metadata.Message,'',{duration:2000});
    });
  }

  tokenAddEditDiagnosisPrescription(){
    const formData = new FormData();
    formData.append("tokenid",this.selectedTokenId);
    formData.append("consumerid",this.selectedPatient);
    formData.append("tokendate",this.queue_date);
    formData.append("tokentime",this.selectedPrescriptionDetails.token_time);
    //form history values
    var d_index:any = 0;
    if(this.selectedHistory.length > 0){
      this.selectedHistory.forEach((value)=>{
        formData.append("entry_type["+d_index+"]","D");
        formData.append("diagnosis_sub_entry_type["+d_index+"]","H");
        formData.append("diagnosis_remarks["+d_index+"]","");
        formData.append("diagnosis_complaint_text["+d_index+"]",value);
        d_index++;
      })
    }
    //for history notes
    formData.append("entry_type["+d_index+"]","D");
    formData.append("diagnosis_sub_entry_type["+d_index+"]","HN");
    formData.append("diagnosis_remarks["+d_index+"]",this.historyNotes);
    formData.append("diagnosis_complaint_text["+d_index+"]","");

    // form complaint values
    if(this.selectedComplaint.length > 0){
      d_index++;
      this.selectedComplaint.forEach((value)=>{
        formData.append("entry_type["+d_index+"]","D");
        formData.append("diagnosis_sub_entry_type["+d_index+"]","C");
        formData.append("diagnosis_remarks["+d_index+"]","");
        formData.append("diagnosis_complaint_text["+d_index+"]",value);
        d_index++;
      })
    }
    //for complaint notes
    formData.append("entry_type["+d_index+"]","D");
    formData.append("diagnosis_sub_entry_type["+d_index+"]","CN");
    formData.append("diagnosis_remarks["+d_index+"]",this.complaintNotes);
    formData.append("diagnosis_complaint_text["+d_index+"]","");
    //diagnosis notes
    d_index++;
    formData.append("entry_type["+d_index+"]","D");
    formData.append("diagnosis_sub_entry_type["+d_index+"]","DN");
    formData.append("diagnosis_remarks["+d_index+"]",this.diagnosisNotes);
    formData.append("diagnosis_complaint_text["+d_index+"]","");


    /////prescription set
    formData.append("remarks",this.prescriptionRemarks);
    //form medicin data
    if(this.medicinfieldsArray.length > 0){
      this.medicinfieldsArray.forEach((value,index)=>{
        formData.append("medicine_name["+index+"]",value.medicin);
        formData.append("medicine_qty["+index+"]","");
        formData.append("medicine_duration["+index+"]",value.duration);
        formData.append("medicine_frequency["+index+"]",value.dosage);
        formData.append("medicine_dispense["+index+"]",(value.dispanse)?'Yes':'No');
        formData.append("medicine_remark["+index+"]",value.medicin_note);
      });
    }

    //form test data
    if(this.testPrescriptionfieldsArray.length > 0){
      this.testPrescriptionfieldsArray.forEach((value,index)=>{
        formData.append("test_master_id["+index+"]",value.test_type);
        formData.append("test_name["+index+"]",value.test_name);
      });
    }
    formData.append("followup_date",this.formatedFollowupDate);
    this.providerService.tokenAddEditDiagnosisPrescription(formData).subscribe(response=>{
      this.snackbar.open(response.Metadata.Message,'',{duration:2000});
      if(response.status_code == 200){
        this.getProviderTokenHistory();
        this.isDirty = false;
      }
    });
  }

  printPrescription(){
    this.patientservice.getPrescriptionInvoicePrintDataHtml(this.selectedTokenId,"P").subscribe((response)=>{
			if(response.status_code == "400"){
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}else{
        var printWin = window.open("","Print",'_self');
        printWin.moveTo(0,0);
        printWin.resizeTo(screen.availWidth, screen.availHeight);
				printWin.document.open();
				printWin.document.write(response.Data);
				printWin.print();
				printWin.close();
			}
		})
  }

  openFilesDialog(element:any,selector:any){
    var files = []
    element.map((value:any)=>{
      var temp = []
      temp['file'] = value.precription_report_image_path;
      if(selector == value.uploaded_precription_report_id){
        temp['status'] = "active";
      }else{
        temp['status'] = "inactive";
      }
      temp['extension'] = this.get_url_extension(value.precription_report_image_path);
      files.push(temp)
    })
    this.dialog.open(GalleryPreviewComponent,{
      data:{
        'files':files
      },
      width: '100vw',
      height:"600px",
      panelClass: 'galleryDialogWrapper'
    })
  }

  get_url_extension( url ) {
    return url.split(/[#?]/)[0].split('.').pop().trim();
  }

  deletePrescriptionReportImage(callFrom:any,id:any,name:any){
    this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you want to delete this file.').then((confirmed) => {
				if (confirmed) {
          this.providerService.deletePrescriptionReportImage(id,name).subscribe((response:any)=>{
            if(response.status_code == "200"){
              if(callFrom == "reports"){
                this.report_images = this.report_images.filter(function(obj) {
                    obj.report_images_data = obj.report_images_data.filter(function(inrobj){
                        return inrobj.uploaded_precription_report_id != id;
                    });
                    if(obj.report_images_data.length > 0){
                      return obj;
                    }else{
                      return {};
                    }
                });
              }else{
                this.prescription_images = this.prescription_images.filter(function(obj) {
                  return obj.uploaded_precription_report_id != id;
                });
              }
            }
            this.snackbar.open(response.Metadata.Message,'',{duration:2000});
          })
        }
    });
  }

  openFileUpload(fileFor:any){
    if(fileFor == "prescription"){
      this.dialog.open(FileUploadComponent,{
        data:{
          'token_id':this.selectedTokenId,
          'fileFor':fileFor
        },
        width: '50vw',
        disableClose: true
      }).afterClosed().subscribe(response => {
        if(response != "false"){
          this.providerService.getProviderTokenHistory(
            this.selectedPatient,
            this.selectedProvider,
            this.queue_date,
            this.queue_date,
            this.selectedPatientType,
            this.page,
            this.selectedTokenId
          ).subscribe((response)=>{
            if(response.status_code == 200){
              this.allPrescriptionDetails.map((item:any)=>{
                if(item.token_id == this.selectedTokenId){
                  item.prescription_images = response.Data[0].prescription_images;
                }
              });
              this.loadPrescriptionDetails(this.selectedTokenId,1);
            }
          });
          this.isDirty = false;
        }
      });
    }else if(fileFor =="report"){
      this.dialog.open(FileUploadComponent,{
        data:{
          'token_id':this.selectedTokenId,
          'test_types':this.testTypesDetails,
          'testnames':this.testnameSuggestions,
          'fileFor':fileFor
        },
        width: '50vw',
        disableClose: true
      }).afterClosed().subscribe(response => {
        if(response != "false"){
          this.providerService.getProviderTokenHistory(
            this.selectedPatient,
            this.selectedProvider,
            this.queue_date,
            this.queue_date,
            this.selectedPatientType,
            this.page,
            this.selectedTokenId
          ).subscribe((response)=>{
            if(response.status_code == 200){
              this.allPrescriptionDetails.map((item:any)=>{
                if(item.token_id == this.selectedTokenId){
                  item.report_images = response.Data[0].report_images;
                }
              });
              this.loadPrescriptionDetails(this.selectedTokenId,2);
            }
          });
          this.isDirty = false;
        }
      });
    }
  }

  back(){
    if(this.isDirty){
      if(confirm("It looks like you have been editing something. If you leave before saving, your changes will be lost.")){
        if(this.screenname == "home"){
          this.router.navigate(['/']);
        }else if(this.screenname == "Search Tokens"){
          this.router.navigate(['/searchtokens/']);
        }else if(this.screenname == "Ongoing Queues"){
          this.router.navigate(['/ongoingqueues/']);
        }else if(this.screenname == "Today's Queues"){
          this.router.navigate(['/todaysqueue/']);
        }else if(this.screenname == "Future Past Token"){
          this.router.navigate(['/futurepastqueues/']);
        }
      }else{
        return false;
      }
    }else{
      if(this.screenname == "home"){
        this.router.navigate(['/']);
      }else if(this.screenname == "Search Tokens"){
        this.router.navigate(['/searchtokens/']);
      }else if(this.screenname == "Ongoing Queues"){
        // this.router.navigate(['/ongoingqueues/']);
        this.router.navigate(['/ongoingqueues/'],{
          queryParams: {
            "backfrom":"prescription",
            "tokenid": this.tokenId,
            "screenname":this.screenname,
            "statusforrequestreshadule":this.statusforrequestreshadule,
            "ongoingprovider_name":this.ongoingprovider_name,
            "ongoingqueue_name":this.ongoingqueue_name,
            "queue_transaction_id":this.queue_transaction_id
          },skipLocationChange: true});
      }else if(this.screenname == "Today's Queues"){
        // this.router.navigate(['/todaysqueue/']);
        this.router.navigate(['/todaysqueue/'],{
          queryParams: {
            "backfrom":"prescription",
            "tokenid": this.tokenId,
            "screenname":this.screenname,
            "statusforrequestreshadule":this.statusforrequestreshadule,
            "ongoingprovider_name":this.ongoingprovider_name,
            "ongoingqueue_name":this.ongoingqueue_name,
            "queue_transaction_id":this.queue_transaction_id
          },skipLocationChange: true});
      }else if(this.screenname == "Future Past Token"){
        // this.router.navigate(['/futurepastqueues/']);
        this.router.navigate(['/todaysqueue/'],{
          queryParams: {
            "backfrom":"prescription",
            "tokenid": this.tokenId,
            "screenname":this.screenname,
            "statusforrequestreshadule":this.statusforrequestreshadule,
            "ongoingprovider_name":this.ongoingprovider_name,
            "ongoingqueue_name":this.ongoingqueue_name,
            "queue_transaction_id":this.queue_transaction_id,
            "futurepastmasterid":this.futurepastmasterid,
            "futurepastfromdate":this.futurepastfromdate,
            "futurepasttodate":this.futurepasttodate,
            "futurepastlastname":this.futurepastlastname,
            "futurepastfirstname":this.futurepastfirstname,
            "futurepastmiddle_name":this.futurepastmiddle_name,
            "futurepastprofileimage_path":this.futurepastprofileimage_path,
            "futurepastorg_location":this.futurepastorg_location,
            "fromdate": this.fromdate,
            "patient": this.patient,
            "todate": this.todate,
            "tokentypesearch": this.tokentypesearch,
            "searchpage": this.searchpage,
            "queue_type":this.queue_type,
            "todaysongoingprovidername":this.todaysongoingprovidername,
            "provider_name":this.provider_name
          },skipLocationChange: true});
      }
    }
  }

  onchangeFollowupdate(event:any = ""){
    this.pipe = new DatePipe('en-US');
    this.formatedFollowupDate = this.followupdate;
    if(event != ""){
      let selecteddate = event.formatted.split("-");
      let selectedDay = selecteddate[0];
      let selectedMonth = selecteddate[1];
      let selectedYear = selecteddate[2];
      let selectedDate: Date = new Date(selectedYear+"-"+selectedMonth+"-"+selectedDay);
      let dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][selectedDate.getDay()];
      let monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][selectedDate.getUTCMonth()];
      if(dayName != undefined){
        // this.followupdate = dayName+", "+selectedDay+" "+monthName+" "+selectedYear;
        this.followupdate = selectedDay+" "+monthName+" "+selectedYear;
      }else{
        this.followupdate = "";
      }
    }
    this.formatedFollowupDate = this.pipe.transform(this.followupdate, 'dd-MM-yyyy');
  }

  shareHistory(){
    var token_date = new Date(this.selectedPrescriptionDetails.token_date)
    token_date.setDate(token_date.getDate() + 1)
    var token_new_date = token_date.toJSON().slice(0,10).replace(/-/g,'-')
    this.dialog.open(ShareHistoryComponent,{
      data:{
        'token_id':this.selectedTokenId,
        'booking_for_name':this.booking_for_name,
        'provider_name':this.provider_name,
        'token_date':this.selectedPrescriptionDetails.token_date,
        'booking_for_id':this.booking_for_user_id,
        'provider_id':this.provider_user_master_id,
        'bookingfor':this.selectedPatientType
      },
      width: '50vw',
    })
  }

  changeDirty(){
    this.isDirty = true;
  }
}
