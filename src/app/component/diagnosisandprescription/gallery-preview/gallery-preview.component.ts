import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-gallery-preview',
  templateUrl: './gallery-preview.component.html',
  styleUrls: ['./gallery-preview.component.scss'],
  animations: [
    trigger("rotatedState", [
      state("0", style({ transform: "rotate(0)" })),
      state("90", style({ transform: "rotate(90deg)",margin:'70px 0px 0px 0px'  })),
      state("180", style({ transform: "rotate(180deg)"})),
      state("270", style({ transform: "rotate(270deg)" })),
      transition("rotated => default", animate("1500ms ease-out")),
      transition("default => rotated", animate("400ms ease-in"))
    ]),
    trigger("zoom",[
      state('0', style({overflow: 'scroll',height: '*',width: '300px',margin: '10px 0px 0px 0px'})),
      state('10', style({overflow: 'scroll',height: '*',width: '400px',margin: '20px 0px 0px 0px'})),
      state('20', style({overflow: 'scroll',height: '*',width: '500px',margin: '25px 0px 0px 0px'})),
      state('30', style({overflow: 'scroll',height: '*',width: '600px',margin: '30px 0px 0px 0px'})),
      state('40', style({overflow: 'scroll',height: '*',width: '700px',margin: '35px 0px 0px 0px'})),
      state('50', style({overflow: 'scroll',height: '*',width: '800px',margin: '40px 0px 0px 0px'})),
      state('60', style({overflow: 'scroll',height: '*',width: '900px',margin: '45px 0px 0px 0px'})),
      state('70', style({overflow: 'scroll',height: '*',width: '1000px',margin: '50px 0px 0px 0px'})),
      state('80', style({overflow: 'scroll',height: '*',width: '1100px',margin: '55px 0px 0px 0px'})),
      state('90', style({overflow: 'scroll',height: '*',width: '1200px',margin: '60px 0px 0px 0px'})),
      state('100', style({overflow: 'scroll',height: '*',width: '1300px',margin: '65px 0px 0px 0px'}))
    ])
  ]
})

export class GalleryPreviewComponent implements OnInit {
  files:any = [];
  btnPrev:boolean = true;
  btnNext:boolean = true;
  state: any = 0;
  zoomState:any =0;
  constructor(
    public dialogRef: MatDialogRef<GalleryPreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.files = this.data.files; 
   }

  ngOnInit() {
  }

  changeFile(action:String){
    var activeindex = this.files.findIndex(elem => elem.status ==="active");
    if(action == 'next'){
      if(typeof this.files[activeindex+1] != 'undefined') {
        this.state = 0;
        this.zoomState = 0;
        this.btnNext = true;
        this.files.map(function(elem) { 
          elem.status = true; 
          return elem
        });
        this.files[activeindex+1].status = 'active';
      }else{
        this.btnNext = false;
      }
      this.btnPrev = true;
    }else{
      if(typeof this.files[activeindex-1] != 'undefined') {
        this.state = 0;
        this.zoomState = 0;
        this.btnPrev = true;
        this.files.map(function(elem) { 
          elem.status = true; 
          return elem
        });
        this.files[activeindex-1].status = 'active';
      }else{
        this.btnPrev = false;
      }
      this.btnNext = true;
    }
  }

  rotate() {
    if (this.state == "270") {
      this.state = 0;
    } else {
      this.state = Number(this.state) + 90;
    }
  }

  zoom(action:any){
    if(action == 'in'){
      if(this.zoomState < 100){
        this.zoomState = Number(this.zoomState)+10;
      }
    }else{
      if(this.zoomState != 0){
        this.zoomState = Number(this.zoomState)-10;
      }
    }
  }

  close(){
    this.dialogRef.close()
  }
}
