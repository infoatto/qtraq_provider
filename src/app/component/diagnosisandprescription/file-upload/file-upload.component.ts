import { Component, OnInit, Inject, HostListener, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { ProvidersService } from 'app/_services/providers.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  filesArray: Array<any> = [];
  filesNewAttribute: any = {};
  uploadHeading:string = "";
  token_id:any ="";
  fileFor:any = ""
  testTypesDetails:any = "";
  testtype:any = "";
  loading:boolean = false;
  testname:any = new FormControl();
  testnameModel:any = "";
  testnameSuggestions:Array<any> = [];
  filteredTestname: Observable<string[]>;
  constructor(
    public dialogRef: MatDialogRef<FileUploadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private providerService:ProvidersService,
    private snackbar:MatSnackBar,
  ) { 
    this.token_id = data.token_id;
    this.fileFor  = data.fileFor
    if(data.fileFor == "prescription"){
      this.uploadHeading = "Upload Prescriptions";
    }else{
      this.uploadHeading = "Upload Reports";
      this.testTypesDetails = data.test_types;
      this.testnameSuggestions = data.testnames.filter(function (el) {
        return el != "";
      });
      this.filteredTestname = this.testname.valueChanges.pipe(startWith(''),map((value:any) => this.autocompleteFilter(value)));
    }
  }

  ngOnInit() {
    this.addMoreFiles();
  }

  private autocompleteFilter(value: string): string[] {
    const filterValue = this.normalizeValue(value);
    return this.testnameSuggestions.filter(street => this.normalizeValue(street).includes(filterValue));
  }

  private normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  addMoreFiles(){
    this.filesNewAttribute = {
      files:"",
      files_value:"",
      titles:"",
    }
    this.filesArray.push(this.filesNewAttribute);
    const lastIndex = this.filesArray.length-1 ;
    this.filesArray[lastIndex].filesControl = new FormControl();
    this.filesArray[lastIndex].titlesControl = new FormControl();
  }

  handleFileInput(files: FileList,index:any) {
    this.filesArray[index].files_value = files.item(0)
  }

  removeFiles(index:any){
    this.filesArray.splice(index, 1);
  }

  uploadFiles(){
    // this.loading = true;
    var formData = new FormData();
    this.filesArray.forEach((element,index) => {
        formData.append("image_title["+index+"]",element.titles);
        formData.append("precription_report_image["+index+"]",element.files_value);
    });
    if(this.fileFor == "prescription"){
      this.providerService.tokenUploadPrescription(this.token_id,formData).subscribe((response)=>{
        this.loading = false;
        this.snackbar.open(response.Metadata.Message, '', {
          duration: 2000,
        })
        setTimeout(() => {
          if(response.status_code == "200"){
            this.close('true');
          }
        }, 2000);
      });
    }else if(this.fileFor == "report"){
      var  test_type_name = "";
      this.testTypesDetails.map((el) => {
        if(this.testtype == el.test_master_id){
          test_type_name =  el.test_name;
        }
      });
      formData.append("testmasterid",this.testtype);
      formData.append("test_type_name",test_type_name);
      formData.append("testnames",this.testnameModel);
      formData.append("uploadedby","provider");
      this.providerService.tokenUploadTestReports(this.token_id,formData).subscribe((response)=>{
        this.loading = false;
        this.snackbar.open(response.Metadata.Message, '', {
          duration: 2000,
        })
        setTimeout(() => {
          if(response.status_code == "200"){
            this.close('true');
          }
        }, 2000);
      });
    }
  }


  close(response:any){
    this.dialogRef.close(response);
  }

}
