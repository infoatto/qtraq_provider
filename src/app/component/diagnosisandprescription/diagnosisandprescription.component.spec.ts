import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosisandprescriptionComponent } from './diagnosisandprescription.component';

describe('DiagnosisandprescriptionComponent', () => {
  let component: DiagnosisandprescriptionComponent;
  let fixture: ComponentFixture<DiagnosisandprescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnosisandprescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisandprescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
