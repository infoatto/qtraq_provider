import { Component, OnInit, Inject, HostListener, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProvidersService } from 'app/_services/providers.service';

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.scss']
})
export class PatientSearchComponent implements OnInit {
  patientList:any;
  providerid:any;
  fullDataLoaded:any;
  page:any = 1;
  searchKey:string = "";
  @ViewChild("patientWrapper") patientWrapper:ElementRef;
  constructor(
    public dialogRef: MatDialogRef<PatientSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private providerService:ProvidersService
  ) {
    this.patientList = data.patientList;
    this.providerid = data.providerid;
  }

  ngOnInit() {
  }

  // @HostListener('scroll', ['$event']) 
  searchPatient(searchFrom:any){
    let searchFlag= false;
    if(searchFrom != "scroll"){
      this.page = 0;
      searchFlag = true;
      this.fullDataLoaded=false;
    }else{
      // if(parseInt(this.patientWrapper.nativeElement.scrollTop) === (this.patientWrapper.nativeElement.scrollHeight - this.patientWrapper.nativeElement.offsetHeight) && !this.fullDataLoaded){
      if(!this.fullDataLoaded){
        this.page++;
        searchFlag = true;
      }
    }
    if(searchFlag){
      this.providerService.getShareHistoryProviderAppConsumers(this.page.toString(),this.providerid,this.providerid,"",this.searchKey).subscribe((response)=>{
        if(response.status_code == 200){
          if(searchFrom != "scroll"){
            this.patientList  = response.Data;
          }else{
            this.patientList  = this.patientList.concat(response.Data);
          }
        }else{
          this.fullDataLoaded = true;
        }
      });
    }
  }

  selectPatient(consumer_id:any,relation_type:any){
    if(consumer_id !=""){
      let selectedDetails = this.patientList.find(x => x.consumer_id === consumer_id && x.relation_type == relation_type);
      this.dialogRef.close(selectedDetails);
    }else{
      this.dialogRef.close();
    }
  }



}
