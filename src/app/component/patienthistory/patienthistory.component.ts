import {Component,OnInit,Renderer,ViewChild} from '@angular/core';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
import { PatienthistoryService } from './../../_services/patienthistory.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { MatDialogModule } from '@angular/material'
import { MatDialogConfig } from '@angular/material'

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SharetokenComponent } from './../../component/sharetoken/sharetoken.component'


@Component({
  selector: 'app-patienthistory-master',
  templateUrl: './patienthistory.component.html',
  styleUrls: ['./patienthistory.component.scss']
})
export class PatienthistoryComponent implements OnInit {
/*
  constructor() { }
  ngOnInit() {
  }
*/

//showformfield:any

gettokenlist:[any]
consumertype:any
consumertypeid:any
unsharehistorydata:any

sharehistorylist:any

ontestchangeid:any
/*fack data  3-5-2019 */
public lengthCheckToaddMore = 0;
public lengthCheckToReportaddMore = 0;
public patienthistoryaddmore:Boolean=false
public patientreportaddmore:Boolean=false
public showformfield:Boolean=true
public sharehistorydata:Boolean=false
//public TokenTestList:any
image_name:any
public tokentestlist  
preascription_title:any
//upload report
reportimg_title:any
report_imgdata:any
//upload report
myOptions: INgxMyDpOptions = {
dateFormat: 'dd-mm-yyyy',
};
//4-1-2019
//4-1-2019
public totalfiles: Array < File > = [];
public totalFileName = [];
public reportfiles:Array <File>=[]
public reportFileName = [];
public documentGrp: FormGroup;
public uploadReport:FormGroup;
/*fack end */
/*new code 3-4-2019 */
report_image_title:any
report_image:any
precription_image_title:any
precription_image:any
public reportimgupload: FormGroup;
public prescriptionupload:FormGroup;
token_details:any
diagnosis_details:any
prescription_details:any
test_details:any
prescription_images:any
report_images:any
uploadprescriptiondata:Boolean=false
uploadreportsdata:Boolean=false
/*new code end 3-4-2019 */
showpatienthistory:Boolean=true
showpatienthistoryone:Boolean=false
singletokendata:any
patienthistoryself:Boolean=false
fromdate:any
todateany:any
providerchange:any
patienthistorydata:any
ontypeselect:any
sharehistory:any

notification:any;
  totalRecords = "";
  page=0;
  patienthistoryProviders:any
  notificationfilter: any = {
 notificationtitle:"",
 notificationstatus:""
};

public getpriscription = 0;
org_id
size = 10;
getconsumerfor:any
dialogRef:any
tokendatagettokenid:any


constructor(private patientservice:PatienthistoryService,
  private router: Router,
  private snackbar:MatSnackBar,
  private formBuilder: FormBuilder,
  public dialog: MatDialog
) { }

ngOnInit() 
{

 // this.patienthistory("12")

this.newgetprovider()

/*
this.documentGrp = this.formBuilder.group({
  items: this.formBuilder.array([this.createUploadDocuments(this.org_image_id,this.image_name)])
})
*/

this.showformfield=true
//extra code start
this.documentGrp = 
this.formBuilder.group({documentFile: new FormControl(File),
items: this.formBuilder.array([this.createUploadDocuments(this.preascription_title,this.image_name)]),
});

this.uploadReport=
this.formBuilder.group({
  reports: this.formBuilder.array([this.createUploadReport(this.reportimg_title,this.report_imgdata)]),
  });
  


//extra code end

this.patienthistoryself=true
}



createUploadDocuments(preascription_title,image_name): FormGroup {
  return this.formBuilder.group({
    preascription_title:preascription_title,
  image_name:image_name,
   });
  }





 createUploadReport(reportimg_title,report_imgdata): FormGroup {
    return this.formBuilder.group({
    reportimg_title:reportimg_title,
    report_imgdata:report_imgdata,
  });
  }
  
  

get items(): FormArray {
return this.documentGrp.get('items') as FormArray;
};
 
   
get reports(): FormArray {
return this.uploadReport.get('reports') as FormArray;
};
 




addPrescription(): void 
{
this.items.insert(0, this.createUploadDocuments(this.preascription_title,this.image_name))
this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
}

addreports():void
{
this.reports.insert(0, this.createUploadReport(this.reportimg_title,this.report_imgdata))
this.lengthCheckToReportaddMore = this.lengthCheckToReportaddMore + 1;
}

removeItem(index: number) {
  this.totalfiles.splice(index);
  this.totalFileName.splice(index);
  this.items.removeAt(index);
  this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
 }

 removereports(index: number) 
 {
  this.reportfiles.splice(index);
  this.reportFileName.splice(index);
  this.reports.removeAt(index);
  this.lengthCheckToReportaddMore = this.lengthCheckToReportaddMore - 1;
 }


//delet image start 3-7-2019 
deleteImageprescription(report_id,report_image)
{
this.patientservice.deleteImageprescription(report_id,report_image)
.subscribe(
(response) => 
{
if(response.status_code==200)
{
//this.patienthistoryProviders=response.Data
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
//reload code 3-7-2019
this.patientservice.getTokenDPRDetails(this.tokendatagettokenid)
.subscribe(
(response) => 
{
if(response.status_code==200)
{
this.singletokendata = response.Data
this.token_details = response.Data.token_details
this.diagnosis_details = response.Data.diagnosis_details
this.prescription_details = response.Data.prescription_details
this.test_details = response.Data.test_details
this.prescription_images = response.Data.prescription_images
this.report_images = response.Data.report_images
}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
},);

//reload code end 3-7-2019

}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});

}
},
(err: any) => console.log("error",err),
() =>
console.log("getCustomersPage() retrieved customers for page:")
);
}

//patient delet image end 3-7-2019
patientfromdate(eventdate)
{
this.fromdate=eventdate.formatted
this.patienthistory(this.consumertypeid)
/*

if(((this.consumertypeid!=null)&&(this.consumertypeid!=undefined))&&
((this.providerchange!=null)&&(this.providerchange!=undefined))
&&((this.fromdate!=null)&&(this.fromdate!=undefined))&&
((this.todateany!=null)&&(this.todateany!=undefined))
)
{

this.patienthistory(this.consumertypeid)


}
else{

if((this.consumertypeid!=null)&&(this.consumertypeid!=undefined))
{

  this.snackbar.open("Select Consumers",'', {
    duration: 2000,
    });
    

}else if((this.providerchange!=null)&&(this.providerchange!=undefined))
{


  this.snackbar.open("please Select Provider",'', {
    duration: 2000,
    });
    


}else if((this.fromdate!=null)&&(this.fromdate!=undefined)){


  this.snackbar.open("please select from date",'', {
    duration: 2000,
    });
    

}else{

  this.snackbar.open("please seleact to date",'', {
    duration: 2000,
    });
    

}
*/
}



patienttodate(eventdate)
{
this.todateany=eventdate.formatted

this.patienthistory(this.consumertypeid)

}

/*
ontypeChange(changetype)
{

if(changetype=="Other")
{
this.patienthistoryself=false
}
else
{
  this.patienthistoryself=true
}


this.getconsumerfor = changetype

this.patientservice.getShareHistoryProviderAppProviders(changetype)
.subscribe(
(response) => 
{
if(response.status_code==200)
{
this.patienthistoryProviders=response.Data
}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
},
(err: any) => console.log("error",err),
() =>
console.log("getCustomersPage() retrieved customers for page:")
);




}
*/



onProviderchange(changeid)
{




this.providerchange = changeid
this.patientservice.getShareHistoryProviderAppConsumers(this.getconsumerfor,changeid)
.subscribe(
(response) => 
{

if(response.status_code==200)
{
this.sharehistory=response.Data


this.patienthistory(this.consumertypeid)


}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
  else 
{

  this.sharehistory=null

this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:")
  );
}


patienthistory(consumer_id)
{
//dont need start




if(this.sharehistory!=undefined)
{
if((this.sharehistory.length>0))
{
for(var i=0;i<this.sharehistory.length;i++)
{
  if(this.sharehistory[i]['consumer_id']==consumer_id)
{
  this.consumertype=this.sharehistory[i]['relation_type']
}
}
}
}
this.consumertypeid=consumer_id


//this.getconsumerfor="Self"
//this.getconsumerfor="Self"
//this.getconsumerfor="Self"
this.fromdate=this.fromdate
this.todateany=this.todateany
//this.providerchange="6"
//dont need end
//if(this.getconsumerfor=="Self")
//{




 this.patientservice.getShareHistoryProviderAppConsumersself(
 this.fromdate,
 this.todateany,
 this.providerchange,
 consumer_id,
 this.consumertype
 )
.subscribe(
(response) => 
{
if(response.status_code==200)
{
this.patienthistorydata=response.Data
this.sharehistorydata=true
}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});

this.patienthistorydata=null
this.sharehistorydata=false
}
},);

//else
//{


/*
this.patientservice.getShareHistoryProviderAppConsumersother(this.providerchange,
  consumer_id)
  .subscribe(
  (response) => 
  {
if(response.status_code==200)
{

this.sharehistorydata = true  
this.patienthistorydata=response.Data

}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  },);

*/
//}


}


tokendataget(token_id)
{

this.tokendatagettokenid=token_id
this.showpatienthistory=false
this.showpatienthistoryone=true
this.patientservice.getTokenDPRDetails(token_id)
.subscribe(
(response) => 
{
if(response.status_code==200)
{
this.singletokendata = response.Data
this.token_details = response.Data.token_details
this.diagnosis_details = response.Data.diagnosis_details
this.prescription_details = response.Data.prescription_details
this.test_details = response.Data.test_details
this.prescription_images = response.Data.prescription_images
this.report_images = response.Data.report_images




}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}
else 
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
},);
//report dropdown end 6-3-2019
}


/* ravi new code */
public fileSelectionEvent(fileInput: any, oldIndex) {
  if (fileInput.target.files && fileInput.target.files[0]) {
   var reader = new FileReader();
   reader.onload = (event: any) => {}
   if (oldIndex == 0) {
    this.totalfiles.unshift((fileInput.target.files[0]))
    this.totalFileName.unshift(fileInput.target.files[0].name)
   } else {
   // let main_form: FormData = new FormData();
    this.totalfiles[oldIndex] = (fileInput.target.files[0]);
if(this.org_id!=0)
{
    this.totalFileName[oldIndex-1] = fileInput.target.files[0].name
}
else
{
    this.totalFileName[oldIndex] = fileInput.target.files[0].name
}
}
reader.readAsDataURL(fileInput.target.files[0]);
}
if (this.totalfiles.length == 1) {
this.lengthCheckToaddMore = 1;
}


}



public fileselectreport(fileInput: any, oldIndex) {
  if (fileInput.target.files && fileInput.target.files[0]) {
   var reader = new FileReader();
   reader.onload = (event: any) => {}
   if (oldIndex == 0) {
    this.reportfiles.unshift((fileInput.target.files[0]))
    this.reportFileName.unshift(fileInput.target.files[0].name)
   } else {
   // let main_form: FormData = new FormData();
    this.reportfiles[oldIndex] = (fileInput.target.files[0]);
if(this.org_id!=0)
{
    this.reportFileName[oldIndex-1] = fileInput.target.files[0].name
}
else
{
    this.reportFileName[oldIndex] = fileInput.target.files[0].name
}
}
reader.readAsDataURL(fileInput.target.files[0]);
}
if (this.reportfiles.length == 1) {
this.lengthCheckToReportaddMore = 1;
}

}



public uploadprescription(formValue: any,token_id) 
{
let main_form: FormData = new FormData();
for (let j = 0; j < this.items.length; j++) {
main_form.append("image_title["+j+"]", formValue.items[j]['preascription_title'])
if(< File > this.totalfiles[j])
{
main_form.append("precription_report_image["+j+"]",< File > this.totalfiles[j])
}  
}
   this.patientservice.getuploadprescription(main_form,token_id)
  .subscribe(
 (response) => 
 {
 if(response.status_code==200)
 {

  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
 
}
 else if(response.status_code==402)
 {
 localStorage.clear();
 this.router.navigate(['/login']);
 }
 else 
 {
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 }
 },);
}

ontestchange(testid)
{
this.ontestchangeid = testid
}

showprescriptionform(tokenid)
{
this.showformfield=false
this.patienthistoryaddmore=true
}

showreportform(tokenid)
{
  this.patientservice.getTokenTestList(tokenid)
  .subscribe(
  (response) => 
  {
  if(response.status_code==200)
  {
  this.tokentestlist = response.Data
  }
  else if(response.status_code==402)
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else 
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  },);
this.showformfield=false
this.patientreportaddmore=true
}


public uploadreport(formValue: any,token_id,testname)
{

let main_form: FormData = new FormData();
for (let k = 0; k < this.reports.length; k++) 
{
   main_form.append("image_title["+k+"]", formValue.reports[k]['reportimg_title'])
   if(< File > this.reportfiles[k])
    {
    main_form.append("precription_report_image["+k+"]",< File > this.reportfiles[k])
    }  
  }
 this.patientservice.uploadreport(main_form,token_id,this.ontestchangeid,testname.value)
 .subscribe(
(response) => 
{
if(response.status_code==200)
{
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
 else if(response.status_code==402)
{
 localStorage.clear();
 this.router.navigate(['/login']);
}
 else 
{
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
}
},);


}

shartokencall()
{


const config = new MatDialogConfig();
config.data = { fromdate: this.fromdate,todate:this.todateany,consumertype:this.consumertype,consumer_id:this.consumertypeid};
this.dialogRef=this.dialog.open(SharetokenComponent,config);
this.dialogRef.afterClosed().subscribe(value => {
//ravi new code 3-8-2019





let main_form: FormData = new FormData();


for (let k = 0; k < this.patienthistorydata.length; k++) 
{
main_form.append("tokenid["+k+"]",this.patienthistorydata[k].token_id)
}


this.patientservice.shartoken(this.providerchange,this.consumertypeid,main_form,value.diagnosis,
value.fromdate,value.todate,value.prescription,value.report,
value.user_master_id,value.bookingtype,value.bookingforid)
 .subscribe(
(response) => 
{
if(response.status_code==200)
{

  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
 else if(response.status_code==402)
{
 localStorage.clear();
 this.router.navigate(['/login']);
}
 else 
{
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
}
},);

//ravi new code end 3-8-2019
});


}


unsharehistory()
{
this.unsharehistorylist(1)
this.showformfield=false

this.unsharehistorydata=true
}

unsharehistorylist(page: number)
{
this.patientservice.unsharehistory(page - 1).subscribe((response) => 
{
  
if(response.status_code==200)
{

  this.sharehistorylist=response.Data

    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      });
  }
   else if(response.status_code==402)
  {
   localStorage.clear();
   this.router.navigate(['/login']);
  }
   else 
  {
   this.snackbar.open(response.Metadata.Message,'', {
   duration: 2000,
   });
  }
  });
  
  //ravi new code end 3-8-2019
}

pageChanged(event) 
{
this.unsharehistorylist(event);
}

unshareclick(historyid)
{

  this.patientservice.unsharehistorydata(historyid).subscribe((response) => 
  {
   
  if(response.status_code==200)
  {
  
    this.showformfield=true

this.unsharehistorydata=false
    //this.sharehistorylist=response.Data
    
    this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
        });
    
        this.router.navigate(['/patienthistory']);
      }
     else if(response.status_code==402)
    {
     localStorage.clear();
     this.router.navigate(['/login']);
    }
     else 
    {
     this.snackbar.open(response.Metadata.Message,'', {
     duration: 2000,
     });
    }
    });
}



newgetprovider(){


  this.patientservice.getShareHistoryProviderAppProviders()
  .subscribe(
  (response) => 
  {
  if(response.status_code==200)
  {
  this.patienthistoryProviders=response.Data



  }
  else if(response.status_code==402)
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else 
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:")
  );
  
}





}
