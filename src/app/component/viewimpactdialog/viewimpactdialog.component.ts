import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from '../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ AssignnewtokenComponent } from '../assignnewtoken/assignnewtoken.component'
import { AssignnewtokenService } from '../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators,FormArray,ReactiveFormsModule,FormControl } from '@angular/forms';



@Component({
  selector: 'viewimpactdialog',
  templateUrl: './viewimpactdialog.component.html',
  styleUrls: ['./viewimpactdialog.component.scss']
})
export class ViewimpactdialogComponent implements OnInit {
  sp1:any
  sp2:any

  response:any
  getdata:any
  responsecount:any
  //constructor(private notificationservice:NotificationService) { }
  constructor(
    public dialogRef: MatDialogRef<AssignnewtokenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private assignnewtoken:AssignnewtokenService,
    private snackbar:MatSnackBar,private router: Router,private formBuilder: FormBuilder
  ){
    this.response=this.data.tokendate
  }

  ngOnInit(){
    this.getdata=this.response.Data.impacted_queues
    this.responsecount=this.response.Data.total_count
    this.sp1="(";
    this.sp2=")";
  }


  close() {
    this.dialogRef.close();
  }

}
