
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewimpactdialogComponent } from './viewimpactdialog.component';

describe('ViewimpactdialogComponent', () => {
  let component: ViewimpactdialogComponent;
  let fixture: ComponentFixture<ViewimpactdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewimpactdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewimpactdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

