import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrglistchangeComponent } from './orglistchange.component';

describe('OrglistchangeComponent', () => {
  let component: OrglistchangeComponent;
  let fixture: ComponentFixture<OrglistchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrglistchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrglistchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
