

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TransactionqueuesummaryComponent } from './transactionqueuesummary.component';

describe('TransactionqueuesummaryComponent', () => {
  let component: TransactionqueuesummaryComponent;
  let fixture: ComponentFixture<TransactionqueuesummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionqueuesummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionqueuesummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

