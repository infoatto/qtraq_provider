import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from '../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ AssignnewtokenComponent } from '../assignnewtoken/assignnewtoken.component'
import { AssignnewtokenService } from '../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators,FormArray,ReactiveFormsModule,FormControl } from '@angular/forms';
export interface queueinfo{
  queue_date:any;
  queue_name:any;
}
export interface transactionqueuesummary{
  cancelled_tokens:any;
  completed_tokens:any;
  consultation_pending:any;
  custom_field_total:any;
  custom_fields:any;
  issued_tokens:any;
  no_show_tokens:any;
  pending_tokens:any;
  amt_type_tot_array:any;
  queue_info:queueinfo;
}
@Component({
  selector: 'transactionqueuesummary',
  templateUrl: './transactionqueuesummary.component.html',
  styleUrls: ['./transactionqueuesummary.component.scss']
})
export class TransactionqueuesummaryComponent implements OnInit {
  queue_transaction_id:any
  transactionqueuesummary:transactionqueuesummary;
  customfield:any
  queuename:any
  dateRangeStart:any
  dateRangeEnd:any
  transactionsummarykey:any
  transactionsummaryvalue:any
  providerid: any;
  providerorgid: any;
  queue_start_date: any;
  queue_end_date:any;
  constructor(public dialogRef: MatDialogRef<AssignnewtokenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private assignnewtoken:AssignnewtokenService,
    private snackbar:MatSnackBar,private router: Router,private formBuilder: FormBuilder
  ){
    if(this.data.callfrom == "singlequeue"){
      this.queue_transaction_id=this.data.queue_transaction_id
      this.queuename=this.data.queuename
      this.dateRangeStart = this.data.queuedate
      this.dateRangeEnd = this.data.queuedate
      this.singleQueueSummary();
    }else{
      this.providerid = this.data.providerid;
      this.providerorgid = this.data.providerorgid;
      this.queue_start_date = this.data.queue_start_date;
      this.queue_end_date = this.data.queue_end_date ;
      this.dateRangeStart = new Date(this.data.queue_start_date).toString().substr(0, 15);
      this.dateRangeEnd = new Date(this.data.queue_end_date).toString().substr(0, 15);
      this.daterangeQueueSummary();

    }
  }

  ngOnInit(){

  }

  singleQueueSummary(){
    this.assignnewtoken.TransactionqueuesummaryComponent(this.queue_transaction_id).subscribe((response) =>{
      if(response.status_code=='200'){
        this.transactionqueuesummary=response.Data
        if(this.transactionqueuesummary.amt_type_tot_array != undefined){
          this.transactionsummarykey=Object.keys(this.transactionqueuesummary.amt_type_tot_array)
          this.transactionsummaryvalue=objectValues(this.transactionqueuesummary.amt_type_tot_array)
        }
      }else if(response.status_code=='400'){
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
    function objectValues(obj) {
      let vals = [];
      for (const prop in obj) {
          vals.push(obj[prop]);
      }
      return vals;
    }
  }

  daterangeQueueSummary(){
    this.assignnewtoken.daterangeQueueSummary(this.providerid,this.providerorgid,this.queue_start_date,this.queue_end_date).subscribe((response:any) =>{
      if(response.status_code=='200'){
        this.transactionqueuesummary = response.Data;
        if(this.transactionqueuesummary.amt_type_tot_array != undefined){
          this.transactionsummarykey = Object.keys(this.transactionqueuesummary.amt_type_tot_array);
          this.transactionsummaryvalue = objectValues(this.transactionqueuesummary.amt_type_tot_array);
        }
      }else if(response.status_code=='400'){
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    })
    function objectValues(obj) {
      let vals = [];
      for (const prop in obj) {
          vals.push(obj[prop]);
      }
      return vals;
    }
  }

  close(){
    this.dialogRef.close();
  }
}
