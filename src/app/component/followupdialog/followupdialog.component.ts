import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TodaysqueueService } from 'app/_services/todaysqueue.service';
import { DatePipe } from '@angular/common';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { MatSnackBar } from '@angular/material';
export interface DialogData {
  providerid:any;
  providerorgid:any;
  followupdate:any;
}
@Component({
  selector: 'app-followupdialog',
  templateUrl: './followupdialog.component.html',
  styleUrls: ['./followupdialog.component.scss']
})
export class FollowupdialogComponent implements OnInit {
  queueDate:any;
  providerid:any;
  providerorgid:any;
  followupdate:any;
  pipe: any;
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate()-1;
  currentMonth: any = this.mytime.getUTCMonth() + 1;
  myOptionsassign:INgxMyDpOptions ={
    dateFormat: 'dd-mm-yyyy',
  }
  followupTokens:any;
  constructor(
    public dialogRef: MatDialogRef<FollowupdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private todayQueueService:TodaysqueueService,
    private snackbar:MatSnackBar
  ) { 
    let actorganistion = JSON.parse(localStorage.getItem('orguserrole'));
	  this.providerorgid = actorganistion.orgid;
    this.followupdate = data.followupdate;
    this.providerid = data.providerid;
    this.onchangeFollowupdate();
    
  }

  ngOnInit() {
    
  }

  onchangeFollowupdate(event:any = ""){
    
    this.pipe = new DatePipe('en-US');
    let formatedFollowupDate = this.followupdate;
    if(event != ""){
      let selecteddate = event.formatted.split("-");
      let selectedDay = selecteddate[0];
      let selectedMonth = selecteddate[1];
      let selectedYear = selecteddate[2];
      let selectedDate: Date = new Date(selectedYear+"-"+selectedMonth+"-"+selectedDay);
      let dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][selectedDate.getDay()];
      let monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][selectedDate.getUTCMonth()];
      this.followupdate = dayName+", "+selectedDay+" "+monthName+" "+selectedYear;
    }
    formatedFollowupDate = this.pipe.transform(this.followupdate, 'dd-MM-yyyy');
    this.todayQueueService.getFollowUpTokens(this.providerid,this.providerorgid,formatedFollowupDate).subscribe((response:any)=>{
      if(response.status_code == 200){
        this.followupTokens = response.Data;
      }else{
        this.followupTokens = "";
        // this.snackbar.open(response.Metadata.Message, '', {
        //   duration: 2000,
        //   verticalPosition: 'top'
        // })
      }
    })
  }

  onNoClick(selectedTokenIndex:any = "",action:any=""): void {
    let data:any = {};
    if(selectedTokenIndex >= 0){
      if(this.followupTokens.length > 0){
        if(this.followupTokens[selectedTokenIndex].member_type == "non_member"){
          data={
            'fullname':this.followupTokens[selectedTokenIndex].non_mem_name,
            'mobilenumber':this.followupTokens[selectedTokenIndex].non_mem_phone,
            "bookingtypeselectedtype":this.followupTokens[selectedTokenIndex].member_type,
            "mobilesearchmasterid":this.followupTokens[selectedTokenIndex].user_master_id,
            "selectedproid":this.providerid,
            "familymemberid":this.followupTokens[selectedTokenIndex].family_member_id,
            "profileimg":this.followupTokens[selectedTokenIndex].profile_pic,
            "reg_no":this.followupTokens[selectedTokenIndex].reg_no,
            "age":this.followupTokens[selectedTokenIndex].non_mem_age,
            "gender":this.followupTokens[selectedTokenIndex].non_mem_gender,
            "last_visited":this.followupTokens[selectedTokenIndex].last_visited,
            "token_id":this.followupTokens[selectedTokenIndex].token_id,
            "action":action
          }
        }else{
          data={
            'fullname':this.followupTokens[selectedTokenIndex].full_name,
            'mobilenumber':this.followupTokens[selectedTokenIndex].user_name,
            "bookingtypeselectedtype":this.followupTokens[selectedTokenIndex].member_type,
            "mobilesearchmasterid":this.followupTokens[selectedTokenIndex].user_master_id,
            "selectedproid":this.providerid,
            "familymemberid":this.followupTokens[selectedTokenIndex].family_member_id,
            "profileimg":this.followupTokens[selectedTokenIndex].profile_pic,
            "reg_no":this.followupTokens[selectedTokenIndex].reg_no,
            "age":this.followupTokens[selectedTokenIndex].age,
            "gender":this.followupTokens[selectedTokenIndex].gender,
            "last_visited":this.followupTokens[selectedTokenIndex].last_visited,
            "token_id":this.followupTokens[selectedTokenIndex].token_id,
            "action":action
          }
        }
      }
    }
    this.dialogRef.close(data);
  }

  dialogClose(){
    this.dialogRef.close("");
  }
}


