import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowupdialogComponent } from './followupdialog.component';

describe('FollowupdialogComponent', () => {
  let component: FollowupdialogComponent;
  let fixture: ComponentFixture<FollowupdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowupdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowupdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
