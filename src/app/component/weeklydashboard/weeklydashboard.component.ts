import { Component, OnInit } from '@angular/core';
import { WeeklydashboardService } from './../../_services/weeklydashboard.service';
import {MatSnackBar} from '@angular/material';
import { MatDialogModule } from '@angular/material'
import { MatDialogConfig } from '@angular/material'
import { DatePipe } from '@angular/common';
import{ DialogclinicchangeComponent } from './../../component/dialogclinicchange/dialogclinicchange.component'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { ProvidersService } from 'app/_services/providers.service';
import { TransactionqueuesummaryComponent } from '../transactionqueuesummary/transactionqueuesummary.component';

@Component({
  selector: 'app-weeklydashboard-master',
  templateUrl: './weeklydashboard.component.html',
  styleUrls: ['./weeklydashboard.component.scss']
})
export class WeeklydashboardComponent implements OnInit {
  width:any;
  arraydata:any;
  arraydatayear:any;
  pipe:any;
  weeaklydashboarddatatotle:any;
  organisationid:any;
  initialdates:any;
  weeaklydashboarddata:any;
  yearlydashboarddata:any;
  yearlydashboarddatatotle:any;
  calendertype:any;
  messages=[];
  notification:any;
  totalRecords = "";
  page=0;
  date: Date;
  date1: Date;
  getdate:any;
  quedata:any;
  keys: String[];
  dialogRef:any;
  org_id:any;
  dashboardtype:any;
  monthlycalandershow:Boolean=true;
  yearlycalandershow:Boolean=false;
  showmonthselect:Boolean=true;
  totalmonthname:any;
  currentselectedmonthcount:any;
  currentselectedyearcount:any
  currentselectedmonth:any;
  currentdatecount:any;
  selectedmonth:any;
  seleactedbydefault:any;
  getProviderOrgs:any;
  selectedyear:any;
  notificationfilter: any = {
    notificationtitle:"",
    notificationstatus:""
  };
  currentdate:any;
  size = 10;
  provider_id:any;
  providersDetails:any;
  //selectbox models
  providerSelectModel:any;
  monthSelectModel:any;
  yearSelectModel:any;
  typeSelectModel:any;
  orgSelectModel:any;
  constructor(
    private weeklydashboardservice:WeeklydashboardService,
    private providerService:ProvidersService,
    public dialog: MatDialog, private router: Router,private snackbar:MatSnackBar
  ){

  }
  ngOnInit() {
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    //this.organisationid = 3
    this.organisationid = orguserlist.orgid;
    this.provider_id = orguserlist.orgid;
    var totalmonthname=[];
    totalmonthname.push("January");
    totalmonthname.push("February");
    totalmonthname.push("March");
    totalmonthname.push("April");
    totalmonthname.push("May");
    totalmonthname.push("June");
    totalmonthname.push("July");
    totalmonthname.push("August");
    totalmonthname.push("September");
    totalmonthname.push("October");
    totalmonthname.push("November");
    totalmonthname.push("December");
    this.totalmonthname=totalmonthname;
    var date = new Date();
    var arraydata=[];
    var arraydatayear=[];
    var newdate = new Date(date);
    var dd = newdate.getDate();
    var mm = newdate.getMonth();
    var y = newdate.getFullYear();
    this.initialdates=this.getDaysInMonth(mm, y);
    this.currentdate= this.pipe.transform(date, 'dd-MM-yyyy');
    this.currentdatecount= this.pipe.transform(date, 'dd_MM_yyyy');
    this.currentselectedmonth=this.pipe.transform(newdate, 'MMMM');
    this.currentselectedmonthcount=this.pipe.transform(newdate, 'M');
    this.currentselectedyearcount=this.pipe.transform(newdate, 'yyyy');
    this.calendertype='Monthly'
    this.selectedmonth=mm+1
    this.seleactedbydefault=mm
    this.monthlycalandershow=true
    this.yearlycalandershow=false
    let adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let user_master_id = atob(adminuser.user_master_id);
    this.provider_id = atob(adminuser.user_master_id);
    //get providers list
    this.providerService.checkOrganisationslist().subscribe((response) =>{
      if(response.status_code=="200"){
        this.providersDetails = response.Data;
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
    this.weeklydashboardservice.getProviderWorkingCalender(mm+1,y,this.calendertype,this.organisationid,this.provider_id).subscribe((response) =>{
      if(response.status_code=="200"){
        response.Data.queue_name.map((elem:any)=>{
          elem.date_data.map((dateElem:any)=>{
            var dateArray = dateElem.date.split("_");
            var checkDate = new Date(dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0]);
            checkDate.setDate(checkDate.getDate() +1)
            var currentdate = new Date();
            dateElem.checkcurrent = "";
            if(checkDate < currentdate){
              dateElem.checkcurrent = false;
            }else{
              dateElem.checkcurrent = true;
            }
            return dateElem;
          });
          return elem;
        });
        this.weeaklydashboarddata=response.Data.queue_name
        this.weeaklydashboarddatatotle=response.Data.total_count_array
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.monthlycalandershow = false;
        this.yearlycalandershow = false;
      }
    });
    this.weeklydashboardservice.getProviderOrgs(this.provider_id).subscribe((response) => {
      if(response.status_code=="200"){
        this.getProviderOrgs=response.Data
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });

    this.weeklydashboardservice.getProviderOrgMinMaxBookingDates(this.organisationid).subscribe((response) => {
      if(response.status_code=="200"){
        response.Data.min_month = 1;
        response.Data.max_month = 12;
        for(var j=response.Data.min_month-1;j<response.Data.max_month;j++){
          if(j==0){
            arraydata.push({
              key:   j,
              value: "January"
            });
          }else if(j==1){
            arraydata.push({
              key:   j,
              value: "February"
            });
          }else if(j==2){
            arraydata.push({
              key:   j,
              value: "March"
            });
          }else if(j==3){
            arraydata.push({
              key:   j,
              value: "April"
            });
          }else if(j==4){
            arraydata.push({
              key:   j,
              value: "May"
            });
          }else if(j==5){
            arraydata.push({
              key:   j,
              value: "June"
            });
          }else if(j==6){
            arraydata.push({
              key:   j,
              value: "July"
            });
          }else if(j==7){
            arraydata.push({
              key:   j,
              value: "August"
            });
          }else if(j==8){
            arraydata.push({
              key:   j,
              value: "September"
            });
          }else if(j==9){
            arraydata.push({
              key:   j,
              value: "October"
            });
          }else if(j==10){
            arraydata.push({
              key:   j,
              value: "November"
            });
          }else if(j==11){
            arraydata.push({
              key:   j,
              value: "December"
            });
          }
        }
        this.arraydata=arraydata;
        for(var k=response.Data.min_year;k<=response.Data.max_year;k++){
          arraydatayear.push({
            key:"year"+k,
            value:Number(k)
          });
        }
        this.arraydatayear=arraydatayear;
        this.selectedyear=y;
      }else{
      }
    })
  }


  selectProvider(selectedId:any){
    this.provider_id = selectedId;
    this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,this.calendertype,this.organisationid,this.provider_id).subscribe((response) =>{
      if(response.status_code=="200"){
        if(this.calendertype=="Monthly"){
          response.Data.queue_name.map((elem:any)=>{
            elem.date_data.map((dateElem:any)=>{
              var dateArray = dateElem.date.split("_");
              var checkDate = new Date(dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0]);
              checkDate.setDate(checkDate.getDate() +1)
              var currentdate = new Date();
              dateElem.checkcurrent = "";
              if(checkDate < currentdate){
                dateElem.checkcurrent = false;
              }else{
                dateElem.checkcurrent = true;
              }
              return dateElem;
            });
            return elem;
          });
          this.weeaklydashboarddata=response.Data.queue_name
          this.weeaklydashboarddatatotle=response.Data.total_count_array
        }else{
          this.yearlydashboarddata=response.Data.queue_name
          this.yearlydashboarddatatotle=response.Data.total_count_array
        }
        if(this.calendertype=="Monthly"){
          this.yearlycalandershow=false
          this.showmonthselect=true
          this.monthlycalandershow=true
        }else{
          this.monthlycalandershow=false
          this.yearlycalandershow=true
          this.showmonthselect=false
        }
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.monthlycalandershow = false;
        this.yearlycalandershow = false;
      }
    })

  }


  getDaysInMonth(month, year) {
    var date = new Date(year, month, 1);
    var days = [];
    while (date.getMonth() === month) {
      this.pipe = new DatePipe('en-US');
      days.push(this.pipe.transform(date, 'dd-MM-yyyy'));
      date.setDate(date.getDate() + 1);
    }
    return days;
  }

  getclinic(){
    const config = new MatDialogConfig();
    this.dialogRef=this.dialog.open(DialogclinicchangeComponent,config);
    this.dialogRef.afterClosed().subscribe((value:any) => {
      this.organisationid=value.orgid;
      this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,this.calendertype,this.organisationid,this.provider_id).subscribe((response) => {
        if(response.status_code=="200"){
        //this.calendertype=dashboardtype
          if(this.calendertype=="Monthly"){
            this.yearlycalandershow=false
            this.showmonthselect=true
            this.monthlycalandershow=true
          }else{
            this.monthlycalandershow=false
            this.yearlycalandershow=true
            this.showmonthselect=false
          }
          this.yearlydashboarddata=response.Data.queue_name
          this.yearlydashboarddatatotle=response.Data.total_count_array
        }else if(response.status_code=="402"){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
          this.monthlycalandershow = false;
          this.yearlycalandershow = false;
        }
      })
    });
  }

  getdashboardtype(dashboardtype){
    this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,dashboardtype,this.organisationid,this.provider_id)
    .subscribe(
    (response) =>
    {
    if(response.status_code=="200")
    {
    this.calendertype=dashboardtype
    if(dashboardtype=="Monthly")
    {
    this.yearlycalandershow=false
    this.showmonthselect=true
    this.monthlycalandershow=true
    }
    else
    {
    this.monthlycalandershow=false
    this.yearlycalandershow=true
    this.showmonthselect=false
    }
    this.yearlydashboarddata=response.Data.queue_name
    this.yearlydashboarddatatotle=response.Data.total_count_array
    }
    else if(response.status_code=="402")
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
    this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
      this.monthlycalandershow = false;
      this.yearlycalandershow = false;
    }
    })
  }

  getselectmonth(month){
    if(this.selectedyear!=""){
      this.selectedmonth=parseInt(month)+1
      this.initialdates=this.getDaysInMonth(parseInt(month),this.selectedyear)
      this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,this.calendertype,this.organisationid,this.provider_id).subscribe((response) => {
        if(response.status_code=="200"){
          if(this.calendertype=="Monthly"){
            this.yearlycalandershow=false
            this.showmonthselect=true
            this.monthlycalandershow=true
          }else{
            this.monthlycalandershow=false
            this.yearlycalandershow=true
            this.showmonthselect=false
          }
          response.Data.queue_name.map((elem:any)=>{
            elem.date_data.map((dateElem:any)=>{
              var dateArray = dateElem.date.split("_");
              var checkDate = new Date(dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0]);
              checkDate.setDate(checkDate.getDate() +1)
              var currentdate = new Date();
              dateElem.checkcurrent = "";
              if(checkDate < currentdate){
                dateElem.checkcurrent = false;
              }else{
                dateElem.checkcurrent = true;
              }
              return dateElem;
            });
            return elem;
          });
          this.weeaklydashboarddata=response.Data.queue_name
          this.weeaklydashboarddatatotle=response.Data.total_count_array
        }else if(response.status_code=="402"){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
          this.monthlycalandershow = false;
        this.yearlycalandershow = false;
        }
      })
    }else{
      this.snackbar.open("please select year",'', {
        duration: 2000,
      });
    }
  }

  getselectyear(year){
    this.selectedyear=year
    this.initialdates = this.getDaysInMonth(parseInt(this.selectedmonth)-1,this.selectedyear)
    this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,this.calendertype,this.organisationid,this.provider_id).subscribe((response) =>{
      if(response.status_code=="200"){
        //yearlydashboarddata
        if(this.calendertype=="Monthly"){
          this.yearlycalandershow=false
          this.showmonthselect=true
          this.monthlycalandershow=true
        }else{
          this.monthlycalandershow=false
          this.yearlycalandershow=true
          this.showmonthselect=false
        }
        if(this.calendertype=="Monthly"){
          response.Data.queue_name.map((elem:any)=>{
            elem.date_data.map((dateElem:any)=>{
              var dateArray = dateElem.date.split("_");
              var checkDate = new Date(dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0]);
              checkDate.setDate(checkDate.getDate() +1)
              var currentdate = new Date();
              dateElem.checkcurrent = "";
              if(checkDate < currentdate){
                dateElem.checkcurrent = false;
              }else{
                dateElem.checkcurrent = true;
              }
              return dateElem;
            });
            return elem;
          });
          this.weeaklydashboarddata=response.Data.queue_name
          this.weeaklydashboarddatatotle=response.Data.total_count_array
        }else{
          this.yearlydashboarddata=response.Data.queue_name
          this.yearlydashboarddatatotle=response.Data.total_count_array
        }
        //this.weeaklydashboarddata=response.Data.queue_name
        //this.weeaklydashboarddatatotle=response.Data.total_count_array
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.monthlycalandershow = false;
        this.yearlycalandershow = false;
      }
    })
  }



  selectOrganisation(organisationid){
    this.organisationid=organisationid;
    this.weeklydashboardservice.getProviderWorkingCalender(this.selectedmonth,this.selectedyear,this.calendertype,this.organisationid,this.provider_id).subscribe((response) => {
      if(response.status_code=="200"){
        if(this.calendertype=="Monthly"){
          this.yearlycalandershow=false
          this.showmonthselect=true
          this.monthlycalandershow=true
        }else{
          this.monthlycalandershow=false
          this.yearlycalandershow=true
          this.showmonthselect=false
        }
        if(this.calendertype=="Monthly"){
          response.Data.queue_name.map((elem:any)=>{
            elem.date_data.map((dateElem:any)=>{
              var dateArray = dateElem.date.split("_");
              var checkDate = new Date(dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0]);
              checkDate.setDate(checkDate.getDate() +1)
              var currentdate = new Date();
              dateElem.checkcurrent = "";
              if(checkDate < currentdate){
                dateElem.checkcurrent = false;
              }else{
                dateElem.checkcurrent = true;
              }
              return dateElem;
            });
            return elem;
          });
          this.weeaklydashboarddata=response.Data.queue_name
          this.weeaklydashboarddatatotle=response.Data.total_count_array
        //set selected month
          var splited_date = response.Data.total_count_array[0].date.split("_");
          this.seleactedbydefault = parseInt(splited_date[1])-1;

          this.weeklydashboardservice.getProviderOrgMinMaxBookingDates(this.organisationid).subscribe((response) => {
            if(response.status_code=="200"){
              var arraydata=[]
              response.Data.min_month = 1;
              response.Data.max_month = 12;
              for(var j=response.Data.min_month-1;j<response.Data.max_month;j++){
                if(j==0){
                  arraydata.push({
                    key:   j,
                    value: "January"
                  });
                }else if(j==1){
                  arraydata.push({
                    key:   j,
                    value: "February"
                  });
                }else if(j==2){
                  arraydata.push({
                    key:   j,
                    value: "March"
                  });
                }else if(j==3){
                  arraydata.push({
                    key:   j,
                    value: "April"
                  });
                }else if(j==4){
                  arraydata.push({
                    key:   j,
                    value: "May"
                  });
                }else if(j==5){
                  arraydata.push({
                    key:   j,
                    value: "June"
                  });
                }else if(j==6){
                  arraydata.push({
                    key:   j,
                    value: "July"
                  });
                }else if(j==7){
                  arraydata.push({
                    key:   j,
                    value: "August"
                  });
                }else if(j==8){
                  arraydata.push({
                    key:   j,
                    value: "September"
                  });
                }else if(j==9){
                  arraydata.push({
                    key:   j,
                    value: "October"
                  });
                }else if(j==10){
                  arraydata.push({
                    key:   j,
                    value: "November"
                  });
                }else if(j==11){
                  arraydata.push({
                    key:   j,
                    value: "December"
                  });
                }
              }
              this.arraydata=arraydata
              var arraydatayear=[]
              for(var k=response.Data.min_year;k<=response.Data.max_year;k++){
                arraydatayear.push({
                  key:"year"+k,
                  value:Number(k)
                })
              }
              this.arraydatayear=arraydatayear
              this.selectedyear = arraydatayear[0].value;
              this.getselectyear(this.selectedyear);
            }else{
            }
          })
        }else{
          this.yearlydashboarddata=response.Data.queue_name
          this.yearlydashboarddatatotle=response.Data.total_count_array
        }
      }else if(response.status_code=="402"){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.monthlycalandershow = false;
        this.yearlycalandershow = false;
      }
    })
  }



  public changeDateFormat(rawdate){
    this.pipe = new DatePipe('en-US');
    let selecteddate = rawdate.split("-");
    let selectedDay = selecteddate[0];
    let selectedMonth = selecteddate[1];
    let selectedYear = selecteddate[2];
    let selectedDate: Date = new Date(selectedYear+"-"+selectedMonth+"-"+selectedDay);
    let dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][selectedDate.getDay()];
    let monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][selectedDate.getUTCMonth()];
    return "<b>"+selectedDay+"</b><br>"+monthName+"-"+selectedYear+"<br><b>"+dayName+"</b>";
    // return "<b>"+selectedDay+"<br>"+dayName+"</b>";
  }
  //transaction queue end

  transactionQueueSummaryByDateRange(queue_date:any,summaryType:any){
    if(summaryType == "single"){
      var queue_date_array = queue_date.split('_');
      var fromdate = queue_date_array[2]+"-"+queue_date_array[1]+"-"+queue_date_array[0];
      // formating date
      var todate = fromdate;
    }else{
      var month = queue_date;
      var fromdate = this.selectedyear+"-"+month+"-01";
      // formating date
      var date = new Date(fromdate),
      y = date.getFullYear(),
      m = date.getMonth();
      var lastDay = new Date(y, m + 1, 0);
      var todate = lastDay.getFullYear()+"-"+(lastDay.getMonth() + 1)+"-"+lastDay.getDate();
    }

    let orgid=this.organisationid;
    const config = new MatDialogConfig();
    config.data = {
      callfrom:"daterange",
      providerid: this.provider_id,
      providerorgid: orgid,
      queue_start_date:fromdate,
      queue_end_date:todate,
      formated_date:"",
    }
    config.width = "450px";
    this.dialogRef = this.dialog.open(TransactionqueuesummaryComponent, config);
    this.dialogRef.afterClosed().subscribe(value => {})
  }

  setPastHeadingStyle(currentDate:any,givenDate:any){
    let currentDateArray = currentDate.split("-");
    let selectedCurrentDate: Date = new Date(currentDateArray[2]+"-"+currentDateArray[1]+"-"+currentDateArray[0]);
    let givenDateArray = givenDate.split("-");
    let selectedgivenDate: Date = new Date(givenDateArray[2]+"-"+givenDateArray[1]+"-"+givenDateArray[0]);
    if(selectedgivenDate < selectedCurrentDate){
      return true;
    }else{
      return false;
    }

  }


  //transaction queue start
  getTransactionQueueDatas(queue_id,date){
    var datedata=date.split('_')
    var newdate=datedata[0]+"-"+datedata[1]+"-"+datedata[2]
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=atob(currentUser.user_master_id);
    this.router.navigate(['/todaysqueue/'],{
      queryParams:{
        "backfrom":"workcalendar",
        "queue_transaction_id":"",
        "queue_type":"queue_type",
        "weeaklydashboard":"weeaklydashboard",
        "weeaklydashboardqueid":queue_id,
        "usermasterid":usermasterid,
        "weeaklydashboarddate":newdate,
      },
      skipLocationChange: true
    })
  }
}
