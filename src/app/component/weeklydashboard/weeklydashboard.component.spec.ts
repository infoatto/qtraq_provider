import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklydashboardComponent } from './weeklydashboard.component';

describe('WeeklydashboardComponent', () => {
  let component: WeeklydashboardComponent;
  let fixture: ComponentFixture<WeeklydashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklydashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklydashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
