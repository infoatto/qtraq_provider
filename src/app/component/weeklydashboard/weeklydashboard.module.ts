import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { WeeklydashboardComponent } from './weeklydashboard.component'
import {MatTabsModule} from '@angular/material/tabs';
import {WeeklydashboardRoutingModule  } from "./weeklydashboard-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
//import{ TimeFormatPipe } from './pipes/time.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { SidebarComponent } from '../sidebar/sidebar.component'
@NgModule({
  imports: [CommonModule,
    WeeklydashboardRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     FormsModule
    ],
  declarations: [WeeklydashboardComponent]
})
export class WeeklydashboardModule {}