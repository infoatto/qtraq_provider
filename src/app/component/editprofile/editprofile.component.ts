import { Component, OnInit, Renderer, ViewChild } from "@angular/core";
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from "@angular/forms";
import { OrganisationsService } from "./../../_services/organisations.service";
import { ProvidersService } from "./../../_services/providers.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { HttpHeaders } from "@angular/common/http";
import { ReversePipe } from "./../../pipes/reverse.pipe";
import { INgxMyDpOptions, IMyDateModel } from "ngx-mydatepicker";
import {MatSnackBar} from '@angular/material';
import { CommonService } from "./../../_services/common.service";
@Component({
  selector: "editprofile",
  templateUrl: "./editprofile.component.html",
  styleUrls: [
    "./editprofile.component.css",
    "./editprofile.component.scss"
  ]
})
export class EditprofileComponent implements OnInit {
  toggleVal:boolean = false;
  myOptions: INgxMyDpOptions = {
    dateFormat: "dd-mm-yyyy"
  };
  onconvalueget = "";
  conChangegetotp:any
  organisationdataid = 0;
  contactsubmitdata=false
  dob_date = "";
  submobile = "";
  otperrormsg = "";
  useraddresponse = "";
  providerdob="";
  providerdob1:any;
  practisingstarteddate="";
  practisingstarteddate1:any;
  profilepicname="";
  hiddenall:Boolean=true
  cancelmodel:Boolean=false
  provider_idadd = 0;
  provider_id = 0;
  imageposition = "";
  org_image_id = "";
  suberror = "";
  dob: any;
  dob1: any;
  org_logo: any;
  provider_logo: any;
  org_working_hour_id = "";
  datetime_status = "";
  image_status = "";
  image_name = "";
  imagetype = "";
providerdateogbarth:any
providerpractisingstarteddate:any
  //new code start
  timeday = "";
  timestart = "";
  timeend="";
  timedayslot="";
  //new code end
  working_day = "";
  dropdownSettings = {};
  responsedata = "";
  responsedataava="";
  responsedatacontact=""
  //organisationid=0
  docimage = "";
  orglist: any;
  //new code 1-3-2019
  planlist: any;
  listSubscribedPlans: any;
  listexistinguser: any;
  //new code end 1-3-2019
  isReadOnly:boolean=false
  citylist: any;
  statelist: any;
  getCountries: any;
  specialities: any;
  mobilegot:Boolean=false
  mobilegot1:Boolean=false
  PersonalDisabled:boolean=false
  AvalabillityDisabled: boolean = true;
  ContactDisabled: boolean = true;
  SubscriptionDisabled: boolean = true;
  UserDisabled: boolean = true;
  contactenable:boolean=false
  countryiddata:any
  selectedindex = 0;
  public show: boolean = false;
  public subplan: boolean = true;
  public subplanlist: boolean = false;
  public userform: boolean = false;
  public userformsearch: boolean = true;
  public mobilealreadyexist:boolean = false
  public verified_email_id:boolean = false

//mobile validate start
mobile1validate:boolean = false
mobile2validate:boolean = false
phone1validate:boolean = false
phone2validate:boolean = false
emailid1validate:boolean = false
emailid2validate:boolean = false

//isdoctorexist
//mobile validate end

  constructor(
    private renderer: Renderer,
    private formBuilder: FormBuilder,
    // private providerservice: OrganisationsService,
    private providerservice: ProvidersService,
    private router: Router,
    private route: ActivatedRoute,
    private cs:CommonService,
    private snackbar:MatSnackBar
  ) {

  }
  public documentGrp: FormGroup;
  public locationgroup: FormGroup;
  public contactgroup: FormGroup;
  public Subscription: FormGroup;
  public usergroup: FormGroup;
  public mobilesearch: FormGroup;
  public contactsubmit: FormGroup;
  public personalgroup: FormGroup;
  public changemobilegroup: FormGroup;
  public availability: FormGroup;
  public publicorgname
  public masterid:any
  public activerole
  public totalfiles: Array<File> = [];
  public totalFileName = [];
  selectedItems = [];
  public lengthCheckToaddMore = 0;
  public lengthCheckToaddMore1 = 0;
  org_specialities: {
    org_image_id: "";
    image_type: "";
    image_position: "";
  };
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";

  addeditsystemrole:any


 // docexist:Boolean=true

  ngOnInit() {
    this.changemobilegroup= this.formBuilder.group({
      mobilenumber:"",
      countarycode:"",
      otp:"",
      password:"",
    })

    this.personalgroup = this.formBuilder.group({
      mobileno: "",
      firstname: "",
      middlename: "",
      lastname: "",
      gender: "",
      countryid: "",
      state_id: "",
      city_id: "",
      pincode: "",
      emailid: "",
      dob: "",
      age: "",
      autodob:"",
      qualification: "",
      otherspeciality: "",
      consultationfees: "",
      practisingstarteddate: "",
      otp: "",
      org_logo: "",
      specialities: [[], Validators.required]
    });

    this.availability = this.formBuilder.group({
      doctorshadule: this.formBuilder.array([
        this.createdocDatetime(
          this.timedayslot,
          this.timestart,
          this.timeend,
          this.timeday,
          "editpro"
        )
      ])
    });

    this.mobilesearch = this.formBuilder.group({
      mobilenumber: ""
    });

    this.usergroup = this.formBuilder.group({
      mobilenumber: "",
      firstname: "",
      middlename: "",
      lastname: "",
      dob: "",
      otp: "",
      gender: "",
      countryid: "",
      stateid: "",
      cityid: "",
      pincode: "",
      emailid: "",
      isorgdefault: "",
      qtraquserid: ""
    });

    this.locationgroup = this.formBuilder.group({
      countryid: "",
      state_id: "",
      city_id: "",
      address1: "",
      address2: "",
      street: "",
      area: "",
      landmark: "",
      pincode: ""
    });

    this.contactgroup = this.formBuilder.group({
      mobile1:[''],
      mobile1public: 0,
      mobile2:[''],
      mobile2public: 0,
      phone1:[''],
      phone1public: 0,
      phone2:[''],
      phone2public: 0,
      emailid1: "",
      emailid1public: 0,
      emailid2: "",
      emailid2public: 0,
      website: ""
    })

    this.getorglist();
    this.getSpecialities();
    this.getcountry();
    this.dropdownSettings = {
      singleSelection: false,
      disabled:true,
      idField: "speciality_id",
      textField: "speciality_name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 1,
      allowSearchFilter: true
    };

    let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name
    let admin = JSON.parse(localStorage.getItem('Adminuser'));
    this.countryiddata=admin.country_id
    this.changemobilegroup.controls['countarycode'].setValue(this.countryiddata);
    let adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    this.masterid=atob(adminuser.user_master_id)
    let activerole = JSON.parse(localStorage.getItem('activerole'));
    this.activerole=activerole.activerole
    let activerole123 = JSON.parse(localStorage.getItem('orguserrole'));
    if((this.route.snapshot.queryParamMap.get('addeditsystemrole'))!=null){
      this.masterid=this.route.snapshot.queryParamMap.get('user_id_master_id')
      this.addeditsystemrole=this.route.snapshot.queryParamMap.get('addeditsystemrole')
      this.isReadOnly=true;
      var docexist=this.route.snapshot.queryParamMap.get('isdoctor')
      if(docexist=="yes"){
        this.contactenable=true
      }else{
        this.contactenable=false
      }
    }else{
      this.isReadOnly=false
      this.masterid=atob(adminuser.user_master_id)
      if(activerole123.doctor){
        this.contactenable=true
      }else{
        if((this.activerole!='admin')&&(this.activerole!='l1')){
          this.contactenable=true
        }
      }
    }
    this.providerservice.geteditprovider(this.masterid).subscribe(data => {
      if(data.status_code=="200"){
        this.PersonalDisabled=false
        this.AvalabillityDisabled = false;
        this.ContactDisabled = false;
        this.provider_id=this.route.snapshot.params.providerid;
        let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
        this.publicorgname=actorganistion.org_name
        if(data.Data.basic_details!=null){
          this.personalgroup.controls['mobileno'].setValue(data.Data.basic_details[0].user_name);
          this.personalgroup.controls['firstname'].setValue(data.Data.basic_details[0].first_name);
          this.personalgroup.controls['middlename'].setValue(data.Data.basic_details[0].middle_name);
          this.personalgroup.controls['lastname'].setValue(data.Data.basic_details[0].last_name);
          this.personalgroup.controls['pincode'].setValue(data.Data.basic_details[0].pincode);
          this.personalgroup.controls['emailid'].setValue(data.Data.basic_details[0].email_id);
          this.personalgroup.controls['gender'].setValue(data.Data.basic_details[0].gender);
          this.personalgroup.controls['countryid'].setValue(data.Data.basic_details[0].country_details[0].country_id);
          this.personalgroup.controls['state_id'].setValue(data.Data.basic_details[0].state_details[0].state_id);
          this.personalgroup.controls['city_id'].setValue(data.Data.basic_details[0].city_details[0].city_id);
          this.providerdateogbarth=data.Data.basic_details[0].dob
          this.onconChange(data.Data.basic_details[0].country_details[0].country_id)
          this.onstateChange(data.Data.basic_details[0].state_details[0].state_id)
          this.providerdob=data.Data.basic_details[0].dob.split('-');
          this.providerdob1= {
            date: {
              year: parseInt(this.providerdob[0]),
              month: parseInt(this.providerdob[1]),
              day: parseInt(this.providerdob[2])
            },
          }
          this.personalgroup.controls['dob'].setValue(this.providerdob1);
          //dob to age
          const convertAge = new Date(data.Data.basic_details[0].dob.replace(/-/g,"/"));
          const timeDiff = Math.abs(Date.now() - convertAge.getTime());
          this.personalgroup.controls['age'].setValue(Math.floor((timeDiff / (1000 * 3600 * 24)) / 365));
          this.personalgroup.controls['autodob'].setValue(data.Data.basic_details[0].autodob);

          this.profilepicname=data.Data.basic_details[0].profile_pic
          this.provider_logo=data.Data.basic_details[0].profile_pic_url
          this.verified_email_id = data.Data.basic_details[0].verified_email_flag
        }
        if(data.Data.user_availabilities!=null){
          for(var j=0;j<(data.Data.user_availabilities.length);j++){
            this.doctorshadule.insert(j,this.createdocDatetime(
                data.Data.user_availabilities[j].slot_type,
                data.Data.user_availabilities[j].start_time,
                data.Data.user_availabilities[j].end_time,
                data.Data.user_availabilities[j].provider_working_day,
                ""
            ))
            this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
          }
        }

        if(data.Data.user_personal_details!=null){
          this.personalgroup.controls['qualification'].setValue(data.Data.user_personal_details[0].qualification);
          this.personalgroup.controls['otherspeciality'].setValue(data.Data.user_personal_details[0].other_speciality);
          this.personalgroup.controls['consultationfees'].setValue(data.Data.user_personal_details[0].consultation_fees);
          this.selectedItems=data.Data.user_speciality
          this.contactgroup.controls['mobile1'].setValue(data.Data.user_personal_details[0].mobile1);
          this.contactgroup.controls['mobile2'].setValue(data.Data.user_personal_details[0].mobile2);
          this.contactgroup.controls['phone1'].setValue(data.Data.user_personal_details[0].phone1);
          this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
          this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
          this.contactgroup.controls['emailid1'].setValue(data.Data.user_personal_details[0].email1);
          this.contactgroup.controls['emailid2'].setValue(data.Data.user_personal_details[0].email2);
          this.contactgroup.controls['mobile1public'].setValue(data.Data.user_personal_details[0].mobile1_visibility)
          this.contactgroup.controls['mobile2public'].setValue(data.Data.user_personal_details[0].mobile2_visibility)
          this.contactgroup.controls['phone1public'].setValue(data.Data.user_personal_details[0].phone1_visibility)
          this.contactgroup.controls['phone2public'].setValue(data.Data.user_personal_details[0].phone2_visibility)
          this.contactgroup.controls['emailid1public'].setValue(data.Data.user_personal_details[0].email1_visibility)
          this.contactgroup.controls['emailid2public'].setValue(data.Data.user_personal_details[0].email2_visibility)
          this.contactgroup.controls['website'].setValue(data.Data.user_personal_details[0].website);
          this.providerpractisingstarteddate=data.Data.user_personal_details[0].practising_started_date
          this.practisingstarteddate=data.Data.user_personal_details[0].practising_started_date.split('-');
          this.practisingstarteddate1= {
            date: {
                year: parseInt(this.practisingstarteddate[2]),
                month: parseInt(this.practisingstarteddate[1]),
                day: parseInt(this.practisingstarteddate[0])
            },
          }
          this.personalgroup.controls['practisingstarteddate'].setValue(this.practisingstarteddate1);
        }
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
    //end edit form
  }

  //ravi new code 19-12-2018
  createdocDatetime(timedayslot:any,timestart:any,timeend:any,timeday:any,editcheck:any): FormGroup {
    return this.formBuilder.group({
      timedayslot: timedayslot,
      timestart: timestart,
      timeend: timeend,
      timeday: timeday,
      editcheck: editcheck
    });
  }

  createUploadDocuments(org_image_id:any,image_name:any,imageposition:any,imagetype:any,docimage:any,imagestatus:any,editcheck:any): FormGroup {
    return this.formBuilder.group({
      org_image_id: org_image_id,
      image_name: image_name,
      image_position: imageposition,
      image_type: imagetype,
      documentFile: docimage,
      image_status: imagestatus,
      editcheck: editcheck
    });
  }

  getcountry() {
    this.providerservice.getCountries().subscribe(response => {
      this.getCountries = response.Data;
    });
  }

  onconChange(val) {
    this.onconvalueget = val;
    this.providerservice.getState(val).subscribe(response => {
      this.statelist = response.Data;
    });
  }

  onstateChange(val) {
    this.providerservice.getcity(val).subscribe(response => {
      this.citylist = response.Data;
    });
  }

  getorglist() {
    this.providerservice.getallorganisation().subscribe(response => {
      this.orglist = response.Data;
    });
  }

  getSpecialities() {
    this.providerservice.getSpecialities().subscribe(response => {
      this.specialities = response.Data;
    });
  }

  adddocDatetime(): void {
    this.doctorshadule.insert(
      0,
      this.createdocDatetime(
        this.timedayslot,
        this.timestart,
        this.timeend,
         this.timeday,
        ""
      )
    );
    this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
  }

  removedocDatetime(index: number) {
    this.doctorshadule.removeAt(index);
    this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 - 1;
  }

  get doctorshadule(): FormArray {
    return this.availability.get("doctorshadule") as FormArray;
  }

  onFileChanged(event) {
    this.org_logo = event.target.files[0];
  }

  isAnswerProvided(eveb) {
  }

  notifyMe(event) {
  }

  addprop1(event) {
    if (event.target.checked) {
      this.show = true;
    } else {
      this.show = false;
    }
  }

  public PersonalformSubmit(formValue: any) {
    let main_form: FormData = new FormData();
    if(formValue.specialities!=null){
      for (let k = 0; k < formValue.specialities.length; k++) {
        main_form.append(
          "provider_speciality[]",
          formValue.specialities[k]["speciality_id"]
        );
      }
    }
    main_form.append("profile_pic", this.org_logo);
    main_form.append("profilepicname", this.profilepicname);
    main_form.append("mobileno", formValue.mobileno);
    main_form.append("firstname", formValue.firstname);
    main_form.append("middlename", formValue.middlename);
    main_form.append("lastname", formValue.lastname);
    main_form.append("gender", formValue.gender);
    main_form.append("dob", this.providerdateogbarth);
    main_form.append("autodob", formValue.autodob);
    main_form.append("practisingstarteddate", this.providerpractisingstarteddate);
    main_form.append("countryid", formValue.countryid);
    main_form.append("stateid", formValue.state_id);
    main_form.append("cityid", formValue.city_id);
    main_form.append("email", formValue.emailid);
    main_form.append("withprovider", "Yes");
    main_form.append("otherspeciality", formValue.otherspeciality);
    main_form.append("qualification", formValue.qualification);
    this.providerservice.personaladdprofile(this.profilepicname,this.provider_id,this.providerdateogbarth,this.providerpractisingstarteddate,main_form, formValue).subscribe(data => {
      if (data.status_code == 200) {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }else if(data.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
      }else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
  }

  public OnavailabilitySubmit(formValue: any){
    let main_form: FormData = new FormData();
    var timedate = 0;
    if (this.provider_id == 0){
      timedate = this.doctorshadule.length;
    } else {
      timedate = this.doctorshadule.length - 1;
    }
    for (let t = 0; t < (timedate); t++) {
      main_form.append("slot_type[]", formValue.doctorshadule[t]['timedayslot'])
      main_form.append("provider_working_day[]", formValue.doctorshadule[t]['timeday'])
      main_form.append("start_time[]", formValue.doctorshadule[t]['timestart'])
      main_form.append("end_time[]", formValue.doctorshadule[t]['timeend'])
    }

    if(this.provider_id!=0){
      this.provider_idadd=this.provider_id
    }
    this.providerservice.addeditavailability(this.provider_idadd, main_form).subscribe(data => {
      if (data.status_code == 200) {
        this.selectedindex = 2;
        this.AvalabillityDisabled = true;
        this.PersonalDisabled=true;
      } else if(data.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
      }else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
  }
  //set  second box start
  public ContactSubmit(formValue: any) {
    this.contactsubmitdata = true;
    if(this.contactgroup.invalid){
      return;
    }
    if((formValue.mobile1public==true||formValue.mobile1public==1)&&(formValue.mobile1!="")){
      formValue.mobile1public=1
    }else{
      formValue.mobile1public=0
    }
    if((formValue.mobile2public==true||formValue.mobile2public==1)&&(formValue.mobile2!="")){
      formValue.mobile2public=1
    }else{
      formValue.mobile2public=0
    }
    if((formValue.phone1public==true||formValue.phone1public==1)&&(formValue.phone1!="")){
      formValue.phone1public=1
    }else{
      formValue.phone1public=0
    }
    if((formValue.phone2public==true||formValue.phone2public==1)&&(formValue.phone2!="")){
      formValue.phone2public=1
    }else{
      formValue.phone2public=0
    }

    if((formValue.emailid1public==true||formValue.emailid1public==1)&&(formValue.emailid1!="")){
      formValue.emailid1public=1
    }else{
      formValue.emailid1public=0
    }
    if((formValue.emailid2public==true||formValue.emailid2public==1)&&(formValue.emailid2public!="")){
      formValue.emailid2public=1
    }else{
      formValue.emailid2public=0
    }
    if(this.provider_id!=0){
      this.provider_idadd=this.provider_id
    }
    this.providerservice.addeditProviderContactDetailscontact(this.provider_idadd,formValue).subscribe(data => {
      if (data.status_code == 200) {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      } else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
  }

  //1-10-2019
  onproviderclick(event){
    /*
    if(this.provider_id!=0)
    {
      this.providerservice
      .geteditprovider(this.provider_id)
      .subscribe(data => {
        this.PersonalDisabled=false
        this.AvalabillityDisabled = false;
        this.ContactDisabled = false;
        if(event.index==0)
        {
          this.personalgroup.controls['mobileno'].setValue(data.Data.basic_details[0].user_name);
          this.personalgroup.controls['firstname'].setValue(data.Data.basic_details[0].first_name);
          this.personalgroup.controls['middlename'].setValue(data.Data.basic_details[0].middle_name);
          this.personalgroup.controls['lastname'].setValue(data.Data.basic_details[0].last_name);
          this.personalgroup.controls['pincode'].setValue(data.Data.basic_details[0].pincode);
          this.personalgroup.controls['emailid'].setValue(data.Data.basic_details[0].email_id);
          this.personalgroup.controls['gender'].setValue(data.Data.basic_details[0].gender);
          this.personalgroup.controls['qualification'].setValue(data.Data.user_personal_details[0].qualification);
          this.personalgroup.controls['otherspeciality'].setValue(data.Data.user_personal_details[0].other_speciality);
          this.personalgroup.controls['consultationfees'].setValue(data.Data.user_personal_details[0].consultation_fees);
          this.onconChange(data.Data.basic_details[0].country_id)
          this.onstateChange(data.Data.basic_details[0].state_id)
          this.personalgroup.controls['countryid'].setValue(data.Data.basic_details[0].country_id);
          this.personalgroup.controls['state_id'].setValue(data.Data.basic_details[0].state_id);
          this.personalgroup.controls['city_id'].setValue(data.Data.basic_details[0].city_id);
        }
    else if(event.index==1)
      {
      }
    else if(event.index==2)
    {
    }
    })
    }
  */
  }

//1-10-2019

  statuschangeimage(testid, status) {
    this.providerservice.updateimagestatus(testid, status).subscribe(data => {
      if (data.status_code == 200) {
      } else {
      }
    });
  }

  //statuschangedatetime
  statuschangedatetime(testid:any, status:any) {
    this.providerservice.updatedatetimestatus(testid, status).subscribe(data => {
      if (data.status_code == 200) {
      } else {
      }
    });
  }

  planlistdata() {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }

    this.providerservice.getplandescription(this.organisationdataid).subscribe(data => {
      if (data.status_code == 200) {
        this.subplan = false;
        this.subplanlist = true;

        this.planlist = data.Data;
      } else {
      }
    });
  }

  sublist() {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }
    this.subplan = true;
    this.subplanlist = false;
    this.providerservice.listSubscribedPlans(this.organisationdataid).subscribe(data => {
      if (data.status_code == 200) {
        this.listSubscribedPlans = data.Data;
      } else {
      }
    });
  }



  divclick(planid, startdate, enddate) {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }
    this.providerservice.SubscribedPlans(this.organisationdataid, planid, startdate, enddate).subscribe(data => {
      if (data.status_code == 200) {
        this.selectedindex = 4;
      } else {
        this.suberror = data.Metadata.Message;
      }
    });
  }

  OnUsersubmit(formValue: any) {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }
    this.providerservice.addQTRAQUserDetails(formValue, this.dob_date, this.organisationdataid).subscribe(data => {
      if (data.status_code == 200) {
        this.router.navigate(["/"]);
      } else {
        this.useraddresponse = data.Metadata.Message;
      }
    });
  }

  genetarotp(convalueget, mobile) {
    this.providerservice.generateOtp(convalueget, mobile).subscribe(data => {
      if (data.status_code == 200) {
        //otp
        this.usergroup.controls["otp"].setValue(data.otp);
      }else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
  }

  ondobChanged(event: IMyDateModel): void {
    if (event.formatted !== '') {
			this.providerdateogbarth = event.formatted
		}
		var splitedDate = this.providerdateogbarth.split('-');
		const convertAge = new Date(splitedDate[2]+"/"+splitedDate[1]+"/"+splitedDate[0]);
		let timeDiff = Math.abs(Date.now() - convertAge.getTime());
		let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
		this.personalgroup.controls['age'].setValue(Number(age));
		this.personalgroup.controls['autodob'].setValue("No");
  }

  converAgeToDate(ageevent:any){
		const current_datetime:any = new Date();
		current_datetime.setFullYear(current_datetime.getFullYear()-ageevent.target.value);
		const now = {
			date: {
				year: parseInt(current_datetime.getFullYear()),
				month: parseInt(current_datetime.getMonth()+1),
				day: parseInt(current_datetime.getDate())
			},
		}
    this.personalgroup.controls['dob'].setValue(now);
    this.personalgroup.controls['autodob'].setValue("Yes");
	}

  onpractisingstarteChanged(event: IMyDateModel): void {
    if (event.formatted !== "") {
      this.providerpractisingstarteddate = event.formatted;
    }
  }

  toggleHideShow(action:string){
    if(action == "show"){
      this.toggleVal = true;
    }else{
      this.toggleVal = false;
    }
  }
  //set second box end

  //new code 5-2-2019 start
  changemobile(country_id:any,mobile:any){
    this.mobilegot1=true
    this.cancelmodelToggle("open")
    this.changemobilegroup.controls['mobilenumber'].setValue("")
    this.changemobilegroup.controls['otp'].setValue("")
    this.changemobilegroup.controls['password'].setValue("");
    this.changemobilegroup.controls['countarycode'].setValue(country_id);
    this.countryiddata = country_id;
  }

  cancelmodelToggle(action:string){
    if(action == "open"){
      this.cancelmodel = true;
      this.mobilegot=false
    }else{
      this.cancelmodel = false;
    }
  }

  cancelsubmit(formValue: any) {
    if (formValue.password != "") {
      this.providerservice.changeMobileNumber(formValue, this.countryiddata).subscribe(data => {
        if (data.status_code == 200) {
          if (data.status_code == 200) {
            this.cancelmodelToggle("close")
            ///new code 6-11-2019
            this.snackbar.open(data.Metadata.Message, '', {
              duration: 2000,
            });
            this.providerservice.geteditprovider(this.masterid).subscribe(data => {
              if (data.status_code == "200") {
                this.PersonalDisabled = false
                this.AvalabillityDisabled = false;
                this.ContactDisabled = false;
                this.provider_id = this.route.snapshot.params.providerid;
                let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
                this.publicorgname = actorganistion.org_name
                if (data.Data.basic_details != null) {
                  this.personalgroup.controls['mobileno'].setValue(data.Data.basic_details[0].user_name);
                  this.personalgroup.controls['firstname'].setValue(data.Data.basic_details[0].first_name);
                  this.personalgroup.controls['middlename'].setValue(data.Data.basic_details[0].middle_name);
                  this.personalgroup.controls['lastname'].setValue(data.Data.basic_details[0].last_name);
                  this.personalgroup.controls['pincode'].setValue(data.Data.basic_details[0].pincode);
                  this.personalgroup.controls['emailid'].setValue(data.Data.basic_details[0].email_id);
                  this.personalgroup.controls['gender'].setValue(data.Data.basic_details[0].gender);
                  this.personalgroup.controls['countryid'].setValue(data.Data.basic_details[0].country_details[0].country_id);
                  this.personalgroup.controls['state_id'].setValue(data.Data.basic_details[0].state_details[0].state_id);
                  this.personalgroup.controls['city_id'].setValue(data.Data.basic_details[0].city_details[0].city_id);
                  this.providerdateogbarth = data.Data.basic_details[0].dob
                  this.onconChange(data.Data.basic_details[0].country_details[0].country_id)
                  this.onstateChange(data.Data.basic_details[0].state_details[0].state_id)
                  this.providerdob = data.Data.basic_details[0].dob.split('-');
                  this.providerdob1 = {
                    date: {
                      year: parseInt(this.providerdob[0]),
                      month: parseInt(this.providerdob[1]),
                      day: parseInt(this.providerdob[2])
                    },
                  }
                  this.personalgroup.controls['dob'].setValue(this.providerdob1);
                  this.profilepicname = data.Data.basic_details[0].profile_pic
                  this.provider_logo = data.Data.basic_details[0].profile_pic_url
                }
                if (data.Data.user_availabilities != null) {
                  for (var j = 0; j < (data.Data.user_availabilities.length); j++) {
                    this.doctorshadule.insert(j, this.createdocDatetime(
                      data.Data.user_availabilities[j].slot_type,
                      data.Data.user_availabilities[j].start_time,
                      data.Data.user_availabilities[j].end_time,
                      data.Data.user_availabilities[j].provider_working_day,
                      ""
                    ))
                    this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
                  }
                }
                if (data.Data.user_personal_details != null) {
                  this.personalgroup.controls['qualification'].setValue(data.Data.user_personal_details[0].qualification);
                  this.personalgroup.controls['otherspeciality'].setValue(data.Data.user_personal_details[0].other_speciality);
                  this.personalgroup.controls['consultationfees'].setValue(data.Data.user_personal_details[0].consultation_fees);
                  this.selectedItems = data.Data.user_speciality
                  this.contactgroup.controls['mobile1'].setValue(data.Data.user_personal_details[0].mobile1);
                  this.contactgroup.controls['mobile2'].setValue(data.Data.user_personal_details[0].mobile2);
                  this.contactgroup.controls['phone1'].setValue(data.Data.user_personal_details[0].phone1);
                  this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
                  this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
                  this.contactgroup.controls['emailid1'].setValue(data.Data.user_personal_details[0].email1);
                  this.contactgroup.controls['emailid2'].setValue(data.Data.user_personal_details[0].email2);
                  this.contactgroup.controls['mobile1public'].setValue(data.Data.user_personal_details[0].mobile1_visibility)
                  this.contactgroup.controls['mobile2public'].setValue(data.Data.user_personal_details[0].mobile2_visibility)
                  this.contactgroup.controls['phone1public'].setValue(data.Data.user_personal_details[0].phone1_visibility)
                  this.contactgroup.controls['phone2public'].setValue(data.Data.user_personal_details[0].phone2_visibility)
                  this.contactgroup.controls['emailid1public'].setValue(data.Data.user_personal_details[0].email1_visibility)
                  this.contactgroup.controls['emailid2public'].setValue(data.Data.user_personal_details[0].email2_visibility)
                  this.contactgroup.controls['website'].setValue(data.Data.user_personal_details[0].website);
                  this.providerpractisingstarteddate = data.Data.user_personal_details[0].practising_started_date
                  this.practisingstarteddate = data.Data.user_personal_details[0].practising_started_date.split('-');
                  this.practisingstarteddate1 = {
                    date: {
                      year: parseInt(this.practisingstarteddate[2]),
                      month: parseInt(this.practisingstarteddate[1]),
                      day: parseInt(this.practisingstarteddate[0])
                    },
                  }
                  this.personalgroup.controls['practisingstarteddate'].setValue(this.practisingstarteddate1);
                }
                this.changemobilegroup.controls['mobilenumber'].setValue("")
                this.changemobilegroup.controls['otp'].setValue("")
                this.changemobilegroup.controls['password'].setValue("")
              } else if (data.status_code == '402') {
                localStorage.clear();
                this.router.navigate(['/login']);
              } else {
                this.snackbar.open(data.Metadata.Message, '', {
                  duration: 2000,
                });
              }
            });
            //new code end 6-11-2019
          } else {
            this.snackbar.open(data.Metadata.Message, '', {
              duration: 2000,
            });
          }
        } else {
          this.snackbar.open(data.Metadata.Message, '', {
            duration: 2000,
          });
        }
      })
    } else {
      this.providerservice.checkMobileNumberExist(formValue, this.countryiddata).subscribe(data => {
        if (data.status_code == 200) {
          this.mobilegot = true
          this.mobilegot1 = false
          this.mobilealreadyexist = false
        } else {
          this.changemobilegroup.controls['mobilenumber'].setValue("")
          this.changemobilegroup.controls['otp'].setValue("")
          this.changemobilegroup.controls['password'].setValue("")
          this.mobilealreadyexist = true
        }
      })
    }
  }

  genetarotpdata(convalueget, mobile) {
    this.providerservice.generateOtp(convalueget, mobile,"changemobile").subscribe(data => {
      if (data.status_code == 200) {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }else{
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    });
  }

  onconChangegetotp(value){
    this.countryiddata=value
  }

  verifyemail(emailid:any){
    if(emailid!=""){
      if(confirm("Are you sure you want to verify this email")){
        this.providerservice.verifyemailid(emailid).subscribe(data => {
          if (data.status_code == 200) {
            this.snackbar.open(data.Metadata.Message,'', {
              duration: 2000,
            });
          }else{
            this.snackbar.open(data.Metadata.Message,'', {
              duration: 2000,
            });
          }
        })
      }
    }else{
      this.snackbar.open("Please Enter Email",'', {
        duration: 2000,
      });
    }
  }

  backtohome(){
    this.cs.backtohome()
  }

  emailid1change(event){
    if(event.target.checked==true){
      this.emailid1validate=true
    }else{
      this.emailid1validate=false
    }
  }

  emailid2check(event){
    if(event.target.checked==true){
      this.emailid2validate=true
    }else{
      this.emailid2validate=false
    }
  }

  mobile1change(event){
    if(event.target.checked==true){
      this.mobile1validate=true
    }else{
      this.mobile1validate=false
    }
  }

  mobile2change(event){
    if(event.target.checked==true){
      this.mobile2validate=true
    }else{
      this.mobile2validate=false
    }
  }

  phone2change(event){
    if(event.target.checked==true){
      this.phone2validate=true
    }else{
      this.phone2validate=false
    }
  }

  phone1change(event){
    if(event.target.checked==true){
      this.phone1validate=true
    }else{
      this.phone1validate=false
    }
  }

  systemrole(){
    this.router.navigate(['/systemroles/']);
  }
  //new code end  5-2-2019 end
}
