
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OnholdprocessdialogComponent } from './onholdprocessdialog.component';

describe('OnholdprocessdialogComponent', () => {
  let component: OnholdprocessdialogComponent;
  let fixture: ComponentFixture<OnholdprocessdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnholdprocessdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnholdprocessdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

