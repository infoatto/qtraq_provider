import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from '../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ TodaysqueueComponent } from '../todaysqueue/todaysqueue.component'
import { AssignnewtokenService } from '../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators,FormArray,ReactiveFormsModule,FormControl } from '@angular/forms';

@Component({
	selector: 'onholdprocessdialog',
	templateUrl: './onholdprocessdialog.component.html',
	styleUrls: ['./onholdprocessdialog.component.scss']
})
export class OnholdprocessdialogComponent implements OnInit {

	//this.doctoraunavailability=response.Data
	transactionid: any

	showaddform: Boolean = false
	showdata: Boolean = false
	calculatedtime: any
	rawminutes:any = 0;
	msgdata: any
	resondata: any

	//constructor(private notificationservice:NotificationService) { }
	constructor(public dialogRef: MatDialogRef < TodaysqueueComponent > ,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private assignnewtoken: AssignnewtokenService,
		private snackbar: MatSnackBar, private router: Router, private formBuilder: FormBuilder
	) {

	}
	ngOnInit() {
		this.transactionid = this.data.transactionid
		this.assignnewtoken.getHoldQueueDetails(this.transactionid)
			.subscribe((response) => {
					if (response.status_code == "200") {
						if (response.Data.is_on_hold == false) {
							this.showaddform = true
						} else {
							this.showdata = true
							this.msgdata = response.Metadata.Message
							this.resondata = response.Data.hold_reason
						}
					}
				}
			)
	}

	putonhold(nextmin:any, reson:any) {
		if(reson == ""){
			this.snackbar.open("Please provide reason for Hold.", '', {
				duration: 2000,
			});
		}else if(this.rawminutes > 120 || this.rawminutes < 1 || this.rawminutes == ""){
			this.snackbar.open("No. of mins should be 1 to 120 mins.", '', {
				duration: 2000,
			});
		}else{
			if(this.calculatedtime)
			this.assignnewtoken.putonhold(this.transactionid, this.calculatedtime, reson, "hold").subscribe((response) => {
			  if (response.status_code == "200") {
				this.snackbar.open(response.Metadata.Message, '', {
				  duration: 2000,
				});
				this.dialogRef.close()
			  } else {
				this.snackbar.open(response.Metadata.Message, '', {
				  duration: 2000,
				});
				this.dialogRef.close()
			  }
			})
		}
	}

	onSearchChange(time) {
		this.rawminutes = time;
		var now = new Date()
		now.setMinutes(now.getMinutes() + parseInt(time)); // timestamp
		now = new Date(now); // Date object
		if ((time != 0)) {
			this.calculatedtime = now.getHours() + ":" + now.getMinutes()
		} else {
			this.calculatedtime = ""
		}
	}


	restartqueue(nextmin, reson) {
		this.assignnewtoken.putonhold(this.transactionid, "", "", "reset").subscribe((response) => {
      if (response.status_code == "200") {
        this.snackbar.open(response.Metadata.Message, '', {
          duration: 2000,
        });
        this.dialogRef.close()
      } else {
        this.snackbar.open(response.Metadata.Message, '', {
          duration: 2000,
        });
        this.dialogRef.close()
      }
    })
	}

  closeDialog(){
    this.dialogRef.close()
  }
}
