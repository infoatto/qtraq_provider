import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSystemroleMasterComponent } from './add-systemrole-master.component';

describe('AddeditNotificationMasterComponent', () => {
  let component: AddSystemroleMasterComponent;
  let fixture: ComponentFixture<AddSystemroleMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSystemroleMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSystemroleMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
