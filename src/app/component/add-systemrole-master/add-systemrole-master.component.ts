import {Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {FormBuilder,FormControl,FormGroup,Validators,ReactiveFormsModule} from '@angular/forms';
import {SystemroleService} from './../../_services/systemrole.service';
import {isNumeric} from "rxjs/util/isNumeric"
import {INgxMyDpOptions,IMyDateModel} from 'ngx-mydatepicker';
import {CommonService} from "./../../_services/common.service";
import {MatSnackBar} from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-add-systemrole-master',
	templateUrl: './add-systemrole-master.component.html',
	styleUrls: ['./add-systemrole-master.component.scss']
})
export class AddSystemroleMasterComponent implements OnInit {
	toggleVal: boolean = false;
	countryiddata: any
	newusewnamw: any
	newpassword: any
	generatedotp: any
	cancelmodel: Boolean = false
	errorMessage: string;
	submitted = false;
	citylist: any
	statelist: any
	getCountries: any
	mobileresponse: any
	fieldshow: Boolean = false
	dob_date = ""
	age: any = ""
	dob = ""
	dob1: any;
	userdataSubmitted = false;
	firstname: Boolean = true
	middlename: Boolean = true
	lastname: Boolean = true
	pincode: Boolean = true
	emailid: Boolean = true
	countryid: Boolean = true
	state_id: Boolean = true
	city_id: Boolean = true
	gender: Boolean = true
	isl1: Boolean = true
	isl2: Boolean = true
	isorgadmin: Boolean = true
	mobilenumberread: Boolean = false
	datedesable: Boolean = true
	onconvalueget: any
	conChangegetotp = "0"
	mobilenumberotp: any
	publicorgname: any;
	unsubsribeSubmit:Subscription;

	public systemroleform: FormGroup;
	constructor(private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private systemroleservice: SystemroleService,
		private snackbar: MatSnackBar,
		private cs: CommonService
	) {
  }

	ngOnInit() {
		let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
		this.publicorgname = actorganistion.org_name
		let admin = JSON.parse(localStorage.getItem('Adminuser'));
		this.countryiddata = admin.country_id
		this.conChangegetotp = admin.country_id
		this.cancelmodel = false
		this.getcountry()
		this.systemroleform = this.formBuilder.group({
			mobilenumber: [''],
			firstname: ['', [Validators.required]],
			middlename: [''],
			lastname: [''],
			pincode: [''],
			emailid: [''],
			countryid: ['', [Validators.required]],
			state_id: ['', [Validators.required]],
			city_id: ['', [Validators.required]],
			gender: ['', [Validators.required]],
			isl1: [''],
			isl2: [''],
			isorgadmin: [''],
			dob: ['',[Validators.required]],
			age: ['',[Validators.required]],
			autodob:[''],
			user_master_id: [''],
			otp: ['', [Validators.required]],
		})
	}
	myOptions: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
	};

	getcountry() {
		this.systemroleservice.getCountries().subscribe(response => {
			this.getCountries = response.Data
		});
	}

	onconChange(val) {
		this.onconvalueget = val
		this.systemroleservice.getState(val).subscribe(response => {
			this.statelist = response.Data
		});
	}

	onstateChange(val) {
		this.systemroleservice.getcity(val).subscribe(response => {
			this.citylist = response.Data
		});
	}

	getmobilenumber(mobilenumber) {
		if (isNumeric(mobilenumber)) {
			if (this.conChangegetotp != "0") {
				this.mobilenumberotp = mobilenumber
				this.systemroleservice.getdatabymobile(mobilenumber, this.conChangegetotp).subscribe(response => {
					if (response.status_code == 200) {
						if (response.Data[0].useraccess == "Yes") {
							this.snackbar.open('Mobile no. ' + mobilenumber + ' of user ' + response.Data[0].first_name + ' ' + response.Data[0].last_name + ' is already part of the system', '', {
								duration: 2000,
							});
						} else {
							this.onconvalueget = response.Data[0].country_id
							this.firstname = true
							this.middlename = true
							this.lastname = true
							this.pincode = true
							this.datedesable = true
							this.emailid = true
							this.errorMessage = ""
							this.mobilenumberread = true
							this.fieldshow = true
							this.onconChange(response.Data[0].country_id)
							this.onstateChange(response.Data[0].state_id)
							this.systemroleform.controls['user_master_id'].setValue(response.Data[0].user_master_id);
							this.systemroleform.controls['mobilenumber'].setValue(response.Data[0].user_name);
							this.systemroleform.controls['firstname'].setValue(response.Data[0].first_name);
							this.systemroleform.controls['middlename'].setValue(response.Data[0].middle_name);
							this.systemroleform.controls['lastname'].setValue(response.Data[0].last_name);
							this.systemroleform.controls['gender'].setValue(response.Data[0].gender);
							this.systemroleform.controls['pincode'].setValue(response.Data[0].pincode);
							this.systemroleform.controls['countryid'].setValue(response.Data[0].country_id);
							this.systemroleform.controls['state_id'].setValue(response.Data[0].state_id);
							this.systemroleform.controls['city_id'].setValue(response.Data[0].city_id);
							this.systemroleform.controls['emailid'].setValue(response.Data[0].email_id);
							this.systemroleform.get('countryid').disable();
							this.systemroleform.get('state_id').disable();
							this.systemroleform.get('city_id').disable();
							this.systemroleform.get('gender').disable()
							this.dob = response.Data[0].dob.split('-');
							this.dob1 = {
								date: {
									year: parseInt(this.dob[0]),
									month: parseInt(this.dob[1]),
									day: parseInt(this.dob[2])
								},
							}
							this.systemroleform.controls['dob'].setValue(this.dob1);
							//dob to age
							const convertAge = new Date(response.Data[0].dob.replace(/-/g,"/"));
							const timeDiff = Math.abs(Date.now() - convertAge.getTime());
							this.systemroleform.controls['age'].setValue(Math.floor((timeDiff / (1000 * 3600 * 24)) / 365));
							this.systemroleform.controls['autodob'].setValue(response.Data[0].dob.autodob);
						}
					} else {
						this.mobilenumberread = true
						this.fieldshow = true
						this.firstname = false
						this.middlename = false
						this.lastname = false
						this.pincode = false
						this.datedesable = false
						this.emailid = false
						this.mobileresponse = response.Metadata.Message
					}
				});
			} else {
				this.snackbar.open("Please Select Country", '', {
					duration: 2000,
				});
			}
		} else {
			this.snackbar.open("Please Enter Number", '', {
				duration: 2000,
			});
		}
	}

	resatdata() {
		this.mobilenumberread = false
		this.fieldshow = false
		this.systemroleform.reset()
		this.firstname = false
		this.middlename = false
		this.lastname = false
		this.pincode = false
		this.datedesable = false
		this.emailid = false
		this.systemroleform.get('countryid').enable();
		this.systemroleform.get('state_id').enable();
		this.systemroleform.get('city_id').enable();
		this.systemroleform.get('gender').enable()
	}

	OnsystemroleSubmit(formValue: any) {
		this.userdataSubmitted = true
		let admin = formValue.isorgadmin
		let l1 = formValue.isl1
		let l2 = formValue.isl2
		if ((admin == "") && (l1 == "") && (l2 == "")) {
			this.snackbar.open("Please Assign Atleast One Role", '', {
				duration: 2000,
			});
			return;
		}
		if (this.systemroleform.invalid) {
			return;
		}
		this.unsubsribeSubmit = this.systemroleservice.addQTRAQSystemRoleUser(formValue, this.dob_date, this.onconvalueget, this.mobilenumberotp).subscribe(response => {
			if (response.status_code == 200) {
				this.newusewnamw = response.Data.username
				this.newpassword = response.Data.password
				if (this.newusewnamw == undefined) {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					});
					this.router.navigate(['/']);
				} else {
					this.cancelmodelToggle("open")
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					});

				}
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				});
			}
			this.unsubsribeSubmit.unsubscribe();
		});



	}

	onStartDateChanged(event: IMyDateModel): void {
		if (event.formatted !== '') {
			this.dob_date = event.formatted
		}
		var splitedDate = this.dob_date.split('-');
		const convertAge = new Date(splitedDate[2]+"/"+splitedDate[1]+"/"+splitedDate[0]);
		let timeDiff = Math.abs(Date.now() - convertAge.getTime());
		let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
		this.systemroleform.controls['age'].setValue(Number(age));
		this.systemroleform.controls['autodob'].setValue("No");
	}

	converAgeToDate(ageevent:any){
		const current_datetime:any = new Date();
		current_datetime.setFullYear(current_datetime.getFullYear()-ageevent.target.value);
		const now = {
			date: {
				year: parseInt(current_datetime.getFullYear()),
				month: parseInt(current_datetime.getMonth()+1),
				day: parseInt(current_datetime.getDate())
			},
		}
		this.dob_date = parseInt(current_datetime.getDate())+"-"+parseInt(current_datetime.getMonth()+1)+"-"+parseInt(current_datetime.getFullYear())
		this.systemroleform.controls['dob'].setValue(now);
		this.systemroleform.controls['autodob'].setValue("Yes");
		this.systemroleform.controls['age'].setValue(Number(ageevent.target.value));

	}

	toggleHideShow(action: string) {
		if (action == "show") {
			this.toggleVal = true;
		} else {
			this.toggleVal = false;
		}
	}

	onconChangegetotp(value) {
		this.conChangegetotp = value
	}

	generatotp() {
		this.systemroleservice.generateOtp(this.onconvalueget, this.mobilenumberotp)
			.subscribe(
				data => {
					if (data.status_code == 200) {
						this.snackbar.open(data.Metadata.Message, '', {
							duration: 2000,
						});
					} else {
						this.snackbar.open(data.Metadata.Message, '', {
							duration: 2000,
						});
					}
				})
	}

	cancelmodelToggle(action: string) {
		if (action == "open") {
			this.cancelmodel = true;
		} else {
			this.router.navigate(['/systemroles']);
			this.cancelmodel = false;
		}
	}
	backtohome() {
		this.cs.backtohome()
	}

	backtosystemrole() {
		this.router.navigate(['/systemroles']);
	}

}