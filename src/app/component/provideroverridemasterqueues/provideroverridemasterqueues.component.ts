import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import {OverridemasterqueuesService} from './../../_services/overridemasterqueues.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl,FormArray } from '@angular/forms';

@Component({
  selector: 'provideroverridemasterqueues',
  templateUrl: './provideroverridemasterqueues.component.html',
  styleUrls: ['./provideroverridemasterqueues.component.scss']
})

//provideroverridemasterqueues

//overridemasterqueues
//Overridemasterqueues

export class ProvideroverridemasterqueuesComponent implements OnInit {


public overridequeue: FormGroup;

providerlistdata:any
 errorMessage: string;
 doctoraunavailability:any
 providerlistshow:Boolean=true
 orgname:any
 orgimage:any
 totalRecords:any
  constructor(private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private router: Router,
  private overridemasterqueues:OverridemasterqueuesService) 
  { 
  }

providerid:any
queue_transaction_id:any
start_time:any
end_time:any
queue_name:any
queue_pic_url:any
overrideque:any

 page=0;
 size = 10;
  ngOnInit() 
  {
    this.providerlistshow=false
    //this.getproviderlist(1)
this.overrideque=false

this.overridequeuedata()

   }
   

   getproviderlist(page: number)
   {
     
     //this.doctoravailability.doctoravailability


     this.overridemasterqueues.getorgque((page - 1),this.size)
     .subscribe(
     (response) => 
     {
   
   //this.showdoctorlist=false
   this.providerlistshow=true
    
    this.providerlistdata=response.Data
   
   
   
     this.totalRecords=response.total_count
     this.page=page
   },
     (err: any) => console.log("error",err),
     () =>
     console.log("getCustomersPage() retrieved customers for page:", + page)
   );
   }
   



quetimeoverridecall()
{

  this.overridequeue = this.formBuilder.group({
    items: this.formBuilder.array([this.createoverridemasterqueues(this.queue_name,this.queue_transaction_id,this.start_time,
    this.end_time,this.queue_pic_url)]),
    });
        
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        this.orgname=orguserlist.orgname
       this.orgimage=orguserlist.orgimg
    
    
       this.overridemasterqueues.providerTransactionQueuesDetails()
       .subscribe(
       data => {
       
    if(data.status_code=="200")
    {
      for(var i=0;i<(data.Data.length);i++)
      {
      this.items.insert(i, this.createoverridemasterqueues(
        data.Data[i].queue_name,
      data.Data[i].queue_transaction_id,
      data.Data[i].queue_start_time,
      data.Data[i].queue_end_time,
    data.Data[i].queue_pic_url  
    ))
    
    
    }
    
    
    }
    else if(data.status_code=='402')
    {
     localStorage.clear();
      this.router.navigate(['/login']);
    }
    else
    {
    this.errorMessage=data.Metadata.Message
    }
    
    
       })
}



   createoverridemasterqueues(queue_name,queue_transaction_id,start_time,end_time,queue_pic_url): FormGroup {
    return this.formBuilder.group({
      queue_name:queue_name,
      queue_transaction_id:queue_transaction_id,
      start_time:start_time,
      end_time:end_time,
      queue_pic_url:queue_pic_url,
           
    });
    }
    
    
    
    get items(): FormArray {
      return this.overridequeue.get('items') as FormArray;
    };



onoverridesubmit(formValue: any)
{

let main_form: FormData = new FormData();
for (let j = 0; j < this.items.length; j++) {
if(formValue.items[j]['queue_transaction_id']!='')
{
main_form.append("queue_transaction_id["+j+"]", formValue.items[j]['queue_transaction_id'])
main_form.append("start_time["+j+"]", formValue.items[j]['start_time'])
main_form.append("end_time["+j+"]", formValue.items[j]['end_time'])
}

/*
this.overridemasterqueues.submitoverrideque(main_form)
   .subscribe(
   data => {

if(data.status_code=='200')
{

this.router.navigate(['/']);

}
else if(data.status_code=='402')
{
 localStorage.clear();
  this.router.navigate(['/login']);
}
else
{
this.errorMessage=data.Metadata.Message
}
})
*/

}


}


overridequeuedata()
{
//this.providerid=providerid
this.providerlistshow=false
this.overrideque=true
this.quetimeoverridecall()



}

backtooverride()
{
  this.router.navigate(['/']);
}

}
