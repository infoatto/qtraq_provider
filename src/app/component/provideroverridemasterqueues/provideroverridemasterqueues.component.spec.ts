import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProvideroverridemasterqueuesComponent } from './provideroverridemasterqueues.component';

describe('OverridemasterqueuesComponent', () => {
  let component: ProvideroverridemasterqueuesComponent;
  let fixture: ComponentFixture<ProvideroverridemasterqueuesComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvideroverridemasterqueuesComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ProvideroverridemasterqueuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
