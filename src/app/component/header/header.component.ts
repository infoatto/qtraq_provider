import { Component, OnInit, trigger, transition, style, animate } from '@angular/core';
import { SidebarToggleService } from 'app/_services/sidebar-toggle.service';
import { OrglistchangeComponent } from '../orglistchange/orglistchange.component';
import { RolelistComponent } from '../rolelist/rolelist.component';
import { MatDialog } from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { CommonService } from "./../../_services/common.service";


import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateX(-100%)'}))
      ])
    ])
  ]
})
export class HeaderComponent implements OnInit {
toggleVal:any = false;
adminshow:boolean=false
frontdesk:boolean=false
doctorshow:boolean=false
orgimg:any
organisationname:any
changeorg:Boolean=true;
notification_count:any  = 0;


firstname:any
lastname:any

  constructor(
    private sideToggleService:SidebarToggleService,
    private dialog:MatDialog,
    private router:Router,
    private commonservice:CommonService,
    private snackbar:MatSnackBar
  ) { 

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         // trick the Router into believing it's last link wasn't previously loaded
         this.router.navigated = false;
         // if you need to scroll back to top, here is the right place
         window.scrollTo(0, 0);
      }
    });
  }

  AfterViewInit(){
    this.commonservice.getNotificationCount().subscribe(response => {
      this.notification_count = response.notification_count
    })
  }
  ngOnInit() {
    this.commonservice.getNotificationCount().subscribe(response => {
      this.notification_count = response.notification_count
    })
    this.sideToggleService.gettoggleVal().subscribe(response=>{
      this.toggleVal = response;
    });
    let orgerrolelist = JSON.parse(localStorage.getItem("orguserrole"));
    let adminuser = JSON.parse(localStorage.getItem("Adminuser"));
    if(orgerrolelist!=null){
      this.organisationname=orgerrolelist.orgname
      this.firstname=atob(adminuser.first_name)
      this.lastname=atob(adminuser.last_name)
      //this.orgimg=atob(adminuser.profileimage_path)
      let activerole = JSON.parse(localStorage.getItem("activerole"));
      if((orgerrolelist.adminrole==true)){
        if((activerole.activerole=="admin")){
          this.adminshow=false
        }else{
          this.adminshow=true
        }
      } 
      if((orgerrolelist.doctor==true)){
        if((activerole.activerole=="l2")){
          this.doctorshow=false
        }else{
          this.doctorshow=true
        }
      }

      if((orgerrolelist.frontdesk==true)){
        if((activerole.activerole=="l1")){
          this.frontdesk=false
        }else{
          this.frontdesk=true
        }
      }
      this.getdata()
    }else{
      localStorage.clear();
      this.router.navigate(['/login']);
    }
}








getdata()
{
this.commonservice.getLoginUserDetails()
.subscribe(
(response) => 
{
if(response.status_code=="200")
{
this.orgimg=response.Data.user_details[0].profile_pic_path

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
  this.orgimg=response.Data.user_details[0].profile_pic_path
}
})


this.commonservice.getorganisationlist()
.subscribe(
(response) => 
{
if(response.status_code==200)
{

if(response.Data.length>1)
{
this.changeorg=true
}
else
{
this.changeorg=false
}

//this.orgimg=response.Data.user_details[0].profile_pic_path




}
else if(response.status_code==402)
{
localStorage.clear();
this.router.navigate(['/login']);
}

})








}


  toggleHideShow(action: string) {
    if (action == "show") {
      this.toggleVal = true;
      // this.sideToggleService.assignToggleVal(true);
    } else {
      // this.sideToggleService.assignToggleVal(false);
      this.toggleVal = false;
    }
  }

  changeorganisation() {
  
    this.dialog.open(OrglistchangeComponent, { disableClose: true });
  }




  changerole() {
    this.dialog.open(RolelistComponent, { disableClose: true });
  }

  public async logout(): Promise<void> {
    

    this.commonservice.userLogOut()
    .subscribe(
    (response) => 
    {





      if(response.status_code=="200")
      {
     // this.orgimg=response.Data.user_details.profile_pic_path

      localStorage.clear();
      this.router.navigate(["/login"]);

      }
       else if(response.status_code=='402')
       {
      localStorage.clear();
      this.router.navigate(['/login']);
       }
       else
       {
        //this.orgimg=response.Data.user_details[0].profile_pic_path
      
       this.snackbar.open(response.Metadata.Message,'', {
       duration: 2000,
       });
       }
      
      })
    }    
  changeroledata(role){
    localStorage.removeItem('activerole');
    let actas={ "activerole":role}
    localStorage.setItem('activerole',JSON.stringify(actas));
    this.router.navigate(['/']);
  }
}
