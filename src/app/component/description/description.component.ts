
import { Component, OnInit,ViewChild } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
//import { DoctoravailabilityService } from './../../_services/doctoravailability.service';
import { TodaysqueueService } from './../../_services/todaysqueue.service'; 
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import {MatSnackBar} from '@angular/material';
import { SearchtokensService } from './../../_services/searchtokens.service';
import{ MessagequelistComponent } from './../../component/messagequelist/messagequelist.component'
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import { TimeFormatPipe } from './../../pipes/time.pipe'

import { RequestreceivedService } from './../../_services/requestsreceived.service';
import { DatePipe } from '@angular/common';
import { MatDialogConfig } from '@angular/material'
import{ DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
   };
   submitdate:any
   dropdownSettings = {};
   @ViewChild(NgForm) userlogin: NgForm;
   bookingtype:any
onassign_custom_form_values:any
oncomplete_custom_form_values:any
concatarrayforresult:any

usermasterid:any
provideridselected:any

   transactionqueueid:any
   searchtokendatadetail:any
   tokentype:any
   tokenidpriscription:any
   changeseqtokenid:any
   searchtokendatadetailshowdata:Boolean=false
   alldata:any
providerlist:Boolean=true
ongoingqueuelistdata:Boolean=true
ongoingtab:Boolean=false
broadcast:Boolean=false
is_doctor_arrived:any
pending:any
ongoing:any
noshow:any
completedorcancelled:any
profileimage_path:any
first_name:any
user_master_id:any
selectedindex=0;
ongoingqueuelist:any
todayongoingqueuelist:any
progressbar:any
queuelist:Boolean=false
allTokensCount:any
transactionQueueTokens:any
errorMessageongoing:any
doctoravailabilityfilter: any = {
  month:"",
  year:""
};
adddoctoravailability:any={
  unavailabilitydaytype:''
}
errorMessage: string;
  orgname:any
 orgimage:any
 totalRecords:any
 ongoingqueuedata:any
 queue_details:any
 queue_detailstoday:any
 queue_tokens:any
 avg_consulting_time:any
 avg_waiting_time:any
 cancelCompletedTokensCount:any
 checkup_total_amount:any
 noshowTokensCount:any
 ongoingTokensCount:any
 pendingTokensCount:any
 queue_transaction_id:any
 ongoingestimated_end_time
 ongoingqueue_date
 ongoingprovider_name
 ongoingqueue_pic_url
 ongoingqueue_name
broadcastlisting:any
broadcastlistingpage=1
broadcasterror:any
addbroadcastmessage:Boolean=false
broadcastmsgerror:any
page=0;
size = 10;
ongoinglist:any
broadcastmesseglist:any   
getTransactionTokenList:any
completedorcancelledpage=0
noshowpage=0
ongoingpage=0
pendingpage=0
allpage=0
modelFlag:Boolean=false
cancelmodel:Boolean=false
latertoday:Boolean=false
total_tokens:any
ongoingqueuelistdatashow:Boolean=false


//4-22-2019 start
quelistdatareshadule:any
requestreceived:Boolean=false
getTokenDetailsreshadule:any
pipe:any
token_idres:any
token_noconres:any
token_datereq:any
token_timereq:any
queue_namereqexisting:any
newrequested_date:any
reshaduledate:any
queue_reqque:any
queue_namereq:any

token_datefordig:any

token_timefordig:any
booking_for_user_id:any
token_nores:any
token_timeres:any
restoken_time:any
customfield:any
token_id:any
hiddenall:Boolean=true
dialogRef:any
//4-22-2019 end

//dignosis start 

dignosispriscription:Boolean=false
patienthistorydata:any


token_details:any
diagnosis_details:any
prescription_details:any
test_details:any
prescription_images:any
report_images:any
//dignosis end

//assign start
queue_id:any
token_no:any
token_time:any
token_apitime:any
quelistdata:any
queselecteddate:any
myFormattedDate:any
assignnewtokenform:any
selectedproid:any
mobilesearchmasterid:any
selfname:any
tokenrequestorno:any
bookedfor:any
bookedforself:Boolean=false
bookingtypeselectedtype:any
token_date:any


searchfirst_name:any
regno:any
usergendermale:any
searchmobileage:any

bookingusername:any
searchfield:any
assignsearchdiv:any
userprofileimg:any
usergenderfemale:any
usergenderOther:any
familymemeberidfordetail:any

bookedother:any

searchbookinguserid:any
cancelmodelforassigndat:Boolean=false


assignbuttonshow:any
selfnumber:any
selfimage:any
bookingfordropdownvis:any

publicorgname:any


familymember:any
public documentGrp: FormGroup;

public priescriptiondocumentGrp: FormGroup;


testtypelist:any

familymembershow:Boolean=false 
public lengthCheckToaddMore = 0;
//assign end
currentdate = new Date();
constructor(private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private router: Router,
  private ongoingqueues:TodaysqueueService,
  private snackbar:MatSnackBar,
   private searchtoken:SearchtokensService,
  private requestrceived:RequestreceivedService,
  public dialog: MatDialog,
  private assignnewtoken:AssignnewtokenService
) 
  { 
  }


 

  public doctoravunailability: FormGroup;

  public addunavailability: FormGroup;
  familymemberselfshow:boolean=false

  keys:any;
  months:any ;
  complaints

  lengthCheckToaddMorepresc:any
  lengthCheckToaddMoretest:any

medicine:any
qty:any
duration:any
frequencs:any
testtype:any
testname:any
years = [];

ngOnInit() 
{


//new code  4-23-2019

this.dropdownSettings = {
  singleSelection: false,
  idField: 'custom_field_value_id',
  textField: 'custom_list_text',
  selectAllText: 'Select All',
  unSelectAllText: 'UnSelect All',
  itemsShowLimit: 3,

  allowSearchFilter: true
};



if((this.route.snapshot.queryParamMap.get('providerongoingdescription'))!=null)
{



var tokenid   =this.route.snapshot.queryParamMap.get('tokeniddescription')
this.diegnosisprescription(tokenid)

}

this.documentGrp = this.formBuilder.group({
complaints_text: '',
items: this.formBuilder.array([
  this.createUploadDocuments(this.complaints,"editcall")
]),
})
this.priescriptiondocumentGrp = this.formBuilder.group({
  complaints_text: '',
  priesitems: this.formBuilder.array([
    this.priesDocuments(this.medicine,this.qty,this.duration,this.frequencs,"editpries")
  ]),
  
  testitems: this.formBuilder.array([
    this.testDocuments(this.testtype,this.testname,"testdocument")
  ]),
  
})





//new code end 4-23-2019
  this.familymemberselfshow=true
  let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name
this.dignosispriscription=false
this.requestreceived=false
this.hiddenall=true
//this.ongoingqueuelistdatashow=true
this.latertoday=true
//this.quesetuplist(1);
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let user_master_id=atob(currentUser.user_master_id);
let profileimage_path=atob(currentUser.profileimage_path);
let first_name=atob(currentUser.profileimage_path);
//profileimage_path,first_name,user_master_id
this.getongoingqueue(profileimage_path,first_name,user_master_id)
this.pipe = new DatePipe('en-US');
this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
this.queselecteddate=this.myFormattedDate
this.getongoingqueuetoken()
this.bookingtype="New"
}

createUploadDocuments(complaints,editcheck): FormGroup {
  return this.formBuilder.group({
    complaints:complaints,
    editcheck:editcheck
  });
}


priesDocuments(medicine,qty,duration,frequencs,editpries): FormGroup {
  return this.formBuilder.group({
    medicine:medicine,
    qty:qty,
    duration:duration,
    frequencs:frequencs,
    editpries:editpries,

  });
}



testDocuments(testtype,testname,testdocument): FormGroup {
  return this.formBuilder.group({
    testtype:testtype,
    testname:testname,
    testdocument:testdocument
  });
}







addtest(): void {
  this.testitems.insert(0, this.testDocuments(this.testtype,this.testname,""))
  this.lengthCheckToaddMoretest = this.lengthCheckToaddMoretest + 1;
}

removetest(index: number) {
  this.testitems.removeAt(index);
  this.lengthCheckToaddMoretest = this.lengthCheckToaddMoretest - 1;
}

get testitems(): FormArray {
  return this.priescriptiondocumentGrp.get('testitems') as FormArray;
};






addpresc(): void {
  this.priesitems.insert(0, this.priesDocuments(this.medicine,this.qty,this.duration,this.frequencs,""))
  this.lengthCheckToaddMorepresc = this.lengthCheckToaddMorepresc + 1;
}

removepresc(index: number) {
  this.priesitems.removeAt(index);
  this.lengthCheckToaddMorepresc = this.lengthCheckToaddMorepresc - 1;
}

get priesitems(): FormArray {
  return this.priescriptiondocumentGrp.get('priesitems') as FormArray;
};








addItem(): void {
  this.items.insert(0, this.createUploadDocuments(this.complaints,""))
  this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
}

removeItem(index: number) {
  this.items.removeAt(index);
  this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
}

get items(): FormArray {
  return this.documentGrp.get('items') as FormArray;
};















getongoingqueue(profileimage_path,first_name,user_master_id)
{


this.profileimage_path=profileimage_path
this.first_name=first_name
this.user_master_id=user_master_id

this.selectedproid=user_master_id

this.ongoingqueues.getongoingqueue(user_master_id)
  .subscribe(
  (response) => 
{
    if(response.status_code=='200')
    {
this.providerlist=false

this.ongoingqueuelist=response.Data.todaysongoing
this.todayongoingqueuelist=response.Data.todayslater

  }
    else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
    this.errorMessageongoing=response.Metadata.Message
    }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);
}


getongoingqueuetoken()
{

//this.queue_transaction_id=queue_transaction_id
this.ongoingqueues.getproviderongoingtransactionqueue()
.subscribe(
(response) => 
{
if(response.status_code=='200')
{


//queue_transaction_id
this.queue_transaction_id=response.Data.queue_details[0].queue_transaction_id

this.latertoday=false

this.ongoingqueuelist=false
this.ongoingtab=true
this.queue_details=response.Data.queue_details
this.is_doctor_arrived=response.Data.queue_details[0].is_doctor_arrived

this.queue_id=response.Data.queue_details[0].queue_id

this.ongoingestimated_end_time=response.Data.queue_details[0]['ongoingestimated_end_tim']
  this.ongoingqueue_date=response.Data.queue_details[0]['queue_date']
  this.ongoingprovider_name=response.Data.queue_details[0]['provider_name']
  this.ongoingqueue_pic_url=response.Data.queue_details[0]['profileimage_path']
  this.ongoingqueue_name=response.Data.queue_details[0]['queue_name']

  this.queue_reqque=response.Data.queue_details[0].queue_id

  this.onqueselect(response.Data.queue_details[0].queue_id)

  this.avg_consulting_time=response.Data.avg_consulting_time
  this.avg_waiting_time=response.Data.avg_waiting_time
  this.cancelCompletedTokensCount=response.Data.cancelCompletedTokensCount
  this.checkup_total_amount=response.Data.checkup_total_amount
  this.noshowTokensCount=response.Data.noshowTokensCount
  this.ongoingTokensCount=response.Data.ongoingTokensCount
  this.pendingTokensCount=response.Data.pendingTokensCount
  this.allTokensCount=response.Data.allTokensCount


  this.onoganisationclick(4)
}
else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
  //  this.errorMessageongoing=response.Metadata.Message
  //this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
  
  
  
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);

}





todayestansactionclick(queue_transaction_id)
{
  
this.queue_transaction_id=queue_transaction_id
this.ongoingqueues.getongoingtransactionqueue(queue_transaction_id)
.subscribe(
(response) => 
{



if(response.status_code=='200')
{
this.latertoday=false
this.ongoingqueuelist=false
this.ongoingtab=true
this.queuelist=false
this.queue_details=response.Data.queue_details
this.is_doctor_arrived=response.Data.queue_details[0].is_doctor_arrived
this.ongoingestimated_end_time=response.Data.queue_details[0]['ongoingestimated_end_tim']
  this.ongoingqueue_date=response.Data.queue_details[0]['queue_date']
  this.ongoingprovider_name=response.Data.queue_details[0]['provider_name']
  this.ongoingqueue_pic_url=response.Data.queue_details[0]['profileimage_path']
  this.ongoingqueue_name=response.Data.queue_details[0]['queue_name']
  this.avg_consulting_time=response.Data.avg_consulting_time
  this.avg_waiting_time=response.Data.avg_waiting_time
  this.cancelCompletedTokensCount=response.Data.cancelCompletedTokensCount
  this.checkup_total_amount=response.Data.checkup_total_amount
  this.noshowTokensCount=response.Data.noshowTokensCount
  this.ongoingTokensCount=response.Data.ongoingTokensCount
  this.pendingTokensCount=response.Data.pendingTokensCount
  this.allTokensCount=response.Data.allTokensCount
  this.onoganisationclick(0)
}
else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
  //  this.errorMessageongoing=response.Metadata.Message
  //this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
  
  
  
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);

}













onoganisationclick(tokentype)
{
var tokentypedata
if(tokentype.index=='0')
{
  //tokentypedata="all"
tokentypedata="completedorcancelled"

if(this.completedorcancelledpage==0)
{
  this.page=1
}
else
{

this.page=this.completedorcancelledpage
}


}
else if(tokentype.index=='1')
{
tokentypedata="noshow"

if(this.completedorcancelledpage==0)
{
this.page=1

//this.page=this.noshowpage
}
else
{
//  this.page=1
this.page=this.noshowpage

}

}
else if(tokentype.index=='2')
{
  tokentypedata="ongoing"
//ongoing
if(this.ongoingpage==0)
{
  this.page=1
}
else
{
this.page=this.ongoingpage
}
}
else if(tokentype.index=='3')
{
tokentypedata="pending"
//ongoing
if(this.pendingpage==0)
{
  this.page=1
}
else
{
this.page=this.pendingpage
}
}
else if(tokentype.index=='4')
{
  //tokentypedata="completedorcancelled"
  tokentypedata="all"
//ongoing
if(this.allpage==0)
{
this.page=1
}
else
{
this.page=this.allpage
}
}

else
{
  tokentypedata="all"

if(this.allpage==0)
{
this.page=1

}
else
{
this.page=this.allpage
}



}





this.ongoingqueues.getongoingtransactiontype(tokentypedata,this.queue_transaction_id,this.page-1)
.subscribe(
(response) => 
{


if(response.status_code=='200')
{
if(tokentypedata=="noshow")
{
//this.selectedindex=1
this.noshow=response.Data.queue_tokens

}
else if(tokentypedata=="ongoing")
{

this.ongoing=response.Data.queue_tokens
 //this.selectedindex=2
}
else if(tokentypedata=="pending")
{
//this.selectedindex=3
this.pending=response.Data.queue_tokens
}
else if(tokentypedata=="completedorcancelled")
{
//this.selectedindex=4
this.completedorcancelled=response.Data.queue_tokens
}
else if(tokentypedata=="all")
{
//this.selectedindex=3
this.alldata=response.Data.queue_tokens
}
}
    else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
  //  this.errorMessageongoing=response.Metadata.Message
  //this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
  
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);
}





checkboxchanged(event,transaction_id)
{


if(event.target.checked==true)
{
this.ongoingqueues.markDrArrived(transaction_id)
  .subscribe(
  (response) => 
{

  

    if(response.status_code=='200')
    {
    }
    else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
    this.errorMessageongoing=response.Metadata.Message
    }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);

}
}

  
broadcastmesseg()
{

this.ongoingqueues.transactionQueueMessages((this.broadcastlistingpage - 1),
this.queue_transaction_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
this.ongoingtab=false
this.ongoingqueuelist=false
this.broadcast=true
this.broadcastmesseglist=response.Data.queue_messages
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.broadcasterror=response.Metadata.Message
}
});


}


addbroadcast()
{
this.addbroadcastmessage=true
this.broadcast=false
}

addmessagesms(messege)
{
this.ongoingqueues.addmessagesms(messege.value,this.queue_transaction_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
this.broadcast=true  
this.addbroadcastmessage=false

this.broadcastmesseg()
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.broadcastmsgerror=response.Metadata.Message
}
});
}


gettransactionqueuetoken(queue_transaction_id)
{


 // this.ngProgress.start();
 // this.ngProgress.set(0)
  
this.ongoingqueuelistdatashow=false
  //this.providerdetail=false
  //this.queuelist=true
  this.ongoingqueues.transactionQueueTokens(queue_transaction_id)
  .subscribe(
  (response) => 
  {
  if(response.status_code=='200')
  {



 // this.providerlist=false
 this.latertoday=false
 this.ongoingtab=false
  this.queuelist=true

this.queue_id=response.Data.queue_details[0].queue_id
this.queue_reqque=response.Data.queue_details[0].queue_id
this.onqueselect(response.Data.queue_details[0].queue_id)


  
this.progressbar =((response.Data.total_tokens)/(response.Data.queue_allowed_tokens))
this.total_tokens=response.Data.total_tokens
this.transactionQueueTokens=response.Data.queue_tokens
this.queue_detailstoday=response.Data.queue_details

  
  }
  else if(response.status_code=='402')
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
{

  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
  })

 
//  this.error=response.Metadata.Message
  
}
})
}

onoganisationpagechange(page,tokentype)
{

var tokentypedata




if(tokentype=='completedorcancelled')
{
this.completedorcancelledpage=page
}
else if(tokentype=='noshow')
{
this.noshowpage=page
}
else if(tokentype=='ongoing')
{
this.ongoingpage=page
}
else if(tokentype=='pending')
{
this.pendingpage=page
}
else if(tokentype=='all')
{
this.allpage=page
}


this.ongoingqueues.getongoingtransactiontype(tokentype,this.queue_transaction_id,page-1)
.subscribe(
(response) => 
{

if(response.status_code=='200')
{



if(tokentype=="noshow")
{
//this.selectedindex=1
this.noshow=response.Data.queue_tokens

}
else if(tokentype=="ongoing")
{

this.ongoing=response.Data.queue_tokens
 //this.selectedindex=2
}
else if(tokentype=="pending")
{
//this.selectedindex=3
this.pending=response.Data.queue_tokens
}
else if(tokentype=="completedorcancelled")
{
//this.selectedindex=4
this.completedorcancelled=response.Data.queue_tokens
}
else if(tokentype=="all")
{
//this.selectedindex=3
this.alldata=response.Data.queue_tokens
}




}
    else if(response.status_code=='402')
    {
    localStorage.clear();
    this.router.navigate(['/login']);
    }
    else
    {
  //  this.errorMessageongoing=response.Metadata.Message
  //this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
})
  
  }
  },
  (err: any) => console.log("error",err),
  () =>
  console.log("error in ongoing queue")
);
}



/*
pageChanged(event)
{
this.page=event
this.quesetuplist(this.page)
}
*/
/*new code 4-17-2019 */





tokendetail(token_id)
{
this.searchtoken.gettokendetail("",token_id)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{


  this.token_id=response.Data[0].token_id
  this.transactionqueueid=response.Data[0].queue_transaction_id



this.token_datefordig=response.Data[0].token_date
this.token_timefordig=response.Data[0].token_time
this.booking_for_user_id=response.Data[0].booking_for_user_id





this.ongoingtab=false
//this.searchtokendatadetailshow=false
this.searchtokendatadetailshowdata=true



this.searchtokendatadetail=response.Data


this.onassign_custom_form_values=response.Data[0].onassign_custom_form_values
this.oncomplete_custom_form_values=response.Data[0].oncomplete_custom_form_values



if((response.Data[0].oncomplete_custom_form_values!=null)&&(response.Data[0].onassign_custom_form_values!=null))
{
this.concatarrayforresult=response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values)
}
else if(response.Data[0].oncomplete_custom_form_values!=null)
{
  this.concatarrayforresult=response.Data[0].oncomplete_custom_form_values
}
else if(response.Data[0].onassign_custom_form_values!=null)
{
  this.concatarrayforresult=response.Data[0].onassign_custom_form_values
}
else
{
  this.concatarrayforresult=""
}






this.searchtoken.getcustomlist(response.Data[0].queue_id)
.subscribe(
(response) => 
{




  if(response.status_code=="200")
  {
  

this.customfield=response.Data
  }
  else if(response.status_code=='402')
{
     localStorage.clear();
     this.router.navigate(['/login']);
}
  else 
{
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
  })
}


},
  (err: any) => console.log("error",err),
  () =>
  console.log("error in assign new token response")
);


//this.tokentype=tokentype
}
else if(response.status_code=='402')
{
 localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
}
)
}

markasnoshow(tokenid,queue_transaction_id)
{

if(confirm("Are you sure you want to mark this patient as absent")) {
  this.searchtoken.markasnoshow(tokenid,queue_transaction_id)
  .subscribe(
  (response) => 
  {

if(response.status_code=="200")
{
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })
}
else if(response.status_code=='402')
{
 localStorage.clear();
this.router.navigate(['/login']);
}
else
{
//this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})
}


}

//change 

  markasarrived(tokenid){
    if(confirm("Are you sure you want to mark this patient as arrived")) {
      this.searchtoken.markasarrived(tokenid).subscribe((response) => {
        if(response.status_code=="200"){
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          })
          this.tokendetail(tokenid)
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          //this.error=response.Metadata.Message
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          })
        }
      })
    }
  // tokenMarkArrived
  }


/*
startprocessing(tokenid,transaction_id,token_no)
{

  if(confirm("Are you sure you want to start processing")) {
    this.searchtoken.tokenStartProcessing(tokenid,transaction_id,token_no)
    .subscribe(
    (response) => 
    {
  
  if(response.status_code=="200")
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      })
  
      this.tokendetail(tokenid)
    }
  else if(response.status_code=='402')
  {
   localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
  {
  //this.error=response.Metadata.Message
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  }
  })
  }
}

completeprocessing(queue_id,tokenid,transaction_id)
{

  if(confirm("Are you sure you want to complete processing")) {
    this.searchtoken.tokenProcessingCompleted(queue_id,tokenid,transaction_id)
    .subscribe(
    (response) => 
    {
  
  if(response.status_code=="200")
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      })
  
      this.tokendetail(tokenid)
    }
  else if(response.status_code=='402')
  {
   localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
  {
  //this.error=response.Metadata.Message
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  }
  })
  }


 // tokenProcessingCompleted
}
*/
/*
diegnosisprescription(tokenid)
{

}
*/
changesequence(tokenid,transactionqueueid)
{

//this.modelToggle("open")



this.searchtoken.getTransactionTokenList(transactionqueueid,tokenid)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{




  this.getTransactionTokenList=response.Data
  
  this.modelToggle("open")

  /*
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
*/


}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
//this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})












  //tokenChangeSequence

}

cancelbooking()
{
  this.cancelmodelToggle("open")
}


cancelsubmit(reasonforcancel,token_id,queue_transaction_id)
{
  this.searchtoken.cancelsubmit(reasonforcancel,token_id,queue_transaction_id,this.changeseqtokenid)
  .subscribe(
  (response) => 
  {
  
  if(response.status_code=="200")
  {
  //  this.getTransactionTokenList=response.Data
  
  this.cancelmodelToggle("close")
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })
    this.tokendetail(token_id)
  }
  else if(response.status_code=='402')
  {
  localStorage.clear();
  this.router.navigate(['/login']);
  }
  else
  {
  //this.error=response.Metadata.Message
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  }
  })
  

}







modelToggle(action:string){
  if(action == "open"){
  this.modelFlag = true;
  }else{
  this.modelFlag = false;
  }
  }

  cancelmodelToggle(action:string){
    if(action == "open"){
    this.cancelmodel = true;
    }else{
    this.cancelmodel = false;
    }
    }
  



ontokenChange(tokenid)
{
this.changeseqtokenid=tokenid

}

changesequencesubmit(reson,token_id,transactionid)
{
let position:any = "";
this.searchtoken.changesequencesubmit(reson,token_id,transactionid,this.changeseqtokenid,position)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{
//  this.getTransactionTokenList=response.Data

this.modelToggle("close")
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  })
  this.tokendetail(token_id)
}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
//this.error=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})
}

/*new code 4-17-2019 */



/* new code 4-22-2019*/



reschedulelistsclick(token_id)
{




//this.showalltabs=false
this.requestrceived.getTokenDetails(token_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
  
this.hiddenall=false

this.requestreceived=true
this.getTokenDetailsreshadule=response.Data
var requested_on_date =  response.Data[0].opt1_rescheduling_requested_date
var token_provider_id =  response.Data[0].token_provider_id
this.pipe = new DatePipe('en-US');

this.token_idres = response.Data[0].token_id
this.token_noconres=response.Data[0].token_no


this.customfield=null
this.provideridselected =  response.Data[0].token_provider_id
this.token_datereq=response.Data[0].token_date
this.token_timereq=response.Data[0].token_time
this.queue_namereqexisting=response.Data[0].queue_name
this.newrequested_date=response.Data[0].opt1_rescheduling_requested_date
//const myFormattedDatenew = this.pipe.transform(requested_on_date, 'dd-MM-yyyy');
//this.reshaduledate=myFormattedDatenew
this.queue_reqque=response.Data[0].opt1_rescheduling_requested_queue
this.queue_namereq=response.Data[0].opt1_reshedule_queue_name
this.token_noconres=response.Data[0].opt1_queuename
//this.gettokenlistreshadule(myFormattedDatenew,token_provider_id)
//this.onqueselectreshadule(this.queue_reqque)

}
 else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);

}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}


gettokenlistreshadule(reqdate,token_provider_id)
{


this.requestrceived.gettokenlist(reqdate,token_provider_id)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{



this.quelistdatareshadule=response.Data

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}




onqueselectreshadule(event)
{

this.requestrceived.getmonitorqueQueues(event,this.reshaduledate)
.subscribe(
(response) => 
{

this.queue_reqque=event

this.concatarrayforresult=null






 //this.queue_id=event
if(response.status_code=="200")
{
this.token_nores=response.Data.last_token.token_no
this.token_timeres=response.Data.last_token.token_conveted_time
this.restoken_time=response.Data.last_token.token_time



this.requestrceived.getcustomlist(event)
.subscribe(
(response) => 
{

this.customfield=response.Data
},
(err: any) => console.log("error",err),
() =>
console.log("error in assign new token response")
);




}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})

}



onAssignresDateChanged(event)
{
if(event.formatted!='')
{
  this.requestrceived.gettokenlist(event.formatted,this.provideridselected)
  .subscribe(
  (response) => 
  {
 //this.queue_id=event
  if(response.status_code=="200")
 {


this.quelistdatareshadule=response.Data
this.reshaduledate=event.formatted

  /*
 
 this.reshaduledate=event.formatted
 this.token_nores=response.Data.last_token.token_no
 this.token_timeres=response.Data.last_token.token_conveted_time
 this.restoken_time=response.Data.last_token.token_time
 this.gettokenlistreshadule(this.reshaduledate,this.token_provider_id)
*/


}
 else if(response.status_code=='402')
 {
      localStorage.clear();
      this.router.navigate(['/login']);
 }
 else
 {
 
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 
 }
  })
}
else
{
  this.snackbar.open("Please select Date",'', {
    duration: 1000,
    });
}
}


reshaduleonqueclick()
{

const config = new MatDialogConfig();
config.data = { tokendate: this.reshaduledate,queue_id: this.queue_reqque,token_noexist:this.token_nores};
this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
this.dialogRef.afterClosed().subscribe(value => {
this.token_nores=value.token_no
this.token_timeres=value.token_conveted_time
this.restoken_time=value.token_time
});
}



reshadulemessageclick()
{

  const config = new MatDialogConfig();

  

  config.data = { tokenid: this.token_idres};


  this.dialogRef=this.dialog.open(MessagequelistComponent,config);

  this.dialogRef.afterClosed().subscribe(value => {
  
  })

}
newreqmessageclick()
{

  const config = new MatDialogConfig();

  

  config.data = { tokenid: this.token_id};


  this.dialogRef=this.dialog.open(MessagequelistComponent,config);

  this.dialogRef.afterClosed().subscribe(value => {
  
  })

}



diegnosisprescription(tokenid)
{

  this.tokenidpriscription=tokenid
this.hiddenall=false
this.requestrceived.getTestTypes()
.subscribe(
(response) => 
 {
  if(response.status_code=="200")
  {
    this.testtypelist=response.Data
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
 })




this.requestrceived.getTokenDPRDetails(tokenid)
.subscribe(
(response) => 
 {

if(response.status_code=="200")
{
if(response.Data.token_details!=null)
{
this.token_id=response.Data.token_details[0].token_id
this.token_timefordig=response.Data.token_details[0].token_time
this.token_details=response.Data.token_details[0]
}

if(response.Data.diagnosis_details!=null){
this.diagnosis_details=response.Data.diagnosis_details
this.prescription_details=response.Data.prescription_details
this.test_details=response.Data.test_details
}

if(response.Data.diagnosis_details!=null)
{
for(var i=0;i<(response.Data.diagnosis_details.length);i++){

  this.items.insert(i, this.createUploadDocuments(
    response.Data.diagnosis_details[i].complaint_text,
 ""))
  
  this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
}
}

if(response.Data.prescription_details!=null)
{
for(var i=0;i<(response.Data.prescription_details.length);i++){

  this.priesitems.insert(i, this.priesDocuments(
    response.Data.prescription_details[i].medicine_name,
    response.Data.prescription_details[i].medicine_qty,
    response.Data.prescription_details[i].medicine_duration,
    response.Data.prescription_details[i].medicine_frequency,
 ""))
  this.lengthCheckToaddMorepresc = this.lengthCheckToaddMorepresc + 1;
}
}
if(response.Data.test_details!=null)
{
for(var i=0;i<(response.Data.test_details.length);i++){

  this.testitems.insert(i, this.testDocuments(
    response.Data.test_details[i].test_type_name,
    response.Data.test_details[i].test_master_id,
 ""))
  
  this.lengthCheckToaddMoretest = this.lengthCheckToaddMoretest + 1;
}
}
if(response.Data.prescription_images!=null){
this.prescription_images=response.Data.prescription_images
}
if(response.Data.report_images!=null){
this.report_images=response.Data.report_images
}
this.dignosispriscription=true
}
    else if(response.status_code=='402')
 {
      localStorage.clear();
      this.router.navigate(['/login']);
 }
 else
 {
 
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 
 }
})
}


backtodetail()
{
  this.router.navigate(['/todaysqueue/'],{

    queryParams:{"description":"description",
    "tokenidpriscription":this.tokenidpriscription,
  }, skipLocationChange: true
  //this.tokenidpriscription
  })
}

/* assign new token*/

assigntoken()
{

  this.cancelmodelforassign("open")
  //cancelmodelforassign


}


cancelmodelforassign(action:string){
  if(action == "open"){
    this.cancelmodelforassigndat = true;
  }else{
    this.cancelmodelforassigndat = false;
  }
}




  onSubmitdata(data){
   
    const formData = new FormData();
    if(this.customfield!=undefined){
      let counte=this.customfield.length
      if(this.customfield!=null){
        let counte=this.customfield.length;
        for(let i=0;i<this.customfield.length;i++){
          var datalast=this.customfield[i]['custom_field_id']
          var datalastlist=(data[this.customfield[i]['custom_field_id']])
          var newStr
          // if(datalastlist != undefined){
            // if(datalastlist){
              var multileat="";
              if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                if(datalastlist != undefined){
                  for(let j=0;j<datalastlist.length;j++){
                    multileat+=datalastlist[j].custom_field_value_id+","
                  }
                }
                newStr = multileat.substring(0, multileat.length-1);
              }
              if(this.customfield[i]['custom_field_type']=="List Multi Select"){
                if(newStr!=undefined){
                  formData.append('custom_field_text_value['+i+']',newStr);  
                }
              }else if(this.customfield[i]['custom_field_type']=="Date"){
                var formattedVal:any = "";
                if(datalastlist != undefined){
                  formattedVal = datalastlist.formatted;
                }
                formData.append('custom_field_text_value['+i+']',formattedVal);  
              }else{
                if(datalastlist!=undefined){
                    formData.append('custom_field_text_value['+i+']',datalastlist);  
                }
              }

              if(this.customfield[i]['custom_field_type']=="Date"){
                if(datalastlist != undefined){
                  formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                  formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
                }
              }else if(this.customfield[i]['custom_field_type']=="Amount"){
                if(datalastlist!=undefined){
                  formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                  formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
                  formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);  
                }
              }else{
                if(datalastlist!=undefined){
                  formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);  
                  formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);  
                }
              }
            // }
          // }
        }
      }
    }
    
    if(data.remark!=""){
      formData.append('token_remark',data.remark);  
    }
    if(this.submitdate!=""){
      //this.submitdate
      formData.append('followup_date',this.submitdate);  
    }
    formData.append('booking_type',this.bookingtype);
    this.searchtoken.tokenSaveOnCompleteDetails(this.token_id,this.transactionqueueid,formData).subscribe((response) =>{
      if(response.status_code=="200"){
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        //this.error=response.Metadata.Message
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
      }
    });
}
  
  
  
  onqueselect(event)
  {
  this.assignnewtoken.getmonitorqueQueues(this.myFormattedDate,event)
   .subscribe(
   (response) => 
   {
  this.queue_id=event
   if(response.status_code=="200")
  {
  
  
  
  this.token_no=response.Data.last_token.token_no
  
  
  
  //this.token_no=response.Data.last_token.token_no
  this.token_time=response.Data.last_token.token_conveted_time
  this.token_apitime=response.Data.last_token.token_time
  
  //this.token_time=response.Data.last_token.token_no_converted_slot
  this.token_date=this.queselecteddate
  
  
  
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  
  }
  })
  //custom field list start
  
  this.providerlist=false
  this.assignnewtokenform=true
  this.assignnewtoken.getcustomlist(event)
  .subscribe(
  (response) => 
  {
  
    if(response.status_code=="200")
    {
    
  
  this.customfield=response.Data
    }
    else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
    else 
  {
    this.customfield=null
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
    })
  }
  
  
  },
    (err: any) => console.log("error",err),
    () =>
    console.log("error in assign new token response")
  );
  }
  
 
onAssignDateChanged(event)
{
if(event.formatted!='')
{
this.queselecteddate=event.formatted

this.myFormattedDate=event.formatted

this.assignnewtoken.getProviderQueues(event.formatted,this.selectedproid)
.subscribe(
(response) => 
{



if(response.status_code=="200")
{
 // this.quelistdata=response.Data
this.queue_reqque=""
 this.quelistdata=""
  this.token_no=null
  this.token_time=null
  this.token_apitime=null
  this.token_date=null
  this.queue_id=""
  this.customfield = null
this.quelistdata=response.Data

this.userlogin.resetForm();


}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{

  this.quelistdata=[]
this.token_time=""
this.token_no=""
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
}
else
{
  this.snackbar.open("Please select Date",'', {
    duration: 1000,
    });
}
}

  

onqueclick()
{
const config = new MatDialogConfig();
config.data = { tokendate: this.queselecteddate,queue_id: this.queue_id,token_noexist:this.token_no};
this.dialogRef=this.dialog.open(DialogmonitorqueComponent,config);
this.dialogRef.afterClosed().subscribe(value => {
this.token_no=value.token_no
this.token_time=value.token_conveted_time
this.token_apitime=value.token_time


});


}




assignnow(mobilenumber)
{

  //this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');


this.bookingusername=mobilenumber


if(this.bookingtypeselectedtype=="Self")
{

  this.bookedfor="self"
  this.bookedforself=true
  this.assignnewtoken.getQTRAQUserDetails(mobilenumber,this.selectedproid)
  .subscribe(
  (response) => 
  {



  if(response.status_code=='200')
  {

 
  this.searchfield=true
  this.assignsearchdiv=true
  this.hiddenall=false
  
  this.providerlist=false
  this.assignsearchdiv=true




this.regno=response.Data[0]['reg_no']
this.usermasterid=response.Data[0]['user_master_id']
this.searchmobileage=response.Data[0]['age']
this.searchfirst_name=response.Data[0]['first_name']
this.usergendermale=response.Data[0]['gender']
this.userprofileimg=response.Data[0]['profile_pic_path']

this.datedata(this.myFormattedDate)
this.onqueselect(this.queue_id)

this.cancelmodelforassign("closefamily")

  }
  else if(response.status_code=='402')
  {
      localStorage.clear();
      this.router.navigate(['/login']);
  }
  else
  {
   // this.bookingusername=""
    this.searchmobileage=""
    this.searchfirst_name=""
    this.usergendermale=false
    this.usergenderfemale=false
    this.usergenderOther=false
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      });
  }
  },
    (err: any) => console.log("error",err),
    () =>
    console.log("error in assign new token response")
  );



}
else if(this.bookingtypeselectedtype=="Family")
{
//this.familymemeberidfordetail






this.assignnewtoken.getQTRAQfamilymemberuserdetail(this.mobilesearchmasterid,
  this.familymemeberidfordetail)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{

  this.bookedfor=response.Data[0]['member_type']




//this.bookingusername=response.Data[0]['member_first_name']

//this.searchmobileage=response.Data[0]['age']
//this.searchfirst_name=response.Data[0]['first_name']
this.regno=response.Data[0]['reg_no']



if(response.Data[0]['member_type']=="family_member_without_qtraqid")
{
  this.usermasterid=response.Data[0]['family_member_id']
}
else
{
this.usermasterid=response.Data[0]['family_member_user_id']
}



this.hiddenall=false
//this.searchmobileage=
this.searchfirst_name=response.Data[0]['member_first_name']
this.usergendermale=response.Data[0]['member_gender']
this.userprofileimg=response.Data[0]['member_pic_path']


//this.searchmobileage=20
/*

if(response.Data[0]['gender']=='Male')
{
this.usergendermale=true
}
else if(response.Data[0]['gender']=='Female')
{
this.usergenderfemale=true
}
else if(response.Data[0]['gender']=='Other')
{
this.usergenderOther=true
}
*/

this.searchbookinguserid = response.Data[0]['family_member_id']


//closefamily

//this.cancelmodelforassign("close")

this.assignsearchdiv=true
this.providerlist=false
this.assignsearchdiv=true
this.searchfield=true

this.datedata(this.myFormattedDate)
this.onqueselect(this.queue_id)

this.cancelmodelforassign("closefamily")



}


else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{
  this.bookingusername=""
  this.searchmobileage=""
  this.searchfirst_name=""
  this.usergendermale=false
  this.usergenderfemale=false
  this.usergenderOther=false
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("error in assign new token response")
);
}
else if(this.bookingtypeselectedtype=="other")
{

//this.bookingtypeselectedtype=="Self"

this.bookedother=true

this.bookedfor="self"
this.bookedforself=true

this.cancelmodelToggle("close")

this.assignsearchdiv=true
//this.providerlist=false
this.assignsearchdiv=true
this.searchfield=true
this.datedata(this.myFormattedDate)
}
}



/*

assignnow(mobilenumber)
{

this.bookingusername=mobilenumber


if(this.bookingtypeselectedtype=="Self")
{

  this.bookedfor="self"
  this.bookedforself=true
  this.assignnewtoken.getQTRAQUserDetails(mobilenumber,this.selectedproid)
  .subscribe(
  (response) => 
  {



  if(response.status_code=='200')
  {

 
  this.searchfield=true
  this.assignsearchdiv=true
  this.hiddenall=false
  this.cancelmodelforassign("close")
  this.providerlist=false
  this.assignsearchdiv=true




this.regno=response.Data[0]['reg_no']
this.usermasterid=response.Data[0]['user_master_id']
this.searchmobileage=response.Data[0]['age']
this.searchfirst_name=response.Data[0]['first_name']
this.usergendermale=response.Data[0]['gender']
this.userprofileimg=response.Data[0]['profile_pic_path']

this.datedata(this.myFormattedDate)
this.onqueselect(this.queue_id)



  }
  else if(response.status_code=='402')
  {
      localStorage.clear();
      this.router.navigate(['/login']);
  }
  else
  {
   // this.bookingusername=""
    this.searchmobileage=""
    this.searchfirst_name=""
    this.usergendermale=false
    this.usergenderfemale=false
    this.usergenderOther=false
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      });
  }
  },
    (err: any) => console.log("error",err),
    () =>
    console.log("error in assign new token response")
  );



}
else if(this.bookingtypeselectedtype=="Family")
{


this.assignnewtoken.getQTRAQfamilymemberuserdetail(this.mobilesearchmasterid,
  this.familymemeberidfordetail)
.subscribe(
(response) => 
{
if(response.status_code=='200')
{
  this.bookedfor=response.Data[0]['member_type']

this.regno=response.Data[0]['reg_no']
if(response.Data[0]['member_type']=="family_member_without_qtraqid")
{
  this.usermasterid=response.Data[0]['family_member_id']
}
else
{
this.usermasterid=response.Data[0]['family_member_user_id']
}
this.hiddenall=false
//this.searchmobileage=
this.searchfirst_name=response.Data[0]['member_first_name']
this.usergendermale=response.Data[0]['member_gender']
this.userprofileimg=response.Data[0]['member_pic_path']
this.searchbookinguserid = response.Data[0]['family_member_id']
this.cancelmodelforassign("close")
this.assignsearchdiv=true
this.providerlist=false
this.assignsearchdiv=true
this.searchfield=true

this.datedata(this.myFormattedDate)

}


else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{
  this.bookingusername=""
  this.searchmobileage=""
  this.searchfirst_name=""
  this.usergendermale=false
  this.usergenderfemale=false
  this.usergenderOther=false
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("error in assign new token response")
);
}
else if(this.bookingtypeselectedtype=="other")
{
this.bookedother=true
this.bookedfor="self"
this.bookedforself=true
this.cancelmodelToggle("close")
this.assignsearchdiv=true
this.assignsearchdiv=true
this.searchfield=true
this.datedata(this.myFormattedDate)
}
}
*/








datedata(event)
{
//if(event.formatted!='')
//{
//this.queselecteddate=event.formatted
this.assignnewtoken.getProviderQueues(event,this.selectedproid)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{
  this.quelistdata=response.Data



}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
}



cancelsubmitdata(mobilenumber)
{

  this.assignnewtoken.providergetQTRAQUserDetails(mobilenumber)
  .subscribe(
  (response) => 
  {





  if(response.status_code=='200')
  {


this.assignbuttonshow=true

this.bookingtypeselectedtype="Self"

this.tokenrequestorno=mobilenumber





this.mobilesearchmasterid=response.Data[0].user_master_id
this.selfname=response.Data[0].first_name
 this.selfnumber=response.Data[0].user_name
  this.selfimage=response.Data[0].profile_pic_path
  this.bookingfordropdownvis=true

  }
else if(response.status_code=='400')
{
  this.tokenrequestorno=mobilenumber
  this.bookingtypeselectedtype="other"
}
})
}

onbookingchange(event)
{
if(event=="Family")
{
this.assignnewtoken.getQTRAQfamilymember(this.mobilesearchmasterid)
.subscribe(
(response) => 
{


this.bookingtypeselectedtype="Family"

if(response.status_code=='200')
{
this.familymember=response.Data

this.familymemberselfshow=false

this.familymembershow=true

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
   });
}
})
}
else if(event=="Self")
{
this.familymembershow=false
this.familymemberselfshow=true
this.bookingtypeselectedtype="Self"


}

}


familymemberchackboxcheck(masterid)
{




this.familymemeberidfordetail=masterid
}



public OnSubmitprescription(formValue: any) 
{

let main_form: FormData = new FormData();
for (let k = 0; k < this.items.length-1; k++) {
  main_form.append("complaint_text["+k+"]",formValue.items[k].complaints)
  main_form.append("entry_type["+k+"]","D")
  
}

main_form.append("tokenid",this.token_id)
main_form.append("consumerid",this.booking_for_user_id)
  
main_form.append("tokendate",this.token_datefordig)
main_form.append("tokentime",this.token_timefordig)
main_form.append("remarks",formValue.complaints_text)

//main_form.append("remarks",formValue.complaints_text)
 


this.assignnewtoken.dignosissubmit(main_form,formValue,this.token_id,
  this.token_datefordig,this.token_timefordig,this.booking_for_user_id)
.subscribe(
(response) => 
{
  if(response.status_code=='200')
  {

    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
     });


  }
  else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
  else
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
     });
  }
})
}


/*assign new token end*/







public OnSubmitprescriptiondata(formValue: any) 
{





let main_form: FormData = new FormData();

for (let k = 0; k < this.priesitems.length-1; k++) {
  main_form.append("medicine_name["+k+"]",formValue.priesitems[k].medicine)
  main_form.append("medicine_qty["+k+"]",formValue.priesitems[k].qty)
  main_form.append("medicine_frequency["+k+"]",formValue.priesitems[k].frequencs)
  main_form.append("medicine_duration["+k+"]",formValue.priesitems[k].duration)
}



for (let i = 0; i < this.testitems.length-1; i++) {

 // main_form.append("test_master_id["+i+"]",formValue.testitems[i].testname)
 // main_form.append("test_name["+i+"]",formValue.testitems[i].testtype)
  
 main_form.append("test_master_id["+i+"]",formValue.testitems[i].testtype)
 main_form.append("test_name["+i+"]",formValue.testitems[i].testname)
 

}
main_form.append("tokenid",this.token_id)
main_form.append("consumerid",this.booking_for_user_id)
  
main_form.append("tokendate",this.token_datefordig)
main_form.append("tokentime",this.token_timefordig)
main_form.append("remarks",formValue.complaints_text)





this.assignnewtoken.prescriptionsubmit(main_form,formValue,this.token_id,
  this.token_datefordig,this.token_timefordig,this.booking_for_user_id)
.subscribe(
(response) => 
{
  if(response.status_code=='200')
  {

    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
     });


  }
  else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
  else
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
     });
  }
})



}



broadcastbuttontapped()
{
  this.broadcast=false
  this.ongoingtab=true
  this.ongoingqueuelist=true
  this.getongoingqueuetoken()
}


broadcastbuttonbrod()
{
  this.addbroadcastmessage=false
  this.broadcast=true

}

onbookingchangedata(event)
{
this.bookingtype = event
}



submitdateChanged(event)
{
  if(event.formatted!='')
  {
this.submitdate=event.formatted
  }
 
}


backbuttontabbed()
{
  this.searchtokendatadetailshowdata=false
  this.getongoingqueuetoken()
}


//reshadule token click start 4-4-2019

onreshaduleSubmit(data)
{
if(this.queue_reqque!=undefined)
{
const formData = new FormData();

if(this.customfield!=undefined)
{

 
  let counte=this.customfield.length



if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr



if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}


}
}
}
}
}
formData.append('tokenmsg',data.tokenmessageres);  


formData.append('tokenno',this.token_nores);  
formData.append('tokendate',this.reshaduledate);  
formData.append('tokentime',this.restoken_time);  

formData.append('requesttype','reschedule');  



this.requestrceived.confirmtokenres(formData,this.token_nores,this.restoken_time,this.reshaduledate,this.token_idres,this.queue_reqque,data.tokenmessageres)
.subscribe(
(response) => 
{
if(response.status_code=="200")
{

this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
});
this.router.navigate(['/']);

}
else if(response.status_code=='402')
{
localStorage.clear();
this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
}
})
}
else
{
  this.snackbar.open("please select Queue",'', {
    duration: 2000,
    });
}
}
//reshadule token end 4-4-2019



backbuttontappedtoday()
{
this.assignsearchdiv=false
this.hiddenall=true
this.ongoingtab=true
this.queuelist=false
//this.getongoingqueuetoken(this.queue_transaction_id)
}



onSubmit(data)
{

if(data.firstname!=undefined)
{

const formData = new FormData();

if(this.customfield!=undefined)
{

 
  let counte=this.customfield.length



if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr



if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true))
{

  

if(datalastlist==""){

this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {
  duration: 2000,
  });
return false
}
}



if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}


}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}






if(this.queue_id!=undefined)
{
  this.assignnewtoken.submitassignquecustomforunknown(
    formData,this.queue_id,this.selectedproid,
    this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
    this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
    data.firstname,data.regno,this.usermasterid,data.gender,
    data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
  )
   .subscribe(
   (response) => 
   {




  this.queue_id=event




   if(response.status_code=="200")
   {
  
  this.router.navigate(['/']);
  
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
  
  }
  else if(response.status_code=='402')
  {
       localStorage.clear();
       this.router.navigate(['/login']);
  }
  else
  {
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  })
}
else
{
this.snackbar.open("please select queue",'', {
duration: 2000,
})
}

}
else
{






const formData = new FormData();

if(this.customfield!=undefined)
{

 
let counte=this.customfield.length












if(this.customfield!=null)
{
let counte=this.customfield.length



for(let i=0;i<this.customfield.length;i++)
{
var datalast=this.customfield[i]['custom_field_id']
var datalastlist=(data[this.customfield[i]['custom_field_id']])
var newStr


if((this.customfield[i]['custom_field_type']=="List Multi Select")&&(this.customfield[i]['is_mandatory']==true))
{

  

if(datalastlist==""){

this.snackbar.open("please select multiselect field  "+this.customfield[i]['custom_label'],'', {
  duration: 2000,
  });
return false
}
}


if(datalastlist != undefined)
{
if(datalastlist)
{
var multileat=""
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{
for(let j=0;j<datalastlist.length;j++)
{
  multileat+=datalastlist[j].custom_field_value_id+","
}
newStr = multileat.substring(0, multileat.length-1);
}
if(this.customfield[i]['custom_field_type']=="List Multi Select")
{



/*
if((this.customfield[i]['is_mandatory']==true)&&(newStr==undefined))
{

  this.snackbar.open("please select multiselect field",'', {
    duration: 2000,
    });

}
*/
//else{
if(newStr!=undefined){
formData.append('custom_field_text_value[]',newStr);  
}
//}

}
else if(this.customfield[i]['custom_field_type']=="Date")
{


if(datalastlist.formatted!=undefined){
formData.append('custom_field_text_value[]',datalastlist.formatted);  
}
}
else
{
  if(datalastlist!=undefined){
formData.append('custom_field_text_value[]',datalastlist);  
  }
}

if(this.customfield[i]['custom_field_type']=="Date")
{
if(datalastlist.formatted!=undefined)
{
formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
}
}
else{
  if(datalastlist!=undefined)
  {
  formData.append('custom_field_id[]',this.customfield[i]['custom_field_id']);  
  formData.append('custom_field_type[]',this.customfield[i]['custom_field_type']);  
  }

}
}
}
}
}
}



if(data.age==0)
{
  this.snackbar.open("please provide Age",'', {
    duration: 2000,
    });

return false
}




if(this.queue_id!=undefined)
{
  
this.assignnewtoken.submitassignquecustom(
  formData,this.queue_id,this.selectedproid,
  this.mobilesearchmasterid,this.selfname,this.tokenrequestorno,
  this.token_no,this.token_apitime,this.queselecteddate,this.bookedfor,
  this.searchfirst_name,data.regno,this.usermasterid,this.usergendermale,
  data.age,this.bookingtype,data.tokenmsg,this.bookingtypeselectedtype
)
 .subscribe(
 (response) => 
 {
this.queue_id=event




 if(response.status_code=="200")
 {

this.router.navigate(['/']);

this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });

}
else if(response.status_code=='402')
{
     localStorage.clear();
     this.router.navigate(['/login']);
}
else if(response.status_code=="400")
{
this.assignsearchdiv=true
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    })
}
else 
{


  this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
})
}
})




}
else
{
  this.snackbar.open("please select queue",'', {
    duration: 2000,
    })

}
}

}



/*new code end 4-22-2019*/
}






