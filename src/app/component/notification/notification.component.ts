import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CommonService } from 'app/_services';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  notificationDetails:any = [];
  page:any = 0;
  moreFlag:any = true;
  constructor(
    private commonService:CommonService,
    private snackbar:MatSnackBar
  ) { 
  }

  ngOnInit() {
    this.getTokenNotifications();
  }

  getTokenNotifications(){
    this.page = this.page+1;
    this.commonService.getTokenNotifications(this.page).subscribe((response)=>{
      if(response.status_code == "200"){
        if(this.page <= 1){
          this.notificationDetails = response.Data; 
        }else{
          if ( this.notificationDetails instanceof Array ){
            this.notificationDetails = this.notificationDetails.concat( response.Data );
          }else{
            this.notificationDetails.push( response.Data );
          }
        }
      }else{
        this.snackbar.open(response.Metadata.Message, '', {duration: 2000});
        this.moreFlag = false;
      }
    });
  }

}
