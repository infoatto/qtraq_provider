import { Component, OnInit, trigger, transition, style, animate } from '@angular/core';
import { SidebarToggleService } from 'app/_services/sidebar-toggle.service';
import { OrglistComponent } from '../orglist/orglist.component';
import { RolelistComponent } from '../rolelist/rolelist.component';
import { MatDialog } from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss'],
})
export class SiteComponent implements OnInit {

  constructor(
    private router:Router
  ) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         // trick the Router into believing it's last link wasn't previously loaded
         this.router.navigated = false;
         // if you need to scroll back to top, here is the right place
         window.scrollTo(0, 0);
      }
    });
  }

ngOnInit() {


}
}
