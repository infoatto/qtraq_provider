import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { SearchtokensService } from './../../_services/searchtokens.service';
import { TodaysqueueService } from 'app/_services/todaysqueue.service';

@Component({
 selector: 'app-searchtokens',
 templateUrl: './searchtoken.component.html',
 styleUrls: ['./searchtoken.component.scss']
})
export class SearchtokensComponent implements OnInit {

 /*
   constructor() { }
   ngOnInit() {
   }
 */

 /*   prescription start 6-20-2019    */

  public totalfiles: Array<File> = [];
  patienthistoryaddmore: Boolean = false
  showformfield: Boolean = false
  image_name: any
  /*priscription end  6-20-2019*/
  public lengthCheckToaddMore = 0;
  concatarrayforresult: any
  submitdate: any
  requestreceived: any
  dropdownSettings = {};
  dialogRef: any
  dropdownSettingsnewdata = {};
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };

  getTransactionTokenList: any
  public systemroleform: FormGroup;
  public searchtokendatadetailshow: Boolean = false
  public searchtokendatadetailshowdata: Boolean = false
  changeseqtokenid: any
  requestsreceived: any;
  totalRecords = "";
  page = 0;
  size = 10;
  searchtokendata: any;
  searchtokendatadetail: any
  error = ""
  tokentype: any
  modelFlag: Boolean = false
  onassign_custom_form_values: any
  oncomplete_custom_form_values: any
  dignosispriscription: Boolean = false
  cancelmodel: Boolean = false
  public documentGrp: FormGroup;
  //new code for reshadule
  getTokenDetailsreshadule: any
  pipe: any
  token_idres: any
  token_noconres: any
  token_datereq: any
  token_timereq: any
  queue_namereqexisting: any
  newrequested_date: any
  preascription_title: any
  reshaduledate: any

  queue_reqque: any
  queue_namereq: any
  //token_noconres
  //queue_reqque
  token_nores: any
  token_timeres: any
  restoken_time: any
  customfield: any
  quelistdatareshadule: any
  searchtokenallshow: Boolean = true
  provideridselected: any
  token_id: any
  transactionqueueid: any
  formdata: any
  patienthistorydata: any
  token_details: any
  diagnosis_details: any
  prescription_details: any
  test_details: any
  prescription_images: any
  report_images: any
  public totalFileName = [];
  user_data_from_token_details:any = "";
 //disableUntil: {year: 2016, month: 8, day: 10}
 constructor(
  private searchtoken: SearchtokensService,
  private formBuilder: FormBuilder,
  private router: Router,
  private snackbar: MatSnackBar,
  public dialog: MatDialog,
  public route: ActivatedRoute,
  public todaysQueuesService:TodaysqueueService
) { 
  this.user_data_from_token_details = this.todaysQueuesService.getSharedData();
}
ngOnInit() {
  this.documentGrp =
   this.formBuilder.group({
    documentFile: new FormControl(File),
    items: this.formBuilder.array([this.createUploadDocuments(this.preascription_title, this.image_name)]),
   });
  this.dignosispriscription = false
  //flag start
  this.modelFlag = false
  this.cancelmodel = false
  this.searchtokenallshow = true
  //flage end

  this.searchtokendatadetailshow = true


  this.page = 1;


  this.dropdownSettings = {
   singleSelection: false,
   idField: 'custom_field_value_id',
   textField: 'custom_list_text',
   selectAllText: 'Select All',
   unSelectAllText: 'UnSelect All',
   itemsShowLimit: 3,
   allowSearchFilter: true
  };



  this.dropdownSettingsnewdata = {
   singleSelection: false,
   idField: 'custom_field_value_id',
   textField: 'custom_list_text',
   selectAllText: 'Select All',
   unSelectAllText: 'UnSelect All',
   itemsShowLimit: 3,
   allowSearchFilter: true
  };



  this.systemroleform = this.formBuilder.group({
    patient: [this.user_data_from_token_details],
    tokentype: "upcoming",
  })
  if(this.user_data_from_token_details != ""){
    this.OnsystemroleSubmit(this.systemroleform.value);
  }
  if ((this.route.snapshot.queryParamMap.get('searchtoken')) != null) {
    //this.backbuttontapped()
    var formdata = this.route.snapshot.queryParamMap.get('searchformdata')
    let page = this.route.snapshot.queryParamMap.get('searchpage')
    let fromdate = this.route.snapshot.queryParamMap.get('fromdate')
    let patient = this.route.snapshot.queryParamMap.get('patient')
    let todate = this.route.snapshot.queryParamMap.get('todate')
    let tokentypesearch = this.route.snapshot.queryParamMap.get('tokentypesearch')
    this.systemroleform = this.formBuilder.group({
      patient: patient,
      tokentype: tokentypesearch,
    })

   var pagenew = Number(page)
   this.Onsystemrolegetdata(this.systemroleform.value, pagenew)
  }
  //this.systemroleform.value.patient="s"
 }


 createUploadDocuments(preascription_title, image_name): FormGroup {
  return this.formBuilder.group({
   preascription_title: preascription_title,
   image_name: image_name,
  });
 }



 addPrescription(): void {
  this.items.insert(0, this.createUploadDocuments(this.preascription_title, this.image_name))
  this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
 }

 get items(): FormArray {
  return this.documentGrp.get('items') as FormArray;
 };

 Onsystemrolegetdata(formValue, page) {

  this.searchtoken.tokensearchlist((page - 1), this.size, formValue)
   .subscribe(
    (response) => {
     if (response.status_code == "200") {
      this.searchtokendata = response.Data
      this.totalRecords = response.total_count
      this.page = page
      this.error = ""
     }
     else if (response.status_code == '402') {
      localStorage.clear();
      this.router.navigate(['/login']);
     }
     else {
      //this.error=response.Metadata.Message
      this.snackbar.open(response.Metadata.Message, '', {
       duration: 2000,
      })
      this.searchtokendata=[]
      this.totalRecords=''
      this.page=1


     }
    }
   )
 }




  OnsystemroleSubmit(formValue: any) {
      // this.systemroleform.patient
      this.page = 1
      this.formdata = formValue
      this.Onsystemrolegetdata(formValue, this.page)
  }


 pageChanged(page) {

  this.Onsystemrolegetdata(this.systemroleform.value, page)
  // this.newtokenlist(event,this.tokenproviderid)
  //this.getnotificationlist(event,this.notificationfilter)
 }


 tokendetail(tokentype, token_id) {

  this.router.navigate(['/todaysqueue/'], {
   queryParams: {
    "tokenidsearch": token_id,
    "patient": this.systemroleform.value.patient,
    "tokentype": this.systemroleform.value.tokentype,
    "searchpage": this.page
   }, skipLocationChange: true
  });
 }


 modelToggle(action: string) {
  if (action == "open") {
   this.modelFlag = true;
  } else {
   this.modelFlag = false;
  }
 }

 cancelmodelToggle(action: string) {
  if (action == "open") {
   this.cancelmodel = true;
  } else {
   this.cancelmodel = false;
  }
 }

 backtohome()
 {
   this.router.navigate(['/'])
 }
 /* ravi new code */
 //resadule submit end


checkbox(type,textvalue){


this.systemroleform.value.patient=textvalue
this.systemroleform.value.tokentype=type

this.page = 1

this.Onsystemrolegetdata(this.systemroleform.value,this.page)



 }



}
