import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchtokensComponent } from './searchtoken.component';

describe('SearchtokensComponent', () => {
  let component: SearchtokensComponent;
  let fixture: ComponentFixture<SearchtokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchtokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchtokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
