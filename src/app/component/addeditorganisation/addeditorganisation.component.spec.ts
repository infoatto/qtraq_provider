import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditorganisationComponent } from './addeditorganisation.component';

describe('AddeditorganisationComponent', () => {
  let component: AddeditorganisationComponent;
  let fixture: ComponentFixture<AddeditorganisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditorganisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditorganisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
