import { Component, OnInit, Renderer, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { OrganisationsService } from './../../_services/organisations.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { MatSnackBar } from '@angular/material';
import { SidebarToggleService } from 'app/_services/sidebar-toggle.service';
import { CommonService } from "./../../_services/common.service";
//declare var test:any;

@Component({
    selector: 'app-addeditorganisation',
    templateUrl: './addeditorganisation.component.html',
    styleUrls: ['./addeditorganisation.component.css', './addeditorganisation.component.scss']
})
export class AddeditorganisationComponent implements AfterViewInit {
    ngAfterViewInit(): void {
       // test()
    }
    orglog:any
    toggleVal: boolean = false;
    custom: any = {
        mon: "0",
        tue: "0",
        wen: "0",
        thu: "0",
        fri: "0",
        sat: "0",
        sun: "0",
        org_logo: ""
    }
    modelcatlogo:Boolean=false
    myOptions: INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    };

/*        variable for image delete start         */
deleteimageindex:any
deleteorg_image_id:any
deleteimagename:any
deleteimagecatindex:any
deleteorg_catimage_id:any
deleteimagecatname:any
productLoading:Boolean=false
showorgindex:number
showorgindexcat:number
/*  variable for image delete start  */

//delete data start
deleteindex:any
deleteday:any
deletestart:any
deleteend:any

workinghouresdelete:Boolean=false

//delete data end


    contactlocation: any =
        {
            mobile1public: "0",
            mobile2public: "0",
            phone1public: "0",
            phone2public: "0",
            emailid1public: "0",
            emailid2public: "0"
        }
        contactsubmitdata=false
    onconvalueget = ""
    organizationType:any
    organisationdataid = 0

    //4-1-2019
    dob_date = ""
    submobile = ""
    otperrormsg = ""
    useraddresponse = ""
    //4-1-2019
    org_id = 0
    imageposition = ""
    org_image_id = ""
    suberror = ""
    dob: any
    dob1: any
publicorgname:any
    //ravi new code 2-25-2019
    org_catelog_images_id: any
    org_catelog_name: any
    org_catelog_imageposition: any
    org_catelog_imgurl: any
    //ravi new code end 2-25-2019

    org_working_hour_id = ""
    datetime_status = ""
    image_status = ""

    image_name = ""
    imagetype = ""
    start_time = ""
    end_time = ""
    working_day = ""
    dropdownSettings = {};
    responsedata = ""
    organisationid = 0
    docimage = ""
    orglist: any
    //new code 1-3-2019
    planlist: any
    listSubscribedPlans: any
    listexistinguser: any
    //new code end 1-3-2019
orgtypepost:any
    citylist: any
    statelist: any
    getCountries: any
    specialities: any
    locationDisabled: boolean = true;
    ContactDisabled: boolean = true;
    SubscriptionDisabled: boolean = true;
    UserDisabled: boolean = true;

    selectedindex = 0;
    public show: boolean = false;
    public subplan: boolean = true;
    public subplanlist: boolean = false;
    public userform: boolean = false


//mobile validate start
mobile1validate:boolean = false
mobile2validate:boolean = false
phone1validate:boolean = false
phone2validate:boolean = false
emailid1validate:boolean = false
emailid2validate:boolean = false 
//mobile validate end 




    public userformsearch: boolean = true

    constructor(private renderer: Renderer,
        private formBuilder: FormBuilder,
        private orgservice: OrganisationsService,
        private router: Router,
        private route: ActivatedRoute,
        private snackbar: MatSnackBar,
        private sideToggleService:SidebarToggleService,
        private cs:CommonService  

    ) { }
    public documentGrp: FormGroup;
    public locationgroup: FormGroup;
    public contactgroup: FormGroup;
    public Subscription: FormGroup
    public usergroup: FormGroup;

    public mobilesearch: FormGroup;
    public contactsubmit: FormGroup;

    public totalfiles: Array<File> = [];
    public totalFileName = [];
    selectedItems = [];

    //ravi new code 2-25-2019 start
    public catfiles: Array<File> = [];
    public totalcatFileName = [];
    catselectedItems = [];
    //ravi new code end 2-25-2019

    public lengthCheckToaddMore = 0;
    public lengthCheckToaddMore1 = 0;


    //ravi new code 2-25-2019
    public catalogimg = 0;
    modelFlag:Boolean=false
    modelcatToggle:Boolean=false
    //ravi new code start 2-25-2019

parentorgname:any
parentorgcity:any


    org_specialities:
        {
            org_image_id: ""
            image_type: ""
            image_position: ""
        }

        mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$"; 

    ngOnInit() {
        this.productLoading=true 

        this.getOrganizationType()


        this.documentGrp = this.formBuilder.group({
            org_name: '',
            orgtype: '',
            org_logo: '',
            hidden_org_logo:'',
            ordanisationid: '',
            address1: '',
            address2: '',
            street: '',
            area: '',
            landmark: '',
            pincode: '',
            countryid: '',
            monworking: 0,
            tueworking: 0,
            wedworking: 0,
            thuworking: 0,
            friworking: 0,
            state_id: 0,
            satworking: 0,
            sunworking: 0,
            orgcheckbox: 0,
            skills: [
                [], Validators.required
            ],
            imagecatFile: new FormControl(File),

            documentFile: new FormControl(File),
            items: this.formBuilder.array([this.createUploadDocuments(this.org_image_id, this.image_name,
                this.imageposition, this.imagetype, this.docimage, this.image_status, "editcall")]),

            org_catelog_images: this.formBuilder.array([this.org_catelog_imagesdata(this.org_catelog_images_id,
                this.org_catelog_name, this.imageposition, this.org_catelog_imgurl, "editcall")]),



            doctorshadule: this.formBuilder.array([this.createdocDatetime(this.start_time, this.end_time,
                this.working_day, this.datetime_status, this.org_working_hour_id, "editcall")])


        });

        this.mobilesearch = this.formBuilder.group({
            mobilenumber: ""
        })

        this.usergroup = this.formBuilder.group({
            mobilenumber: "",
            firstname: "",
            middlename: "",
            lastname: "",
            dob: "",
            otp: "",
            gender: "",
            countryid: "",
            stateid: "",
            cityid: "",
            pincode: "",
            emailid: "",
            isorgdefault: "",
            qtraquserid: ""
        })


        this.locationgroup = this.formBuilder.group({
            countryid: "",
            state_id: "",
            city_id: "",
            gpscords:[''],
            address1: "",
            address2: "",
            street: "",
            area: "",
            landmark: "",
            pincode: ""
        })

        this.contactgroup = this.formBuilder.group({
            mobile1:['', Validators.pattern(this.mobnumPattern)],
            mobile1public: 0,
            mobile2:['', Validators.pattern(this.mobnumPattern)],
            mobile2public: 0,
            phone1:['', Validators.pattern(this.mobnumPattern)],
            phone1public: 0,
            phone2:['', Validators.pattern(this.mobnumPattern)],
            phone2public: 0,
            emailid1: "",
            emailid1public: 0,
            emailid2: "",
            emailid2public: 0,
            website: ""
        })

        this.getorglist()
      
        this.getcountry()
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'speciality_id',
            textField: 'speciality_name',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
        //edit part start



        this.route.params.subscribe(
            params => {
                if (this.route.snapshot.params.orgid) {
                    this.orgservice.geteditorganisation(this.route.snapshot.params.orgid)
                        .subscribe(
                            data => {
let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
if (data.status_code == 200) {


if(data.Data.org_details[0].parent_data!="")
{
this.parentorgname=data.Data.org_details[0].parent_data[0].org_name
this.parentorgcity=data.Data.org_details[0].parent_data[0].city_name
}


    this.productLoading=false 

this.org_id = this.route.snapshot.params.orgid

this.getSpecialities()


if(data.Data.org_details[0].org_logo!="")
{
this.orglog=data.Data.org_details[0].org_logo
}

this.documentGrp.controls['org_name'].setValue(data.Data.org_details[0].org_name);
this.documentGrp.controls['orgtype'].setValue(data.Data.org_details[0].org_type_name);
this.orgtypepost=data.Data.org_details[0].org_type
//this.orgtypeselectedValue = data.Data.org_details[0].org_type;
if ((data.Data.org_details[0].parent_id) != 0) {
                                        this.documentGrp.controls['orgcheckbox'].setValue(true)
                                        this.show = true
                                        this.documentGrp.controls['ordanisationid'].setValue(data.Data.org_details[0].parent_id);
                                    }

                                    this.documentGrp.controls['monworking'].setValue(data.Data.org_details[0].is_mon_working);
                                    this.documentGrp.controls['tueworking'].setValue(data.Data.org_details[0].is_tue_working);
                                    this.documentGrp.controls['wedworking'].setValue(data.Data.org_details[0].is_wed_working);
                                    this.documentGrp.controls['thuworking'].setValue(data.Data.org_details[0].is_thu_working);
                                    this.documentGrp.controls['friworking'].setValue(data.Data.org_details[0].is_fri_working);
                                    this.documentGrp.controls['satworking'].setValue(data.Data.org_details[0].is_sat_working);
                                    this.documentGrp.controls['sunworking'].setValue(data.Data.org_details[0].is_sun_working);
                                    this.selectedItems = data.Data.org_specialities
                                    this.custom.org_logo = data.Data.org_details[0].org_logo_url
                 
   this.documentGrp.controls['hidden_org_logo'].setValue(data.Data.org_details[0].org_logo);
  
              
                               
                               if(data.Data.org_images!=null)
                               {
                                    for (var i = 0; i < (data.Data.org_images.length); i++) {
                                        this.items.insert(i, this.createUploadDocuments(
                                            data.Data.org_images[i].org_image_id,
                                            data.Data.org_images[i].image_name,
                                            data.Data.org_images[i].image_position,
                                            data.Data.org_images[i].image_type,
                                            data.Data.org_images[i].org_image_url, data.Data.org_images[i].image_status, ""))
                                      
                                        this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
                                    }
                                }

                                    //ravi new code start 2-25-2019
if(data.Data.org_catelog_images!=null)
{
                                    for (var k = 0; k < (data.Data.org_catelog_images.length); k++) {
                                        this.org_catelog_images.insert(k, this.org_catelog_imagesdata(
                                            data.Data.org_catelog_images[k].org_image_id,
                                            data.Data.org_catelog_images[k].image_name,
                                            data.Data.org_catelog_images[k].image_position,
                                            data.Data.org_catelog_images[k].org_image_url,
                                            ""))
                                        this.catalogimg = this.catalogimg + 1;
                                    }
                                }



                                    //ravi new code end  2-25-2019


                                    /*
                                    for (var j = 0; j < (data.Data.org_working_hrs.length); j++) {
                                        this.doctorshadule.insert(j, this.createdocDatetime(
                                            data.Data.org_working_hrs[j].start_time,
                                            data.Data.org_working_hrs[j].end_time,
                                            data.Data.org_working_hrs[j].working_day,
                                            data.Data.org_working_hrs[j].working_hr_status,
                                            data.Data.org_working_hrs[j].org_working_hour_id,
                                            ""
                                        ))
                                        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
                                    }

*/

if(data.Data.org_working_hrs != null){
    for(var j=0;j<(data.Data.org_working_hrs.length);j++){

if(data.Data.org_working_hrs[j].working_day=="Mon")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Monday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }
    if(data.Data.org_working_hrs[j].working_day=="Tue")
    {
                            this.doctorshadule.insert(j,this.createdocDatetime(
                                data.Data.org_working_hrs[j].start_time,
                                data.Data.org_working_hrs[j].end_time,
                                "Tuesday",
                                data.Data.org_working_hrs[j].working_hr_status,
                                data.Data.org_working_hrs[j].org_working_hour_id,
                                ""
                            ))
                            this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
                        }
                        if(data.Data.org_working_hrs[j].working_day=="Wed")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Wednesday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }
    if(data.Data.org_working_hrs[j].working_day=="Thu")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Thursday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }
    if(data.Data.org_working_hrs[j].working_day=="Fri")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Friday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }
    if(data.Data.org_working_hrs[j].working_day=="Sat")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Saturday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }
    if(data.Data.org_working_hrs[j].working_day=="Sun")
{
        this.doctorshadule.insert(j,this.createdocDatetime(
            data.Data.org_working_hrs[j].start_time,
            data.Data.org_working_hrs[j].end_time,
            "Sunday",
            data.Data.org_working_hrs[j].working_hr_status,
            data.Data.org_working_hrs[j].org_working_hour_id,
            ""
        ))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }           
    
    
    }
}




                                    this.locationDisabled = false
                                    this.ContactDisabled = false
                                    this.SubscriptionDisabled = false;
                                    this.UserDisabled = false;

if(data.Data.org_location_info[0].country_details!=null)
{
this.onconChange(data.Data.org_location_info[0].country_details[0].country_id)    
}
                                    
if(data.Data.org_location_info[0].state_details!=null)
{
this.onstateChange(data.Data.org_location_info[0].state_details[0].state_id)
}
this.locationgroup.controls['address1'].setValue(data.Data.org_location_info[0].address_1);
this.locationgroup.controls['address2'].setValue(data.Data.org_location_info[0].address_2);
this.locationgroup.controls['street'].setValue(data.Data.org_location_info[0].street);
this.locationgroup.controls['area'].setValue(data.Data.org_location_info[0].area);
this.locationgroup.controls['landmark'].setValue(data.Data.org_location_info[0].landmark);
                                    this.locationgroup.controls['pincode'].setValue(data.Data.org_location_info[0].pincode);
                                    this.locationgroup.controls['gpscords'].setValue(data.Data.org_location_info[0].gps_cords);
                           
                                   
                                   
                                    if(data.Data.org_location_info[0].country_details!=null)
                                    {
                                    this.locationgroup.controls['countryid'].setValue(data.Data.org_location_info[0].country_details[0].country_id);
                                    }
                                    
                                    if(data.Data.org_location_info[0].state_details!=null)
                                    {
                                    this.locationgroup.controls['state_id'].setValue(data.Data.org_location_info[0].state_details[0].state_id);
                                    }
                                    
                                    if(data.Data.org_location_info[0].city_details!=null)
                                    {
                                    this.locationgroup.controls['city_id'].setValue(data.Data.org_location_info[0].city_details[0].city_id);
                                    }
                       if(data.Data.org_contact_info[0]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['mobile1'].setValue(data.Data.org_contact_info[0]['phone_no']);
                                    }
                                    
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['mobile1public'].setValue(data.Data.org_contact_info[0]['is_visibility']);
                                    //}
                                    
                                    if(data.Data.org_contact_info[1]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['mobile2'].setValue(data.Data.org_contact_info[1]['phone_no']);
                                    }
                                    
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['mobile2public'].setValue(data.Data.org_contact_info[1]['is_visibility']);
                                    //}
                                    
                                    if(data.Data.org_contact_info[2]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['phone1'].setValue(data.Data.org_contact_info[2]['phone_no']);
                                    }
                                    
                                    
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['phone1public'].setValue(data.Data.org_contact_info[2]['is_visibility']);
                                    //}
                                    
                                    if(data.Data.org_contact_info[3]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['phone2'].setValue(data.Data.org_contact_info[3]['phone_no']);
                                    }
                                    
                                    
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['phone2public'].setValue(data.Data.org_contact_info[3]['is_visibility']);
                                    //}
                                    if(data.Data.org_contact_info[4]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['emailid1'].setValue(data.Data.org_contact_info[4]['phone_no']);
                                    }
                                    
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['emailid1public'].setValue(data.Data.org_contact_info[4]['is_visibility']);
                                    //}
                                    
                                    if(data.Data.org_contact_info[5]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['emailid2'].setValue(data.Data.org_contact_info[5]['phone_no']);
                                    }
                                    //if(data.Data.org_contact_info[0]['contact']!='')
                                    //{
                                    this.contactgroup.controls['emailid2public'].setValue(data.Data.org_contact_info[5]['is_visibility']);
                                    //}
                                    
                                    if(data.Data.org_contact_info[6]['phone_no']!=null)
                                    {
                                    this.contactgroup.controls['website'].setValue(data.Data.org_contact_info[6]['phone_no']);
                                    }
                                           }
                                else if (data.status_code == '402') {
                                    localStorage.clear();
                                    this.router.navigate(['/login']);

                                }
                                else {
                                    this.snackbar.open(data.Metadata.Message, '', {
                                        duration: 2000,
                                    });
                                }




                            })
                }
            })
        this.sideToggleService.gettoggleVal().subscribe(response=>{
            this.toggleVal = response;
        })
    }
    //ravi new code 19-12-2018
    createdocDatetime(start_time, end_time, working_day, datetimestatus, org_working_hour_id, editcheck): FormGroup {
        return this.formBuilder.group({
            timestart: start_time,
            timeend: end_time,
            timeday: working_day,
            datetime_status: datetimestatus,
            org_working_hour_id: org_working_hour_id,
            editcheck: editcheck
        });
    }

    //org_image_id,image_name

    createUploadDocuments(org_image_id, image_name, imageposition, imagetype, docimage, imagestatus, editcheck): FormGroup {
        return this.formBuilder.group({
            org_image_id: org_image_id,
            image_name: image_name,
            image_position: imageposition,
            image_type: imagetype,
            documentFile: docimage,
            image_status: imagestatus,
            editcheck: editcheck
        });
    }


    //ravi new code 2-25-2019

    org_catelog_imagesdata(org_catelog_images_id, org_catelog_name, imageposition, imageurl,
        editcheck): FormGroup {
        return this.formBuilder.group({
            org_image_id: org_catelog_images_id,
            image_name: org_catelog_name,
            image_position: imageposition,
            imagecatFile: imageurl,
            editcheck: editcheck
        });
    }

    //ravi new code end 2-25-2019

    getcountry() {
        this.orgservice.getCountries().subscribe(response => {
            this.getCountries = response.Data
        });
    }

    onconChange(val) {

        this.onconvalueget = val
        this.orgservice.getState(val).subscribe(response => {
            this.statelist = response.Data
        });
    }

    onstateChange(val) {


        this.orgservice.getcity(val).subscribe(response => {
            this.citylist = response.Data
        });
    }

    getorglist() {
        this.orgservice.getallorganisation().subscribe(response => {
            this.orglist = response.Data
        });
    }


    getSpecialities() {

        this.orgservice.getSpecialities(this.org_id).subscribe(response => {
            this.specialities = response.Data
        });
    }

//delete image start
deleteimage(index:number,org_image_id,imagename)
{
this.showorgindex=index+1
    this.deleteimageindex=index
    this.deleteorg_image_id=org_image_id
    this.deleteimagename=imagename
    this.modelToggle("open")
}


confirm()
{
this.removeItemexist(this.deleteimageindex,this.deleteorg_image_id, this.deleteimagename)
}

cancel()
{
    this.modelToggle("canel")   
}

deleteimageno()
{
    this.deleteimageindex=""
    this.deleteorg_image_id=""
    this.deleteimagename=""
}


modelToggle(action:string){

    if(action == "open"){
    this.modelFlag = true;
        }else{
            this.modelFlag = false;
            }
            }
    
    
    modelcatTogglefun(action:string)
    {
    
        if(action == "open"){
            this.modelcatToggle = true;
                }else{
                    this.modelcatToggle = false;
                    }
    
    }









//delete image end

    adddocDatetime(): void {

        /*
        this.doctorshadule.insert(0, this.createdocDatetime(this.start_time, this.end_time, this.working_day, this.datetime_status, this.org_working_hour_id, ""))
        */
        
if((this.start_time!="")&&(this.end_time!=""))
{
  this.doctorshadule.insert(0, this.createdocDatetime(this.start_time,
  this.end_time,this.working_day,this.datetime_status,this.org_working_hour_id,""))
  }
 else
 {

  

    this.doctorshadule.insert(0, this.createdocDatetime("12:00:00",
        "23:00:00",this.working_day,this.datetime_status,this.org_working_hour_id,""))
 }          
       
        
        
        
        
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    
    
    
    }

    removedocDatetime(index: number) {
        this.doctorshadule.removeAt(index);
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 - 1;
    }


    //ravi new code 2-25-2019 start

    addcatelogimg(): void {
        this.org_catelog_images.insert(0, this.org_catelog_imagesdata(this.org_catelog_images_id, this.org_catelog_name, this.imageposition, this.org_catelog_imgurl, ""))
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 + 1;
    }

    removecatelogimg(index: number) {
        this.org_catelog_images.removeAt(index);
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 - 1;
    }
    get org_catelog_images(): FormArray {
        return this.documentGrp.get('org_catelog_images') as FormArray;
    };
    //ravi new code end 2-25-2019 end


    get items(): FormArray {
        return this.documentGrp.get('items') as FormArray;
    };


    get doctorshadule(): FormArray {
        return this.documentGrp.get('doctorshadule') as FormArray;
    };


    //ravi new code start 2-25-2019


    addorgcatimg(): void {
        this.org_catelog_images.insert(0, this.org_catelog_imagesdata(this.org_catelog_images_id,
            this.org_catelog_name,
            this.imageposition, this.org_catelog_imgurl, "tampimage"))
        this.catalogimg = this.catalogimg + 1;
    }

    removeorgcatimg(index: number) {
        this.catfiles.splice(index);
        this.totalcatFileName.splice(index);
        this.org_catelog_images.removeAt(index);
        this.catalogimg = this.catalogimg - 1;
    }








    removeItemexist(index: number, org_image_id, imagename) {
        //if(confirm("Are you sure you want to delete this image")) {

        if ((org_image_id != '')) {
            this.orgservice.deleteimage(this.org_id, org_image_id).subscribe(data => {
                if (data.status_code == 200) {
                    this.totalfiles.splice(index);
                    this.totalFileName.splice(index);
                    this.items.removeAt(index);
                    this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
                    this.snackbar.open(data.Metadata.Message, '', {
                        duration: 2000,
                    });
                }
                else if (data.status_code == 402) {
                    localStorage.clear();
                    this.router.navigate(['/login']);

                }
                else {
                    this.snackbar.open(data.Metadata.Message, '', {
                        duration: 2000,
                    });
                }
            })
        }
        else {
            this.totalfiles.splice(index);
            this.totalFileName.splice(index);
            this.items.removeAt(index);
            this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
        }
        this.modelToggle("canel")
    //}


        //removeItemexist
    }



    removeorgcatimgdata(index: number, org_image_id, imagename) {
    
    
      //  if(confirm("Are you sure you want to delete this image")) {

        if ((org_image_id != null)) {
           this.orgservice.deleteimage(this.org_id, org_image_id).subscribe(data => {
                if (data.status_code == 200) {
                    this.catfiles.splice(index);
                    this.totalcatFileName.splice(index);
                    this.org_catelog_images.removeAt(index);
                    this.catalogimg = this.catalogimg - 1;
                    this.snackbar.open(data.Metadata.Message, '', {
                        duration: 2000,
                    });
                }
                else if (data.status_code == 402) {
                    localStorage.clear();
                    this.router.navigate(['/login']);
                }
                else {
                    this.snackbar.open(data.Metadata.Message, '', {
                        duration: 2000,
                    });
                }
            })
        }
        else {

            this.catfiles.splice(index);
            this.totalcatFileName.splice(index);
            this.org_catelog_images.removeAt(index);
            this.catalogimg = this.catalogimg - 1;
        }
        this.modelcatTogglefun("canel")  
   // }
    }





    deleteworking(index:number,start,end,day)
    {
    
        this.deleteindex=index
        this.deleteday=day
        this.deletestart=start
        this.deleteend=end
      
        this.workinghourfun("open")
    
    }
    
    
    deleteworkingconform()
    {
    
    
    
        this.doctorshadule.removeAt(this.deleteindex);
        this.lengthCheckToaddMore1 = this.lengthCheckToaddMore1 - 1;
        this.workinghourfun("close")
    
    }
    
    deleteworkingcancel()
    {
        this.workinghourfun("cancel")  
    }


    workinghourfun(action:string)
    {
    
        if(action == "open"){
            this.workinghouresdelete = true;
                }else{
                    this.workinghouresdelete = false;
                    }
    
    }
    



    //ravi new code 2-26-2019 end





    removeravicall(index: number) {
        
    }



    //ravi new code end 2-25-2019

    addItem(): void {
        this.items.insert(0, this.createUploadDocuments(this.org_image_id, this.image_name, this.imageposition, this.imagetype, this.docimage, this.image_status, ""))
        this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
    }

    removeItem(index: number) {
        this.totalfiles.splice(index);
        this.totalFileName.splice(index);
        this.items.removeAt(index);
        this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
    }

    public fileSelectionEvent(fileInput: any, oldIndex) {
        if((fileInput.target.files[0].type=="image/jpeg")||(fileInput.target.files[0].type=="image/jpg")||(fileInput.target.files[0].type=="image/png"))
{
        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (event: any) => { }
            if (oldIndex == 0) {
                this.totalfiles.unshift((fileInput.target.files[0]))
                this.totalFileName.unshift(fileInput.target.files[0].name)
            } else {
                // let main_form: FormData = new FormData();
                this.totalfiles[oldIndex] = (fileInput.target.files[0]);
                if (this.org_id != 0) {
                    this.totalFileName[oldIndex - 1] = fileInput.target.files[0].name
                }
                else {
                    this.totalFileName[oldIndex] = fileInput.target.files[0].name
                }
            }
            reader.readAsDataURL(fileInput.target.files[0]);
        }
        if (this.totalfiles.length == 1) {
            this.lengthCheckToaddMore = 1;
        }
    }
    else
    {
        this.snackbar.open("image not supported",'', {
            duration: 2000,
            verticalPosition:"top",
            extraClasses: ['bg-red']
        });
    }

    }

    //ravi new code 2-25-2019 start

    public catlogimgSelectionEvent(catfileInput: any, catoldIndex) {

        if((catfileInput.target.files[0].type=="image/jpeg")||(catfileInput.target.files[0].type=="image/jpg")||(catfileInput.target.files[0].type=="image/png"))
        {
        if (catfileInput.target.files && catfileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (event: any) => { }
            if (catoldIndex == 0) {
                this.catfiles.unshift((catfileInput.target.files[0]))
                this.totalcatFileName.unshift(catfileInput.target.files[0].name)
            } else {
                // let main_form: FormData = new FormData();
                this.catfiles[catoldIndex] = (catfileInput.target.files[0]);
                if (this.org_id != 0) {
                    this.totalcatFileName[catoldIndex - 1] = catfileInput.target.files[0].name
                }
                else {
                    this.totalcatFileName[catoldIndex] = catfileInput.target.files[0].name
                }
            }
            reader.readAsDataURL(catfileInput.target.files[0]);
        }
        if (this.catfiles.length == 1) {
            this.catalogimg = 1;
        }
    }
    else
    {
        this.snackbar.open("image not supported",'', {
            duration: 2000,
            verticalPosition:"top",
            extraClasses: ['bg-red']
        });
    }


    }






    //ravi new code end 2-25-2019 end


    onFileChanged(event) {
        this.custom.org_logo = event.target.files[0]
    }


    isAnswerProvided(eveb) {
    }



    notifyMe(event) {

    }

    addprop1(event) {

        if (event.target.checked) {
            this.show = true

        } else {
            this.show = false
        }
    }

    public OnSubmit(formValue: any) {
        if (formValue.monworking) {
            this.custom.mon = "1"
        }
        if (formValue.tueworking) {
            this.custom.tue = "1"
        }
        if (formValue.wedworking) {
            this.custom.wen = "1"
        }
        if (formValue.thuworking) {
            this.custom.thu = "1"
        }
        if (formValue.friworking) {
            this.custom.fri = "1"
        }
        if (formValue.satworking) {
            this.custom.sat = "1"
        }
        if (formValue.sunworking) {
            this.custom.sun = "1"
        }
        let main_form: FormData = new FormData();
        var imglen = 0
        if (this.org_id == 0) {
            imglen = this.items.length
        }
        else {
            imglen = this.items.length - 1
        }
        //ravi new code start 2-25-2019
        var catlen = 0
        if (this.org_id == 0) {
            catlen = this.org_catelog_images.length
        }
        else {
            catlen = this.org_catelog_images.length - 1
        }
       
if(imglen!=0)
{



for (let j = 0; j < imglen; j++) {

    if(< File > this.totalfiles[j])
    {
    main_form.append("org_image_type["+j+"]", "org_pic")
    main_form.append("org_image_position["+j+"]", formValue.items[j]['image_position'])
    main_form.append("org_image_id["+j+"]", formValue.items[j]['org_image_id'])
    main_form.append("org_image_hidden["+j+"]", formValue.items[j]['image_name'])
    if(< File > this.totalfiles[j]){
        main_form.append("org_image["+j+"]",< File > this.totalfiles[j])
    }  
}

}
}

if(catlen!=0)
{
        for (let k = 0; k < catlen; k++) {

            if(< File > this.catfiles[k])
            {
            main_form.append("catelog_image_type["+k+"]",'service_catelog')
            main_form.append("catelog_image_position["+k+"]", formValue.org_catelog_images[k]['image_position'])
            if(formValue.org_catelog_images[k]['org_image_id']!=null){
                main_form.append("catelog_image_id["+k+"]", formValue.org_catelog_images[k]['org_image_id'])
            }

            if(formValue.org_catelog_images[k]['image_name']!=null){
                main_form.append("catelog_image_hidden["+k+"]", formValue.org_catelog_images[k]['image_name'])
            }
            if(< File > this.catfiles[k]){
                main_form.append("catelog_image["+k+"]",< File > this.catfiles[k])
            } 
        } 


        }
}



        var timedate = 0
        if (this.org_id == 0) {
            timedate = this.doctorshadule.length
        }
        else {
            timedate = this.doctorshadule.length - 1
        }
        
       if(formValue.skills!=null)
       {
       if(formValue.skills.length!=0)
       {
        for (let k = 0; k < (formValue.skills.length); k++) {
            main_form.append("speciality_id[]", formValue.skills[k]['speciality_id'])
        }
    }
}

        main_form.append("org_logo", this.custom.org_logo)
        main_form.append("hidden_org_logo", formValue.hidden_org_logo)


        main_form.append("orgname", formValue.org_name)

     


       //     localStorage.removeItem('actorganistion');    
       // localStorage.setItem('actorganistion',"")


       if(this.custom.org_logo!="")
     {
        var orgactive=
        {
        "org_name":formValue.org_name,
        "org_image":this.custom.org_logo,
        }
        localStorage.setItem('actorganistion', JSON.stringify(orgactive));
    }
    else
    {
        let existdata = JSON.parse(localStorage.getItem('orguserrole'));
        let imgorg=existdata.orgimg
        var orgactive=
        {
        "org_name":formValue.org_name,
        "org_image":imgorg,
        }
        localStorage.setItem('actorganistion', JSON.stringify(orgactive));
}


let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
    


        this.orgservice.organisation(this.org_id, main_form, formValue, this.custom,this.orgtypepost).subscribe(data => {
            if (data.status_code == 200) {

                this.selectedindex = 1

/*                
                this.snackbar.open(data.Metadata.Message, '', {
                    duration: 2000,
                });
  */          
 this.snackbar.open(data.Metadata.Message,'', {
       verticalPosition:"top",
       duration:2000,
    extraClasses: ['bg-green']
});         
            }
            else if (data.status_code == 402) {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
            else {
          this.snackbar.open(data.Metadata.Message,'', {
            duration: 2000,
            verticalPosition:"top",
            extraClasses: ['bg-red']
        });
            }
        })
    }


    //ravi new code 2-27-2019

    /*
    OnWorkingSubmit(formValue: any)
    {
    }
    */
    OnSubmitWorking(formValue: any) {
        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }
        let main_form: FormData = new FormData();
        var timedate = 0
        if (this.org_id == 0) {
            timedate = this.doctorshadule.length
        }
        else {
            timedate = this.doctorshadule.length - 1
        }

        for (let t = 0; t < (timedate); t++) {
            if(formValue.doctorshadule[t]['timestart']!=""){
var days=""

if(formValue.doctorshadule[t]['timeday']=="Monday")
{
     days="Mon"
}else if(formValue.doctorshadule[t]['timeday']=="Tuesday")
{
    days="Tue"
}
else if(formValue.doctorshadule[t]['timeday']=="Wednesday")
{
    days="Wed"
}
else if(formValue.doctorshadule[t]['timeday']=="Thursday")
{
    days="Thu"
}
else if(formValue.doctorshadule[t]['timeday']=="Friday")
{
    days="Fri"
}
else if(formValue.doctorshadule[t]['timeday']=="Saturday")
{
    days="Sat"
}
else if(formValue.doctorshadule[t]['timeday']=="Sunday")
{
    days="Sun"
}
               main_form.append("org_working_day["+t+"]", days)
                main_form.append("org_start_time["+t+"]", formValue.doctorshadule[t]['timestart'])
                main_form.append("org_end_time["+t+"]", formValue.doctorshadule[t]['timeend'])
            }
        }


        this.orgservice.saveworkinghoures(this.organisationdataid, main_form).subscribe(data => {
            if (data.status_code == 200) {
                this.router.navigate(['/']);
               
this.snackbar.open(data.Metadata.Message, '', {
    verticalPosition:"top",
    duration:2000,
    extraClasses: ['bg-green']
  });



            }
            else {
                this.snackbar.open(data.Metadata.Message, '', {
                    duration: 2000,
                });
            }
        })


    }


    //ravi new code end 2-27-2019


    public LocationSubmit(formValue: any) {
        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }
        this.orgservice.editorganisationlocation(this.organisationdataid, formValue).subscribe(data => {
            if (data.status_code == 200) {
                this.selectedindex = 2
                this.ContactDisabled = false
            }
            else if (data.status_code == '402') {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
            else {
                this.snackbar.open(data.Metadata.Message, '', {
                    verticalPosition:"top",
                    extraClasses: ['bg-red']
                  });
            }
        })
    }
    //set  second box start


    public ContactSubmit(formValue: any) {


        this.contactsubmitdata = true;

        if(this.contactgroup.invalid)
        {
        return;
        }
       

        if (formValue.mobile1public) {
            this.contactlocation.mobile1public = "1"
        }else{
            this.contactlocation.mobile1public = "0"
      

        }



        if (formValue.mobile2public) {
            this.contactlocation.mobile2public = "1"
        }else{
            this.contactlocation.mobile2public = "0"
            
        }


        if (formValue.phone1public) {
            this.contactlocation.phone1public = "1"
        }else{
            this.contactlocation.phone1public = "0"
            
        }
        if (formValue.phone2public) {
            this.contactlocation.phone2public = "1"
        }else{
            this.contactlocation.phone2public = "0"
      

        }

        if (formValue.emailid1public) {
            this.contactlocation.emailid1public = "1"
        }else{
            this.contactlocation.emailid1public = "0"
            
        }

        if (formValue.emailid2public) {
            this.contactlocation.emailid2public = "1"
        }else{
            this.contactlocation.emailid2public = "0"
            
        }


        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }




        this.orgservice.editorganisationcontact(this.organisationdataid, formValue, this.contactlocation).subscribe(data => {
            if (data.status_code == 200) {

              
               this.snackbar.open(data.Metadata.Message, '', {
                verticalPosition:"top",
                duration:2000,
                extraClasses: ['bg-green']
                
                });
                this.selectedindex = 3
              
            
            }
            else if (data.status_code == '402') {
                localStorage.clear();
                this.router.navigate(['/login']);

            }
            else {


                this.snackbar.open(data.Metadata.Message, '', {
                    verticalPosition:"top",
                    duration:2000,
                    extraClasses: ['bg-red']
                });
                
                
             
            }
        })

    }


onoganisationclick(org) {
    }


    statuschangeimage(testid, status) {
        this.orgservice.updateimagestatus(testid, status)
            .subscribe(
                data => {
                    if (data.status_code == 200) {

                    }
                    else {

                    }
                })
    }

    //statuschangedatetime

    statuschangedatetime(testid, status) {
        this.orgservice.updatedatetimestatus(testid, status)
            .subscribe(
                data => {
                    if (data.status_code == 200) {

                    }
                    else {

                    }
                })

    }

    planlistdata() {
        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }
        this.orgservice.getplandescription(this.organisationdataid)
            .subscribe(
                data => {
                    if (data.status_code == 200) {
                        this.subplan = false
                        this.subplanlist = true
                        this.planlist = data.Data
                    }
                    else if (data.status_code == '402') {
                        localStorage.clear();
                        this.router.navigate(['/login']);

                    }
                    else {
                    }
                })

    }


    sublist() {

        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }
        this.subplan = true
        this.subplanlist = false
        this.orgservice.listSubscribedPlans(this.organisationdataid)
            .subscribe(
                data => {
                    if (data.status_code == 200) {
                        //this.planlist=  
                        this.listSubscribedPlans = data.Data
                    }
                    else if (data.status_code == '402') {
                        localStorage.clear();
                        this.router.navigate(['/login']);

                    }
                    else {
                    }
                })

    }

    divclick(planid, startdate, enddate) {
        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }

        this.orgservice.SubscribedPlans(this.organisationdataid, planid, startdate, enddate)
            .subscribe(
                data => {


                    if (data.status_code == 200) {

                        this.snackbar.open(data.Metadata.Message, '', {
                            verticalPosition:"top",
                            extraClasses: ['bg-green']

                        })

                        this.selectedindex = 4

                    } else if (data.status_code == '402') {
                        localStorage.clear();
                        this.router.navigate(['/login']);

                    }

                    else {
                        this.snackbar.open(data.Metadata.Message, '', {
                            verticalPosition:"top",
                            extraClasses: ['bg-red']

                        })

                       // this.suberror = data.Metadata.Message
                    }
                })



    }


    OnSearch(formValue: any) {

        this.orgservice.getQTRAQUserDetails(formValue.mobilenumber)
            .subscribe(
                data => {
                    if (data.status_code == 200) {

                        this.userformsearch = false
                        this.userform = true

                        this.onconChange(data.Data[0].country_id)
                        this.onstateChange(data.Data[0].state_id)
                        this.usergroup.controls['mobilenumber'].setValue(data.Data[0].user_name);
                        this.usergroup.controls['firstname'].setValue(data.Data[0].first_name);
                        this.usergroup.controls['middlename'].setValue(data.Data[0].middle_name);
                        this.usergroup.controls['lastname'].setValue(data.Data[0].last_name);

                        this.usergroup.controls['pincode'].setValue(data.Data[0].pincode);
                        this.usergroup.controls['countryid'].setValue(data.Data[0].country_id);
                        this.usergroup.controls['stateid'].setValue(data.Data[0].state_id);
                        this.usergroup.controls['cityid'].setValue(data.Data[0].city_id);

                        this.usergroup.controls['emailid'].setValue(data.Data[0].email_id);

                        this.usergroup.controls['isorgdefault'].setValue(data.Data[0].system_user);
                        this.usergroup.controls['gender'].setValue(data.Data[0].gender);


                        this.usergroup.controls['qtraquserid'].setValue(data.Data[0].user_master_id);


                        this.onconvalueget = data.Data[0].country_id

                        this.submobile = data.Data[0].user_name

                        this.dob = data.Data[0].dob.split('-');

                        this.dob1 = {
                            date: {
                                year: parseInt(this.dob[0]),
                                month: parseInt(this.dob[1]),
                                day: parseInt(this.dob[2])
                            },
                        }
                        this.usergroup.controls['dob'].setValue(this.dob1);

                    }
                    else if (data.status_code == '402') {
                        localStorage.clear();
                        this.router.navigate(['/login']);

                    }

                    else {


                        this.usergroup.controls['mobilenumber'].setValue(formValue.mobilenumber);

                        this.submobile = formValue.mobilenumber

                        this.userformsearch = false
                        this.userform = true
                        //this.selectedindex=4
                    }
                })

    }

    OnUsersubmit(formValue: any) {
        if (this.org_id != 0) {
            this.organisationdataid = this.org_id
        }
        else {
            this.organisationdataid = this.organisationid
        }
        this.orgservice.addQTRAQUserDetails(formValue, this.dob_date, this.organisationdataid)
            .subscribe(
                data => {
                    if (data.status_code == 200) {
                        this.router.navigate(['/']);
                    }
                    else if (data.status_code == '402') {
                        localStorage.clear();
                        this.router.navigate(['/login']);

                    }
                    else {
                        this.useraddresponse = data.Metadata.Message
                    }
                })
    }


    carconfirm()
    {
    this.removeorgcatimgdata(this.deleteimagecatindex,this.deleteorg_catimage_id, this.deleteimagecatname)
    }
    
    catcancel()
    {
   this.modelcatTogglefun("canel")   
    }
    
    

    genetarotp() {



        this.orgservice.generateOtp(this.onconvalueget, this.submobile)
            .subscribe(
                data => {
                    if (data.status_code == 200) {
                        //otp
                       // this.usergroup.controls['otp'].setValue(data.otp);
                        //  this.selectedindex=4
                        this.snackbar.open(data.Metadata.Message, '', {
                            verticalPosition:"top",
                            extraClasses: ['bg-green']

                        })

                    }
                    else {


                        this.snackbar.open(data.Metadata.Message, '', {
                            verticalPosition:"top",
                            extraClasses: ['bg-green']

                        })


                        //this.otperrormsg = data.Metadata.Message
                        // this.userformsearch=false
                        // this.userform=true
                    }
                })
    }


    onStartDateChanged(event: IMyDateModel): void {
        if (event.formatted !== '') {
            //    this.start_date=event.formatted
            this.dob_date = event.formatted
        }

    }


    getOrganizationType(){
        this.orgservice.getOrganizationType().subscribe((response)=>{
            this.organizationType = response.Data
        })
    }

    deletecatimage(index:number,org_image_id,imagename)
    {
    //catmodelFlag
    this.showorgindexcat=index+1
    this.deleteimagecatindex=index
    this.deleteorg_catimage_id=org_image_id
    this.deleteimagecatname=imagename
    this.modelcatTogglefun("open")
    }
    


//new code 5-22-2019 start


deleteorg()
{
this.logofun("open")
}

logoconfirm()
{
this.orgservice.deleteorglogo(this.org_id).subscribe((response)=>{
if(response.status_code=="200")
{
this.logofun("cancel")
this.custom.org_logo=null
this.orglog=null



var orgactive=
{
"org_name":this.publicorgname,
"org_image":"",
}
localStorage.setItem('actorganistion', JSON.stringify(orgactive));



this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
        verticalPosition:"top",
        extraClasses: ['bg-green']
    });
}
else
{
    this.snackbar.open(response.Metadata.Message,'', {
        duration: 2000,
        verticalPosition:"top",
        extraClasses: ['bg-red']
    });
}
})
}


logocancel()
{
    this.logofun("cancel")
}



logofun(action:string)
{

    if(action == "open"){
        this.modelcatlogo = true;
            }else{
                this.modelcatlogo = false;
                }

}



backtohome()
{
  this.cs.backtohome()
}




emailid1change(event)
{
  if(event.target.checked==true)
  {
  this.emailid1validate=true
  }
  else
  {
    this.emailid1validate=false
  }

}
emailid2check(event)
{

  if(event.target.checked==true)
  {
  this.emailid2validate=true
  }
  else
  {
    this.emailid2validate=false
  }
}

mobile1change(event)
{
  if(event.target.checked==true)
  {
  this.mobile1validate=true
  }
  else
  {
    this.mobile1validate=false
  }

}

mobile2change(event)
{

  if(event.target.checked==true)
  {
  this.mobile2validate=true
  }
  else
  {
    this.mobile2validate=false
  }
}

phone2change(event)
{

if(event.target.checked==true)
{
this.phone2validate=true
}
else
{
 this.phone2validate=false
}

}















//new code 5-22-2019 end

    //set second box end

}