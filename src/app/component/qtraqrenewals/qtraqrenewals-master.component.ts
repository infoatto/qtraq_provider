import { Component, OnInit,Renderer2 } from '@angular/core';
import { QueuerenewalsService } from './../../_services/queuerenewals.service';

import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatSnackBar} from '@angular/material';

import { WindowRef } from './../../_services/WindowRef.service';
import { CommonService } from "./../../_services/common.service";

declare var RazorpayCheckout: any;
@Component({
  selector: 'app-qtraqrenewals',
  templateUrl: './qtraqrenewals-master.component.html',
  styleUrls: ['./qtraqrenewals-master.component.scss']
})
export class QtraqrenewalsMasterComponent implements OnInit {
  toggleVal:boolean = false;
  
datanotfound:""
sublist:""
sublistshow:Boolean=true
planlistshow:Boolean=false
rzp1:any;

totalplan:""
publicorgname:any

//payment getway start
fullname:any
contactname:any
emailid:any


//payment getway end


constructor(private queuerenewal:QueuerenewalsService, private router: Router,
  private snackbar:MatSnackBar,private winRef: WindowRef,private cs:CommonService) { }

ngOnInit() 
{

//this.pay()

  let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
  this.publicorgname=actorganistion.org_name  


  this.queuerenewal.getsubplanlist()
  .subscribe(
  (response) => 
  {


if(response.status_code=="200")
{
this.sublist=response.Data
}
else
{
//this.datanotfound=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });


}
}
)
}


addrenewplan()
{
this.queuerenewal.getplanlist()
.subscribe(
(response) => 
{


if(response.status_code=="200")
{

this.sublistshow=false
this.planlistshow=true
this.totalplan=response.Data
}
else if(response.status_code=="402")
{
  localStorage.clear();
  this.router.navigate(['/login']);

}
else
{
//this.datanotfound=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });

}
}
)

}


plansubcall(plan_id,org_id,plan_name,plan_amount)
{



let currentUser = JSON.parse(localStorage.getItem('Adminuser'));

 
this.fullname=atob(currentUser.first_name);
this.contactname=atob(currentUser.user_name);
this.emailid=atob(currentUser.email_id);


  var options = {
    description: 'Real Time Queue Management',
    image: 'http://demo.qtraq.in/qtraqservices/images/paymentgateway/QtraqLogo.png',
    currency: 'INR',
    key: 'rzp_test_uGoHC84OO0J3pq',
    amount: plan_amount*100,
    name: 'Qtraq',
    "handler": this.planget.bind(this,plan_id,org_id),
    prefill: {
      email: ""+this.emailid+"",
      contact: this.contactname,
      name: this.fullname
    },
    
    theme: {
      color: '#1b592a'
    },
    modal: {
      ondismiss: function() {
       // alert('dismissed')
      }



    }
  };
this.rzp1 = new this.winRef.nativeWindow.Razorpay(options);

this.rzp1.open();


/*
this.queuerenewal.subplan(plan_id,org_id)
.subscribe(
(response) => 
{

if(response.status_code=="200")
{
this.router.navigate(['/queuerenewal']);
}
else
{
this.datanotfound=response.Metadata.Message
}
}
)
*/


}


planget(planid,orgid,payid)
{
this.queuerenewal.subplan(payid.razorpay_payment_id,orgid,planid)
.subscribe(
(response) => 
{

  
if(response.status_code=="200")
{

  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });

 

  this.queuerenewal.getsubplanlist()
  .subscribe(
  (response) => 
  {

    

if(response.status_code=="200")
{
  this.sublistshow=true
  this.planlistshow=false

  this.sublist=response.Data

}
else
{
//this.datanotfound=response.Metadata.Message
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });


}
})




}
else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
}


/*
planlastcall(plan_id,org_id,razorpay_payment_id)
{
this.queuerenewal.subplan("1","1","1")
.subscribe(
(response) => 
{
if(response.status_code=="200")
{
  this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
  }
  else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
}
)
}
*/


backtodashbord()
{
this.sublistshow=true
this.planlistshow=false
}
toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}



backtohome()
{
this.cs.backtohome()
}

//payment getway start

/*
pay() {
    var options = {
      description: 'Real Time Queue Management',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_uGoHC84OO0J3pq',
      amount: '5000',
      name: 'Qtraq',
      "handler": function (response){
        
        alert(response.razorpay_payment_id);
       },
      prefill: {
        email: 'demo@email.com',
        contact: '1234567890',
        name: 'My Name'
      },
      
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function() {
        }
      }
    };
this.rzp1 = new this.winRef.nativeWindow.Razorpay(options);
this.rzp1.open();
}
*/


//payment getway end




}
