import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QtraqrenewalsMasterComponent } from './qtraqrenewals-master.component';

describe('QtraqrenewalsMasterComponent', () => {
  let component: QtraqrenewalsMasterComponent;
  let fixture: ComponentFixture<QtraqrenewalsMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QtraqrenewalsMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QtraqrenewalsMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
