import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuesetupMasterComponent } from './quesetup-master.component';

describe('QuesetupMasterComponent', () => {
  let component: QuesetupMasterComponent;
  let fixture: ComponentFixture<QuesetupMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuesetupMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuesetupMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
