import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { ProvidersService } from "./../../_services/providers.service";
import { QuesetupService } from './../../_services/quesetup.service';
import {MatSnackBar} from '@angular/material';
import { CommonService } from "./../../_services/common.service";

@Component({
  selector: 'app-notification-master',
  templateUrl: './quesetup-master.component.html',
  styleUrls: ['./quesetup-master.component.scss']
})
export class QuesetupMasterComponent implements OnInit {
  toggleVal:boolean = false;
/*
  constructor() { }
  ngOnInit() {
  }
*/
//notification:any;

quelist:any;
quesetupdata:any;
providerlist:Boolean=true
providerdetail:Boolean=false
user_master_id:any
queuser_name:any
provider_specialities:any
org_location:any
first_name:any
profileimage_path:any

orgimg:any
orgname:any
orgspecialities:any
activerole:any

  totalRecords = "";
  page=0;
 size = 10;
 publicorgname:any
constructor(private quesetup:QuesetupService, private providerserv: ProvidersService, private router:Router,
  private snackbar:MatSnackBar,private cs:CommonService, private route: ActivatedRoute,) { }
ngOnInit() 
{
let currentUser = JSON.parse(localStorage.getItem("orguserrole"));
this.orgimg = currentUser.orgimg;
this.orgname = currentUser.orgname
 


let activerole = JSON.parse(localStorage.getItem("activerole"));
this.activerole = activerole.activerole

let currentorg = JSON.parse(localStorage.getItem('orguserrole'));

let orgname = currentorg.orgname

if(this.activerole=="l2")
{


/*

this.divclick(response.Data[0].user_master_id,response.Data[0].user_name,response.Data[0].first_name
,response.Data[0].last_name,response.Data[0].profileimage_path,
response.Data[0].org_location,
response.Data[0].provider_specialities)

*/

  let currentUser = JSON.parse(localStorage.getItem('Adminuser'));

  let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));


let actorganistionmumbai =  actorganistion.org_name+" "+actorganistion.cityname

  let usermasterid=currentUser.user_master_id;
  let first_name=currentUser.first_name; 
  let middle_name=currentUser.middle_name; 
  let last_name=currentUser.last_name; 
let provider_specialities = currentUser.provider_specialities 
let user_name = currentUser.user_name
let profileimage_path = currentUser.profileimage_path 


this.divclick(atob(usermasterid),atob(user_name),atob(first_name)
  ,atob(last_name),atob(profileimage_path),
  actorganistionmumbai,
  atob(provider_specialities))






}

else{


this.providerserv
.checkOrganisationslist()
.subscribe(
response => {
if(response.status_code=="200")
{
if(response.Data.length>1)
{
this.quesetuplist(1);
}
else
{
this.divclick(response.Data[0].user_master_id,response.Data[0].user_name,response.Data[0].first_name
,response.Data[0].last_name,response.Data[0].profileimage_path,
response.Data[0].org_location,
response.Data[0].provider_specialities)
}
}
else
{
  this.router.navigate(['/']);
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}


})




let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
this.orgspecialities = currentUser.provider_specialities





if((this.route.snapshot.queryParamMap.get('providernamedata'))!=null)
{

  this.providerserv
  .getprovideronlyweb(this.route.snapshot.queryParamMap.get('providernamedata'))
  .subscribe(
  response => {
  if(response.status_code=="200")
  {






this.divclick(response.Data[0].user_master_id,
  response.Data[0].user_name,
  response.Data[0].first_name,
  response.Data[0].last_name,
  response.Data[0].profileimage_path,
  response.Data[0].org_location,
  response.Data[0].provider_specialities
)



  }
  else
  {
    this.snackbar.open(response.Metadata.Message,'', {
      duration: 2000,
      });
  }
})
}
}
}


quesetuplist(page: number)
  {

  this.quesetup.getorgque((page - 1),this.size)
  .subscribe(
  (response) => 
  {
if(response.status_code=="200")
{

  this.quesetupdata=response.Data
  this.totalRecords=response.total_count
  this.page=page
}
else
{
  this.router.navigate(['/']);
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
  


pageChanged(event)
{
this.quesetuplist(event)
}


divclick(user_master_id,user_name,first_name,last_name,profileimage_path,org_location,provider_specialities)
{
this.providerlist=false
this.providerdetail=true
this.quesetup.getproviderqueues(user_master_id)
.subscribe(
(response) => 
{

if(response.status_code=='200')
{

this.quelist=response.Data
}
else if(response.status_code=='402')
{
 localStorage.clear();
  this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
}
})
this.queuser_name="Dr. "+first_name+" "+last_name
this.first_name=first_name
this.profileimage_path=profileimage_path
this.user_master_id=user_master_id
this.org_location=org_location
this.provider_specialities=provider_specialities
}










backclick()
{
  this.providerlist=true
  this.providerdetail=false
}


/*
onSubmit() 
{
  this.getnotificationlist(1,this.notificationfilter)
}
   
  clearfilter()
  {
  this.notificationfilter.specialityname=""
  this.notificationfilter.specialitystatus=""
  this.getnotificationlist(1,this.notificationfilter)
}
   
  
  
  statuschange(specialityid,status)
  {
    
  this.notificationservice.updatestatus(specialityid,status)
  .subscribe(
  data => {
  
  if(data.status_code==200)
  {

  }
  else
  {
  
  }
  })
  }
*/
toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}



backtohome()
{
  this.cs.backtohome()
}

backtoprovider()
{
  this.providerlist=true
  this.providerdetail=false
}

}
