import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditQueMasterComponent } from './addedit-que-master.component';

describe('AddeditQueMasterComponent', () => {
  let component: AddeditQueMasterComponent;
  let fixture: ComponentFixture<AddeditQueMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditQueMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditQueMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
