import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportCustomFieldsComponent } from './import-custom-fields.component';

describe('ImportCustomFieldsComponent', () => {
  let component: ImportCustomFieldsComponent;
  let fixture: ComponentFixture<ImportCustomFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportCustomFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportCustomFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
