import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { AssignnewtokenService } from 'app/_services/assignnewtoken.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'app/_services';

@Component({
  selector: 'app-import-custom-fields',
  templateUrl: './import-custom-fields.component.html',
  styleUrls: ['./import-custom-fields.component.scss']
})
export class ImportCustomFieldsComponent implements OnInit {

  provider_list:any;
  queue_list:any;
  importCustomFieldsForm:any;
  current_queue_id:any; 
  importCustomFiledFormSubmitted:boolean =  false;
  constructor(
    public dialogRef: MatDialogRef < ImportCustomFieldsComponent > ,
		@Inject(MAT_DIALOG_DATA) public data: any,
    private snackbar: MatSnackBar,
    private router: Router, 
    private formBuilder: FormBuilder,
    private common: CommonService
	) { 
    this.current_queue_id = this.data.current_queue_id;
  }

  ngOnInit() {
    this.importCustomFieldsForm = this.formBuilder.group({
      provider_id:['',[Validators.required]],
      queue_id:['',[Validators.required]],
      current_queue_id:[this.current_queue_id]
    });
    this.common.getProvidersList().subscribe((response)=>{
      this.provider_list = response.Data;
    })
  }

  getProviderQueue(provider_id:number){
    console.log(provider_id);
    this.common.getOrgProviderQueues(provider_id).subscribe((response)=>{
      this.queue_list = response.Data;
    })
  }

  importCustomFieldsFormSubmit(formValues:any){
    this.importCustomFiledFormSubmitted = true;
    if(this.importCustomFieldsForm.valid){
      this.common.importCustomFieldsForm(formValues).subscribe((response)=>{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000});
        this.closeDialog();
      })
    }else{
      return;
    }
  }

  closeDialog(){
    this.dialogRef.close();
  }
}

