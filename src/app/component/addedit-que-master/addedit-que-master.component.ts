import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormArray,ReactiveFormsModule,FormControl } from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { QuesetupService } from './../../_services/quesetup.service';
import {MatSnackBar} from '@angular/material';
import { MatDialogConfig } from '@angular/material'
import { CommonService } from "./../../_services/common.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { QueuetamplateComponent } from './../../component/addedit-que-master/queuetamplate/queuetamplate.component'
import { PlatformLocation } from '@angular/common';
import { ImportCustomFieldsComponent } from './import-custom-fields/import-custom-fields.component';



@Component({
  selector: 'app-addedit-que-master',
  templateUrl: './addedit-que-master.component.html',
  styleUrls: ['./addedit-que-master.component.scss']
})
export class AddeditQueMasterComponent implements OnInit {
  toggleVal:boolean = true;
  //selected = new FormControl(0);
  public addeditque: FormGroup;
  dialogRef:any
  public queacces: FormGroup;
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
  };
  customoption:Boolean=true
  public noticeque:FormGroup;
  tokenrequest:any;
  tokencompletion:any;
  modelcatlogo:Boolean=false;
  questatus:any;
  modedeactivatqueue:Boolean = false;
  customfieldonefielddeactive:Boolean=false;
  public customfield:FormGroup;
  public publicorgname;
  queue_assigned="";
  public trayaltext:FormGroup;
  errorMessage: string;
  submitted = false;
  quelogo="";
  selectedindex=0;
  noticebord:Boolean=false;
  noticebordlist:Boolean=true;
  noticebordadd:Boolean=false
  quecustom:any;
  customlist:Boolean=true;
  customlistedit:Boolean=false;
  customform:Boolean=false;
  customlists:any
  optiondefault="";
  errorqueMessage="";
  errornoticeadd="";
  noticebordlistdate="";
  queue_assigned_users="";
  providerid="";
  queueid=0;
  queueidedit=0;
  hiddenqueuepic="";
  queueimage_path="";
  noticefromdatedate="";
  noticetodatedate="";
  noticetodatedata="";
  accesstatus="";
  org_image_id="";
  user_master_id="";
  first_name="";
  middle_name="";
  last_name="";
  quenamedataname="";
  user_name="";
  is_queue_assigned="";
  profileimage_path="";
  showdatefield:Boolean=false;
  dialogqueuename:any;
  dialogqueueimage:any;
  dialogqueueurl:any;
  noticeid:any;
  queue_template_id:any;
  public customfieldeditform:FormGroup;
  public documentGrp: FormGroup;
  public totalfiles: Array<File> =[];
  public totaldoc: Array<File> =[];
  public totalFileName = [];
  public lengthCheckToaddMore =0;
  public lengthCheckToaddMore1 =0;
  quename:any;
  queimage:any;
  queueSubmitted = false;
  noticeboard:Boolean=false;
  access:Boolean=false;
  customformshow:Boolean=false;
  public custom_field_options:any;
  dielogstatus:any;
  providername:any;
  quenamedata:any;
  providerorgname:any;
  queuetemplateid="0";
  visible_on_complet:Boolean=false;
  visible_on_assign:Boolean=false;
  is_mandatory:Boolean=false;
  visible_to_patient:Boolean=false;
  notifyuser:Boolean=false;
  custom_field_id:any;
  custom_label:any;
  customidstatus:any;
  customexistingstatus:any;
  classnameshowtokenreq:any;
  classnameshowtokencompletion:any;
  showindex:any;
  customvalueid:any;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar:MatSnackBar,
    public dialog: MatDialog,
    private cs:CommonService,
    location: PlatformLocation,
    private quesetup:QuesetupService) 
  { 
 
  }

  ngOnInit(){
    this.customoption=false
    this.documentGrp = this.formBuilder.group({
      items: this.formBuilder.array([this.createUploadDocuments(this.user_master_id,this.first_name,this.middle_name,
      this.last_name,
      this.user_name,this.is_queue_assigned,this.profileimage_path,"editcall")]),
    });
    let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
    this.publicorgname=actorganistion.org_name;
    this.addeditque = this.formBuilder.group({
      quename:"",
      monworking: 0,
      tueworking: 0,
      wedworking: 0,
      thuworking: 0,
      friworking: 0,
       state_id: 0,
      satworking: 0,
      sunworking: 0,
      ispublic:0,
      maxtoken:"",
      buffertokens:"",
      advanced_booking_allowed:"",
      booking_mode:"",
      no_days_allowed:"",
      starttime:"11:59",
      endtime:"23:59",
      avgconsultingtime:"",
      status:"Active",
      queue_pic:""
    });
    this.customfieldeditform= this.formBuilder.group({
      custom_field_id:"",
      custom_label :"",
      custom_field_type :"",
      visible_on_assign :"",
      visible_on_complet :"",
      is_mandatory :"",
      visibletopatient:"",
      notifytokenonchange:""
      //custom_field_options:""
    })
    
    this.noticeque = this.formBuilder.group({
      noticemsg:"",
      fromdate:"",
      todate:"",
      allQueue:"",
      status:"Active"
    })

    this.customfield = this.formBuilder.group({
      fieldlabel:"",
      fieldtype:"",
      optionlist:"",
      ismandatory:"",
      visibletopatient:"",
      notifytokenonchange:"",
      oncomplet:"",
      onassign:""
    })

    this.customlists=[""];
    this.route.params.subscribe(params => {
      if(this.route.snapshot.params.providerid)
      {
        this.providerid=this.route.snapshot.params.providerid
      }
    });

    this.route.params.subscribe(params => {
      if(this.route.snapshot.params.queue_id){
        this.queueid=this.route.snapshot.params.queue_id
        this.queueidedit=this.route.snapshot.params.queue_id
        this.quesetup.getaddeditque(this.queueid).subscribe(data => {
          if(data.status_code==200){
            this.quename="'"+data.Data[0].queue_name+"'"+" "+"queue"
            this.quenamedataname = data.Data[0].queue_name
            this.queimage=data.Data[0].queueimage_path
            this.addeditque.controls['quename'].setValue(data.Data[0].queue_name);
            this.addeditque.controls['maxtoken'].setValue(data.Data[0].max_token);
            this.addeditque.controls['buffertokens'].setValue(data.Data[0].buffer_tokens);
            this.addeditque.controls['starttime'].setValue(data.Data[0].start_time);
            this.addeditque.controls['endtime'].setValue(data.Data[0].end_time);
            this.addeditque.controls['no_days_allowed'].setValue(data.Data[0].advanced_booking_days);
            this.addeditque.controls['status'].setValue(data.Data[0].status);
            this.addeditque.controls['booking_mode'].setValue(data.Data[0].booking_mode);
            this.addeditque.controls['avgconsultingtime'].setValue(data.Data[0].avg_consulting_time);
            this.questatus = data.Data[0].status
            this.addeditque.controls['advanced_booking_allowed'].setValue(data.Data[0].advanced_booking_allowed)
            this.addeditque.controls['ispublic'].setValue(data.Data[0].is_public)
            this.addeditque.controls['tueworking'].setValue(data.Data[0].tue_available)
            this.addeditque.controls['monworking'].setValue(data.Data[0].mon_available)
            this.addeditque.controls['wedworking'].setValue(data.Data[0].wed_available)
            this.addeditque.controls['thuworking'].setValue(data.Data[0].thu_available)
            this.addeditque.controls['friworking'].setValue(data.Data[0].fri_available)
            this.addeditque.controls['satworking'].setValue(data.Data[0].sat_available)
            this.addeditque.controls['sunworking'].setValue(data.Data[0].sun_available)
            this.queueimage_path=data.Data[0].queueimage_path
            this.hiddenqueuepic=data.Data[0].queue_pic
            this.noticetodatedata=data.Data[0].queue_notices
            for(var i=0;i<(data.Data[0].queue_assigned_users.length);i++){
              this.items.insert(i, this.createUploadDocuments(
                data.Data[0].queue_assigned_users[i].user_master_id,
                data.Data[0].queue_assigned_users[i].first_name,
                data.Data[0].queue_assigned_users[i].middle_name,
                data.Data[0].queue_assigned_users[i].last_name,
                data.Data[0].queue_assigned_users[i].user_name,
                data.Data[0].queue_assigned_users[i].is_queue_assigned,
                data.Data[0].queue_assigned_users[i].profileimage_path,
                ""
              ))
            }
          }else if(data.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }else{
          this.snackbar.open(data.Metadata.Message,'', {
            duration: 2000,
          });
          }
        });
      }else{
        this.quename="New Queue"
      }
    })
    this.quesetup.getLoginUserDetails(this.providerid).subscribe(data => {
      this.providername="Dr. "+data.Data.user_details[0].first_name+" "+data.Data.user_details[0].middle_name+" "+data.Data.user_details[0].last_name
    })
    if(this.queueid==0){
      this.noticeboard=true
      this.access=true
      this.customformshow=true
    }

    if(this.queueid==0){
      this.dialogRef=this.dialog.open(QueuetamplateComponent, { disableClose: true })
      this.dialogRef.afterClosed().subscribe(value => {
        if(value!=undefined){
          this.dialogqueuename=value.queue_name
          this.dialogqueueimage=value.queue_image
          this.dialogqueueurl=value.image_path
          this.queueimage_path=value.image_path
          this.queue_template_id=value.queue_template_id
          this.addeditque.controls['quename'].setValue(value.queue_name);
        }
      });
    }
  }


  createUploadDocuments(user_master_id:any,first_name:any,middle_name:any,last_name:any,user_name:any,is_queue_assigned:any,profileimage_path:any,editcall:any): FormGroup {
    return this.formBuilder.group({
      user_master_id: user_master_id,
      first_name: first_name,
      middle_name:middle_name,
      last_name:last_name,
      user_name : user_name,
      is_queue_assigned:is_queue_assigned,
      profileimage_path:profileimage_path,
      editcall:editcall
    });
  }
 
  get items(): FormArray {
    return this.documentGrp.get('items') as FormArray;
  };

  createaccess(org_image_id:any): FormGroup {
    return this.formBuilder.group({
      org_image_id:org_image_id
    });
  }

  onoganisationclick(event){
    if(event.index==3){
      this.quesetup.quecustom(this.queueid).subscribe(data => {
        if(data.status_code==200){
          this.quecustom=data.Data 
        }else if(data.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(data.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
    }else if(event.index==2){
      this.customformshow=false
      if(this.queueidedit==0){
        this.quesetup.getaddeditque(this.queueid).subscribe(data => {
          if(data.status_code==200){
            for(var i=0;i<(data.Data[0].queue_assigned_users.length);i++){
              this.items.insert(i, this.createUploadDocuments(
                data.Data[0].queue_assigned_users[i].user_master_id,
                data.Data[0].queue_assigned_users[i].first_name,
                data.Data[0].queue_assigned_users[i].middle_name,
                data.Data[0].queue_assigned_users[i].last_name,
                data.Data[0].queue_assigned_users[i].user_name,
                data.Data[0].queue_assigned_users[i].is_queue_assigned,
                data.Data[0].queue_assigned_users[i].profileimage_path,
                "")
              )
            }
          }else if(data.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }else{
            //this.errorMessage=data.Metadata.Message
            this.snackbar.open(data.Metadata.Message,'', {
              duration: 2000,
            });  
          }
        })
      }
    }else if(event.index==1){
      this.access=false
    }
  }

  addquenotice(){
    this.customlists=[""]
    this.noticebordlist=false
    this.noticebordadd=true
    this.noticeque.reset()
  }

  addNewOption(valueitem){
    if(valueitem!=""){
      this.customlists.push(valueitem)
    }
    this.optiondefault=null
  }

  Oncustomsubmit(formValue: any) {
    if (formValue.notifytokenonchange != null) {
      if (formValue.notifytokenonchange == true) {
        formValue.notifytokenonchange = "Yes"
      } else {
        formValue.notifytokenonchange = "No"
      }
    } else {
      formValue.notifytokenonchange = "notset"
    }
    if (formValue.visibletopatient == true) {
      formValue.visibletopatient = "Yes"
    } else {
      formValue.visibletopatient = "No"
    }
    if (formValue.ismandatory == true) {
      formValue.ismandatory = "Yes"
    } else {
      formValue.ismandatory = "No"
    }
    if (this.tokenrequest == "Yes") {
      formValue.onassign = "Yes"
    } else {
      formValue.onassign = "No"
    }
    if (this.tokencompletion == "Yes") {
      formValue.oncomplet = "Yes"
    } else {
      formValue.oncomplet = "No"
    }
    let main_form: FormData = new FormData();
    for (let j = 1; j < this.customlists.length; j++) {
      main_form.append("field_option[]", this.customlists[j])
    }
    this.quesetup.editcustomEdit(this.queueid, main_form, formValue).subscribe(data => {
      if (data.status_code == 200) {
        this.quesetup.quecustom(this.queueid).subscribe(data => {
          if (data.status_code == 200) {
            this.quecustom = data.Data
          } else if (data.status_code == '402') {
            localStorage.clear();
            this.router.navigate(['/login']);
          } else {
            this.snackbar.open(data.Metadata.Message, '', {
              duration: 2000,
            });
          }
        })
        this.customlist = true
        this.customform = false
      } else if (data.status_code == '402') {
        localStorage.clear();
        this.router.navigate(['/login']);
      }// else {
        this.snackbar.open(data.Metadata.Message, '', {
          duration: 2000,
        });
      //}
    })
  }

  OnqueSubmit(formValue: any){
    this.queueSubmitted = true;
    if(this.addeditque.invalid){
      return;
    }
    if(formValue.ispublic==true){
      formValue.ispublic='Yes'
    }else{
      formValue.ispublic='No'
    } 

    if(formValue.monworking==true){
      formValue.monworking='Yes'
    }else{
      formValue.monworking='No'
    } 

    if(formValue.tueworking==true){
      formValue.tueworking='Yes'
    }else{
      formValue.tueworking='No'
    } 

    if(formValue.wedworking==true){
      formValue.wedworking='Yes'
    }else{
      formValue.wedworking='No'
    } 

    if(formValue.thuworking==true){
      formValue.thuworking='Yes'
    }else{
      formValue.thuworking='No'
    } 

    if(formValue.friworking==true){
      formValue.friworking='Yes'
    }else{
      formValue.friworking='No'
    } 

    if(formValue.satworking==true){
      formValue.satworking='Yes'
    }else{
      formValue.satworking='No'
    } 

    if(formValue.sunworking==true){
      formValue.sunworking='Yes'
    }else{
      formValue.sunworking='No'
    } 

    let hiddenid = "";
    if(this.hiddenqueuepic!=null){
      hiddenid=this.hiddenqueuepic
    }else{
      hiddenid="";
    }
    let dialog = "" ;
    if(this.dialogqueueimage!=null){
      let dialog = this.dialogqueueimage
    }else{
      let dialog = ""
    }

    if((this.dialogqueuename==formValue.quename)&&(this.quelogo=="")){
      this.queuetemplateid=this.queue_template_id
    }else{
      this.queuetemplateid="0"
    }

    this.quesetup.addeditque(this.queueid,this.quelogo,formValue,this.providerid,hiddenid,this.dialogqueueimage,this.queuetemplateid).subscribe(data => {
      if(data.status_code==200){
        this.errorqueMessage=""
        this.selectedindex=1
        if(this.queueid==0){
          this.queueid=data.Data
          this.noticeboard=false
        }
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
      }else{
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
      }
    })
  }

  onFileChanged(event:any){
    this.quelogo = event.target.files[0]
  }

  OnnoticeSubmit(formValue: any){
    if(formValue.allQueue==true){
      formValue.allQueue="Yes";
    }else{
      formValue.allQueue="No";
    }
    this.quesetup.addMasterQueueNotice(this.queueid,formValue,this.providerid,this.noticefromdatedate,this.noticetodatedate,this.showdatefield).subscribe(data => {
      if(data.status_code==200){
        this.quesetup.getaddeditque(this.queueid).subscribe(data => {
          if(data.status_code==200){
            this.noticetodatedata=data.Data[0].queue_notices
          }else if(data.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }else{
            this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
          }
        })  
        this.errornoticeadd=""
        this.noticebordlist=true
        this.noticebordadd=false
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.errornoticeadd=data.Metadata.Message
      }
    })
  }

  noticefromdate(event: IMyDateModel): void {
    if(event.formatted !== ''){
      this.noticefromdatedate=event.formatted
    }
  }

  statuschange(testid:any,status:any){
    this.quesetup.updatestatus(testid,status).subscribe(data => {
      if(data.status_code==200){
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000
        });
      }
    })
  }

  noticetodate(event: IMyDateModel): void {
    if(event.formatted !== '') {
      this.noticetodatedate=event.formatted
    }
  }

  nexttonotice(){
    this.selectedindex=2
  }

  OnqueueaccesSubmit(formValue: any){
    let main_form: FormData = new FormData();
    for (let j = 0; j < formValue.items.length-1; j++){
      main_form.append("provider_id[]", formValue.items[j]['user_master_id'])
      if((formValue.items[j]['is_queue_assigned']==true) || (formValue.items[j]['is_queue_assigned']=="Yes")){
        main_form.append("provider_access["+j+"]", "Yes")
      }else{
        main_form.append("provider_access["+j+"]", "No")
      }
    }

    this.quesetup.addeditqueue_assigned(this.queueid,main_form).subscribe(data => {
      if(data.status_code==200){
        this.queue_assigned=""
        this.selectedindex=3
        this.customlist=true
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
  }


  questatuschange(customid:any,status:any){
    this.customidstatus=customid;
    this.customexistingstatus=status;
    this.deactiviatequeue(status)
  }

  editqueuecustom(custom_label,custom_field_id){
    this.quesetup.getexistingcustom(custom_field_id,status).subscribe(data => {
      if(data.status_code==200){
        this.customlists=[""];
        this.customlistedit=true;
        this.customlist=false
        this.customform=false
        this.customfieldeditform.controls['custom_label'].setValue(data.Data[0].custom_label);
        this.customfieldeditform.controls['custom_field_id'].setValue(data.Data[0].custom_field_id);
        this.customfieldeditform.controls['custom_field_type'].setValue(data.Data[0].custom_field_type);
        this.is_mandatory=data.Data[0].is_mandatory
        if(data.Data[0].visible_on_assign==true){
          this.classnameshowtokenreq="text_pink";
          this.visible_on_assign=true;
        }else{
          this.classnameshowtokenreq="text_grey";
          this.visible_on_assign=false;
        }

        if(data.Data[0].visible_on_complet==true){
          this.classnameshowtokencompletion="text_pink";
          this.visible_on_complet=true;
        }else{
          this.classnameshowtokencompletion="text_grey";
          this.visible_on_complet=false;
        }
        this.customfieldeditform.controls['is_mandatory'].setValue(data.Data[0].is_mandatory);
        this.customfieldeditform.controls['visibletopatient'].setValue(data.Data[0].visible_to_patient);
        this.customfieldeditform.controls['notifytokenonchange'].setValue(data.Data[0].notify_user);
        this.custom_field_options=data.Data[0].custom_field_options;
        this.notifyuser=data.Data[0].notify_user;
        this.custom_label=custom_label;
        this.custom_field_id=custom_field_id;
      }
    })
  }



  addqueuecustom(){
    this.customlist=false;
    this.customform=true;
    this.customfield.reset();
    this.customoption=false;
  }

  customfieldoption(custom){
  
  }

  Oncustomeditsubmit(formValue: any){
    if(formValue.notifytokenonchange!=null){
      if(formValue.notifytokenonchange==true){
        formValue.notifytokenonchange="Yes"
      }else{
        formValue.notifytokenonchange="No"
      }
    }else{
      formValue.notifytokenonchange="notset"
    }

    if((this.tokenrequest=="Yes") || ((this.visible_on_assign==true))){
      formValue.onassign="Yes"
    }else{
      formValue.onassign="No"
    }

    if((this.tokencompletion=="Yes")||((this.visible_on_complet==true))){
      formValue.oncomplet="Yes"
    }else{
      formValue.oncomplet="No"
    }

    if(formValue.visibletopatient==true){
      formValue.visibletopatient="Yes"
    }else{
      formValue.visibletopatient="No"
    }

    if(formValue.is_mandatory==true){
      formValue.is_mandatory="Yes"
    }else{
      formValue.is_mandatory="No"
    }

    let main_form: FormData = new FormData();
    for (let j = 1; j < this.customlists.length; j++){
      main_form.append("field_option[]", this.customlists[j])
    }
    this.quesetup.customfieldlabelupdate(formValue,formValue.custom_field_id,formValue.custom_label,this.queueid,main_form).subscribe(data => {
      if(data.status_code==200){
        this.quesetup.quecustom(this.queueid).subscribe(data => {
          if(data.status_code==200){
            this.quecustom=data.Data 
          }else if(data.status_code=='402'){
            localStorage.clear();
            this.router.navigate(['/login']);
          }
          this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
        })
        this.customlist=true
        this.customform=false
        this.customlistedit=false
      }
    })
  }

  addqueuecustomfinish(){
    this.router.navigate(['/quesetup']);
  }

  toggleHideShow(action:string){
    if(action == "show"){
      this.toggleVal = true;
    }else{
      this.toggleVal = false;
    }
  }

  removecustomoption(index: number){
    this.modedeactivatqueue=true
  }

  onChangefiletype(value){
    if((value=="List Single Select")||(value=="List Multi Select")){
      this.customlists=[""]
      this.customoption=true
    }else{
      this.customoption=false
    }
  }

  setrequest(value){
    if(value=="onassign"){
      this.tokenrequest="Yes"
      this.visible_on_assign=true
    }else{
      this.tokenrequest="No"
      this.visible_on_assign=false
    }

    if(value=="oncomplet"){
      this.tokencompletion="Yes"
      this.visible_on_complet=true
    }else{
      this.tokencompletion="No"
      this.visible_on_complet=false
    }
  } 

  deleteorgnotices(notice_id){
    this.noticeid=notice_id;
    this.logofun("open")
  }

  addprop1(event) {
    this.noticeque.controls['fromdate'].setValue('');
    this.noticeque.controls['todate'].setValue('');
    if(event.target.checked) {
      this.showdatefield = true
    }else{
      this.showdatefield = false
    }
  }

  backbutton(){
    this.router.navigate(['/quesetup']);
  }


  logofun(action:string){
      if(action == "open"){
        this.modelcatlogo = true;
      }else{
        this.modelcatlogo = false;
      }
  }

  logoconfirm(){
    this.quesetup.deletequeuedata(this.noticeid).subscribe(data => {
      if(data.status_code=="200"){
        this.logofun("cancel")
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
        this.quesetup.getaddeditque(this.queueid).subscribe(data => {
          if(data.status_code=="200"){
            this.noticetodatedata=data.Data[0].queue_notices
          }
          else{
            this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
          }
        })
      }else{
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
      }
    })
  }

  logocancel(){
    this.logofun("cancel")
  }

  opentamplate(){
    this.dialogRef=this.dialog.open(QueuetamplateComponent, { disableClose: true })
    this.dialogRef.afterClosed().subscribe(value => {
      if(value!=undefined){
        this.dialogqueuename=value.queue_name
        this.dialogqueueimage=value.queue_image
        this.dialogqueueurl=value.image_path
        this.queueimage_path=value.image_path
        this.queue_template_id=value.queue_template_id
        if(value.queue_name!=""){
          this.addeditque.controls['quename'].setValue(value.queue_name);
        }
      }
    });
  }

  backtohome(){
    this.cs.backtohome()
  }

  backtoqueue(){
    this.router.navigate(['/quesetup/'],{
      queryParams:{"providernamedata":this.providerid}, skipLocationChange: true}
    );
  }

  cancelqueue(){
    this.noticebordadd=false
    this.noticebordlist=true
  }

  deactive(){
    this.quesetup.updatequestatus(this.customidstatus,this.customexistingstatus).subscribe(data => {
      if(data.status_code==200){
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
        this.deactivequeue("cancel")        
      }else{
        this.deactivequeue("cancel")
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
        });
      }
    })
  }

  deactivequeue(action:string){
    if(action == "open"){
      this.modedeactivatqueue = true;
    }else{
      this.modedeactivatqueue = false;
    }
  }

  deactiviatequeue(action:string){
    this.dielogstatus=action
    this.deactivequeue("open")
  }
  
  deactiviatequeuecancel(){
    this.quesetup.quecustom(this.queueid).subscribe(data => {
      if(data.status_code==200){
        this.quecustom=data.Data 
      }
    })
    this.deactivequeue("cancel")
  }

  backtoleast(){
    this.customlist=true
    this.customform=false
  }

  backtoleastedit(){
    this.quesetup.quecustom(this.queueid).subscribe(data => {
      if(data.status_code==200){
        this.quecustom=data.Data 
      }
    })
    this.customlist=true
    this.customlistedit=false
  }

  showconformation(index,customvalue){
    this.showindex=index
    this.customvalueid=customvalue
    this.customfieldonefielddeactive=true
  }

  deactivethisfield(){
    this.removeonlyoption(this.showindex,this.customvalueid)
  }

  deactiviatequeuecancelfield(){
    this.customfieldonefielddeactive=false
  }

  removeonlyoption(i,ids){
    this.quesetup.changeQueueCustomeFieldStatus(ids).subscribe(data => {
      if(data.status_code==200){
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
        this.customfieldonefielddeactive=false
        this.editqueuecustom(this.custom_label,this.custom_field_id)
      }else{
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000});
      }
    })
  }

  importProviderQueuePopup(){
    this.dialog.open(ImportCustomFieldsComponent, {disableClose: true ,
      data:{
        current_queue_id:this.route.snapshot.params.queue_id
      }
    });
  }

}
