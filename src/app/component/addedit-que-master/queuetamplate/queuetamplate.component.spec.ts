import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuetamplateComponent } from './queuetamplate.component';

describe('QueuetamplateComponent', () => {
  let component: QueuetamplateComponent;
  let fixture: ComponentFixture<QueuetamplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueuetamplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuetamplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
