import { Component, OnInit } from '@angular/core';
import { QuesetupService } from '../../../_services/quesetup.service';
import {MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-queuetamplate',
  templateUrl: './queuetamplate.component.html',
  styleUrls: ['./queuetamplate.component.scss']
})
export class QueuetamplateComponent implements OnInit {

  constructor(private quesetup:QuesetupService, private snackbar:MatSnackBar, 
    public dialogRef: MatDialogRef<QueuetamplateComponent>,) { }
  responsedata:any

  ngOnInit() {
    


this.quesetup.getQueueTemplates().subscribe(
data => {



if(data.status_code=="200")
{

this.responsedata=data.Data
}
else
{
  this.snackbar.open(data.Metadata.Message,'', {
    duration: 2000,
    });


}
})
}


onNoClick(queue_name,queue_image,image_path,queue_template_id)
{

let data={
'queue_name':queue_name,'queue_image':queue_image,
'image_path':image_path,'queue_template_id':queue_template_id
}

this.dialogRef.close(data);

}


closebox()
{
this.dialogRef.close();
}


}
