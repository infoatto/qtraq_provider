import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import {OverridemasterqueuesService} from './../../_services/overridemasterqueues.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl,FormArray } from '@angular/forms';
import { CommonService } from "./../../_services/common.service";
import {MatSnackBar} from '@angular/material';
import { RequestreceivedService } from './../../_services/requestsreceived.service';
import { TodaysqueueService } from 'app/_services/todaysqueue.service';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';
@Component({
  selector: 'overridemasterqueues',
  // template: `{{ now | date:'HH:mm'}}`,
  templateUrl: './overridemasterqueues.component.html',
  styleUrls: ['./overridemasterqueues.component.scss']
})

export class OverridemasterqueuesComponent implements OnInit {
  user_master_id:any
  first_name:any
  middle_name:any
  last_name:any
  org_location:any
  activerole:any
  profileimage_path:any
  savebutton:Boolean=false
  public overridequeue: FormGroup;
  providerlistdata:any
  errorMessage: string;
  doctoraunavailability:any
  providerlistshow:Boolean=true
  orgname:any
  orgimage:any
  totalRecords:any
  bread:Boolean=false
  responsedata:any
  queue_id=""
  queue_transaction_id_data=""
  providerid:any
  queue_transaction_id:any
  start_time:any
  end_time:any
  max_token:any
  buffer_tokens:any
  queue_name:any
  queue_pic_url:any
  overrideque:any
  now:number;
  page=0;
  size = 10;
  selected_queue_id:any;
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate()-1;
  currentMonth: any = this.mytime.getUTCMonth() + 1;
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableUntil:{
      year: this.currentYear,
      month: this.currentMonth,
      day: this.currentDate
    }
  };
  quelistdata:any
  providerdate:any
  timeReadonly:boolean = false;
  queue_selected_date:any;
  queuetransactionid:any;
  queue_details:any;
  pipe: any
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private cs: CommonService,
    private snackbar:MatSnackBar,
    private requestrceived:RequestreceivedService,
    private overridemasterqueues:OverridemasterqueuesService,
    private ongoingqueues:TodaysqueueService
  ){
  }

  ngOnInit(){
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    if(this.activerole=="l2"){
      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      let usermasterid=currentUser.user_master_id;
      let first_name=currentUser.first_name;
      let middle_name=currentUser.middle_name;
      let last_name=currentUser.last_name;
      let profileimage_path=currentUser.profileimage_path;
      this.overridequeuedata(atob(usermasterid),atob(first_name),atob(middle_name),atob(last_name),atob(profileimage_path))
    }else if(this.activerole=="l1" && (this.route.snapshot.queryParamMap.get("queuetransactionid") == undefined || this.route.snapshot.queryParamMap.get("queuetransactionid") == "")){
      this.providerlistshow=true
      this.overridemasterqueues.getorgque(0,this.size).subscribe((response) => {
        if(response.Data.length>1){
          this.getproviderlist(1)
          this.bread=true
        }else{
          this.bread=false
          this.overridequeuedata(
            response.Data[0].user_master_id,
            response.Data[0].first_name,
            response.Data[0].middle_name,
            response.Data[0].last_name,
            response.Data[0].profileimage_path
          )
          //this.getproviderlist(1)
        }
      })
      this.overrideque=false
    }
    if(this.route.snapshot.queryParamMap.get("queuetransactionid") != undefined || this.route.snapshot.queryParamMap.get("queuetransactionid") != ""){
      //get queue override details
      this.queuetransactionid = this.route.snapshot.queryParamMap.get("queuetransactionid")
      var unsubscribeTransaction:Subscription =this.ongoingqueues.transactionQueueTokens(this.queuetransactionid).subscribe((response)=>{
        if(response.status_code == 200){
          this.queue_details = response.Data.queue_details[0];
          this.selected_queue_id = response.Data.queue_details[0].queue_id;
          this.providerid =  response.Data.queue_details[0].provider_id;
          this.pipe = new DatePipe('en-US');
          this.providerdate = this.pipe.transform(response.Data.queue_details[0].queue_date, 'dd-MM-yyyy');
          this.queue_selected_date = this.pipe.transform(response.Data.queue_details[0].queue_date, 'dd-MM-yyyy');
          var dateValue = {formatted:this.queue_selected_date}
          this.onAssignDateChanged(dateValue);
          this.newoverridemasterqueue(this.selected_queue_id);
          this.bread=false
          this.overridemasterqueues.getorgque(0,this.size).subscribe((response) => {
            this.overridequeuedata(
              response.Data[0].user_master_id,
              response.Data[0].first_name,
              response.Data[0].middle_name,
              response.Data[0].last_name,
              response.Data[0].profileimage_path
            )
          })
          this.providerlistshow=false;
          this.overrideque = true;
        }
        unsubscribeTransaction.unsubscribe();
      });

    }
  }


  getproviderlist(page: number){
    //this.doctoravailability.doctoravailability
    this.overridemasterqueues.getorgque((page - 1),this.size).subscribe((response) => {
      //this.showdoctorlist=false
      this.providerlistshow=true
      this.providerlistdata=response.Data
      this.totalRecords=response.total_count
      this.page=page
    },(err: any) => console.log("error",err),() =>
      console.log("getCustomersPage() retrieved customers for page:", + page)
    );
  }

  quetimeoverridecall(){
    this.overridequeue = this.formBuilder.group({
      items: this.formBuilder.array([
        this.createoverridemasterqueues(
          this.queue_name,
          this.queue_transaction_id,
          this.start_time,
          this.end_time,
          this.max_token,
          this.buffer_tokens,
          this.queue_pic_url
        )
      ]),
    });
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    this.orgname=orguserlist.orgname
    this.orgimage=orguserlist.orgimg
  }

  createoverridemasterqueues(queue_name,queue_transaction_id,start_time,end_time,max_token,buffer_tokens,queue_pic_url): FormGroup {
    return this.formBuilder.group({
      queue_name:queue_name,
      queue_transaction_id:queue_transaction_id,
      start_time:start_time,
      end_time:end_time,
      max_token:max_token,
      buffer_tokens:buffer_tokens,
      queue_pic_url:queue_pic_url,
    });
  }

  get items(): FormArray {
    return this.overridequeue.get('items') as FormArray;
  };

  onoverridesubmit(formValue: any){
    let main_form: FormData = new FormData();
    for (let j = 0; j < this.items.length; j++) {
      if((formValue.items[j]['start_time']!='')&&(formValue.items[j]['start_time']!=null)){
        main_form.append("queue_transaction_id["+j+"]", formValue.items[j]['queue_transaction_id'])
        main_form.append("start_time["+j+"]", formValue.items[j]['start_time'])
        main_form.append("end_time["+j+"]", formValue.items[j]['end_time'])
        main_form.append("max_token["+j+"]", formValue.items[j]['max_token'])
        main_form.append("buffer_tokens["+j+"]", formValue.items[j]['buffer_tokens'])
      }
    }
    main_form.append("queue_id",this.queue_id)
    main_form.append("queue_date",this.providerdate)
    this.overridemasterqueues.submitoverrideque(main_form,this.queue_id,this.queue_transaction_id_data).subscribe(data => {
      if(data.status_code=='200'){
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000,})
        // this.router.navigate(['/']);
      }else if(data.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(data.Metadata.Message,'', {duration: 2000,})
      }
    })
  }

  overridequeuedata(providerid,first_name,middle_name,last_name,profileimage_path){
    this.first_name=first_name
    this.middle_name=middle_name
    this.last_name=last_name
    this.providerid=providerid
    this.providerlistshow=false
    this.overrideque=true
    this.profileimage_path=profileimage_path
    this.quetimeoverridecall()
  }

  onAssignDateChanged(event){
    this.requestrceived.getmonitorqueQueuesassign(this.providerid,event.formatted).subscribe((response) => {
      if(response.status_code=="200"){
        this.providerdate=event.formatted;
        this.quelistdata=response.Data;
        var FindResult =  this.quelistdata.filter(x => x.queue_id == this.selected_queue_id)[0];
        if(FindResult == undefined){
          this.overridequeue = this.formBuilder.group({
            items: this.formBuilder.array([
              this.createoverridemasterqueues(
                "","","","","","",""
              )
            ]),
          });
        }else{
          this.newoverridemasterqueue(this.selected_queue_id);
        }
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
        this.quelistdata= [];
        this.overridequeue = this.formBuilder.group({
          items: this.formBuilder.array([
            this.createoverridemasterqueues(
              "","","","","","",""
            )
          ]),
        });
      }
    })
  }

  backtooverride(){
    this.router.navigate(['/']);
  }

  backtohome(){
    this.cs.backtohome()
  }

  movedoclist(){
    this.providerlistshow=true
    this.getproviderlist(1)
    this.overrideque=false
  }

  onSearchChange(event,index,start_time,end_time){
    var countdata1
    var countdata2
    var datanew: any
    for(var i=0;i<this.responsedata.Data.length;i++){
      if(i==index){
        var endtime1 = new Date("10-10-2019 "+this.responsedata.Data[i].queue_end_time)
        var starttime = new Date("10-10-2019 "+this.responsedata.Data[i].queue_start_time)
        var daytime = new Date("10-10-2019 "+start_time)
        if(endtime1.getTime()>starttime.getTime()){
          datanew = (Date.parse(""+endtime1+"") - Date.parse(""+starttime+"")) + Date.parse(""+daytime+"");
        }else{
          this.snackbar.open("End time cannot be <= Start time",'', {
            duration: 2000,
          })
        }
      }
    }
    var gethoures = new Date(datanew).getHours()
    var minitus
    var getminites = new Date(datanew).getMinutes()
    if(getminites==0){
      minitus="00"
    }else if(getminites<10){
      minitus="0"+getminites
    }else{
      minitus=getminites
    }
    var hours
    if(gethoures==0){
      hours="00"
    }else if(gethoures<10){
      hours="0"+gethoures
    }else{
      hours=gethoures
    }
    this.overridequeue.get(`items.${index}.end_time`).setValue(hours+':'+minitus+':00');
  }

  onSearchChangeenddate(event,index,start_time,end_time){
    var endtime1 = new Date("10-10-2019 "+end_time)
    var starttime = new Date("10-10-2019 "+start_time)
    if(endtime1.getTime()<starttime.getTime()){
      this.snackbar.open("End time cannot be <= Start time",'', {duration: 2000,})
      var datanew = (Date.parse(""+starttime+""));
      var gethoures = new Date(datanew).getHours()
      var minitus
      var getminites = new Date(datanew).getMinutes()
      if(getminites==0){
        minitus="00"
      }else if(getminites<10){
        minitus="0"+getminites
      }else{
        minitus=getminites
      }
      var hours
      if(gethoures==0){
        hours="00"
      }else if(gethoures<10){
        hours="0"+gethoures
      }else{
        hours=gethoures
      }
      this.overridequeue.get(`items.${index}.end_time`).setValue(hours+':'+minitus+':00');
    }
  }

  newoverridemasterqueue(event){
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    this.orgname=orguserlist.orgname
    this.orgimage=orguserlist.orgimg
    if(event != ""){
      this.overridequeue = this.formBuilder.group({
        items: this.formBuilder.array([
          this.createoverridemasterqueues(
            this.queue_name,
            this.queue_transaction_id,
            this.start_time,
            this.end_time,
            this.max_token,
            this.buffer_tokens,
            this.queue_pic_url
          )
        ]),
      });
      this.overridemasterqueues.getTransactionQueuesDetails(this.providerid,event, this.providerdate).subscribe(data => {
        if(data.status_code=="200"){
          this.queue_id=data.Data[0].queue_id
          this.queue_transaction_id_data=data.Data[0].queue_transaction_id
          this.responsedata=data;
          if(data.Data[0].queue_start_status == "started"){
            this.timeReadonly = true;
          }else{
            this.timeReadonly = false;
          }
          for(var i=0;i<(data.Data.length);i++){
            this.items.insert(
              i,
              this.createoverridemasterqueues(
                data.Data[i].queue_name,
                data.Data[i].queue_transaction_id,
                data.Data[i].queue_start_time,
                data.Data[i].queue_end_time,
                data.Data[i].max_token,
                data.Data[i].buffer_tokens,
                data.Data[i].queue_pic_url
              )
            )
          }
          this.savebutton=true
        }else if(data.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(data.Metadata.Message,'', {
            duration: 2000,
          })
          this.savebutton=false
        }
      })
    }
  }
}