import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OverridemasterqueuesComponent } from './overridemasterqueues.component';

describe('OverridemasterqueuesComponent', () => {
  let component: OverridemasterqueuesComponent;
  let fixture: ComponentFixture<OverridemasterqueuesComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverridemasterqueuesComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(OverridemasterqueuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
