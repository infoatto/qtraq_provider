import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DoctoravailabilityComponent } from './doctoravailability.component';

describe('DoctoravailabilityComponent', () => {
  let component: DoctoravailabilityComponent;
  let fixture: ComponentFixture<DoctoravailabilityComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctoravailabilityComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(DoctoravailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
