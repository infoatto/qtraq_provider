import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {Component} from '@angular/core';
import { DoctoravailabilityComponent } from './doctoravailability.component'
import {MatTabsModule} from '@angular/material/tabs';
import {DoctoravailabilityRoutingModule  } from "./doctoravailability-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
import {MatRadioModule} from '@angular/material/radio';
//import{ TimeFormatPipe } from './pipes/time.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
// import { FocusDirective } from './../../directive/myFocus.directive';

import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { SidebarComponent } from '../sidebar/sidebar.component'
@NgModule({
  imports: [CommonModule,
    DoctoravailabilityRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,MatRadioModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     FormsModule,ReactiveFormsModule
    ],
  declarations: [DoctoravailabilityComponent]
})
export class DoctoravailabilityModule {}