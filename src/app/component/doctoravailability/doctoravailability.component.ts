import { Component, OnInit,Renderer2,ElementRef, QueryList, ViewChildren  } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {FormBuilder,FormGroup,Validators,FormArray,FormControl,NgForm} from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
import { DoctoravailabilityService } from './../../_services/doctoravailability.service';

import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import {MatSnackBar} from '@angular/material';
import { ConfirmationDialogService } from './../../component/confirmation-dialog/confirmation-dialog.service';

import { ViewimpactdialogComponent } from './../../component/viewimpactdialog/viewimpactdialog.component'

import { MatDialogConfig } from '@angular/material'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

//viewimpactclick

@Component({
  selector: 'doctoravailability',
  templateUrl: './doctoravailability.component.html',
  styleUrls: ['./doctoravailability.component.scss']
})
export class DoctoravailabilityComponent implements OnInit {
  //@ViewChild('input1') input1;
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate()-1;
  currentMonth: any = this.mytime.getUTCMonth() + 1;
  myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    disableUntil:{year: this.currentYear, month: this.currentMonth, day: this.currentDate}
  };
  doctor_unavailability_id:any
  getUnavailabilityDetailsid:any
  desabledata:Boolean=false
  status:any
  profileimage_path:any
  showdoctorlist:Boolean=true
  showendtodate:Boolean=false
  leavetimetype:Boolean=false
  queue_id:any = ""
  addunavailabilitydata:Boolean=false
  broadcasttoalldata:Boolean=false
  block_further_appointmentsdata:any
  providerlistdata:any
  providerlistshow:Boolean=false
  doctoravailabilityfilter: any = {
    month:"",
    year:""
  };
  adddoctoravailability:any={
    unavailabilitydaytype:''
  }
  errorMessage: string;
  doctoraunavailability:any
  orgname:any
  orgimage:any
  totalRecords:any
  providerid:any
  viewimpact:any
  firstname:any
  middlename:any
  lastname:any
  dialogRef:any
  fromdatevalidation:any
  todatevalidation:any
  checkdatevalidation:any
  queueList:any;
  cancelchecked:boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private doctoravailability:DoctoravailabilityService,
    private snackbar:MatSnackBar,
    private confirmationDialogService: ConfirmationDialogService,
    // private Renderer2:renderer
    private renderer:Renderer2,
    public dialog: MatDialog,
  ){}

  @ViewChildren('txtArea') textAreas: QueryList<ElementRef>;
  public doctoravunailability: FormGroup;
  public addunavailability: FormGroup;
  activerole:any
  keys:any;
  months:any ;
  years = [];

  getDates() {
    var date = new Date();
    var currentYear = Number(2019)
    //set values for year dropdown
    for (var i = 0; i <= 5; i++) {
      this.years.push(currentYear + i);
    }
    this.months = [
      {"id":"1","value":"Jan"},
      {"id":"2","value":"Feb"},
      {"id":"3","value":"Mar"},
      {"id":"4","value":"Apr"},
      {"id":"5","value":"May"},
      {"id":"6","value":"Jun"},
      {"id":"7","value":"Jul"},
      {"id":"8","value":"Aug"},
      {"id":"9","value":"Sep"},
      {"id":"10","value":"Oct"},
      {"id":"11","value":"Nov"},
      {"id":"12","value":"Dec"}
    ]
  }

  page=0;
  size = 10;
  bread:Boolean=true

  ngOnInit(){
    //addunavailabilitydata
    this.providerlistshow=false
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole;
    if(this.activerole=="l2"){
      let currentUser = JSON.parse(localStorage.getItem("Adminuser"));
      let usermasterid=currentUser.user_master_id;
      let first_name=currentUser.first_name;
      let middle_name=currentUser.middle_name;
      let last_name=currentUser.last_name;
      let profileimage_path=currentUser.profileimage_path;
      this.orgname=orguserlist.orgname
      //this.overridequeuedata(atob(usermasterid),atob(first_name),atob(middle_name),atob(last_name),atob(profileimage_path))
      //this.adddoctoraunavailabilitysame(atob(usermasterid),atob(first_name),atob(middle_name),atob(last_name),atob(profileimage_path))
    }else{
      this.orgname=orguserlist.orgname
    }
    this.orgimage=orguserlist.orgimg
    this.addunavailability = this.formBuilder.group({
      unavailabilitydaytype:"Single",
      fromdata:"",
      fullday:"Yes",
      broadcastedmessage:"",
      todate:"",
      leavetime:"",
      leavetimetype:"",
      queue_id:"",
      unavailabilitytype:"Tentative",
      canceltokens:"",
      broadcasttoallqueue:"",
      block_further_appointments:"No",
      unavailabilityreason:"",
      applytoall:""
    })
    var DateObj = new Date();
    this.doctoravunailability= this.formBuilder.group({month:DateObj.getMonth() + 1,year:DateObj.getFullYear()});
    this.getDates()
    this.OnSubmit(this.doctoravunailability)
  }


  paginglist(page: number,formdata){
    this.doctoravailability.doctoravailability((page - 1) ,this.size,formdata).subscribe((response) =>{
      if(response.status_code=="200"){
        this.doctoraunavailability=response.Data
        this.totalRecords=response.total_count
        this.page=page
        this.errorMessage=""
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        //this.errorMessage=response.Metadata.Message
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        })
      }},(err: any) => console.log("error",err),() =>console.log("getCustomersPage() retrieved customers for page:", + page)
    );
  }

  OnSubmit(formValue: any){
    this.doctoravailabilityfilter.month=formValue.value.month
    this.doctoravailabilityfilter.year=formValue.value.year
    this.paginglist(1,formValue)
  }


  pageChanged(event){
    this.paginglist(event,this.doctoravailabilityfilter)
  }

  clearfilter(){
    this.doctoravailabilityfilter.month=""
    this.doctoravailabilityfilter.year=""
    this.paginglist(1,this.doctoravailabilityfilter)
  }

  providerlist(){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    this.desabledata=false
    this.broadcasttoalldata=false
    this.showendtodate=false
    this.leavetimetype=false
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole
    if(this.activerole=="l2"){
      //this.adddoctoraunavailability(usermasterid)
      let currentUser = JSON.parse(localStorage.getItem("Adminuser"));
      let usermasterid=currentUser.user_master_id;
      let first_name=currentUser.first_name;
      let middle_name=currentUser.middle_name;
      let last_name=currentUser.last_name;
      let profileimage_path=currentUser.profileimage_path;
      //this.orgname=orguserlist.orgname
      this.adddoctoraunavailabilitysame(atob(usermasterid),atob(first_name),atob(middle_name),atob(last_name),atob(profileimage_path))
    }else{
      this.doctoravailability.getorgque(0,this.size).subscribe((response) =>{
        if(response.Data.length>1){
          this.bread=true
          this.getproviderlist(1)
        }else{
          this.bread=false
          //this.adddoctoraunavailabilitysame(providerid,firstname,midlename,lastname,profileimage_path)
          this.adddoctoraunavailabilitysame(
            response.Data[0].user_master_id,
            response.Data[0].first_name,
            response.Data[0].middle_name,
            response.Data[0].last_name,
            response.Data[0].profileimage_path
          )
        }
      })
    }
  }

  getproviderlist(page: number){
    //this.doctoravailability.doctoravailability
    this.doctoravailability.getorgque((page - 1),this.size).subscribe((response) =>{
      this.showdoctorlist=false
      this.providerlistshow=true
      this.providerlistdata=response.Data
      this.totalRecords=response.total_count
      this.page=page
    },(err: any) => console.log("error",err),() =>console.log("getCustomersPage() retrieved customers for page:", + page));
  }

  adddoctoraunavailabilitysame(providerid,firstname,midlename,lastname,profileimage_path){
    this.providerlistshow=false
    this.showdoctorlist=false
    this.providerid=providerid
    this.firstname=firstname
    this.middlename=midlename
    this.lastname=lastname
    this.profileimage_path=profileimage_path
    this.addunavailabilitydata=true
    this.showendtodate=false
    this.leavetimetype=false
    this.addunavailability.reset()
    this.addunavailability = this.formBuilder.group({
      unavailabilitydaytype:"Single",
      fromdata:"",
      fullday:"Yes",
      broadcastedmessage:"",
      todate:"",
      leavetime:"",
      leavetimetype:"",
      queue_id:"",
      unavailabilitytype:"Tentative",
      canceltokens:"",
      broadcasttoallqueue:"",
      block_further_appointments:"",
      unavailabilityreason:"",
      applytoall:""
    })
    this.block_further_appointmentsdata='No'
  }

  adddoctoraunavailability(providerid){
    this.providerlistshow=false
    this.showdoctorlist=false
    this.providerid=providerid
    this.addunavailabilitydata=true
  }

  canceldoctorunavailability(){
    this.showdoctorlist=true
    this.addunavailabilitydata=false
  }

  viewimpactclick(formValue: any){
    const config = new MatDialogConfig();
    this.doctoravailability.viewimpact(formValue,this.providerid).subscribe((response) =>{
      if(response.status_code=="200"){
        config.data = { tokendate: response};
        this.viewimpact=response.Data.total_count
        this.dialogRef=this.dialog.open(ViewimpactdialogComponent,config);
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        })
      }
    },(err: any) => console.log("error",err),() =>console.log("getCustomersPage() retrieved customers for page:"));
  }

  OnunavailabilitySubmit(formValue: any){
    if(this.desabledata==true){
      this.confirmationDialogService.confirm('Confirmation Alert', 'Do you wish to Update this absence and also CANCEL ALL impacted tokens ?').then((confirmed) =>{
        if(confirmed){
          if(this.block_further_appointmentsdata==undefined){
            formValue.block_further_appointments='No'
          }else if(this.block_further_appointmentsdata=='No'){
            formValue.block_further_appointments='No'
          }else if(this.block_further_appointmentsdata==true){
            formValue.block_further_appointments='Yes'
          }else if(this.block_further_appointmentsdata==false){
            formValue.block_further_appointments='No'
          }else{
            formValue.block_further_appointments=this.block_further_appointmentsdata
          }
          this.doctoravailability.changeUnavailabilityupdatedata(formValue,this.getUnavailabilityDetailsid).subscribe((response) =>{
            if(response.status_code=="200"){
              this.showdoctorlist=true
              this.addunavailabilitydata=false
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              })
              this.addunavailability.reset();
              var DateObj = new Date();
              this.doctoravunailability= this.formBuilder.group({month:DateObj.getMonth() + 1,year:DateObj.getFullYear()})
              this.getDates()
              this.OnSubmit(this.doctoravunailability)
              this.showendtodate=false
              this.leavetimetype=false
              this.addunavailability = this.formBuilder.group({
                unavailabilitydaytype:"Single",
                fromdata:"",
                fullday:"Yes",
                broadcastedmessage:"",
                todate:"",
                leavetime:"",
                leavetimetype:"",
                queue_id:"",
                unavailabilitytype:"Tentative",
                canceltokens:"",
                broadcasttoallqueue:"",
                block_further_appointments:"",
                unavailabilityreason:"",
                applytoall:""
              })
              //this.paginglist(1,this.doctoravailabilityfilter)
            }else if(response.status_code=='402'){
              localStorage.clear();
              this.router.navigate(['/login']);
            }else{
              this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
            }
          }
        )}
      })
    }else{
      if((formValue.todate.formatted==undefined)&&(formValue.todate.date!=undefined)){
        var month:any;
        var day:any;
        if(formValue.todate.date.month>9){
          month=formValue.todate.date.month
        }else{
          month='0'+formValue.todate.date.month
        }
        if(formValue.todate.date.day>9){
          day=formValue.todate.date.day
        }else{
          day='0'+formValue.todate.date.day
        }
        formValue.todate=day+'-'+month+'-'+formValue.todate.date.year
      }
      if(this.block_further_appointmentsdata==undefined){
        formValue.block_further_appointments='No'
      }else if(this.block_further_appointmentsdata=='No'){
        formValue.block_further_appointments='No'
      }else{
        formValue.block_further_appointments=this.block_further_appointmentsdata
      }
      this.confirmationDialogService.confirm('Confirmation Alert', 'Do you wish to SAVE this absence and also CANCEL ALL impacted tokens ?').then((confirmed) =>{
        if(confirmed){
          this.doctoravailability.adddoctoravailability(formValue,this.providerid).subscribe((response) =>{
            if(response.status_code=="200"){
              this.showdoctorlist=true
              this.addunavailabilitydata=false
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              })
              this.addunavailability.reset();
              var DateObj = new Date();
              this.doctoravunailability= this.formBuilder.group({month:DateObj.getMonth() + 1,year:DateObj.getFullYear()})
              this.getDates()
              this.OnSubmit(this.doctoravunailability)
              this.showendtodate=false
              this.leavetimetype=false
              this.addunavailability = this.formBuilder.group({
                unavailabilitydaytype:"Single",
                fromdata:"",
                fullday:"Yes",
                broadcastedmessage:"",
                todate:"",
                leavetime:"",
                leavetimetype:"",
                queue_id:"",
                unavailabilitytype:"Tentative",
                canceltokens:"",
                broadcasttoallqueue:"",
                block_further_appointments:"",
                unavailabilityreason:"",
                applytoall:""
              })
            }else if(response.status_code=='402'){
                localStorage.clear();
                this.router.navigate(['/login']);
            }else{
              this.snackbar.open(response.Metadata.Message,'', {
                duration: 2000,
              })
            }
          },(err: any) => console.log("error",err),() =>console.log("getCustomersPage() retrieved customers for page:"));
        }
      })
    }
  }

  fulldaychange(event){
    if(event=="Yes"){
      this.leavetimetype=false
    }else{
      this.leavetimetype=true
      this.getProviderAssignedQueuesAll(this.providerid);
    }
  }

  broadcasttoall(event){
    if(event.target.checked==false){
      this.broadcasttoalldata=false
    }else{
      this.broadcasttoalldata=true
    }
  }

  block_further_appointments(event){
    if(event.target.checked==false){
      this.block_further_appointmentsdata='No'
    }else{
      this.block_further_appointmentsdata='Yes'
    }
  }

  unavailabilitydaytypechange(event){
    if(event=="Single"){
      this.showendtodate=false
    }else{
      this.showendtodate=true
    }
  }

  onAssignresDateChanged(event,formValue: any){
    this.fromdatevalidation=event.formatted
    var f1 = this.fromdatevalidation.split('-')
    if((this.todatevalidation!="")&&(this.todatevalidation!=undefined)){
      var to = this.todatevalidation.split('-')
    }
    if((this.todatevalidation!='')&&(this.todatevalidation!=undefined)){
      var tod = this.todatevalidation.split('-')
      let from = new Date(f1[1]+'/'+f1[0]+'/'+f1[2]);
      let todate = new Date(tod[1]+'/'+tod[0]+'/'+tod[2]);
      if(from.getTime() > todate.getTime()){
        this.addunavailability.controls['fromdata'].setValue({date: { year: Number(f1[2]), month:Number(f1[1]), day:Number(f1[0]) }});
        this.addunavailability.controls['todate'].setValue({date: { year: Number(f1[2]), month:Number(f1[1]), day:Number(f1[0]) }});
        this.todatevalidation=f1[0]+'-'+f1[1]+'-'+f1[2]
      }else{
        this.addunavailability.controls['fromdata'].setValue({date: { year: Number(f1[2]), month:Number(f1[1]), day:Number(f1[0]) }});
        this.addunavailability.controls['todate'].setValue({date: { year: Number(to[2]), month:Number(to[1]), day:Number(to[0]) }});
      }
    }else{
      this.addunavailability.controls['fromdata'].setValue({date: { year: Number(f1[2]), month:Number(f1[1]), day:Number(f1[0]) }});
      this.addunavailability.controls['todate'].setValue({date: { year: Number(f1[2]), month:Number(f1[1]), day:Number(f1[0]) }});
      this.todatevalidation=event.formatted
    }
  }

  onAssigntoDateChanged(event,formValue: any){
    this.todatevalidation=event.formatted
    var fromdate =this.fromdatevalidation.split('-')
    var todate =this.todatevalidation.split('-')
    let from = new Date(fromdate[1]+'/'+fromdate[0]+'/'+fromdate[2]);
    let to = new Date(todate[1]+'/'+todate[0]+'/'+todate[2]);
    if(to.getTime()<from.getTime()){
      this.addunavailability.controls['fromdata'].setValue({date: { year: Number(todate[2]), month:Number(todate[1]), day:Number(todate[0]) }});
      this.addunavailability.controls['todate'].setValue({date: { year: Number(todate[2]), month:Number(todate[1]), day:Number(todate[0]) }});
      this.fromdatevalidation= todate[0]+'-'+todate[1]+'-'+todate[2]
    }else{
      this.addunavailability.controls['fromdata'].setValue({date: { year: Number(fromdate[2]), month:Number(fromdate[1]), day:Number(fromdate[0]) }});
      this.addunavailability.controls['todate'].setValue({date: { year: Number(todate[2]), month:Number(todate[1]), day:Number(todate[0]) }});
    }
  }

  backtoavailability(){
    this.router.navigate(['/']);
  }

  selectdoctor(){
    this.showdoctorlist=true
    this.providerlistshow=false
    this.addunavailabilitydata=false
    this.router.navigate(['/doctoravailability/']);
    var DateObj = new Date();
    this.doctoravunailability= this.formBuilder.group({
      month:DateObj.getMonth() + 1,
      year:DateObj.getFullYear()
    })
    this.getDates()
    this.OnSubmit(this.doctoravunailability)
  }

  selectdoctorfromlist(){
    this.addunavailability.reset();
    this.showdoctorlist=false
    this.addunavailabilitydata=false
    this.providerlistshow=true
  }

  doctor_unavailability(doctor_unavailability_id){
    this.confirmationDialogService.confirm('Confirmation Alert','Do you wish to Cancel Availability ?').then((confirmed) =>{
      if(confirmed){
        this.doctoravailability.changeUnavailability(doctor_unavailability_id).subscribe((response) =>{
          if(response.status_code=="200"){
            this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
            var DateObj = new Date();
            this.doctoravunailability= this.formBuilder.group({
              month:DateObj.getMonth() + 1,
              year:DateObj.getFullYear()
            })
            this.OnSubmit(this.doctoravunailability)
          }else{
            this.snackbar.open(response.Metadata.Message,'', {
              duration: 2000,
            })
          }
        })
      }
    })
  }


  getUnavailabilityDetails(id,providerid,firstname,midlename,lastname,profileimage_path){
    this.getUnavailabilityDetailsid=id
    this.providerid=providerid
    this.firstname=firstname
    this.middlename=midlename
    this.lastname=lastname
    this.profileimage_path=profileimage_path
    this.doctoravailability.getUnavailabilityDetails(id).subscribe((response) =>{
      if(response.status_code=="200") {
        this.desabledata=true
        this.addunavailability.controls['unavailabilitydaytype'].setValue(response.Data[0]['unavailability_day_type']);
        if(response.Data[0]['unavailability_day_type']=='Range'){
          this.showendtodate=true
        }else{
          this.showendtodate=false
        }
        this.addunavailability.controls['fromdata'].setValue(response.Data[0]['web_from_date']);
        this.addunavailability.controls['fullday'].setValue(response.Data[0]['is_full_day']);
        if(response.Data[0]['is_full_day']=='Yes'){
          this.leavetimetype=false
        }else{
          this.leavetimetype=true
        }
        this.addunavailability.controls['leavetimetype'].setValue(response.Data[0]['leave_time_type']);
        this.addunavailability.controls['queue_id'].setValue(response.Data[0]['queue_id']);
        this.addunavailability.controls['broadcastedmessage'].setValue(response.Data[0]['broadcasted_message']);
        this.addunavailability.controls['todate'].setValue(response.Data[0]['web_to_date']);
        this.addunavailability.controls['leavetime'].setValue(response.Data[0]['leave_time']);
        this.addunavailability.controls['unavailabilitytype'].setValue(response.Data[0]['unavailability_type']);
        this.addunavailability.controls['broadcasttoallqueue'].setValue(response.Data[0]['broadcast_to_all_queue']);
        this.addunavailability.controls['block_further_appointments'].setValue(response.Data[0]['block_further_appointments']);
        this.addunavailability.controls['unavailabilityreason'].setValue(response.Data[0]['unavailability_reason']);
        if(response.Data[0]['broadcast_to_all_queue']==true){
          this.broadcasttoalldata=true
        }else{
          this.broadcasttoalldata=false
        }
        this.status=response.Data[0]['status']
        this.showdoctorlist=false
        this.addunavailabilitydata=true
        this.doctoraunavailability=false
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
      }
    })
  }

  getProviderAssignedQueuesAll(providerid:any){
    this.doctoravailability.getProviderAssignedQueuesAll(providerid).subscribe((response)=>{
      if(response.status_code == "200"){
        this.queueList = response.Data;
      }else{
        this.queueList = [];
        this.snackbar.open(response.Metadata.Message,'', {duration: 2000,})
      }
    });
  }

  cancelTokenChecked(ischecked:any){
    if(ischecked){
      this.cancelchecked = true;
    }else{
      this.cancelchecked = false;
    }
  }




}
