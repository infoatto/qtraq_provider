import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//import {  PostsComponent} from './posts.component'
import { DoctoravailabilityComponent } from './doctoravailability.component'
//import { HomeComponent } from './home/home.component';
//import { DatanewComponent } from './datanew/datanew.component';
const routes: Routes = [

   // { path: "po123", component: HomeComponent},

    { path: "", component: DoctoravailabilityComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [
        RouterModule.forChild(routes)
    
    ]
})
export class DoctoravailabilityRoutingModule { }