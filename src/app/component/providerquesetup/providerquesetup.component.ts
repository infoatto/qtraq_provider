import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';

import { QuesetupService } from './../../_services/quesetup.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'providerquesetup',
  templateUrl: './providerquesetup.component.html',
  styleUrls: ['./providerquesetup.component.scss']
})
export class ProviderquesetupComponent implements OnInit {
  toggleVal:boolean = false;
/*
  constructor() { }
  ngOnInit() {
  }
*/
//notification:any;

quelist:any;
quesetupdata:any;
providerlist:Boolean=true
providerdetail:Boolean=false
user_master_id:any
queuser_name:any
provider_specialities:any
org_location:any
first_name:any
profileimage_path:any

orgimg:any
orgname:any
orgspecialities:any


  totalRecords = "";
  page=0;
 size = 10;
 publicorgname:any
constructor(private quesetup:QuesetupService, private router:Router,
  private snackbar:MatSnackBar) { }
ngOnInit() 
{
let currentUser = JSON.parse(localStorage.getItem("orguserrole"));
this.orgimg = currentUser.orgimg;
this.orgname = currentUser.orgname

let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
this.orgspecialities = currentUser.provider_specialities
//this.quesetuplist(1);

this.divclick()
}


quesetuplist(page: number)
  {
   
  this.quesetup.getorgque((page - 1),this.size)
  .subscribe(
  (response) => 
  {

if(response.status_code=="200")
{
  this.quesetupdata=response.Data
  this.totalRecords=response.total_count
  this.page=page
}
else
{
 

  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
    this.router.navigate(['/']);
}


},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
  


pageChanged(event)
{
this.quesetuplist(event)
}


divclick()
{





this.providerlist=false
this.providerdetail=true
this.quesetup.getproviderqueuesforprovider()
.subscribe(
(response) => 
{


if(response.status_code=='200')
{

this.quelist=response.Data
}
else if(response.status_code=='402')
{
 localStorage.clear();
  this.router.navigate(['/login']);
}
else
{
this.snackbar.open(response.Metadata.Message,'', {
  duration: 2000,
  });
this.router.navigate(['/']);
}
})


let adminuser = JSON.parse(localStorage.getItem("Adminuser"));
this.queuser_name = "Dr"+atob(adminuser.first_name)+" "+atob(adminuser.last_name);
this.first_name=atob(adminuser.first_name)
this.profileimage_path=atob(adminuser.profileimage_path)
this.user_master_id=atob(adminuser.user_master_id)
//this.org_location=atob(adminuser.profileimage_path)
this.provider_specialities=atob(adminuser.provider_specialities)

this.orgname = adminuser.orgname



/*
this.queuser_name="Dr "+first_name+" "+last_name
this.first_name=first_name
this.profileimage_path=profileimage_path
this.user_master_id=user_master_id
this.org_location=org_location
this.provider_specialities=provider_specialities
*/

}

backclick()
{
  this.providerlist=true
  this.providerdetail=false
}


/*
onSubmit() 
{
  this.getnotificationlist(1,this.notificationfilter)
}
   
  clearfilter()
  {
  this.notificationfilter.specialityname=""
  this.notificationfilter.specialitystatus=""
  this.getnotificationlist(1,this.notificationfilter)
}
   
  
  
  statuschange(specialityid,status)
  {
    
  this.notificationservice.updatestatus(specialityid,status)
  .subscribe(
  data => {
  
  if(data.status_code==200)
  {
  
  }
  else
  {
  
  }
  })
  }
*/
toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}


}
