import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderquesetupComponent } from './providerquesetup.component';

describe('ProviderquesetupComponent', () => {
  let component: ProviderquesetupComponent;
  let fixture: ComponentFixture<ProviderquesetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderquesetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderquesetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
