import { Component, OnInit, trigger, transition, style, animate, AfterViewInit } from '@angular/core';
//import { TestService } from 'src/app/_services/test.service';
import { TestService } from './../../_services/test.service';
//import { SpecialityService } from './../../_services/speciality.service';
import { FormsModule } from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { SidebarToggleService } from 'app/_services/sidebar-toggle.service';
import {Observable} from 'rxjs/Observable';
import { EventEmitter } from 'protractor';
import { Router, NavigationEnd } from '@angular/router';
//declare var test:any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    //test()
  }
  toggleVal:any = true;
  testlist:any;
  activerole:any
  totalRecords = "";
  page=0;
  organisationid: any;
  testfilter: any = {
    testname:"",
    teststatus:""
  };
  size = 10;
  isDoctor:boolean = false;
  isFrontdesk:boolean = false;


  constructor(
    private testservice:TestService,
    private sideToggleService:SidebarToggleService,
    private router:Router
  ){   
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        // trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        // if you need to scroll back to top, here is the right place
        window.scrollTo(0, 0);
      }
    });
  }

  ngOnInit(){
    let currentUser = JSON.parse(localStorage.getItem("orguserrole"));
    this.organisationid = currentUser.orgid;
    this.isDoctor = currentUser.doctor;
    this.isFrontdesk = currentUser.frontdesk;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    this.activerole = activerole.activerole
    //this.gettestlist(1,this.testfilter);
    //this.sideToggleService.gettoggleVal().subscribe(response=>{
    //this.toggleVal = response;
    //});
  }

  changerolecall(role){
  }
  
  toggleHideShow(action: string) {
    if (action == "show") {
      this.sideToggleService.assignToggleVal(true);
      // this.toggleVal = true;
    } else {
      this.sideToggleService.assignToggleVal(false);
      // this.toggleVal = false;
    }
  }
}
