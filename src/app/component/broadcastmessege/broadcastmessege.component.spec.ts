import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BroadcastmessegeComponent } from './broadcastmessege.component';

describe('BroadcastmessegeComponent', () => {
  let component: BroadcastmessegeComponent;
  let fixture: ComponentFixture<BroadcastmessegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BroadcastmessegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BroadcastmessegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
