import { Component, OnInit } from '@angular/core';
import { NotificationService } from './../../_services/notification.service';


@Component({
  selector: 'broadcastmessege',
  templateUrl: './broadcastmessege.component.html',
  styleUrls: ['./broadcastmessege.component.scss']
})
export class BroadcastmessegeComponent implements OnInit {

/*broadcastmessege
  constructor() { }
  ngOnInit() {
  }
*/
notification:any;
  totalRecords = "";
  page=0;
  notificationfilter: any = {
notificationtitle:"",
notificationstatus:""
};

size = 10;



constructor(private notificationservice:NotificationService) { }

ngOnInit() 
{
this.getnotificationlist(1,this.notificationfilter);
}


getnotificationlist(page: number,notificationfilter)
  {
   
  this.notificationservice.getnotificationlist((page - 1) ,this.size,notificationfilter)
  .subscribe(
  (response) => 
  {

  this.notification=response.Data
  this.totalRecords=response.total_count
  this.page=page
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
  





onSubmit() 
{
  this.getnotificationlist(1,this.notificationfilter)
}
  
  
  clearfilter()
  {
  this.notificationfilter.specialityname=""
  this.notificationfilter.specialitystatus=""
  this.getnotificationlist(1,this.notificationfilter)
}
  
  
  
  pageChanged(event)
  {
  this.getnotificationlist(event,this.notificationfilter)
  }
  
  
  statuschange(specialityid,status)
  {
    
  this.notificationservice.updatestatus(specialityid,status)
  .subscribe(
  data => {
  
  if(data.status_code==200)
  {
  
  }
  else
  {
  
  }
  })
  }




}
