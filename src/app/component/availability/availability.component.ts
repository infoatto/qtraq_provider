//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Renderer, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl,
  NgForm
} from "@angular/forms";

import { OrganisationsService } from "./../../_services/organisations.service";

import { ProvidersService } from "./../../_services/providers.service";

//import{ }
import { Router, ActivatedRoute, Params } from "@angular/router";
import { HttpHeaders } from "@angular/common/http";
import { ReversePipe } from "./../../pipes/reverse.pipe";
import { INgxMyDpOptions, IMyDateModel } from "ngx-mydatepicker";
import {MatSnackBar} from '@angular/material';
@Component({
  selector: "availability",
  templateUrl: "./availability.component.html",
  styleUrls: [
    "./availability.component.css",
    "./availability.component.scss"
  ]
})
export class AvailabilityComponent implements OnInit {
  toggleVal:boolean = false;
  myOptions: INgxMyDpOptions = {
    dateFormat: "dd-mm-yyyy"
  };

  
  onconvalueget = "";

  organisationdataid = 0;

  monany=0
  //4-1-2019
  dob_date = "";
  submobile = "";
  otperrormsg = "";
  useraddresponse = "";
  
  providerdob="";
  providerdob1:any;
   // selectedItems = [];

  practisingstarteddate="";
  practisingstarteddate1:any;

  profilepicname="";

  
  //4-1-2019
  provider_idadd = 0;
  provider_id = 0;
  imageposition = "";
  org_image_id = "";
  suberror = "";
  dob: any;
  dob1: any;
  org_logo: any;
  provider_logo: any;
  org_working_hour_id = "";
  datetime_status = "";
  image_status = "";
  image_name = "";
  imagetype = "";

providerdateogbarth:any

providerpractisingstarteddate:any
  
  //new code start
  timeday = "";
  timestart = "";
  timeend="";
  timedayslot="";
  //new code end
  working_day = "";
  dropdownSettings = {};
  responsedata = "";
  responsedataava="";
  responsedatacontact=""
  //organisationid=0
  docimage = "";
  orglist: any;
  //new code 1-3-2019
  planlist: any;
  listSubscribedPlans: any;
  listexistinguser: any;
  //new code end 1-3-2019
  isReadOnly:boolean=false
  citylist: any;
  statelist: any;
  getCountries: any;
  specialities: any;
  PersonalDisabled:boolean=false
  AvalabillityDisabled: boolean = true;
  ContactDisabled: boolean = true;
  SubscriptionDisabled: boolean = true;
  UserDisabled: boolean = true;
  selectedindex = 0;
  public show: boolean = false;
  public subplan: boolean = true;
  public subplanlist: boolean = false;
  public userform: boolean = false;
  public userformsearch: boolean = true;
  constructor(
    private renderer: Renderer,
    private formBuilder: FormBuilder,
    // private providerservice: OrganisationsService,
    private providerservice: ProvidersService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbar:MatSnackBar
  ) {}
  public documentGrp: FormGroup;
  public locationgroup: FormGroup;
  public contactgroup: FormGroup;
  public Subscription: FormGroup;
  public usergroup: FormGroup;
  public mobilesearch: FormGroup;
  public contactsubmit: FormGroup;
  public personalgroup: FormGroup;
  public availability: FormGroup;

  public publicorgname
  
public mondaywor:Boolean=false
public tuedaywor:Boolean=false
public weddaywor:Boolean=false
public thudaywor:Boolean=false
public fridaywor:Boolean=false
public satdaywor:Boolean=false
public sundaywor:Boolean=false

masterid:any

  public totalfiles: Array<File> = [];
  public totalFileName = [];
  selectedItems = [];
  public lengthCheckToaddMore = 0;
  public lengthCheckToaddMore1 = 0;
  org_specialities: {
    org_image_id: "";
    image_type: "";
    image_position: "";
  };
  ngOnInit() {
    this.personalgroup = this.formBuilder.group({
      mobileno: "",
      firstname: "",
      middlename: "",
      lastname: "",
      gender: "",
      countryid: "",
      state_id: "",
      city_id: "",
      pincode: "",
      emailid: "",
      dob: "",
      qualification: "",
      otherspeciality: "",
      consultationfees: "",
      practisingstarteddate: "",
      otp: "",
      org_logo: "",
      specialities: [[], Validators.required]
    });

this.availability = this.formBuilder.group({
mondaymorningstart:"",
mondaymorningend:"",
mondayafterstart:"",
mondayafterend:"",
mondayevestart:"",
mondayeveend:"",
tuedaymorningstart:"",
tuedaymorningend:"",
tuedayafterstart:"",
tuedayafterend:"",
tuedayevestart:"",
tuedayeveend:"",
weddaymorningstart:"",
weddaymorningend:"",
weddayafterstart:"",
weddayafterend:"",
weddayevestart:"",
weddayeveend:"",
thudaywor:"",
thudaymorningstart:"",
thudaymorningend:"",
thudayafterstart:"",
thudayafterend:"",
thudayevestart:"",
thudayeveend:"",
fridaymorningstart:"",
fridaymorningend:"",
fridayafterstart:"",
fridayafterend:"",
fridayevestart:"",
fridayeveend:"",
satdaymorningstart:"",
satdaymorningend:"",
satdayafterstart:"",
satdayafterend:"",
satdayevestart:"",
satdayeveend:"",
sundaymorningstart:"",
sundaymorningend:"",
sundayafterstart:"",
sundayafterend:"",
sundayevestart:"",
sundayeveend:""

    })

   
    this.mobilesearch = this.formBuilder.group({
      mobilenumber: ""
    });

    this.usergroup = this.formBuilder.group({
      mobilenumber: "",
      firstname: "",
      middlename: "",
      lastname: "",
      dob: "",
      otp: "",
      gender: "",
      countryid: "",
      stateid: "",
      cityid: "",
      pincode: "",
      emailid: "",
      isorgdefault: "",
      qtraquserid: ""
    });

    this.locationgroup = this.formBuilder.group({
      countryid: "",
      state_id: "",
      city_id: "",
      address1: "",
      address2: "",
      street: "",
      area: "",
      landmark: "",
      pincode: ""
    });

    this.contactgroup = this.formBuilder.group({
      mobile1: "",
      mobile2: "",
      phone1: "",
      phone2: "",
      emailid1: "",
      emailid2: "",
      email1visibility: "",
      email2visibility: "",
      mobile1visibility: "",
      mobile2visibility: "",
      phone1visibility: "",
      phone2visibility: "",
      website: ""
    });
  
    
    

    this.getorglist();
    this.getSpecialities();
    this.getcountry();
    this.dropdownSettings = {
      singleSelection: false,
      disabled:true,
      idField: "speciality_id",
      textField: "speciality_name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    //edit part start

   
        let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
        this.publicorgname=actorganistion.org_name



        let adminuser = JSON.parse(localStorage.getItem('Adminuser'));
        this.masterid=atob(adminuser.user_master_id)




        this.isReadOnly=true
        this.providerservice
          .geteditprovider(this.masterid)
          .subscribe(data => {

if(data.status_code=="200")
{
this.PersonalDisabled=false
this.AvalabillityDisabled = false;
this.ContactDisabled = false;
this.provider_id=this.masterid;


let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name

this.monany=1




if(data.Data.basic_details!=null)
{
 this.personalgroup.controls['mobileno'].setValue(data.Data.basic_details[0].user_name);
 this.personalgroup.controls['firstname'].setValue(data.Data.basic_details[0].first_name);
 this.personalgroup.controls['middlename'].setValue(data.Data.basic_details[0].middle_name);
 this.personalgroup.controls['lastname'].setValue(data.Data.basic_details[0].last_name);
 this.personalgroup.controls['pincode'].setValue(data.Data.basic_details[0].pincode);
 this.personalgroup.controls['emailid'].setValue(data.Data.basic_details[0].email_id);
 this.personalgroup.controls['gender'].setValue(data.Data.basic_details[0].gender);
 this.personalgroup.controls['countryid'].setValue(data.Data.basic_details[0].country_details[0].country_id);
 this.personalgroup.controls['state_id'].setValue(data.Data.basic_details[0].state_details[0].state_id);
 this.personalgroup.controls['city_id'].setValue(data.Data.basic_details[0].city_details[0].city_id);
 this.providerdateogbarth=data.Data.basic_details[0].dob 

 this.onconChange(data.Data.basic_details[0].country_details[0].country_id)
 this.onstateChange(data.Data.basic_details[0].state_details[0].state_id)
 

 this.providerdob=data.Data.basic_details[0].dob.split('-');
 this.providerdob1= {  
 date: {  
 year: parseInt(this.providerdob[0]),  
 month: parseInt(this.providerdob[1]),  
 day: parseInt(this.providerdob[2])  
 },
 }
 this.profilepicname=data.Data.basic_details[0].profile_pic
 this.provider_logo=data.Data.basic_details[0].profile_pic_url
}




if(data.Data.user_availabilities!=null)
{

for(var j=0;j<(data.Data.user_availabilities.length);j++)
{
if(data.Data.user_availabilities[j].working_day_id=="1")
{
this.mondaywor=true
if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['mondaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['mondaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['mondayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['mondayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['mondayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['mondayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}
}











if(data.Data.user_availabilities[j].working_day_id==2)
{
this.tuedaywor=true

if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['tuedaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['tuedaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['tuedayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['tuedayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}


if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['tuedayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['tuedayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}


}


if(data.Data.user_availabilities[j].working_day_id=="3")
{
this.weddaywor=true

if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['weddaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['weddaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['weddayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['weddayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['weddayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['weddayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}


}


if(data.Data.user_availabilities[j].working_day_id=="4")
{
this.thudaywor=true
if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['thudaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['thudaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['thudayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['thudayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}


if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['thudayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['thudayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}





}

if(data.Data.user_availabilities[j].working_day_id=="5")
{
this.fridaywor=true
if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['fridaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['fridaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}
if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['fridayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['fridayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['fridayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['fridayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}




}


if(data.Data.user_availabilities[j].working_day_id=="6")
{
this.satdaywor=true
if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['satdaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['satdaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}
if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['satdayafterstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['satdayafterend'].setValue(data.Data.user_availabilities[j].end_time);
}


if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['satdayevestart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['satdayeveend'].setValue(data.Data.user_availabilities[j].end_time);
}




}

if(data.Data.user_availabilities[j].working_day_id=="7")
{
this.sundaywor=true

if(data.Data.user_availabilities[j].slot_id=="1")
{
  this.availability.controls['sundaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['sundaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="2")
{
  this.availability.controls['sundaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['sundaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}

if(data.Data.user_availabilities[j].slot_id=="3")
{
  this.availability.controls['sundaymorningstart'].setValue(data.Data.user_availabilities[j].start_time);
  this.availability.controls['sundaymorningend'].setValue(data.Data.user_availabilities[j].end_time);
}
}

}
}



if(data.Data.user_personal_details!=null)
{
 this.personalgroup.controls['qualification'].setValue(data.Data.user_personal_details[0].qualification);
 this.personalgroup.controls['otherspeciality'].setValue(data.Data.user_personal_details[0].other_speciality);
 this.personalgroup.controls['consultationfees'].setValue(data.Data.user_personal_details[0].consultation_fees);
 this.selectedItems=data.Data.user_speciality
this.contactgroup.controls['mobile1'].setValue(data.Data.user_personal_details[0].mobile1);
this.contactgroup.controls['mobile2'].setValue(data.Data.user_personal_details[0].mobile2);
this.contactgroup.controls['phone1'].setValue(data.Data.user_personal_details[0].phone1);
this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
this.contactgroup.controls['phone2'].setValue(data.Data.user_personal_details[0].phone2);
this.contactgroup.controls['emailid1'].setValue(data.Data.user_personal_details[0].email1);
this.contactgroup.controls['emailid2'].setValue(data.Data.user_personal_details[0].email2);
this.contactgroup.controls['mobile1visibility'].setValue(data.Data.user_personal_details[0].mobile1_visibility)
this.contactgroup.controls['mobile2visibility'].setValue(data.Data.user_personal_details[0].mobile2_visibility)
this.contactgroup.controls['phone1visibility'].setValue(data.Data.user_personal_details[0].phone1_visibility)
this.contactgroup.controls['phone2visibility'].setValue(data.Data.user_personal_details[0].phone2_visibility)
this.contactgroup.controls['email1visibility'].setValue(data.Data.user_personal_details[0].email1_visibility)
this.contactgroup.controls['email2visibility'].setValue(data.Data.user_personal_details[0].email2_visibility)
this.contactgroup.controls['website'].setValue(data.Data.user_personal_details[0].website);

this.providerpractisingstarteddate=data.Data.user_personal_details[0].practising_started_date
 this.personalgroup.controls['dob'].setValue(this.providerdob1);
 this.practisingstarteddate=data.Data.user_personal_details[0].practising_started_date.split('-');
 this.practisingstarteddate1= {  
    date: {  
        year: parseInt(this.practisingstarteddate[2]),  
        month: parseInt(this.practisingstarteddate[1]),  
         day: parseInt(this.practisingstarteddate[0])  
    },
 }
 this.personalgroup.controls['practisingstarteddate'].setValue(this.practisingstarteddate1);


}



}
else if(data.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);

} 
else
{
  this.snackbar.open(data.Metadata.Message,'', {
    duration: 2000,
    });


  }
       
          });
      }
   
    //end edit form
  
  //ravi new code 19-12-2018
  createdocDatetime(
    timedayslot,
    timestart,
    timeend,
    timeday,
   editcheck
  ): FormGroup {
    return this.formBuilder.group({
      timedayslot: timedayslot,
      timestart: timestart,
      timeend: timeend,
      timeday: timeday,
      editcheck: editcheck
    });
  }

  //org_image_id,image_name

  createUploadDocuments(
    org_image_id,
    image_name,
    imageposition,
    imagetype,
    docimage,
    imagestatus,
    editcheck
  ): FormGroup {
    return this.formBuilder.group({
      org_image_id: org_image_id,
      image_name: image_name,
      image_position: imageposition,
      image_type: imagetype,
      documentFile: docimage,
      image_status: imagestatus,
      editcheck: editcheck
    });
  }
  getcountry() {
    this.providerservice.getCountries().subscribe(response => {
      this.getCountries = response.Data;
    });
  }

  onconChange(val) {
    this.onconvalueget = val;
    this.providerservice.getState(val).subscribe(response => {
      this.statelist = response.Data;
      
    });
  }

  onstateChange(val) {
    

    this.providerservice.getcity(val).subscribe(response => {
      this.citylist = response.Data;
      
    });
  }

  getorglist() {
    this.providerservice.getallorganisation().subscribe(response => {
      this.orglist = response.Data;
      
    });
  }

  getSpecialities() {
    this.providerservice.getSpecialities().subscribe(response => {
      this.specialities = response.Data;
      
    });
  }
  
  onFileChanged(event) {
    this.org_logo = event.target.files[0];
  }

  isAnswerProvided(eveb) {
    
  }

  notifyMe(event) {
    
  }

  addprop1(event) {
    if (event.target.checked) {
      this.show = true;
    } else {
      this.show = false;
    }
  }

public PersonalformSubmit(formValue: any) {


    let main_form: FormData = new FormData();



    for (let k = 0; k < formValue.specialities.length; k++) {
      main_form.append(
        "provider_speciality[]",
        formValue.specialities[k]["speciality_id"]
      );
    }




    main_form.append("profile_pic", this.org_logo);



    main_form.append("firstname", formValue.firstname);
    main_form.append("middlename", formValue.middlename);
    main_form.append("lastname", formValue.lastname);
    main_form.append("gender", formValue.gender);
    main_form.append("dob", this.providerdateogbarth);
    main_form.append("countryid", formValue.countryid);
    main_form.append("state_id", formValue.state_id);
    main_form.append("city_id", formValue.city_id);
    main_form.append("pincode", formValue.pincode);
    main_form.append("email", formValue.emailid);
    main_form.append("withprovider", "Yes");
    main_form.append("otherspeciality", formValue.otherspeciality);
    main_form.append("qualification", formValue.qualification);
    main_form.append("consultationfees", formValue.consultationfees);
    main_form.append("practisingstarteddate", formValue.providerpractisingstarteddate);
    


    this.providerservice
      .personaladd(this.profilepicname,this.provider_id,this.providerdateogbarth,this.providerpractisingstarteddate, main_form, formValue)
      .subscribe(data => {
        if (data.status_code == 200) {
        

//this.PersonalDisabled:boolean=false

         this.PersonalDisabled=true
          this.selectedindex = 1;
          this.AvalabillityDisabled = false;
          this.provider_idadd = data.Data;

        }
        else if(data.status_code=='402')
        {
            localStorage.clear();
            this.router.navigate(['/login']);
        
        } 
        else 
        {
          this.responsedata = data.Metadata.Message;
        }
      });


  }

public OnavailabilitySubmit(formValue: any) 
{




let main_form: FormData = new FormData();


if((formValue.mondaymorningstart!="")&&(formValue.mondaymorningend!=""))
{
    main_form.append("slot_type[0]", "Morning")
    main_form.append("provider_working_day[0]","Mon")
    main_form.append("start_time[0]",formValue.mondaymorningstart)
    main_form.append("end_time[0]",formValue.mondaymorningend)
}
if((formValue.mondayafterstart!="")&&(formValue.mondayafterend!=""))
{
    main_form.append("slot_type[1]", "Afternon")
    main_form.append("provider_working_day[1]","Mon")
    main_form.append("start_time[1]",formValue.mondayafterstart)
    main_form.append("end_time[1]",formValue.mondayafterend)
}
if((formValue.mondayevestart!="")&&(formValue.mondayeveend!=""))
{
    main_form.append("slot_type[2]", "Evening")
    main_form.append("provider_working_day[2]","Mon")
    main_form.append("start_time[2]",formValue.mondayevestart)
    main_form.append("end_time[2]",formValue.mondayeveend)
}
   
if((formValue.tuedaymorningstart!="")&&(formValue.tuedaymorningend!=""))
{
    main_form.append("slot_type[3]", "Morning")
    main_form.append("provider_working_day[3]","Tue")
    main_form.append("start_time[3]",formValue.tuedaymorningstart)
    main_form.append("end_time[3]",formValue.tuedaymorningend)
}
if((formValue.tuedayafterstart!="")&&(formValue.tuedayafterend!=""))
{
    main_form.append("slot_type[4]", "Afternon")
    main_form.append("provider_working_day[4]","Tue")
    main_form.append("start_time[4]",formValue.tuedayafterstart)
    main_form.append("end_time[4]",formValue.tuedayafterend)
}

if((formValue.tuedayevestart!="")&&(formValue.tuedayeveend!=""))
{
    main_form.append("slot_type[5]", "Evening")
    main_form.append("provider_working_day[5]","Tue")
    main_form.append("start_time[5]",formValue.tuedayevestart)
    main_form.append("end_time[5]",formValue.tuedayeveend)
}
if((formValue.weddaymorningstart!="")&&(formValue.weddaymorningend!=""))
{
    main_form.append("slot_type[6]", "Morning")
    main_form.append("provider_working_day[6]","Wed")
    main_form.append("start_time[6]",formValue.weddaymorningstart)
    main_form.append("end_time[6]",formValue.weddaymorningend)
}
if((formValue.weddayafterstart!="")&&(formValue.weddayafterend!=""))
{
    main_form.append("slot_type[7]", "Afternon")
    main_form.append("provider_working_day[7]","Wed")
    main_form.append("start_time[7]",formValue.weddayafterstart)
    main_form.append("end_time[7]",formValue.weddayafterend)
}
if((formValue.weddayevestart!="")&&(formValue.weddayeveend!=""))
{
    main_form.append("slot_type[8]", "Evening")
    main_form.append("provider_working_day[8]","Wed")
    main_form.append("start_time[8]",formValue.weddayevestart)
    main_form.append("end_time[8]",formValue.weddayeveend)
}
if((formValue.thudaymorningstart!="")&&(formValue.thudaymorningend!=""))
{
    main_form.append("slot_type[9]", "Morning")
    main_form.append("provider_working_day[9]","Thu")
    main_form.append("start_time[9]",formValue.thudaymorningstart)
    main_form.append("end_time[9]",formValue.thudaymorningend)
}
if((formValue.thudayafterstart!="")&&(formValue.thudayafterend!=""))
{
    main_form.append("slot_type[10]", "Afternon")
    main_form.append("provider_working_day[10]","Thu")
    main_form.append("start_time[10]",formValue.thudayafterstart)
    main_form.append("end_time[10]",formValue.thudayafterend)
}
if((formValue.thudayevestart!="")&&(formValue.thudayeveend!=""))
{
    main_form.append("slot_type[11]", "Evening")
    main_form.append("provider_working_day[11]","Thu")
    main_form.append("start_time[11]",formValue.thudayevestart)
    main_form.append("end_time[11]",formValue.thudayeveend)
}
if((formValue.fridaymorningstart!="")&&(formValue.fridaymorningend!=""))
{
    main_form.append("slot_type[12]", "Morning")
    main_form.append("provider_working_day[12]","Fri")
    main_form.append("start_time[12]",formValue.fridaymorningstart)
    main_form.append("end_time[12]",formValue.fridaymorningend)
}
if((formValue.fridayafterstart!="")&&(formValue.fridayafterend!=""))
{
    main_form.append("slot_type[13]", "Afternon")
    main_form.append("provider_working_day[13]","Fri")
    main_form.append("start_time[13]",formValue.fridayafterstart)
    main_form.append("end_time[13]",formValue.fridayafterend)
}
if((formValue.fridayevestart!="")&&(formValue.fridayeveend!=""))
{
    main_form.append("slot_type[14]", "Evening")
    main_form.append("provider_working_day[14]","Fri")
    main_form.append("start_time[14]",formValue.fridayevestart)
    main_form.append("end_time[14]",formValue.fridayeveend)
}
if((formValue.satdaymorningstart!="")&&(formValue.satdaymorningend!=""))
{
    main_form.append("slot_type[15]", "Morning")
    main_form.append("provider_working_day[15]","Sat")
    main_form.append("start_time[15]",formValue.satdaymorningstart)
    main_form.append("end_time[15]",formValue.satdaymorningend)
}
if((formValue.satdayafterstart!="")&&(formValue.satdayafterend!=""))
{
    main_form.append("slot_type[16]", "Afternon")
    main_form.append("provider_working_day[16]","Sat")
    main_form.append("start_time[16]",formValue.satdayafterstart)
    main_form.append("end_time[16]",formValue.satdayafterend)
}
if((formValue.satdayevestart!="")&&(formValue.satdayeveend!=""))
{
    main_form.append("slot_type[17]", "Evening")
    main_form.append("provider_working_day[17]","Sat")
    main_form.append("start_time[17]",formValue.satdayevestart)
    main_form.append("end_time[17]",formValue.satdayeveend)
}
if((formValue.sundaymorningstart!="")&&(formValue.sundaymorningend!=""))
{
    main_form.append("slot_type[18]", "Morning")
    main_form.append("provider_working_day[18]","Sun")
    main_form.append("start_time[18]",formValue.sundaymorningstart)
    main_form.append("end_time[18]",formValue.sundaymorningend)
}
if((formValue.sundayafterstart!="")&&(formValue.sundayafterend!=""))
{
    main_form.append("slot_type[19]", "Afternon")
    main_form.append("provider_working_day[19]","Sun")
    main_form.append("start_time[19]",formValue.sundayafterstart)
    main_form.append("end_time[19]",formValue.sundayafterend)
}
if((formValue.sundayevestart!="")&&(formValue.sundayeveend!=""))
{
    main_form.append("slot_type[20]", "Evening")
    main_form.append("provider_working_day[20]","Sun")
    main_form.append("start_time[20]",formValue.sundayevestart)
    main_form.append("end_time[20]",formValue.sundayeveend)
}



this.provider_idadd=this.provider_id


    this.providerservice
    .addeditavailability(this.provider_idadd, main_form)
    .subscribe(data => {
      if (data.status_code == 200) {
        
         // this.selectedindex = 2;

         this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
          });
      
          this.router.navigate(['/']);
        //this.AvalabillityDisabled = true;
        //this.PersonalDisabled=true;
        //this.provider_idadd = data.Data;

    
      } else if(data.status_code=='402')
      {
       
       
          localStorage.clear();
          this.router.navigate(['/login']);
      
      }else {


        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
          });
      



       // this.responsedataava = data.Metadata.Message;
      }
    });

  }
  //set  second box start

  public ContactSubmit(formValue: any) {





if(formValue.mobile1visibility==true||formValue.mobile1visibility==1)
{
  formValue.mobile1visibility=1
}
else
{
  formValue.mobile1visibility=0
}

if(formValue.mobile2visibility==true||formValue.mobile2visibility==1)
{
  formValue.mobile2visibility=1
}
else
{
  formValue.mobile2visibility=0
}

if(formValue.phone1visibility==true||formValue.phone1visibility==1)
{
  formValue.phone1visibility=1
}
else
{
  formValue.phone1visibility=0
}


if(formValue.phone2visibility==true||formValue.phone2visibility==1)
{
  formValue.phone2visibility=1
}
else
{
  formValue.phone2visibility=0
}


if(formValue.email1visibility==true||formValue.email1visibility==1)
{
  formValue.email1visibility=1
}
else
{
  formValue.email1visibility=0
}



if(formValue.email2visibility==true||formValue.email2visibility==1)
{
  formValue.email2visibility=1
}
else
{
  formValue.email2visibility=0
}





    if(this.provider_id!=0)
    {
      this.provider_idadd=this.provider_id
    
    }
    this.providerservice.addeditProviderContactDetails(
        this.provider_idadd,
        formValue
        )
      .subscribe(data => {
        if (data.status_code == 200) {
          //this.selectedindex = 3;
          
        
          this.router.navigate(['/providersList']);
        
        }
        else if(data.status_code=='402')
        {
            localStorage.clear();
            this.router.navigate(['/login']);
        
        } else {
          this.responsedatacontact = data.Metadata.Message;
        }
      });
  }

 
//1-10-2019





onproviderclick(event)
{
 
}





//1-10-2019


  statuschangeimage(testid, status) {
    this.providerservice.updateimagestatus(testid, status).subscribe(data => {
      
      if (data.status_code == 200) {
        
      } else {
        
      }
    });
  }

  //statuschangedatetime

  statuschangedatetime(testid, status) {
    this.providerservice
      .updatedatetimestatus(testid, status)
      .subscribe(data => {
        
        if (data.status_code == 200) {
          
        } else {
          
        }
      });
  }

  planlistdata() {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }

    this.providerservice
      .getplandescription(this.organisationdataid)
      .subscribe(data => {
        if (data.status_code == 200) {
          
          this.subplan = false;
          this.subplanlist = true;

          this.planlist = data.Data;
          
        } else {
          
        }
      });
  }

  sublist() {
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }
    this.subplan = true;
    this.subplanlist = false;
    
    this.providerservice
      .listSubscribedPlans(this.organisationdataid)
      .subscribe(data => {
        if (data.status_code == 200) {
          
          //this.planlist=
          this.listSubscribedPlans = data.Data;
          
        } else {
          
        }
      });
  }

 

  divclick(planid, startdate, enddate) {
    
    
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }

    this.providerservice
      .SubscribedPlans(this.organisationdataid, planid, startdate, enddate)
      .subscribe(data => {
        

        if (data.status_code == 200) {
          

          this.selectedindex = 4;

          
        } else {
          this.suberror = data.Metadata.Message;
          
        }
      });
  }

 





  OnUsersubmit(formValue: any) {
    
    if (this.provider_id != 0) {
      this.organisationdataid = this.provider_id;
    } else {
      this.organisationdataid = this.provider_id;
    }

    this.providerservice
      .addQTRAQUserDetails(formValue, this.dob_date, this.organisationdataid)
      .subscribe(data => {
        
        if (data.status_code == 200) {
          this.router.navigate(["/"]);
        } else {
          this.useraddresponse = data.Metadata.Message;
        }
      });
  }

  genetarotp(convalueget, mobile) {


    this.providerservice.generateOtp(convalueget, mobile).subscribe(data => {
      
      if (data.status_code == 200) {
        //otp
        this.usergroup.controls["otp"].setValue(data.otp);
        
        //  this.selectedindex=4
      }
      
      else {
        this.snackbar.open(data.Metadata.Message,'', {
          duration: 2000,
          });
        //this.otperrormsg = data.msg;
        // this.userformsearch=false
        // this.userform=true
      }
    });
  }

  ondobChanged(event: IMyDateModel): void {
    if (event.formatted !== "") {
      
      this.providerdateogbarth = event.formatted;
    }
  }

  onpractisingstarteChanged(event: IMyDateModel): void {
    if (event.formatted !== "") {
      
      this.providerpractisingstarteddate = event.formatted;
    }
  }



  toggleHideShow(action:string){
    if(action == "show"){
      this.toggleVal = true;
    }else{
      this.toggleVal = false;
    }
  }


applaytoall(slot,starttime,endtime)
{

if((starttime!="")&&(endtime!=""))
{
  this.mondaywor=true

  this.tuedaywor=true
  
  this.weddaywor=true
  
  this.thudaywor=true
  
  this.fridaywor=true
  this.satdaywor=true
  this.sundaywor=true
}




if(slot=="1")
{
  this.availability.controls['mondaymorningstart'].setValue(starttime);
  this.availability.controls['mondaymorningend'].setValue(endtime);


  this.availability.controls['tuedaymorningstart'].setValue(starttime);
  this.availability.controls['tuedaymorningend'].setValue(endtime);

  this.availability.controls['weddaymorningstart'].setValue(starttime);
  this.availability.controls['weddaymorningend'].setValue(endtime);

  this.availability.controls['thudaymorningstart'].setValue(starttime);
  this.availability.controls['thudaymorningend'].setValue(endtime);

  this.availability.controls['fridaymorningstart'].setValue(starttime);
  this.availability.controls['fridaymorningend'].setValue(endtime);

  this.availability.controls['satdaymorningstart'].setValue(starttime);
  this.availability.controls['satdaymorningend'].setValue(endtime);

  this.availability.controls['sundaymorningstart'].setValue(starttime);
  this.availability.controls['sundaymorningend'].setValue(endtime);




}else if(slot=="2")
{
  this.availability.controls['mondayafterstart'].setValue(starttime);
  this.availability.controls['mondayafterend'].setValue(endtime);

  this.availability.controls['tuedayafterstart'].setValue(starttime);
  this.availability.controls['tuedayafterend'].setValue(endtime);
  this.availability.controls['weddayafterstart'].setValue(starttime);
  this.availability.controls['weddayafterend'].setValue(endtime);
  this.availability.controls['thudayafterstart'].setValue(starttime);
  this.availability.controls['thudayafterend'].setValue(endtime);
  this.availability.controls['fridayafterstart'].setValue(starttime);
  this.availability.controls['fridayafterend'].setValue(endtime);
  this.availability.controls['satdayafterstart'].setValue(starttime);
  this.availability.controls['satdayafterend'].setValue(endtime);
  this.availability.controls['sundayafterstart'].setValue(starttime);
  this.availability.controls['sundayafterend'].setValue(endtime);




}else if(slot=="3")
{
  this.availability.controls['mondayevestart'].setValue(starttime);
  this.availability.controls['mondayeveend'].setValue(endtime);
  this.availability.controls['tuedayevestart'].setValue(starttime);
  this.availability.controls['tuedayeveend'].setValue(endtime); 
   this.availability.controls['weddayevestart'].setValue(starttime);
  this.availability.controls['weddayeveend'].setValue(endtime);
  this.availability.controls['thudayevestart'].setValue(starttime);
  this.availability.controls['thudayeveend'].setValue(endtime);
  this.availability.controls['fridayevestart'].setValue(starttime);
  this.availability.controls['fridayeveend'].setValue(endtime);
  this.availability.controls['satdayevestart'].setValue(starttime);
  this.availability.controls['satdayeveend'].setValue(endtime);
  this.availability.controls['sundayevestart'].setValue(starttime);
  this.availability.controls['sundayeveend'].setValue(endtime);
}
}

clear(field1,field2)
{

this.availability.controls[field1].setValue("");
this.availability.controls[field2].setValue("");

}


oncheckboxchange(event,day)
{




if(event.target.checked==false)
{



if(day=="monday")
{
  this.availability.controls['mondaymorningstart'].setValue("");
  this.availability.controls['mondaymorningend'].setValue("");

  this.availability.controls['mondayevestart'].setValue("");
  this.availability.controls['mondayeveend'].setValue("");
  this.availability.controls['mondayafterstart'].setValue("");
  this.availability.controls['mondayafterend'].setValue("");
}

if(event.target.checked=="tuesday")
{
 
  this.availability.controls['tuedaymorningstart'].setValue("");
  this.availability.controls['tuedaymorningend'].setValue("");

  this.availability.controls['tuedayafterstart'].setValue("");
  this.availability.controls['tuedayafterend'].setValue("");

  this.availability.controls['tuedayevestart'].setValue("");
  this.availability.controls['tuedayeveend'].setValue(""); 
}

if(event.target.checked=="wednesday")
{
  this.availability.controls['weddayevestart'].setValue("");
  this.availability.controls['weddayeveend'].setValue("");
  this.availability.controls['weddayafterstart'].setValue("");
  this.availability.controls['weddayafterend'].setValue("");
  this.availability.controls['weddaymorningstart'].setValue("");
  this.availability.controls['weddaymorningend'].setValue("");
}

if(event.target.checked=="thursday")
{
  this.availability.controls['thudayevestart'].setValue("");
  this.availability.controls['thudayeveend'].setValue("");
  this.availability.controls['thudayafterstart'].setValue("");
  this.availability.controls['thudayafterend'].setValue("");

  this.availability.controls['thudaymorningstart'].setValue("");
  this.availability.controls['thudaymorningend'].setValue("");
}

if(event.target.checked=="friday")
{
  this.availability.controls['fridaymorningstart'].setValue("");
  this.availability.controls['fridaymorningend'].setValue("");
  this.availability.controls['fridayevestart'].setValue("");
  this.availability.controls['fridayeveend'].setValue("");
  this.availability.controls['fridayafterstart'].setValue("");
  this.availability.controls['fridayafterend'].setValue("");
}

if(event.target.checked=="saturday")
{
  this.availability.controls['satdayevestart'].setValue("");
  this.availability.controls['satdayeveend'].setValue("");
  this.availability.controls['satdayafterstart'].setValue("");
  this.availability.controls['satdayafterend'].setValue("");

  this.availability.controls['satdaymorningstart'].setValue("");
  this.availability.controls['satdaymorningend'].setValue("");
}

if(event.target.checked=="sunday")
{
  this.availability.controls['sundayevestart'].setValue("");
  this.availability.controls['sundayeveend'].setValue("");
  this.availability.controls['sundayafterstart'].setValue("");
  this.availability.controls['sundayafterend'].setValue("");
  this.availability.controls['sundaymorningstart'].setValue("");
  this.availability.controls['sundaymorningend'].setValue("");
}





}


}


  //set second box end
}
