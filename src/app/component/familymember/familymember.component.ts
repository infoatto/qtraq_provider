import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from './../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ AssignnewtokenComponent } from './../../component/assignnewtoken/assignnewtoken.component'

import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'familymember',
  templateUrl: './familymember.component.html',
  styleUrls: ['./familymember.component.scss']
})
export class FamilymemberComponent implements OnInit {

/*
dialogmonitorque
constructor() { }
ngOnInit() {
  }
*/
notification:any;
  totalRecords = "";
  page=0;

  family_memberslist:any

bookingfor:any
queue_id:any

size = 10;
//constructor(private notificationservice:NotificationService) { }
constructor(public dialogRef: MatDialogRef<AssignnewtokenComponent>,
@Inject(MAT_DIALOG_DATA) public data: any,
private assignnewtoken:AssignnewtokenService,
private snackbar:MatSnackBar,private router: Router
) {


this.bookingfor=this.data.bookingfor
this.queue_id=this.data.queue_id

}
ngOnInit() 
{


this.assignnewtoken.familymemberdata(this.bookingfor)
.subscribe(
(response) => 
{



//this.queue_id=event


if(response.status_code=="200")
{


this.family_memberslist=response.Data

// this.token_no=response.last_token.token_no
// this.token_time=response.last_token.token_no_converted_slot
// this.token_date=this.queselecteddate
}
 else if(response.status_code=='402')
 {
      localStorage.clear();
      this.router.navigate(['/login']);
 }
 else
 {
 this.snackbar.open(response.Metadata.Message,'', {
 duration: 2000,
 });
 }
 })

 }

/*
getnotificationlist(page: number,notificationfilter)
  {
  this.notificationservice.getnotificationlist((page - 1) ,this.size,notificationfilter)
  .subscribe(
  (response) => 
  {

  this.notification=response.Data
  this.totalRecords=response.total_count
  this.page=page
},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
 onSubmit() 
{
  this.getnotificationlist(1,this.notificationfilter)
}
  clearfilter()
  {
  this.notificationfilter.specialityname=""
  this.notificationfilter.specialitystatus=""
  this.getnotificationlist(1,this.notificationfilter)
}
  pageChanged(event)
  {
  this.getnotificationlist(event,this.notificationfilter)
  }
  statuschange(specialityid,status)
  {
    
  this.notificationservice.updatestatus(specialityid,status)
  .subscribe(
  data => {
  
  if(data.status_code==200)
  {
  
  }
  else
  {
  
  }
  })
  }
*/
/*
onNoClick(token_no,token_status,token_conveted_time)
{

let data={'token_no':token_no,'token_status':token_status,
'token_conveted_time':token_conveted_time}

 this.dialogRef.close(data);
}
*/
onfamilyClick(familymemberid)
{
  let data={'familymemberid':familymemberid}
  
  this.dialogRef.close(data);
  
}

}
