import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilymemberComponent } from './familymember.component';

describe('FamilymemberComponent', () => {
  let component: FamilymemberComponent;
  let fixture: ComponentFixture<FamilymemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilymemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilymemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
