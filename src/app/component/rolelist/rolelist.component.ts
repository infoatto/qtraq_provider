import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { CommonService } from './../../_services';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-rolelist',
  templateUrl: './rolelist.component.html',
  styleUrls: ['./role.component.scss']
})
export class RolelistComponent implements OnInit {

  organisationlist: any

admin:any
frontdesk:any
doctor:any


  constructor(
    private router:Router,
    private orgservice: CommonService,
    public dialogRef: MatDialogRef<RolelistComponent>,
     ) {}

  ngOnInit() 
  {
   this.orgservice.getorgrole()
    .subscribe(
    data => {
          if(data.Data)
          {
this.admin=data.Data[0].isAssigned
this.frontdesk=data.Data[1].isAssigned
this.doctor=data.Data[2].isAssigned
          }
        })
  }




onNoClick(actrole): void {
  //localStorage.setItem('orguserrole',"")


  var actas={
"activerole":actrole
  }
 
   localStorage.setItem('activeas', JSON.stringify(actas));
       this.dialogRef.close();
  
       this.router.navigate(['/']); 
  
      }
}
