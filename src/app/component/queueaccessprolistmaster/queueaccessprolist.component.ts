import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl,FormArray } from '@angular/forms';
//import { SpecialityService } from 'src/app/_services/speciality.service';
//import { SystemroleService } from './../../_services/systemrole.service';
import { ProvidersService } from "./../../_services/providers.service";
import { QueueaccessprolistService } from './../../_services/queueaccessprolist.service';
import {MatSnackBar} from '@angular/material';
import { CommonService } from "./../../_services/common.service";
@Component({
  selector: 'queueaccessprolist',
  templateUrl: './queueaccessprolist.component.html',
  styleUrls: ['./queueaccessprolist.component.scss']
})



export class QueueaccessprolistComponent implements OnInit {
  toggleVal:boolean = false;
  errorMessage: string;
  submitted = false;
 // public documentGrp: FormGroup;
 queueaccesslists:Boolean=true

 queueaccesslistscheckbox:Boolean=false

 queueaccesslist:any;
  totalRecords = "";
  page=0;
  size = 10;
  queue_details="";
  user_queue_data="";
  NAMES = [];
  publicorgname:any
  lastname:any
  firstname:any

  /*user_id:"";
  is_frontdesk:"";
  is_doctordesk:"";
  is_admindesk:"";
  profileimage_path:"";
  user_name:"";
  first_name:"";
*/


  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private queueaccessprolist:QueueaccessprolistService,
    private snackbar:MatSnackBar,
    private providerserv: ProvidersService,
    private cs:CommonService
  ) 
  { 
  }
  ngOnInit() 
  {
let actorganistion = JSON.parse(localStorage.getItem('actorganistion'));
this.publicorgname=actorganistion.org_name
this.providerserv
.checkOrganisationslist()
.subscribe(
response => {
if(response.status_code=="200")
{
if(response.Data.length>1)
{
this.getaccessprolist(1);
}
else
{
this.onNoClick(response.Data[0].last_name,response.Data[0].first_name
,response.Data[0].user_master_id,response.Data[0].profileimage_path)
}
}
else
{
  this.router.navigate(['/']);
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
    });
}
})
}

ngAfterViewInit()
{
//  $(".quepro-list-table").clone(true).appendTo('#table-scroll').addClass('clone'); 
}


getaccessprolist(page: number)
{
this.queueaccessprolist.getOrgProviders((page - 1) ,this.size)
.subscribe(
  (response) => 
  {
if(response.status_code=="200")
{

  this.queueaccesslist=response.Data
  this.totalRecords=response.total_count
  this.page=page
}
else if(response.status_code=="402")
{
  localStorage.clear();
    this.router.navigate(['/login']);

}
else
{
  
  this.snackbar.open(response.Metadata.Message,'', {
    duration: 2000,
  })
  this.router.navigate(['/']);
}


},
  (err: any) => console.log("error",err),
  () =>
  console.log("getCustomersPage() retrieved customers for page:", + page)
);
}
  

pageChanged(event)
{
  this.getaccessprolist(event)
}


onNoClick(user_name,first_name,user_master_id,profileimage_path)
{

this.lastname=user_name
this.firstname=first_name
  
  this.queueaccessprolist.getAllQueueAccessDetails(user_master_id)
  .subscribe(
    (response) => 
    {

if(response.status_code=="200")
{
      this.queueaccesslists=false
      this.queueaccesslistscheckbox=true
      this.queue_details = response.Data.queue_details
      this.user_queue_data = response.Data.user_queue_data
}
else if(response.status_code=='402')
{
    localStorage.clear();
    this.router.navigate(['/login']);

}
else
{
this.snackbar.open(response.Metadata.Message,'', {
duration: 2000,
});
//this.errorMessage=response.Metadata.Message

} 
  
    },
    (err: any) => console.log("error",err),
    () =>
    console.log("getCustomersPage() retrieved customers for page:")
  );



}


checkboxcheckfunc(event,queue_id,user_id)
{
var checkaccess  
if(event.target.checked)
{
    checkaccess="Yes"
}
  else
 {
   checkaccess="No"
 }
  
  let newName = {
    queue_id:queue_id,
    provider_id:user_id,
    provider_access:checkaccess
 };
 this.NAMES.push(newName);
}


checkboxformsubmit()
{



let main_form: FormData = new FormData(); 
for (let k = 0; k < (this.NAMES.length); k++) 
{
main_form.append("provider_id[]",this.NAMES[k].provider_id)
main_form.append("queue_id[]",this.NAMES[k].queue_id)
main_form.append("provider_access[]",this.NAMES[k].provider_access)
//main_form.append("speciality_id[]", formValue.skills[k]['speciality_id'])
}




this.queueaccessprolist.addqueassess(main_form)
  .subscribe(
    (response) => 
    {
      if(response.status_code==200)
      {
      this.queueaccesslists=true
          this.queueaccesslistscheckbox=false
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
            });
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
          });


      }



    },
    (err: any) => console.log("error",err),
    () =>
    console.log("getCustomersPage() retrieved customers for page:")
  );
}




toggleHideShow(action:string){
  if(action == "show"){
    this.toggleVal = true;
  }else{
    this.toggleVal = false;
  }
}


backtohome()
{
this.cs.backtohome()
}


backqueueaccessprolist()
{
this.queueaccesslistscheckbox=false
this.queueaccesslists=true
this.getaccessprolist(1);
}

//http://localhost:4200/queueaccessprolist



}
