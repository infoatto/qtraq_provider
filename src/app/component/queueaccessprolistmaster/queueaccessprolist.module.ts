import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { QueueaccessprolistComponent } from './queueaccessprolist.component'
import {MatTabsModule} from '@angular/material/tabs';
import {QueueaccessprolistRoutingModule  } from "./queueaccessprolist-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
//import{ TimeFormatPipe } from './pipes/time.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
//import { SidebarComponent } from '../sidebar/sidebar.component'
@NgModule({
  imports: [CommonModule,
    QueueaccessprolistRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     FormsModule
    ],
  declarations: [QueueaccessprolistComponent]
})
export class QueueaccessprolistModule {}