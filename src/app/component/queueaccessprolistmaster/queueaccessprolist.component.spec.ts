import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueaccessprolistComponent } from './queueaccessprolist.component';

describe('AddeditSystemRolesMasterComponent', () => {
  let component: QueueaccessprolistComponent;
  let fixture: ComponentFixture<QueueaccessprolistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueaccessprolistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueaccessprolistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
