import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogmonitorqueComponent } from './dialogmonitorque.component';

describe('DialogmonitorqueComponent', () => {
  let component: DialogmonitorqueComponent;
  let fixture: ComponentFixture<DialogmonitorqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogmonitorqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogmonitorqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
