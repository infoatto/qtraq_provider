import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from './../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ AssignnewtokenComponent } from './../../component/assignnewtoken/assignnewtoken.component'
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import {MatSnackBar} from '@angular/material';
interface event{
    formatted:any;
}

@Component({
  selector: 'dialogmonitorque',
  templateUrl: './dialogmonitorque.component.html',
  styleUrls: ['./dialogmonitorque.component.scss']
})
export class DialogmonitorqueComponent implements OnInit {
  notification:any;
  totalRecords = "";
  page=0;
  queuelist:any
  token_no:any
  tokennumberexist:any
  queueexist:any;
  dateexist:any;
  tokendate:any;
  queue_id:any
  is_buffer_token_started:Boolean=false
  size = 10;
  showfilterdata:Boolean=true
  quelistdata:any;
  selectedproid:any
  queueinfo:any = [];
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate()-1;
  currentMonth: any = this.mytime.getUTCMonth() + 1;
  queueDate:any;
  queueId:any;
  dateBack:boolean = true;
  queueNext:boolean = false;
  queueBack:boolean = true;
  token_status:any;
  token_conveted_time:any;
  token_time:any;
  queue_date:any;
  providername:any; 
  myOptionsassign:INgxMyDpOptions ={
    dateFormat: 'dd-mm-yyyy',
    disableUntil:{year: this.currentYear, month: this.currentMonth, day: this.currentDate}
  }
 

  constructor(
    public dialogRef: MatDialogRef<AssignnewtokenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private assignnewtoken:AssignnewtokenService,
    private snackbar:MatSnackBar,private router: Router
  ){
    this.tokendate=this.data.tokendate;
    this.queue_id=this.data.queue_id;
    this.queueexist = this.data.queue_id;
    this.dateexist = this.data.tokendate;
    this.tokennumberexist=this.data.token_noexist;
    this.quelistdata=this.data.quelistdata;
    this.selectedproid=this.data.selectedproid;
    this.providername = (this.data.providername != undefined)?this.data.providername:"";
  }

  ngOnInit(){
    this.showfilterdata=true;
    this.queueinfo.net_available = 0;
    this.queueinfo.net_issued = 0;
    this.queueinfo.queue_date = this.convertDate(this.tokendate);
    this.queueinfo.provider_name = this.providername;
    if(this.queue_id > 0){
      this.assignnewtoken.getmonitorqueQueues(this.tokendate,this.queue_id).subscribe((response) => {
        if(response.status_code=="200"){
          this.queueinfo = response.Data.queue_info
          this.queuelist=response.Data.token_result
          this.token_no=response.Data.last_token.token_no
          this.token_status=response.Data.last_token.token_status
          this.token_conveted_time= response.Data.last_token.token_conveted_time
          this.token_time=response.Data.last_token.token_time
          this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
  
          let today = new Date();
          today.setHours(0,0,0,0);
          this.dateexist = response.Data.queue_info.queue_date_nonformated;
          this.tokendate = response.Data.queue_info.queue_date_nonformated;
          var slipdate = response.Data.queue_info.queue_date_nonformated.split("-");
          var afterDate = new Date(parseInt(slipdate[2])+"/"+parseInt(slipdate[1])+"/"+parseInt(slipdate[0]));
          if(today.getTime() < afterDate.getTime()){
            this.dateBack = false;
          }else{
            this.dateBack = true;
          }
        }else if(response.status_code=='402'){
          localStorage.clear();
          this.router.navigate(['/login']);
        }else{
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000});
          this.queuelist=null
          this.queueinfo = this.queueinfo;
        }
      })
    }
  }

  onNoClick(token_no:any=0,token_status:any,token_conveted_time:any,token_time:any){
    let data={
      'token_no':token_no,
      'token_status':token_status,
      'token_conveted_time':token_conveted_time,
      'token_time':token_time,
      'is_buffer_token_started':this.is_buffer_token_started,
      "tokendate":this.tokendate,
      'queue_id':this.queue_id,
      'selecteddate': this.tokendate,
      'selectedqueue':this.queue_id
    }
    this.dialogRef.close(data);
  }

  close(){
    // this.dialogRef.close()
    let data={
      'token_no':this.token_no,
      'token_status':this.token_status,
      'token_conveted_time':this.token_conveted_time,
      'token_time':this.token_time,
      'is_buffer_token_started':this.is_buffer_token_started,
      "tokendate":this.tokendate,
      'queue_id':this.queue_id,
      'selecteddate': this.tokendate,
      'selectedqueue':this.queue_id
    }
    this.dialogRef.close(data);
  }

  closeDialog(){
    let data={
      'token_no':'0'
    }
    this.dialogRef.close(data)
  }

  ondatacheckboxcheck(event){
    if(event.target.checked==true){
      this.showfilterdata=false
    }else{
      this.showfilterdata=true
    }
  }

  onAssignDateChanged(event:any){
    if(event.formatted !='' ){
      let today = new Date();
      today.setHours(0,0,0,0);
      var slipdate = event.formatted.split("-");
      var afterDate = new Date(parseInt(slipdate[2])+"/"+parseInt(slipdate[1])+"/"+parseInt(slipdate[0]));
      if(today.getTime() < afterDate.getTime()){
        this.dateBack = false;
      }else{
        this.dateBack = true;
      }
      this.assignnewtoken.getProviderQueues(event.formatted,this.selectedproid).subscribe((response) => {
        if(response.status_code == 200){
          this.quelistdata = response.Data;
          this.queue_id = response.Data[0].queue_id;
          this.onqueselect(this.queue_id);
        }else{
          this.quelistdata = [];
          this.queueinfo.net_available = 0;
          this.queueinfo.net_issued = 0;
          this.queueinfo.queue_date = this.convertDate(this.tokendate);
          this.queue_id = '';
          this.queuelist=null
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
        }
      })
      // this.queueinfo.queue_date =  event.formatted;
      this.tokendate = event.formatted;
      
    }
  }

  onqueselect(event:any){
    this.queue_id=event;
    var indexNum = this.quelistdata.findIndex((element:any)=> {
      return (element.queue_id == this.queue_id);
    });
    if(this.quelistdata.length == parseInt(indexNum)+1){
      this.queueNext = true;
    }else{
      this.queueNext = false;
    }
    if(parseInt(indexNum) == 0){
      this.queueBack = true;
    }else{
      this.queueBack = false;
    }
    this.commonfunctioncall();
  }

  commonfunctioncall(){
    this.assignnewtoken.getmonitorqueQueues(this.tokendate,this.queue_id).subscribe((response) => {
      if(response.status_code=="200"){
        this.queueinfo = response.Data.queue_info
        this.queuelist = response.Data.token_result
        this.token_no = (response.Data.last_token != undefined)?response.Data.last_token.token_no:0;
        this.is_buffer_token_started=response.Data.queue_info.is_buffer_token_started
      }else if(response.status_code=='402'){
        localStorage.clear();
        this.router.navigate(['/login']);
      }else{
        this.snackbar.open(response.Metadata.Message,'', {
          duration: 2000,
        });
        this.queuelist=null
      }
    })
  }

  queueDateNavigateChange(nav:any){
    var tokenSelectedDate = "";
    if(this.tokendate.formatted != "" && this.tokendate.formatted != undefined){
      tokenSelectedDate = this.tokendate.formatted;
    }else{
      tokenSelectedDate = this.tokendate;
    }
    var slipdate = tokenSelectedDate.split("-");
    var newdate = new Date(slipdate[2]+"/"+slipdate[1]+"/"+slipdate[0]);
    if(nav == "right"){
      newdate.setDate(newdate.getDate() + 1);
      this.dateBack = false;
    }else{
      newdate.setDate(newdate.getDate() - 1);
    }
    let toyear:any = newdate.getFullYear();
    let todate:any  = newdate.getDate();
    let tomonth:any = newdate.getMonth() + 1;
    let tokendateValue = {
      formatted:todate+'-'+tomonth+'-'+toyear
    }
    let today = new Date();
    today.setHours(0,0,0,0);
    let afterDate = new Date(newdate);
    if(today.getTime() < afterDate.getTime()){
      this.dateBack = false;
    }else{
      this.dateBack = true;
    }
    this.onAssignDateChanged(tokendateValue);
  }

  queueNavigateChange(nav:any,queueList:any){
    var indexNum = queueList.findIndex((element:any)=> {
      return (element.queue_id == this.queue_id);
    });
    if(nav == "right"){
      this.queueBack = false;
      if(queueList.length >= indexNum+2){
        this.queue_id = queueList[parseInt(indexNum)+1].queue_id;
      }
      if(queueList.length == parseInt(indexNum)+2){
        this.queueNext = true;
      }else{
        this.queueNext = false;
      }
    }else{
      this.queueNext = false;
      var indexNum = queueList.findIndex((element:any)=> {
        return (element.queue_id == this.queue_id);
      });
      if(queueList.length >= parseInt(indexNum)-2){
        this.queue_id = queueList[parseInt(indexNum)-1].queue_id;
      }
      if(parseInt(indexNum)-1 == 0){
        this.queueBack = true;
      }else{
        this.queueBack = false;
      }
    }
    if(queueList.length == 1){
      this.queueBack = false;
      this.queueNext = false;
    }
    this.onqueselect(this.queue_id);
  }

  convertDate(paramDate:any){
    var paramdateArray = paramDate.split("-"); 
    var d = new Date(paramdateArray[2]+"-"+paramdateArray[1]+"-"+paramdateArray[0]);
    var finalDayname = d.toString().substr(0, 3);
    var finalMonth = d.toString().substr(4, 3);
    var finalDate = d.getDate();
    var finalYear = d.getFullYear(); 
    return finalDayname+', '+finalDate+', '+finalMonth+' '+finalYear;
  }
}
