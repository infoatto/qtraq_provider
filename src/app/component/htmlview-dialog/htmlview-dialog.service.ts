import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HtmlviewDialogComponent } from './htmlview-dialog.component';
@Injectable()
export class HtmlviewDialogService {
  constructor(private modalService: NgbModal) { }
  public viewHtml(
    htmlContent: string,
    dialogSize: 'sm'|'lg' = 'lg'): Promise<boolean> {
      const modalRef = this.modalService.open(HtmlviewDialogComponent, { size: dialogSize });
      modalRef.componentInstance.htmlContent = htmlContent;
      return modalRef.result;
    }
}
