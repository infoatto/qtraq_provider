import { Component, OnInit, Input, Pipe, PipeTransform, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-htmlview-dialog',
  templateUrl: './htmlview-dialog.component.html',
  styleUrls: ['./htmlview-dialog.component.scss']
})
export class HtmlviewDialogComponent {
  @Input() htmlContent: string;
  @Input() contents:any;
  constructor(
    private dialogRef: MatDialogRef<HtmlviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sanitized: DomSanitizer
  ) { 
    
  }
  onNoClick(){
    this.dialogRef.close
  } 

  printDiv(contents:any) {
    // var originalContents = document.body.innerHTML;
    // // let parsedContent = this.transform(contents);
    // document.body.innerHTML = contents;
    window.print();
    // document.body.innerHTML = originalContents;
  }

  printBill(htmlContent){
    var printWin = window.open("","processPrint");
    printWin.document.open();
    printWin.document.write(htmlContent);
    printWin.print();
    printWin.close();
  }


  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
