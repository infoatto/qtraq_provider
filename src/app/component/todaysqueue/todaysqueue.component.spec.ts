import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TodaysqueueComponent } from './todaysqueue.component';

describe('TodaysqueueComponent', () => {
  let component: TodaysqueueComponent;
  let fixture: ComponentFixture<TodaysqueueComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaysqueueComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(TodaysqueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
