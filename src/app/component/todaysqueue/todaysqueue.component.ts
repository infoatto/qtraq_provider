import { Component,ViewChild,	OnInit } from '@angular/core';
import { Router,ActivatedRoute,Params } from '@angular/router';
import { FormBuilder,FormControl,FormGroup,FormArray,Validators,NgForm } from '@angular/forms';
import { TodaysqueueService } from './../../_services/todaysqueue.service';
import { INgxMyDpOptions,IMyDateModel } from 'ngx-mydatepicker';
import { MatSnackBar } from '@angular/material';
import { NgProgress } from 'ngx-progressbar';
import { SearchtokensService } from './../../_services/searchtokens.service';
import { MessagequelistComponent } from './../../component/messagequelist/messagequelist.component'
import { AssignnewtokenService } from './../../_services/assignnewtoken.service';
import { PatienthistoryService } from './../../_services/patienthistory.service';
import { RequestreceivedService } from './../../_services/requestsreceived.service';
import { DatePipe } from '@angular/common';
import { MatDialogConfig } from '@angular/material';
import { DialogmonitorqueComponent } from './../../component/dialogmonitorque/dialogmonitorque.component';
import { MatDialog,MatDialogRef,MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmationDialogService } from './../../component/confirmation-dialog/confirmation-dialog.service';
import { AssignnewtokendialogComponent } from './../../component/assignnewtokendialog/assignnewtokendialog.component';
import { TransactionqueuesummaryComponent } from './../../component/transactionqueuesummary/transactionqueuesummary.component';
import { OnholdprocessdialogComponent } from './../../component/onholdprocessdialog/onholdprocessdialog.component';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
import { Location } from '@angular/common';
import { HtmlviewDialogService } from '../htmlview-dialog/htmlview-dialog.service';
import { FollowupdialogComponent } from '../followupdialog/followupdialog.component';

@Component({
	selector: 'todaysqueue',
	templateUrl: './todaysqueue.component.html',
	styleUrls: ['./todaysqueue.component.scss']
})
export class TodaysqueueComponent implements OnInit {

	public mytime: Date = new Date();
	is_buffer_token_started: Boolean = false
	afterlastarrivedchecked: Boolean = false
	currentYear: any = this.mytime.getUTCFullYear();
	currentDate: any = this.mytime.getUTCDate() - 1;
	currentMonth: any = this.mytime.getUTCMonth() + 1;
	changesequencelabel = ""
	changeseqcheck: any
	dropdownSettings = {};
	myOptions: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
	};
	myOptionsassign: INgxMyDpOptions = {
		dateFormat: 'dd-mm-yyyy',
		disableUntil: {
			year: this.currentYear,
			month: this.currentMonth,
			day: this.currentDate
		}
	}
	editdetailgender: any = ""
	queue_allowed_tokens: any
	dropdownSettingsnewdata = {};
	searchtokenallshow: Boolean = true
	checktoday = 0
	token_provider_id: any
	usermasterid: any
	searchtokendatadetail: any
	tokentype: any
	changeseqtokenid: any
	searchtokendatadetailshowdata: Boolean = false
	alldata: any
	providerlist: Boolean = true
	ongoingqueuelistdata: Boolean = true
	ongoingtab: Boolean = false
	broadcast: Boolean = false
	broadcasttoday: Boolean = false
	markdoctorarriveddieload: Boolean = false
	is_doctor_arrived: any
	pending: any
	ongoing: any
	noshow: any
	completedorcancelled: any
	profileimage_path: any
	first_name: any
	user_master_id: any
	selectedindex = 0;
	ongoingqueuelist: any
	todayongoingqueuelist: any
	progressbar: any
	queuelist: Boolean = false
	allTokensCount: any
	transactionQueueTokens: any
	errorMessageongoing: any
	doctoravailabilityfilter: any = {
		month: "",
		year: ""
	};
	selectedIndex: any
	todaysongoingprovidername: any
	adddoctoravailability: any = {
		unavailabilitydaytype: ''
	}
	errorMessage: string;
	orgname: any
	orgimage: any
	totalRecords: any
	ongoingqueuedata: any
	queue_details: any
	queue_detailstoday: any
	queue_msg_count: any
	queue_tokens: any
	avg_consulting_time: any
	avg_waiting_time: any
	completedTokensCount: any
	checkup_total_amount: any
	serby: any
	textvalue: any
	mobilenum: any
	checkboxselectcheck: any = "nextforconsultation";
	cancelledTokensCount: any
	notArrivedCount: any
	arrivedCount: any
	queue_transaction_id: any
	ongoingestimated_end_time
	ongoingqueue_date
	ongoingprovider_name
	ongoingqueue_pic_url
	ongoingqueue_name
	broadcastlisting: any
	broadcastlistingpage = 1
	broadcasterror: any
	addbroadcastmessage: Boolean = false
	broadcastmsgerror: any
	page = 0;
	size = 10;
	ongoinglist: any
	broadcastmesseglist: any
	getTransactionTokenList: any
	cancelledpage = 0
	completedpage = 0
	notarrivedpage = 0
	arrivedpage = 0
	allpage = 0
	modelFlag: Boolean = false
	cancelmodel: Boolean = false
	edittokendetail: Boolean = false
	latertoday: Boolean = false
	total_tokens: any
	ongoingqueuelistdatashow: Boolean = false
	transactionqueueid: any
	quelistdatareshadule: any
	requestreceived: Boolean = false
	getTokenDetailsreshadule: any
	pipe: any
	token_idres: any
	token_noconres: any
	token_datereq: any
	token_timereq: any
	queue_namereqexisting: any
	newrequested_date: any
	reshaduledate: any
	queue_reqque: any
	queue_namereq: any
	token_nores: any
	token_timeres: any
	restoken_time: any
	customfield: any
	token_id: any
	hiddenall: Boolean = true
	todaystoken: Boolean = false
	predefinedreson: any
	dialogRef: any
	onassign_custom_form_values: any
	oncomplete_custom_form_values: any
	concatarrayforresult: any
	dignosispriscription: Boolean = false
	patienthistorydata: any
	token_details: any
	diagnosis_details: any
	prescription_details: any
	test_details: any
	prescription_images: any
	report_images: any
	queue_id: any
	token_no: any
	token_time: any
	token_apitime: any
	quelistdata: any
	queselecteddate: any
	myFormattedDate: any
	assignnewtokenform: any
	selectedproid: any
	mobilesearchmasterid: any
	selfname: any
	tokenrequestorno: any
	bookedfor: any
	bookedforself: Boolean = false
	bookingtypeselectedtype: any
	token_date: any
	searchfirst_name: any
	regno: any
	usergendermale: any
	searchmobileage: any = 0;
	bookingusername: any
	searchfield: any
	assignsearchdiv: any
	userprofileimg: any
	usergenderfemale: any
	usergenderOther: any
	familymemeberidfordetail: any
	bookedother: any
	searchbookinguserid: any
	cancelmodelforassigndat: Boolean = false
	assignbuttonshow: any
	selfnumber: any
	selfimage: any
	bookingfordropdownvis: any
	publicorgname: any
	familymember: any
	bookingtype: any
	familymembershow: Boolean = false
	//assign end
	currentdate = new Date();
	//provider id selected
	provideridselected: any
	//provider end
	todayongoingmsg: Boolean = false
	preascription_title: any
	/*   prescription start 6-20-2019    */
	public totalfiles: Array < File > = [];
	patienthistoryaddmore: Boolean = false
	showformfield: Boolean = false
	image_name: any
	submitdate: any
	public lengthCheckToaddMore = 0;
	model: any = {
		date: {
			year: 2018,
			month: 10,
			day: 9
		}
	};
	/*priscription end  6-20-2019*/
	gender: any
	//search token start
	searchpage: any
	fromdate: any
	patient: any
	todate: any
	tokentypesearch: any
	transactionqueuelist: boolean = false
	checkboxchangedcheckeddata: Boolean = false
	assignnew_firstname: any
	assignnew_lastname: any
	assignnew_midlename: any
	callfromreshadule: any
	//searchtoken end
	//assign now new code start
	iseditable: any
	aheadof: Boolean = false
	nextforconsultation: any
	aheadofcheck: Boolean = false
	nextforconsultationchecked: Boolean = false
	//assign now new code end
	//new code start 10-24-2019
	afterlastarriveddata: Boolean = false
	//new code end 10-24-2019
	//new code 10-31-2019 start
	afterlastarrivedcheckbox: Boolean = false
	//new code 10-31-2019 end
	getenumvalues: any
	getenumdefaultvalues: any
	newvisit: Boolean = false
	followup: Boolean = false
	token_prv_page = "0";
	changeSequencePosition:any = "";
	selectShowHide:boolean = false;
	queueDefaultDate:any;
	queue_type:any;
	input_token_time:any;
	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private ongoingqueues: TodaysqueueService,
		private snackbar: MatSnackBar,
		public  ngProgress: NgProgress,
		private searchtoken: SearchtokensService,
		private requestrceived: RequestreceivedService,
		public  dialog: MatDialog,
		private assignnewtoken: AssignnewtokenService,
		private patientservice: PatienthistoryService,
		private confirmationDialogService: ConfirmationDialogService,
		private htmlDialogService: HtmlviewDialogService
	) {

	}
	@ViewChild(NgForm) userlogin: NgForm;
	public documentGrp: FormGroup;
	public doctoravunailability: FormGroup;
	public addunavailability: FormGroup;
	familymemberselfshow: boolean = false
	provider_full_name: any
	profile_pic_url: any
	keys: any;
	months: any;
	years = [];
	public totalFileName = [];
	//assignnow start
	assignnowdata: any
	//assignnow end
	screenname: any
	screenname1: any
	//futurepast start
	futurepast: any
	futurepastmasterid: any
	futurepastfromdate: any
	futurepasttodate: any
	futurepastlastname: any
	futurepastfirstname: any
	futurepastmiddle_name: any
	futurepastprofileimage_path: any
	futurepastorg_location: any
	//future past end
	//ongoing start
	sharedData: any
	sub: any
	//ongoing end
	//new assign token
	profileimg: any
	regnonew: any
	fullnamecheck: any
	agenew: any
	tokenidfortransactionback: any
	//new assign token end
	//request recived start
	requesttype: any
	//request recived end
	statusforrequestreshadule: any
	callfromprovider: any
	outofsequense: any
	outofsequensecall: Boolean = false
	disablecheckbox: Boolean = false
	last_visited: any
	token_status: any
	orgid:any;
	quedefaultdate:any;
	memberid:any;
	ngOnInit() {
		this.outofsequense = "Token started OUT OF SEQUENCE"
		this.nextforconsultation = ""
		this.documentGrp = this.formBuilder.group({
				documentFile: new FormControl(File),
				items: this.formBuilder.array([this.createUploadDocuments(this.preascription_title, this.image_name)]),
		});
		this.dignosispriscription = false
		this.dropdownSettings = {
			singleSelection: false,
			idField: 'custom_field_value_id',
			textField: 'custom_list_text',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};
		this.dropdownSettingsnewdata = {
			singleSelection: false,
			idField: 'custom_field_value_id',
			textField: 'custom_list_text',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};

		this.selectedIndex = 4
		this.callfromreshadule = "no"
		this.callfromprovider = "no"
		this.markdoctorarriveddieload = false
		this.familymemberselfshow = true
		let actorganistion = JSON.parse(localStorage.getItem('orguserrole'));
		this.publicorgname = actorganistion.orgname
		this.orgid = actorganistion.orgid
		this.dignosispriscription = false
		this.requestreceived = false
		this.hiddenall = true
		this.ongoingqueuelistdatashow = true
		this.latertoday = true
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let user_master_id = atob(currentUser.user_master_id);
		let profileimage_path = atob(currentUser.profileimage_path);
		let first_name = atob(currentUser.profileimage_path);
		this.pipe = new DatePipe('en-US');
		this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
		this.queselecteddate = this.myFormattedDate
		this.quedefaultdate = this.myFormattedDate
		this.reshaduledate = this.myFormattedDate
		this.submitdate = ""
		this.bookingtype = "First Time Visit"
		let roletype = JSON.parse(localStorage.getItem('activerole'));
		this.token_apitime = this.route.snapshot.queryParamMap.get('token_apitime');
		if ((this.route.snapshot.queryParamMap.get('tokenidsearch')) != null) {
			var tokenid = this.route.snapshot.queryParamMap.get('tokenidsearch')
			//this.searchformdata=this.route.snapshot.queryParamMap.get('formvaluedata')
			this.searchpage = this.route.snapshot.queryParamMap.get('searchpage')
			//this.tokenidrequestreshadulesearch=
			this.fromdate = this.route.snapshot.queryParamMap.get('fromdate')
			this.patient = this.route.snapshot.queryParamMap.get('patient')
			this.todate = this.route.snapshot.queryParamMap.get('todate')
			this.tokentypesearch = this.route.snapshot.queryParamMap.get('tokentype')
			this.screenname = "Search Tokens"
			this.screenname1 = "Search Tokens"
			this.tokendetail(tokenid);
		} else if ((this.route.snapshot.queryParamMap.get('ongoingqueue')) != null) {
			var ongoing_queue_transaction_id = this.route.snapshot.queryParamMap.get('ongoing_queue_transaction_id')
			this.getongoingqueuetoken(ongoing_queue_transaction_id)
			this.screenname = "Ongoing Queues"
			this.screenname1 = "Ongoing Queues"
		} else if ((this.route.snapshot.queryParamMap.get('futurepasttoken')) != null) {
			this.screenname = "Future Past Token"
			this.screenname1 = "Future Past Token"
			this.futurepast = this.route.snapshot.queryParamMap.get('futurepasttoken')
			var futurepastqueuetype = this.route.snapshot.queryParamMap.get('futurepastqueuetype')
			var futurepastqueue_transaction_id = this.route.snapshot.queryParamMap.get('futurepastqueue_transaction_id')
			this.futurepastmasterid = this.route.snapshot.queryParamMap.get('futurepastmasterid')
			this.futurepastfromdate = this.route.snapshot.queryParamMap.get('futurepastfromdate')
			this.futurepasttodate = this.route.snapshot.queryParamMap.get('futurepasttodate')
			this.futurepastlastname = this.route.snapshot.queryParamMap.get('last_name')
			this.futurepastfirstname = this.route.snapshot.queryParamMap.get('first_name')
			this.futurepastmiddle_name = this.route.snapshot.queryParamMap.get('middle_name')
			this.futurepastprofileimage_path = this.route.snapshot.queryParamMap.get('profileimage_path')
			this.futurepastorg_location = this.route.snapshot.queryParamMap.get('org_location')
			if (futurepastqueuetype == "Ongoing") {
				this.getongoingqueuetoken(futurepastqueue_transaction_id)
			} else {
				this.gettransactionqueuetoken(futurepastqueue_transaction_id)
			}
		} else if ((this.route.snapshot.queryParamMap.get('assignnowtoken')) != null) {
			this.screenname = "Assign Token"
			this.screenname1 = "Assign Token"
			var mobilenumberassignnow = this.route.snapshot.queryParamMap.get('mobilenumberassignnow');
			this.assignnowdata = mobilenumberassignnow
			this.tokenrequestorno = mobilenumberassignnow
			var bookingtypeselectedtype = this.route.snapshot.queryParamMap.get('bookingtypeselectedtype')
			var mobilesearchmasterid = this.route.snapshot.queryParamMap.get('mobilesearchmasterid')
			var fullname = this.route.snapshot.queryParamMap.get('fullname')
			this.selectedproid = this.route.snapshot.queryParamMap.get('selectedproid')
			this.fullnamecheck = this.route.snapshot.queryParamMap.get('fullnamecheck')
			this.memberid = this.route.snapshot.queryParamMap.get("familymemberid");
			this.bookingtypeselectedtype = bookingtypeselectedtype
			this.mobilesearchmasterid = mobilesearchmasterid
			this.provider_full_name = fullname
			this.familymemeberidfordetail = this.route.snapshot.queryParamMap.get('familymemberid')
			this.profileimg = this.route.snapshot.queryParamMap.get('profileimg')
			this.regnonew = this.route.snapshot.queryParamMap.get('regnonew')
			this.agenew = this.route.snapshot.queryParamMap.get('age')
			this.gender = this.route.snapshot.queryParamMap.get('gender')
			this.last_visited = this.route.snapshot.queryParamMap.get('last_visited');
			this.token_status = this.route.snapshot.queryParamMap.get('token_status');
			this.queue_id = this.route.snapshot.queryParamMap.get('queue_id');
			this.myFormattedDate = this.route.snapshot.queryParamMap.get('selecteddate');
			this.token_time = this.route.snapshot.queryParamMap.get('token_time');
			this.token_apitime = this.route.snapshot.queryParamMap.get('token_apitime');
			this.token_timeres = this.route.snapshot.queryParamMap.get('token_time');
			let currentdate = new Date().getMonth();
			let send_date = new Date();
			send_date.setMonth(send_date.getMonth() + 3);
			let formatedcu = this.pipe.transform(send_date, 'dd-MM-yyyy');
			if (this.last_visited != null) {
				var dateex = this.last_visited.split("-");
				var daten = new Date("" + dateex[1] + "/" + dateex[0] + "/" + dateex[2] + "");
				//var d1 = new Date(""+this.last_visited+"");
				let convert = new Date(this.last_visited)
				this.pipe.transform(this.currentdate, );
				var diff = send_date.getTime() - daten.getTime()
				var diff2 = Math.round(diff / (1000 * 60 * 60 * 24))
				if (diff2 > 90) {
					this.newvisit = true
					this.followup = false
				} else if (diff2 < 90) {
					this.newvisit = false
					this.followup = true
				} else {
					this.newvisit = false
					this.followup = false
				}
			}
			if(bookingtypeselectedtype != 'non_member') {
				this.cancelsubmitdata(mobilenumberassignnow)
			}
			if (this.fullnamecheck == '0') {
				this.serby = this.route.snapshot.queryParamMap.get('serby')
				this.textvalue = this.route.snapshot.queryParamMap.get('textvalue')
				this.mobilenum = this.route.snapshot.queryParamMap.get('mobilenum')
			}
			if (this.route.snapshot.queryParamMap.get('tokenno')) {
				this.token_no = this.route.snapshot.queryParamMap.get('tokenno')
				this.token_prv_page = this.route.snapshot.queryParamMap.get('tokenno')
				this.queue_reqque = this.route.snapshot.queryParamMap.get('selectedqueue')
				this.myFormattedDate = this.route.snapshot.queryParamMap.get('selecteddate')
				this.queselecteddate = this.route.snapshot.queryParamMap.get('selecteddate')
				this.quedefaultdate = this.route.snapshot.queryParamMap.get('selecteddate')
				this.onqueselectprv(this.queue_reqque)
			}else{
				var today  = new Date();
				this.myFormattedDate = today.getUTCDate() - 1 +'-'+today.getUTCMonth() + 1+'-'+today.getUTCFullYear();
			}
			this.assignnow(mobilenumberassignnow)
		} else if (roletype.activerole == 'l1e') {
			if (this.route.snapshot.queryParamMap.get('weeaklydashboard') != null) {
				this.screenname = "Weekly Dashboard"
				this.screenname1 = "Weekly Dashboard"
				var weeaklydashboardqueid = this.route.snapshot.queryParamMap.get('weeaklydashboardqueid')
				var usermasterid = this.route.snapshot.queryParamMap.get('usermasterid')
				var weeaklydashboarddate = this.route.snapshot.queryParamMap.get('weeaklydashboarddate')
				this.queue_id = weeaklydashboardqueid
				this.queselecteddate = weeaklydashboarddate
				this.quedefaultdate = weeaklydashboarddate
				this.selectedproid = usermasterid
				this.getTransactionQueueDatas()
			} else if (this.route.snapshot.queryParamMap.get('description') != null) {
				this.screenname = "Ongoing Queues"
				this.screenname1 = "Ongoing Queues"
				var tokenidpriscription = this.route.snapshot.queryParamMap.get('tokenidpriscription')
				this.tokendetail(tokenidpriscription)
			} else if (this.route.snapshot.queryParamMap.get('requestsreceivedreshadule') != null) {
				this.screenname = "Today's Queues"
				this.screenname1 = "Todays Queues"
				var tokenidrequestreshadule = this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
				this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
				this.tokendetail(tokenidrequestreshadule)
			} else {
				this.ongoingqueues.getproviderongoingtransactionqueue().subscribe((response) => {
					if (response.status_code == '200') {
						if (response.Data.queue_tokens.length > 1) {
							this.screenname = "Ongoing Queues"
							this.screenname1 = "Ongoing Queues"
							this.getongoingqueue(profileimage_path, first_name, user_master_id)
							this.latertoday = false
						} else {
							this.screenname = "Ongoing Queues"
							this.screenname1 = "Ongoing Queues"
							this.getongoingqueuetokenfrontdesk()
						}
					} else {
						this.screenname = "Ongoing Queues"
						this.screenname1 = "Ongoing Queues"
					}
				})
			}
		} else if (this.route.snapshot.queryParamMap.get('requestsreceivedreshadule') != null) {
			var screenname = this.route.snapshot.queryParamMap.get('screennameback')
			if (screenname == 'Ongoing Queues') {
				var tokenidrequestreshadule = this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
				this.screenname = "Ongoing Queues"
				this.screenname1 = "Ongoing Queues"
				this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
				var checkforcall = this.route.snapshot.queryParamMap.get('callfrom')
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
				this.latertoday = false
				this.ongoingqueuelistdatashow = false;
				if (checkforcall == "provider") {
					this.getongoingqueuetoken(this.queue_transaction_id)
					this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
					this.callfromreshadule = "no"
					this.callfromprovider = "yes"
				} else {
					this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
					var provider_name = this.route.snapshot.queryParamMap.get('provider_name')
					var queue_namereqexisting = this.route.snapshot.queryParamMap.get('queue_namereqexisting')
					this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
					this.provider_full_name = provider_name
					this.todaysongoingprovidername = queue_namereqexisting
					this.getongoingqueuetoken(this.queue_transaction_id)
					this.token_apitime = this.route.snapshot.queryParamMap.get('token_apitime');
					this.tokendetail(tokenidrequestreshadule)
					// this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
					this.callfromreshadule = "yes"
					this.callfromprovider = "no"
				}

			} else if (screenname == 'Future Past Token') {
				this.futurepastmasterid = this.route.snapshot.queryParamMap.get('futurepastmasterid')
				this.futurepasttodate = this.route.snapshot.queryParamMap.get('futurepasttodate')
				this.futurepastfromdate = this.route.snapshot.queryParamMap.get('futurepastfromdate')
				this.futurepastlastname = this.route.snapshot.queryParamMap.get('futurepastlastname')
				this.futurepastfirstname = this.route.snapshot.queryParamMap.get('futurepastfirstname')
				this.futurepastmiddle_name = this.route.snapshot.queryParamMap.get('futurepastmiddle_name')
				//this.futurepastprofileimage_pathf = this.route.snapshot.queryParamMap.get('futurepastprofileimage_path')
				this.futurepastprofileimage_path = this.route.snapshot.queryParamMap.get('futurepastprofileimage_pathf')
				var tokenidrequestreshadule = this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
				this.screenname = "Future Past Token"
				this.screenname1 = "Future Past Token"
				this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
				var checkforcall = this.route.snapshot.queryParamMap.get('callfrom')
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')

				this.latertoday = false
				this.ongoingqueuelistdatashow = false
				if (checkforcall == "provider") {
					if (this.checktoday == 1) {
						this.getongoingqueuetoken(this.queue_transaction_id)
						this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
						this.callfromreshadule = "no"
						this.callfromprovider = "yes"
					} else {
						this.gettransactionqueuetoken(this.queue_transaction_id)
						this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
						this.callfromreshadule = "no"
						this.callfromprovider = "yes"
					}
				} else {
					this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
					var provider_name = this.route.snapshot.queryParamMap.get('provider_name')
					var queue_namereqexisting = this.route.snapshot.queryParamMap.get('queue_namereqexisting')
					this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
					this.provider_full_name = provider_name
					this.ongoingprovider_name = provider_name
					this.todaysongoingprovidername = queue_namereqexisting
					this.ongoingqueue_name = queue_namereqexisting
					// this.gettransactionqueuetoken(this.queue_transaction_id)
					this.tokendetail(tokenidrequestreshadule)
					this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
					this.callfromreshadule = "yes"
					this.callfromprovider = "no"
				}
			} else if (screenname == 'Search Tokens') {

				var tokenid = this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
				//this.searchformdata=this.route.snapshot.queryParamMap.get('formvaluedata')
				this.searchpage = this.route.snapshot.queryParamMap.get('searchpage')
				this.fromdate = this.route.snapshot.queryParamMap.get('fromdatesearch')
				this.patient = this.route.snapshot.queryParamMap.get('patientsearch')
				this.todate = this.route.snapshot.queryParamMap.get('todatesearch')
				this.tokentypesearch = this.route.snapshot.queryParamMap.get('tokentypesearch')
				this.screenname = "Search Tokens"
				this.screenname1 = "Search Tokens"
				this.tokendetail(tokenid)
			} else {
				var tokenidrequestreshadule = this.route.snapshot.queryParamMap.get('tokenidrequestreshadule')
				this.screenname = "Today's Queues"
				this.screenname1 = "Todays Queues"
				this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
				var checkforcall = this.route.snapshot.queryParamMap.get('callfrom')
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
				this.latertoday = false
				this.ongoingqueuelistdatashow = false
				if (checkforcall == "provider") {


					if (this.checktoday == 1) {
						this.getongoingqueuetoken(this.queue_transaction_id)
						this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
						this.callfromreshadule = "no"
						this.callfromprovider = "yes"
					} else {

						this.gettransactionqueuetoken(this.queue_transaction_id)
						this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
						this.callfromreshadule = "no"
						this.callfromprovider = "yes"
					}

				} else {
					this.checktoday = Number(this.route.snapshot.queryParamMap.get('checktodayid'))
					var provider_name = this.route.snapshot.queryParamMap.get('provider_name')
					var queue_namereqexisting = this.route.snapshot.queryParamMap.get('queue_namereqexisting')
					this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id')
					this.provider_full_name = provider_name
					this.todaysongoingprovidername = queue_namereqexisting
					this.ongoingprovider_name = provider_name;
					this.ongoingqueue_name = queue_namereqexisting;
					// this.gettransactionqueuetoken(this.queue_transaction_id)
					this.tokendetail(tokenidrequestreshadule)
					this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
					this.callfromreshadule = "yes"
					this.callfromprovider = "no"
				}
			}


		} else if (this.route.snapshot.queryParamMap.get('newrequestsreceived') != null) {

			this.screenname = "Requests Received"
			this.screenname1 = "Requests Received"

			//tokenidfortransactionback
			//tokenid

			this.token_idres = this.route.snapshot.queryParamMap.get('tokenid')


			this.tokenidfortransactionback = this.route.snapshot.queryParamMap.get('tokenidfortransactionback')


			this.requesttype = this.route.snapshot.queryParamMap.get('newrequest')

			this.requesttype = this.route.snapshot.queryParamMap.get('newrequest')
			this.queue_id = this.route.snapshot.queryParamMap.get('opt1_requested_queue')
			this.queselecteddate = this.route.snapshot.queryParamMap.get('token_date')
			this.quedefaultdate = this.route.snapshot.queryParamMap.get('token_date')
			this.selectedproid = this.route.snapshot.queryParamMap.get('providerid')

			var screendata = this.route.snapshot.queryParamMap.get('screenname')

			if (screendata == "Todays Queues") {
				this.statusforrequestreshadule = screendata
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('provider_name')
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('queue_namereqexisting')

			} else if (screendata == "Ongoing Queues") {
				this.statusforrequestreshadule = screendata
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('provider_name')
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('queue_namereqexisting')
			} else if (screendata == "Future Past Token") {


				this.futurepastmasterid = this.route.snapshot.queryParamMap.get('futurepastmasterid')
				this.futurepasttodate = this.route.snapshot.queryParamMap.get('futurepasttodate')
				this.futurepastfromdate = this.route.snapshot.queryParamMap.get('futurepastfromdate')
				this.futurepastlastname = this.route.snapshot.queryParamMap.get('futurepastlastname')
				this.futurepastfirstname = this.route.snapshot.queryParamMap.get('futurepastfirstname')
				this.futurepastmiddle_name = this.route.snapshot.queryParamMap.get('futurepastmiddle_name')
				this.futurepastprofileimage_path = this.route.snapshot.queryParamMap.get('futurepastprofileimage_pathf')

				this.statusforrequestreshadule = screendata
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('provider_name')
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('queue_namereqexisting')


			} else {

				this.statusforrequestreshadule = ""
			}


			this.getTransactionQueueDatas()

		} else if (this.route.snapshot.queryParamMap.get('dpr') != null) {
			this.screenname = this.route.snapshot.queryParamMap.get('screenname');
			this.screenname1 = this.route.snapshot.queryParamMap.get('screenname');
			var tokenid = this.route.snapshot.queryParamMap.get('tokenid')
			this.tokendetail(tokenid);
		} else if (this.route.snapshot.queryParamMap.get('backfrom') == "prescription"){
			if(this.route.snapshot.queryParamMap.get('screenname') == "Today's Queues"){
				this.screenname1 ='Todays Queues';
				this.screenname = "Today's Queues";
				this.searchtokendatadetailshowdata = true;
				this.ongoingtab = false;
				this.transactionqueuelist = false;
				this.queuelist = false;
				this.todayongoingqueuelist = null;
				this.latertoday = false;
				this.ongoingqueuelistdatashow =  false;
				this.checktoday = 1;
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
				this.tokendetail(this.route.snapshot.queryParamMap.get('tokenid'));
				return false;
			}else if(this.route.snapshot.queryParamMap.get('screenname') == "Ongoing Queues"){
				this.screenname = "Ongoing Queues";
				this.searchtokendatadetailshowdata = true;
				this.ongoingtab = false;
				this.transactionqueuelist = false;
				this.queuelist = false;
				this.todayongoingqueuelist = null;
				this.latertoday = false;
				this.ongoingqueuelistdatashow =  false;
				this.checktoday = 1;
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
				this.tokendetail(this.route.snapshot.queryParamMap.get('tokenid'));
				return false;
			}else if(this.route.snapshot.queryParamMap.get('screenname') == "Future Past Token"){
				this.screenname = "Future Past Token";
				this.searchtokendatadetailshowdata = true;
				this.ongoingtab = false;
				this.transactionqueuelist = false;
				this.queuelist = false;
				this.todayongoingqueuelist = null;
				this.latertoday = false;
				this.ongoingqueuelistdatashow =  false;
				this.queue_type = this.route.snapshot.queryParamMap.get('queue_type');
				if(this.queue_type == "ongoing"){
					this.checktoday = 1;
				}else{
					this.checktoday = 2;
				}
				this.queue_transaction_id = this.route.snapshot.queryParamMap.get('queue_transaction_id');
				this.ongoingprovider_name = this.route.snapshot.queryParamMap.get('ongoingprovider_name');
				this.ongoingqueue_name = this.route.snapshot.queryParamMap.get('ongoingqueue_name');
				this.fromdate = this.route.snapshot.queryParamMap.get('fromdate');
				this.patient = this.route.snapshot.queryParamMap.get('patient');
				this.todate = this.route.snapshot.queryParamMap.get('todate');
				this.tokentypesearch = this.route.snapshot.queryParamMap.get('tokentypesearch');
				this.searchpage = this.route.snapshot.queryParamMap.get('searchpage');
				this.futurepastmasterid = this.route.snapshot.queryParamMap.get('futurepastmasterid');
				this.futurepastfromdate = this.route.snapshot.queryParamMap.get('futurepastfromdate');
				this.futurepasttodate = this.route.snapshot.queryParamMap.get('futurepasttodate');
				this.futurepastlastname = this.route.snapshot.queryParamMap.get('futurepastlastname');
				this.futurepastfirstname = this.route.snapshot.queryParamMap.get('futurepastfirstname');
				this.futurepastmiddle_name = this.route.snapshot.queryParamMap.get('futurepastmiddle_name');
				this.futurepastprofileimage_path = this.route.snapshot.queryParamMap.get('futurepastprofileimage_path');
				this.futurepastorg_location = this.route.snapshot.queryParamMap.get('futurepastorg_location');
				this.provider_full_name = this.route.snapshot.queryParamMap.get('provider_name');
				this.todaysongoingprovidername = this.route.snapshot.queryParamMap.get('todaysongoingprovidername');
				this.tokendetail(this.route.snapshot.queryParamMap.get('tokenid'));
				return false;
			}
		} else if (this.route.snapshot.queryParamMap.get('backfrom') == "workcalendar"){
			this.screenname = "Weekly Dashboard"
			this.screenname1 = "Weekly Dashboard"
			var weeaklydashboardqueid = this.route.snapshot.queryParamMap.get('weeaklydashboardqueid')
			var usermasterid = this.route.snapshot.queryParamMap.get('usermasterid')
			var weeaklydashboarddate = this.route.snapshot.queryParamMap.get('weeaklydashboarddate')
			this.queue_id = weeaklydashboardqueid
			this.queselecteddate = weeaklydashboarddate
			this.quedefaultdate = weeaklydashboarddate
			this.selectedproid = usermasterid
			this.getTransactionQueueDatas()
			// if (queueType == "Ongoing") {
			// 	this.getongoingqueuetoken(queue_transaction_id);
			// } else {
			// 	this.gettransactionqueuetoken(queue_transaction_id);
			// }
		} else {
			this.screenname = "Today's Queues"
			this.screenname1 = "Todays Queues"
			this.token_apitime = this.route.snapshot.queryParamMap.get('token_apitime');
			this.getongoingqueue(profileimage_path, first_name, user_master_id)
		}
		//ongoing start
		let idx = this.route.pathFromRoot.length - 1;
		while (this.sharedData == null && idx > 0) {
			this.sub = this.route.pathFromRoot[idx].data.subscribe(subData => {
				if (subData.somedata)
					this.sharedData = subData;
			});
			idx--;
		}

		if (this.sharedData != undefined) {
			this.screenname = "Ongoing Queues"
			this.screenname1 = "Ongoing Queues"
			this.getongoingqueue(profileimage_path, first_name, user_master_id)
			this.latertoday = false

		}
		//ongoing end
	}


	createUploadDocuments(preascription_title, image_name): FormGroup {
		return this.formBuilder.group({
			preascription_title: preascription_title,
			image_name: image_name,
		});
	}


	addPrescription(): void {
		this.items.insert(0, this.createUploadDocuments(this.preascription_title, this.image_name))
		this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
	}

	get items(): FormArray {
		return this.documentGrp.get('items') as FormArray;
	};


	getongoingqueue(profileimage_path, first_name, user_master_id) {
		this.profileimage_path = profileimage_path
		this.first_name = first_name
		this.user_master_id = user_master_id
		//this.selectedproid=user_master_id
		this.ongoingqueues.getongoingqueue(user_master_id).subscribe((response) => {
			if (response.status_code == '200') {
				this.providerlist = false
				//this.provider_full_name
				//this.profile_pic_url
				this.ongoingqueuelist = response.Data.todaysongoing
				this.todayongoingqueuelist = response.Data.todayslater
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.ongoingqueuelist = null
				this.todayongoingqueuelist = null
				// this.errorMessageongoing=response.Metadata.Message
			}
		});
	}


	getongoingqueuetoken(queue_transaction_id) {
		this.checktoday = 1
		this.queue_transaction_id = queue_transaction_id
		this.ongoingqueues.getongoingtransactionqueue(queue_transaction_id).subscribe((response) => {
			if (response.status_code == '200') {
				this.ongoingqueuelistdatashow = false
				this.latertoday = false
				this.ongoingtab = false
				this.transactionqueuelist = false
				this.hiddenall = true
				this.latertoday = false
				this.ongoingqueuelistdatashow = false
				if (this.callfromreshadule == "yes") {
					this.ongoingtab = false
				} else {
					this.ongoingtab = true
				}


				this.queue_details = response.Data.queue_details
				this.is_doctor_arrived = response.Data.queue_details[0].is_doctor_arrived
				this.queue_id = response.Data.queue_details[0].queue_id


				this.ongoingestimated_end_time = response.Data.queue_details[0]['ongoingestimated_end_tim']

				this.ongoingqueue_date = response.Data.queue_details[0]['queue_date']

				this.queselecteddate = response.Data.queue_details[0]['queue_date']
				this.quedefaultdate = response.Data.queue_details[0]['queue_date']

				this.ongoingprovider_name = response.Data.queue_details[0]['provider_name']
				this.ongoingqueue_pic_url = response.Data.queue_details[0]['profileimage_path']
				this.ongoingqueue_name = response.Data.queue_details[0]['queue_name']
				this.queue_reqque = response.Data.queue_details[0].queue_id
				// this.onqueselect(response.Data.queue_details[0].queue_id)

				//new assign screen effact start
				this.assignnew_firstname = response.Data.queue_details[0].first_name
				this.assignnew_lastname = response.Data.queue_details[0].last_name
				this.assignnew_midlename = response.Data.queue_details[0].middle_name


				//new assign screen effact stop

				this.selectedproid = response.Data.queue_details[0]['provider_id']

				this.provider_full_name = response.Data.queue_details[0].provider_name

				this.avg_consulting_time = response.Data.avg_consulting_time
				this.avg_waiting_time = response.Data.avg_waiting_time
				this.checkup_total_amount = response.Data.checkup_total_amount


				this.completedTokensCount = response.Data.completedTokensCount
				this.cancelledTokensCount = response.Data.cancelledTokensCount
				this.notArrivedCount = response.Data.notArrivedCount
				this.arrivedCount = response.Data.arrivedCount
				this.allTokensCount = response.Data.allTokensCount
				var selectedIndexObj:any = {"index":this.selectedIndex};
				this.onoganisationclick(selectedIndexObj);
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				//  this.errorMessageongoing=response.Metadata.Message
				//this.error=response.Metadata.Message
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				})


			}
		});

	}

	onoganisationclick(tokentype) {
		var tokentypedata
		this.selectedIndex = tokentype.index;
		if (tokentype.index == '0') {
			//tokentypedata="all"
			tokentypedata = "completed"

			if (this.completedTokensCount == 0) {
				this.page = 1
			} else {

				this.page = this.completedTokensCount
			}


		} else if (tokentype.index == '1') {
			tokentypedata = "cancelled"

			if (this.cancelledpage == 0) {
				this.page = 1

				//this.page=this.noshowpage
			} else {
				//  this.page=1
				this.page = this.cancelledpage

			}

		} else if (tokentype.index == '2') {
			tokentypedata = "notarrived"
			//ongoing
			if (this.notarrivedpage == 0) {
				this.page = 1
			} else {
				this.page = this.notarrivedpage
			}
		} else if (tokentype.index == '3') {
			tokentypedata = "arrived"
			//ongoing
			if (this.arrivedpage == 0) {
				this.page = 1
			} else {
				this.page = this.arrivedpage
			}
		} else if (tokentype.index == '4') {
			//tokentypedata="completedorcancelled"
			tokentypedata = "all"
			//ongoing
			if (this.allpage == 0) {
				this.page = 1
			} else {
				this.page = this.allpage
			}
		} else {
			tokentypedata = "all"
			if (this.completedpage == 0) {
				this.page = 1
			} else {
				this.page = this.completedpage
			}


		}


		this.ongoingqueues.getongoingtransactiontype(tokentypedata, this.queue_transaction_id, this.page - 1)
			.subscribe((response) => {

					if (response.status_code == '200') {

						if (tokentypedata == "cancelled") {
							// this.selectedindex=1
							this.noshow = response.Data.queue_tokens
						} else if (tokentypedata == "notarrived") {
							this.ongoing = response.Data.queue_tokens
							// this.selectedindex=2
						} else if (tokentypedata == "arrived") {
							// this.selectedindex=3
							this.pending = response.Data.queue_tokens
						} else if (tokentypedata == "completed") {
							// this.selectedindex=0
							this.completedorcancelled = response.Data.queue_tokens
						} else if (tokentypedata == "all") {
							// this.selectedindex=4
							this.alldata = response.Data.queue_tokens
						}
						this.completedTokensCount = response.Data.completedTokensCount
						this.cancelledTokensCount = response.Data.cancelledTokensCount
						this.notArrivedCount = response.Data.notArrivedCount
						this.arrivedCount = response.Data.arrivedCount
						this.allTokensCount = response.Data.allTokensCount

					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						//  this.errorMessageongoing=response.Metadata.Message
						//this.error=response.Metadata.Message
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})

					}
				});
	}


	checkboxchanged(transaction_id) {


		this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you want to mark doctor as arrived.')
			.then((confirmed) => {
				if (confirmed) {


					this.ongoingqueues.markDrArrived(transaction_id)
						.subscribe(
							(response) => {

								if (response.status_code == '200') {
									this.is_doctor_arrived = true
									this.snackbar.open(response.Metadata.Message, '', {
										duration: 2000,
									})
									//window.history.back();
									this.refreshscreen()

								} else if (response.status_code == '402') {
									localStorage.clear();
									this.router.navigate(['/login']);
								} else {

									this.snackbar.open(response.Metadata.Message, '', {
										duration: 2000,
									})
								}
							});

				}
			})


	}

	checkboxchangedconform(transaction_id) {
		this.ongoingqueues.markDrArrived(transaction_id)
			.subscribe(
				(response) => {

					if (response.status_code == '200') {
						this.checkboxchangedcheckeddata = true
						this.is_doctor_arrived = true
						this.checkboxchangedmodelToggle('close')
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.errorMessageongoing = response.Metadata.Message
					}
				});
	}

	checkboxchangedconformcancel() {

		this.checkboxchangedmodelToggle('close')
		this.is_doctor_arrived = false
	}


	checkboxchangedmodelToggle(action: string) {
		if (action == "open") {
			this.markdoctorarriveddieload = true;
		} else {

			this.markdoctorarriveddieload = false;
		}
	}


	broadcastmesseg() {

		this.ongoingqueues.transactionQueueMessages((this.broadcastlistingpage - 1),
				this.queue_transaction_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {
						this.ongoingtab = false
						this.ongoingqueuelist = false
						this.broadcast = true
						this.broadcastmesseglist = response.Data.queue_messages
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.broadcasterror = response.Metadata.Message
					}
				});
	}


	broadcastmessegtodays() {

		this.ongoingqueues.transactionQueueMessages((this.broadcastlistingpage - 1),
				this.queue_transaction_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {
						this.ongoingtab = false
						this.ongoingqueuelist = false
						this.queuelist = false

						this.broadcasttoday = true
						this.broadcastmesseglist = response.Data.queue_messages
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.broadcasterror = response.Metadata.Message
					}
				});
	}


	broadcastbuttontapped() {
		this.broadcast = false
		this.addbroadcastmessage = false
		this.ongoingtab = true
		this.ongoingqueuelist = true
		this.getongoingqueuetoken(this.queue_transaction_id)
	}


	broadcastbuttontappedtoday() {
		this.broadcasttoday = false


		//this.ongoingtab=false
		//this.ongoingqueuelist=false
		this.queuelist = true


		//this.gettransactionqueuetoken(this.queue_transaction_id)

		//this.getongoingqueuetoken(this.queue_transaction_id)
	}


	backbuttontapped() {
		this.assignsearchdiv = false
		this.hiddenall = true
		this.ongoingtab = false
		this.queuelist = true
		this.selfimage = null
		this.transactionqueuelist = false
	}


	backbuttontappedtoday() {
		if(this.router.url == "/ongoingqueues"){
			this.assignsearchdiv = false
			this.hiddenall = true
			this.ongoingtab = true
			this.queuelist = false
			this.transactionqueuelist = false
		}else{
			window.history.back();
			return false;
		}

	}


	//reshadule token click start 4-4-2019

	onreshaduleSubmit(data) {

		if (this.queue_reqque != undefined) {
			const formData = new FormData();


			if (this.customfield != undefined) {


				let counte = this.customfield.length


				if(this.customfield != null) {
					let counte = this.customfield.length;
					for(let i = 0; i < this.customfield.length; i++) {
						var datalast = this.customfield[i]['custom_field_id']
						var datalastlist = (data[this.customfield[i]['custom_field_id']])
						var newStr;
						// if(datalastlist != undefined) {
							// if(datalastlist) {
								var multileat="";
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
								  if(datalastlist != undefined){
									for(let j=0;j<datalastlist.length;j++){
									  multileat+=datalastlist[j].custom_field_value_id+","
									}
								  }
								  newStr = multileat.substring(0, multileat.length-1);
								}
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
								  if(newStr!=undefined){
									formData.append('custom_field_text_value['+i+']',newStr);
								  }
								}else if(this.customfield[i]['custom_field_type']=="Date"){
								  var formattedVal:any = "";
								  if(datalastlist != undefined){
									formattedVal = datalastlist.formatted;
								  }
								  formData.append('custom_field_text_value['+i+']',formattedVal);
								}else{
								  if(datalastlist!=undefined){
									  formData.append('custom_field_text_value['+i+']',datalastlist);
								  }
								}

								if(this.customfield[i]['custom_field_type']=="Date"){
								  if(datalastlist != undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
								  }
								}else if(this.customfield[i]['custom_field_type']=="Amount"){
								  if(datalastlist!=undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
									formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
								  }
								}else{
								  if(datalastlist!=undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
								  }
								}
							// }
						// }
					}
				}
			}


			//formData.append('','');


			this.requestrceived.confirmtokenres(formData, this.token_nores, this.restoken_time, this.reshaduledate, this.token_idres, this.queue_reqque, data.tokenmessageres)
				.subscribe(
					(response) => {
						if (response.status_code == "200") {

							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});


							window.history.back();;


						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});
						}
					})
		} else {
			this.snackbar.open("please select Queue", '', {
				duration: 2000,
			});
		}


	}
	//reshadule token end 4-4-2019


	//Transaction start 7-23-2019


	getTransactionQueueDatas() {
		if (this.queue_id != undefined) {
			this.assignnewtoken.getTransactionQueueDatas(this.queue_id, this.queselecteddate, this.selectedproid)
				.subscribe(
					(response) => {
						if (response.status_code == '200') {
							let transaction_id = response.Data[0].queue_transaction_id

							this.queue_transaction_id = transaction_id

							if (transaction_id != "") {
								this.assignnewtoken.transactionQueueTokens(transaction_id)
									.subscribe(
										(response) => {
											this.hiddenall = false
											this.assignsearchdiv = false
											this.transactionqueuelist = true
											this.queue_id = response.Data.queue_details[0].queue_id
											this.queue_reqque = response.Data.queue_details[0].queue_id
											this.progressbar = ((response.Data.total_tokens) / (response.Data.queue_allowed_tokens))

											this.queue_allowed_tokens = response.Data.queue_allowed_tokens


											this.progressbar = ((response.Data.total_tokens) / (response.Data.queue_allowed_tokens))


											this.total_tokens = response.Data.total_tokens
											this.transactionQueueTokens = response.Data.queue_tokens
											this.queue_detailstoday = response.Data.queue_details
											this.queue_msg_count = response.Data.queue_msg_count
										}
									)
							} else {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								});
								let roletype = JSON.parse(localStorage.getItem('activerole'));
								if (roletype.activerole == 'l2') {
									this.router.navigate(['/weeklydashboard']);
								}
							}
						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});
							let roletype = JSON.parse(localStorage.getItem('activerole'));
							if (roletype.activerole == 'l2') {
								this.router.navigate(['/weeklydashboard']);
							}
						}
					})
		} else {
			this.snackbar.open("Please Select Queue ", '', {
				duration: 2000,
			});
		}
	}


	//Transaction start 7-23-2019


	backbuttontappedreshadule() {
		this.hiddenall = true
		this.requestreceived = false
	}


	backbuttontabbed() {
		if (this.searchpage != undefined) {
			this.router.navigate(['/searchtokens/'], {
				queryParams: {
					"searchtoken": "searchtoken",
					"fromdate": this.fromdate,
					"patient": this.patient,
					"todate": this.todate,
					"tokentypesearch": this.tokentypesearch,
					"searchpage": this.searchpage
				},
				skipLocationChange: true
			});
		} else {
			if (this.checktoday == 1) {
				this.callfromreshadule = "no"
				this.searchtokendatadetailshowdata = false
				this.getongoingqueuetoken(this.queue_transaction_id)
			} else if (this.checktoday == 2) {
				this.callfromreshadule = "no"
				this.searchtokendatadetailshowdata = false
				this.gettransactionqueuetoken(this.queue_transaction_id)
			} else {
				window.history.back();
			}
		}
	}


	addbroadcast() {
		this.addbroadcastmessage = true
		this.broadcast = true
		//this.broadcasttoday=false
	}


	addbroadcasttoday() {
		this.todayongoingmsg = true


		//this.broadcast=true
		this.broadcasttoday = true
	}

	todaybroadcastcancel() {
		this.todayongoingmsg = false
	}


	ongoingbroadcastcancel() {
		this.addbroadcastmessage = false


	}


	addmessagesms(messege) {
		this.ongoingqueues.addmessagesms(messege.value, this.queue_transaction_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {
						this.addbroadcastmessage = false
						this.broadcastmesseg()
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.broadcastmsgerror = response.Metadata.Message
					}
				});
	}


	addmessagesmstoday(messege) {
		this.ongoingqueues.addmessagesms(messege.value, this.queue_transaction_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {


						this.addbroadcastmessage = false
						this.todayongoingmsg = false
						//this.broadcastmesseg()

						this.broadcastmessegtodays()
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.broadcastmsgerror = response.Metadata.Message
					}
				});
	}


	gettransactionqueuetoken(queue_transaction_id) {


		this.checktoday = 2
		this.queue_transaction_id = queue_transaction_id
		this.ngProgress.start();
		this.ngProgress.set(0)
		this.ongoingqueuelistdatashow = false
		this.ongoingqueues.transactionQueueTokens(queue_transaction_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {

						this.latertoday = false
						this.ongoingtab = false
						this.transactionqueuelist = false
						this.hiddenall = true
						if (this.callfromreshadule == 'yes') {

							this.queuelist = false
						} else {
							this.queuelist = true
						}


						this.queue_id = response.Data.queue_details[0].queue_id
						this.myFormattedDate = this.pipe.transform(response.Data.queue_details[0].queue_date, 'dd-MM-yyyy');

						this.queselecteddate = this.myFormattedDate
						this.quedefaultdate = this.myFormattedDate

						this.queue_reqque = response.Data.queue_details[0].queue_id
						//this.onqueselect(response.Data.queue_details[0].queue_id)
						this.progressbar = ((response.Data.total_tokens) / (response.Data.queue_allowed_tokens))
						this.queue_allowed_tokens = response.Data.queue_allowed_tokens
						this.total_tokens = response.Data.total_tokens
						this.transactionQueueTokens = response.Data.queue_tokens
						this.queue_detailstoday = response.Data.queue_details
						this.queue_msg_count = response.Data.queue_msg_count
						this.selectedproid = response.Data.queue_details[0].provider_id
						this.provider_full_name = response.Data.queue_details[0].provider_name

						this.todaysongoingprovidername = response.Data.queue_details[0].queue_name


						this.assignnew_firstname = response.Data.queue_details[0].first_name
						this.assignnew_lastname = response.Data.queue_details[0].last_name
						this.assignnew_midlename = response.Data.queue_details[0].middle_name

						this.ongoingprovider_name = ''
						this.ongoingqueue_name = ''

						//this.profile_pic_url=response.Data.queue_details[0].profileimage_path
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
					}
				})
	}


	onoganisationpagechange(page, tokentype) {

		var tokentypedata


		if (tokentype == 'completed') {
			this.completedpage = page
		} else if (tokentype == 'cancelled') {
			this.cancelledpage = page
		} else if (tokentype == 'notarrived') {
			this.notarrivedpage = page
		} else if (tokentype == 'arrived') {
			this.arrivedpage = page
		} else if (tokentype == 'all') {
			this.allpage = page
		}


		this.ongoingqueues.getongoingtransactiontype(tokentype, this.queue_transaction_id, page - 1)
			.subscribe(
				(response) => {

					if (response.status_code == '200') {


						if (tokentype == "cancelled") {
							//this.selectedindex=1
							this.noshow = response.Data.queue_tokens

						} else if (tokentype == "notarrived") {

							this.ongoing = response.Data.queue_tokens
							//this.selectedindex=2
						} else if (tokentype == "arrived") {
							//this.selectedindex=3
							this.pending = response.Data.queue_tokens
						} else if (tokentype == "completed") {
							//this.selectedindex=4
							this.completedorcancelled = response.Data.queue_tokens
						} else if (tokentype == "all") {
							//this.selectedindex=3
							this.alldata = response.Data.queue_tokens
						}


					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						//  this.errorMessageongoing=response.Metadata.Message
						//this.error=response.Metadata.Message
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})

					}
				});
	}


	/*
	pageChanged(event)
	{
	this.page=event
	this.quesetuplist(this.page)
	}
	*/
	/*new code 4-17-2019 */


	tokendetail(token_id) {
		this.searchtoken.gettokendetail("", token_id).subscribe((response) => {
			if (response.status_code == "200") {
				this.queuelist = false
				this.token_id = token_id;
				this.transactionqueueid = response.Data[0].queue_transaction_id;
				this.queue_type = response.Data[0].queue_type;
				this.token_provider_id = response.Data[0].token_provider_id
				this.onassign_custom_form_values = response.Data[0].onassign_custom_form_values
				this.oncomplete_custom_form_values = response.Data[0].oncomplete_custom_form_values
				if ((response.Data[0].oncomplete_custom_form_values != null) && (response.Data[0].onassign_custom_form_values != null)) {
					this.concatarrayforresult = response.Data[0].onassign_custom_form_values.concat(response.Data[0].oncomplete_custom_form_values)
				} else if (response.Data[0].oncomplete_custom_form_values != null) {
					this.concatarrayforresult = response.Data[0].oncomplete_custom_form_values
				} else if (response.Data[0].onassign_custom_form_values != null) {
					this.concatarrayforresult = response.Data[0].onassign_custom_form_values
				} else {
					this.concatarrayforresult = ""
				}
				this.ongoingtab = false
				this.transactionqueuelist = false
				this.searchtokendatadetailshowdata = true
				response.Data[0].followup_date = this.pipe.transform(response.Data[0].followup_date, 'dd-MM-yyyy');
				this.searchtokendatadetail = response.Data
				this.searchtoken.getcustomlist(response.Data[0].queue_id).subscribe((response) => {
					if (response.status_code == "200") {
						this.customfield = response.Data
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						//  this.snackbar.open(response.Metadata.Message,'', {
						//   duration: 2000,
						// })
					}
				});

				this.searchtoken.getEnumValues().subscribe((response) => {
					if (response.status_code == "200") {
						this.getenumvalues = response.Data
						this.getenumdefaultvalues = response.Data[0]
					} else {
						this.getenumdefaultvalues = ""
					}
				})
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				})
			}
		});
	}

	submitdateChanged(event) {
		if (event.formatted != '') {
			this.submitdate = event.formatted
		}

	}

	onSubmitdata(data) {
		const formData = new FormData();
		if (this.customfield != undefined) {
			let counte = this.customfield.length
			if (this.customfield != null) {
				let counte = this.customfield.length
				for (let i = 0; i < this.customfield.length; i++) {
					var datalast = this.customfield[i]['custom_field_id'];
					var datalastlist = (data[this.customfield[i]['custom_field_id']]);
					var newStr;
					var multileat = "";
					if (this.customfield[i]['custom_field_type'] == "List Multi Select") {
						if(datalastlist != undefined){
							for (let j = 0; j < datalastlist.length; j++) {
								multileat += datalastlist[j].custom_field_value_id + ","
							}
						}
						newStr = multileat.substring(0, multileat.length - 1);
					}
					if (this.customfield[i]['custom_field_type'] == "List Multi Select") {
						newStr = newStr != undefined?newStr:"";
						formData.append('custom_field_text_value[' + i + ']', newStr);
					} else if (this.customfield[i]['custom_field_type'] == "Date") {
						var text_value = "";
						if(datalastlist == undefined){
							text_value  =  "";
						}else{
							text_value = datalastlist.date.day+"-"+datalastlist.date.month+"-"+datalastlist.date.year;
							// text_value = datalastlist.formatted;
						}
						formData.append('custom_field_text_value[' + i + ']', text_value);
					} else {
						datalastlist = datalastlist != undefined?datalastlist:"";
						formData.append('custom_field_text_value[' + i + ']', datalastlist);
					}

					if (this.customfield[i]['custom_field_type'] == "Date") {
						formData.append('custom_field_id[' + i + ']', this.customfield[i]['custom_field_id']);
						formData.append('custom_field_type[' + i + ']', this.customfield[i]['custom_field_type']);
					} else if (this.customfield[i]['custom_field_type'] == "Amount") {
						formData.append('custom_field_id[' + i + ']', this.customfield[i]['custom_field_id']);
						formData.append('custom_field_type[' + i + ']', this.customfield[i]['custom_field_type']);
						formData.append('amount_type[' + i + ']', data["amount_type_" + this.customfield[i]['custom_field_id']]);
					} else {
						formData.append('custom_field_id[' + i + ']', this.customfield[i]['custom_field_id']);
						formData.append('custom_field_type[' + i + ']', this.customfield[i]['custom_field_type']);
					}
				}
			}
		}


		if (data.remark != "") {
			formData.append('token_remark', data.remark);
		}
		formData.append('booking_type', this.bookingtype);
		if (this.submitdate != "") {
			formData.append('followup_date', this.submitdate);
		}
		this.searchtoken.tokenSaveOnCompleteDetails(this.token_id, this.transactionqueueid, formData)
			.subscribe(
				(response) => {
					if (response.status_code == "200") {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
					}
				})


	}


	/*
	onSubmitdata()
	{
	}
	*/
	tokenRACMarkConfirm(tokenid, queue_transaction_id){
		if (confirm("Are you sure you want to mark this Token as Confirmed")) {
			this.searchtoken.tokenRACMarkConfirm(tokenid, queue_transaction_id).subscribe((response) => {
				if (response.status_code == "200") {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
					this.tokendetail(tokenid)
				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
				}
			})
		}
	}

	markasnoshow(tokenid, queue_transaction_id) {

		if (confirm("Are you sure you want to mark this patient as absent")) {
			this.searchtoken.markasnoshow(tokenid, queue_transaction_id)
				.subscribe(
					(response) => {

						if (response.status_code == "200") {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {
							//this.error=response.Metadata.Message
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
						}
					})
		}
	}



	//change

	markasarrived(tokenid) {
		if(confirm("Are you sure you want to mark this patient as arrived")) {
			// this.changesequence(tokenid, this.transactionqueueid, 'markasarrived')
			this.searchtoken.markasarrived(tokenid).subscribe((response) => {
				if (response.status_code == "200") {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
					this.tokendetail(tokenid)
					this.changesequence(tokenid, this.transactionqueueid, 'changeseq')
				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
				}
			})
		}else{
			return false;
		}
	}

	startprocessing(tokenid, transaction_id, token_no, tokensequenceno) {
		if (confirm("Are you sure you want to start processing")) {
			this.searchtoken.tokenStartProcessing(tokenid, transaction_id, token_no, tokensequenceno).subscribe((response) => {
				if (response.status_code == "432") {
					this.outofsequensecall = true
					this.searchtoken.getTransactionTokenList(transaction_id, tokenid).subscribe(
						(response) => {
							if (response.status_code == "200") {
								this.getTransactionTokenList = response.Data
								this.changesequence(tokenid, transaction_id, 'detail')
							}
						}
					)
				} else {
					this.searchtoken.tokenStartProcessing(tokenid, transaction_id, token_no, tokensequenceno).subscribe(
						(response) => {
							if (response.status_code == "200") {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})

								this.tokendetail(tokenid)

							} else if (response.status_code == '402') {
								localStorage.clear();
								this.router.navigate(['/login']);
							} else if (response.status_code == '432') {

							} else {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})

							}
						})
				}
			})
		}
	}


	startprocessingafterseq(tokenid, transaction_id, token_no, tokensequenceno) {


		this.searchtoken.tokenStartProcessing(tokenid, transaction_id, token_no, tokensequenceno)
			.subscribe(
				(response) => {

					if (response.status_code == "200") {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})

						this.tokendetail(tokenid)

					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})

					}
				})

	}


	completeprocessing(queue_id, tokenid, transaction_id) {

		if (confirm("Are you sure you want to complete processing")) {
			this.searchtoken.tokenProcessingCompleted(queue_id, tokenid, transaction_id)
				.subscribe(
					(response) => {

						if (response.status_code == "200") {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})

							this.tokendetail(tokenid)
						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {
							//this.error=response.Metadata.Message
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
						}
					})
		}


		// tokenProcessingCompleted
	}

	changesequence(tokenid, transactionqueueid, changeseq) {
		this.changeseqcheck = changeseq;
		if (changeseq == "changeseq") {
			this.outofsequensecall = false
			this.aheadofcheck = false
			this.aheadof = false
			this.nextforconsultationchecked = true
			this.disablecheckbox = false
			this.nextforconsultation = ""
			this.changesequencelabel = "Change Sequence"
			this.selectShowHide = false;
		} else if (changeseq == "detail") {
			this.changesequencelabel = "Start Processing"
			this.nextforconsultation = this.getTransactionTokenList[0].token_sequence
			this.aheadofcheck = false
			this.aheadof = true
			this.nextforconsultationchecked = true
			this.outofsequensecall = true
			this.disablecheckbox = true
		} else {
			this.aheadofcheck = false
			this.afterlastarrivedchecked = false
		}


		this.searchtoken.getChangeSequenceReasons().subscribe((response) => {
			if (response.status_code == "200") {
				this.predefinedreson = response.Data;
			} else {
				this.predefinedreson = [];
			}
		})

		this.searchtoken.getTransactionTokenList(transactionqueueid, tokenid).subscribe((response) => {
			if (response.status_code == "200") {
				this.getTransactionTokenList = response.Data
				this.afterlastarrivedcheckbox = false
				for (let i = 0; i < this.getTransactionTokenList.length; i++) {
					if (this.getTransactionTokenList[i]['is_max_arrival'] == true) {
						this.afterlastarrivedcheckbox = true;
						this.nextforconsultation = this.getTransactionTokenList[i].token_sequence;
					}
				}
				if (changeseq == "markasarrived") {
					this.aheadofcheck = false
					if (this.afterlastarrivedcheckbox == false) {
						this.nextforconsultation = this.getTransactionTokenList[0].token_sequence
						this.nextforconsultationchecked = true
					} else {
						this.nextforconsultationchecked = false
						this.afterlastarrivedchecked = true
					}
					this.aheadofcheck = false
					this.aheadof = true
					this.outofsequensecall = true
					this.disablecheckbox = false
				}
				this.modelToggle("open")
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				});
			}
		});
	}

	cancelbooking() {
		this.cancelmodelToggle("open")
	}


	cancelsubmit(reasonforcancel, token_id, queue_transaction_id) {
		this.searchtoken.cancelsubmit(reasonforcancel, token_id, queue_transaction_id, this.changeseqtokenid)
			.subscribe(
				(response) => {

					if (response.status_code == "200") {


						this.cancelmodelToggle("close")
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
						this.tokendetail(token_id)
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {

						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
					}
				})


	}


	modelToggle(action: string) {
		if (action == "open") {
			this.modelFlag = true;
		} else {
			this.modelFlag = false;
		}
	}


	cancelmodelToggle(action: string) {
		if (action == "open") {
			this.cancelmodel = true;
		} else if (action == "closefamily") {

			this.cancelmodel = false;
		} else {

			this.familymember = null
			this.assignbuttonshow = false
			this.cancelmodel = false;
			this.token_no = null
			this.token_time = null
			this.token_apitime = null
			this.token_date = null
			this.regno = null
			this.usermasterid = null
			this.searchmobileage = 0
			this.searchfirst_name = null
			this.usergendermale = null
			this.userprofileimg = null
		}
	}


	ontokenChange(tokenid) {
		this.changeseqtokenid = tokenid

	}

	changesequencesubmit(reson, token_id, transactionid) {
		let position:any = "";
		if(this.checkboxselectcheck  == "nextforconsultation"){
			position = "nextforconsultation";
			this.changeseqtokenid = "";
		}else if(this.checkboxselectcheck  == "afterlastarrived"){
			position = "afterlastarrived";
			this.changeseqtokenid = "";
		}else if(this.checkboxselectcheck  == "last"){
			position = "after";
		}else {
			position = "aheadof";
		}
		if ((this.changeseqtokenid == "") || (this.changeseqtokenid == undefined)) {
			if (this.nextforconsultation != "") {
				this.changeseqtokenid = this.nextforconsultation
			}
		}
		// if ((this.changeseqtokenid != "") && (this.changeseqtokenid != undefined)) {
			var sequence:any;
			if (this.checkboxselectcheck == "last") {
				sequence = Number(this.changeseqtokenid)
			} else if (this.checkboxselectcheck == "afterlastarrived") {
				sequence = Number(this.changeseqtokenid)
			} else {
				sequence = this.changeseqtokenid
			}
			this.searchtoken.changesequencesubmit(reson, token_id, transactionid, sequence,position).subscribe((response) => {
				if (response.status_code == "200") {
					this.modelToggle("close")
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
					if (this.changeseqcheck != "changeseq") {
						this.startprocessingafterseq(token_id, transactionid, this.searchtokendatadetail[0].token_no, this.changeseqtokenid)
					}
				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					})
				}
			})
		// } else {
		// 	if ((this.changeseqtokenid == "") || (this.changeseqtokenid == undefined)) {
		// 		this.snackbar.open("please select token ", '', {
		// 			duration: 2000,
		// 		})
		// 	}
		// }


	}


	reschedulelistsclicktransaction(data) {
		if (data == 'today') {
			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"screenname": 'Todays Queues',
					"type": 'Todays Queues',
					"tokenidrequestreshadule": this.tokenidfortransactionback,
					"checktodaytokenid": this.checktoday,
					"queue_transaction_id": this.queue_transaction_id
				},
				skipLocationChange: true

			});

		} else if (data == 'ongoing') {

			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"screenname": 'Ongoing Queues',
					"type": 'Ongoing Queues',
					"tokenidrequestreshadule": this.tokenidfortransactionback,
					"checktodaytokenid": this.checktoday,
					"queue_transaction_id": this.queue_transaction_id
				},
				skipLocationChange: true
			});

		} else if (data == 'futurepast') {


			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"screenname": 'Future Past Token',
					"type": 'Future Past Token',
					"tokenidrequestreshadule": this.tokenidfortransactionback,
					"checktodaytokenid": this.checktoday,
					"queue_transaction_id": this.queue_transaction_id,
					"futurepastmasterid": this.futurepastmasterid,
					"futurepasttodate": this.futurepasttodate,
					"futurepastfromdate": this.futurepastfromdate,
					"futurepastlastname": this.futurepastlastname,
					"futurepastfirstname": this.futurepastfirstname,
					"futurepastmiddle_name": this.futurepastmiddle_name,
					"futurepastprofileimage_path": this.futurepastprofileimage_path,
				},
				skipLocationChange: true
			});


		} else {


		}

	}


	reschedulelistsclick(token_id) {
		if (this.screenname1 == "Future Past Token") {
			this.token_idres = token_id
			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"futurepastmasterid": this.futurepastmasterid,
					"futurepasttodate": this.futurepasttodate,
					"futurepastfromdate": this.futurepastfromdate,
					"futurepastlastname": this.futurepastlastname,
					"futurepastfirstname": this.futurepastfirstname,
					"futurepastmiddle_name": this.futurepastmiddle_name,
					"futurepastprofileimage_path": this.futurepastprofileimage_path,
					"futurepastorg_location": this.futurepastorg_location,
					"screenname": this.screenname1,
					"tokenidrequestreshadule": token_id,
					"checktodaytokenid": this.checktoday,
					"queue_transaction_id": this.queue_transaction_id,
					"queselecteddate":this.reshaduledate
				},
				skipLocationChange: true
			});

		} else if (this.screenname1 == "Search Tokens") {
			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"fromdate": this.fromdate,
					"patient": this.patient,
					"tokenidrequestreshadule": token_id,
					"todate": this.todate,
					"screenname": this.screenname1,
					"tokentypesearch": this.tokentypesearch,
					"searchpage": this.searchpage,
					"queselecteddate":this.reshaduledate
				},
				skipLocationChange: true
			});
		} else {
			this.token_idres = token_id
			this.router.navigate(['/requestsreceived/'], {
				queryParams: {
					"requestsreceivedreshadule": "requestsreceivedreshadule",
					"screenname": this.screenname1,
					"tokenidrequestreshadule": token_id,
					"checktodaytokenid": this.checktoday,
					"queue_transaction_id": this.queue_transaction_id,
					"queselecteddate":this.reshaduledate
				},
				skipLocationChange: true

			});

		}
	}


	gettokenlistreshadule(reqdate, token_provider_id) {


		this.requestrceived.gettokenlist(reqdate, token_provider_id)
			.subscribe(
				(response) => {
					if (response.status_code == '200') {


						this.quelistdatareshadule = response.Data

					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						});
					}
				})
	}


	onqueselectreshadule(event:any) {
		this.requestrceived.getmonitorqueQueues(event, this.reshaduledate).subscribe((response) => {
			this.concatarrayforresult = null
			this.queue_reqque = event
			//this.queue_id=event
			if (response.status_code == "200") {
				this.token_nores = response.Data.last_token.token_no
				this.token_timeres = response.Data.last_token.token_conveted_time
				this.token_time = response.Data.last_token.token_conveted_time
				this.restoken_time = response.Data.last_token.token_time
				this.token_apitime = response.Data.last_token.token_time
				this.is_buffer_token_started = response.Data.queue_info.is_buffer_token_started
				this.token_status = response.Data.queue_info.token_status
				this.requestrceived.getcustomlist(event).subscribe((response) => {
					this.customfield = response.Data
				});
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				//this.snackbar.open(response.Metadata.Message,'', {
				//duration: 2000,
				//});
			}
		})
	}


	onAssignresDateChanged(event) {
		if (event.formatted != '') {
			this.requestrceived.gettokenlist(event.formatted, this.provideridselected)
				.subscribe(
					(response) => {
						//this.queue_id=event
						if (response.status_code == "200") {


							this.quelistdatareshadule = response.Data
							this.reshaduledate = event.formatted

							/*

 this.reshaduledate=event.formatted
 this.token_nores=response.Data.last_token.token_no
 this.token_timeres=response.Data.last_token.token_conveted_time
 this.restoken_time=response.Data.last_token.token_time
 this.gettokenlistreshadule(this.reshaduledate,this.token_provider_id)
*/


						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {

							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});

						}
					})
		} else {
			this.snackbar.open("Please select Date", '', {
				duration: 1000,
			});
		}
	}


	reshaduleonqueclick() {
		const config = new MatDialogConfig();
		config.data = {
			tokendate: this.reshaduledate,
			queue_id: this.queue_reqque,
			token_noexist: this.token_no
		};
		this.dialogRef = this.dialog.open(DialogmonitorqueComponent, config);
		this.dialogRef.afterClosed().subscribe((value:any) => {
			this.token_nores = value.token_no
			this.token_timeres = value.token_conveted_time
			this.token_time = value.token_conveted_time
			this.token_apitime = value.token_time
			this.restoken_time = value.token_time
			this.input_token_time = value.token_time;
			this.is_buffer_token_started = value.is_buffer_token_started
			this.token_status = value.token_status
			this.reshaduledate = value.selecteddate
		});
	}


	reshadulemessageclick() {

		const config = new MatDialogConfig();


		config.data = {
			tokenid: this.token_idres
		};


		this.dialogRef = this.dialog.open(MessagequelistComponent, config);

		this.dialogRef.afterClosed().subscribe(value => {

		})

	}
	newreqmessageclick() {

		const config = new MatDialogConfig();


		config.data = {
			tokenid: this.token_id
		};


		this.dialogRef = this.dialog.open(MessagequelistComponent, config);

		this.dialogRef.afterClosed().subscribe(value => {

		})

	}


	/* ravi new code */
	public fileSelectionEvent(fileInput: any, oldIndex) {
		if (fileInput.target.files && fileInput.target.files[0]) {
			var reader = new FileReader();
			reader.onload = (event: any) => {}
			if (oldIndex == 0) {
				this.totalfiles.unshift((fileInput.target.files[0]))
				this.totalFileName.unshift(fileInput.target.files[0].name)
			} else {
				// let main_form: FormData = new FormData();
				this.totalfiles[oldIndex] = (fileInput.target.files[0]);
				this.totalFileName[oldIndex] = fileInput.target.files[0].name
			}
			reader.readAsDataURL(fileInput.target.files[0]);
		}
		if (this.totalfiles.length == 1) {
			this.lengthCheckToaddMore = 1;
		}
	}


	removeItem(index: number) {
		this.totalfiles.splice(index);
		this.totalFileName.splice(index);
		this.items.removeAt(index);
		this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
	}


	/* ravi new code */


	showprescriptionform(tokenid) {
		this.dignosispriscription = false
		this.patienthistoryaddmore = true
	}

	diegnosisprescription(tokenid:any,tokendate:any="") {
		this.router.navigate(['/diagnosisandprescription/'], {
			queryParams: {
				"tokenid": tokenid,
				"queue_transaction_id":this.queue_transaction_id,
				"booking_for": this.searchtokendatadetail[0].booking_for,
				"booking_for_name": this.searchtokendatadetail[0].booking_for_name,
				"booking_for_user_id":this.searchtokendatadetail[0].booking_for_user_id,
				"provider_id":this.searchtokendatadetail[0].provider_details[0].provider_user_master_id,
				"provider_name":this.searchtokendatadetail[0].provider_name,
				"consumer_profile_pic_path":this.searchtokendatadetail[0].consumer_profile_pic_path,
				"tokendate":tokendate,
				"screenname":this.screenname,
				"statusforrequestreshadule":'Todays Queues',
				"ongoingprovider_name":this.ongoingprovider_name,
				"ongoingqueue_name":this.ongoingqueue_name,
				"searchtoken": "searchtoken",
				"fromdate": this.fromdate,
				"patient": this.patient,
				"todate": this.todate,
				"tokentypesearch": this.tokentypesearch,
				"searchpage": this.searchpage,
				"futurepastmasterid": this.futurepastmasterid,
				"futurepastfromdate": this.futurepastfromdate,
				"futurepasttodate": this.futurepasttodate,
				"futurepastlastname": this.futurepastlastname,
				"futurepastfirstname": this.futurepastfirstname,
				"futurepastmiddle_name": this.futurepastmiddle_name,
				"futurepastprofileimage_path": this.futurepastprofileimage_path,
				"futurepastorg_location": this.futurepastorg_location,
				"queue_type":this.queue_type,
				"todaysongoingprovidername":this.todaysongoingprovidername,
			},
			skipLocationChange: true
		});
	}
	/* diegnosisprescription(tokenid) {
		let roletype = JSON.parse(localStorage.getItem('activerole'));
		if (roletype.activerole == "l2") {
			this.hiddenall = false
			this.router.navigate(['/description/'], {
				// providerongoingdescription
				queryParams: {
					"providerongoingdescription": "providerongoingdescription",
					"tokeniddescription": tokenid
				},
				skipLocationChange: true
			});

		} else {
			this.hiddenall = false
			this.requestrceived.getTokenDPRDetails(tokenid)
				.subscribe(
					(response) => {
						if (response.status_code == "200") {
							if (response.Data.token_details.token_details != null) {
								this.token_details = response.Data.token_details[0]
								if (response.Data.token_details.diagnosis_details[0] != null) {
									this.diagnosis_details = response.Data.diagnosis_details[0]
								}
							}
							if (response.Data.prescription_details != null) {
								this.prescription_details = response.Data.prescription_details
							}
							if (response.Data.test_details != null) {
								this.test_details = response.Data.test_details
							}
							if (response.Data.prescription_images != null) {
								this.prescription_images = response.Data.prescription_images
							}
							if (response.Data.report_images != null) {
								this.report_images = response.Data.report_images
							}
							this.dignosispriscription = true
						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});
						}
					})

		}


	} */


	/* assign new token*/
	assigntoken(type:any,queue_id:any = "") {
		this.assignnewtoken.getProviderQueues(this.quedefaultdate, this.selectedproid)
			.subscribe((response) => {
				if (response.status_code == "200") {
					this.quelistdata = response.Data
					this.token_no = 0
					const config = new MatDialogConfig();
					if(queue_id != ""){
						this.queue_id = queue_id;
					}
					config.data = {
						quelistdata: this.quelistdata,
						tokendate: this.quedefaultdate,
						queue_id: this.queue_id,
						token_noexist: this.token_no,
						selectedproid: this.selectedproid
					};
					this.dialogRef = this.dialog.open(DialogmonitorqueComponent, config);
					this.dialogRef.afterClosed().subscribe((value:any) => {
						if (value.token_no != 0) {
							this.token_no = value.token_no
							this.token_prv_page = value.token_no
							// this.myFormattedDate = this.pipe.transform(value.selecteddate, 'dd-MM-yyyy');
							this.token_time = value.token_conveted_time
							this.token_apitime = value.token_time
							this.input_token_time =  value.token_time
							this.myFormattedDate = value.tokendate;
							this.queue_reqque = value.selectedqueue
							this.queselecteddate = value.selecteddate
							this.token_status = value.token_status
							this.queue_id = value.queue_id
							this.token_status = value.token_status
							this.reshaduledate = value.selecteddate
						} else {
							this.myFormattedDate = this.pipe.transform(this.currentdate, 'dd-MM-yyyy');
							this.token_prv_page = "0"
						}
						if (type == "ongoing") {
							this.todaystoken = false
						} else {
							this.todaystoken = true
						}

						let roletype = JSON.parse(localStorage.getItem('activerole'));
						let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
						const config = new MatDialogConfig();
						if (roletype.activerole == 'l2') {
							let usermasterid = atob(currentUser.user_master_id);
							this.provider_full_name = atob(currentUser.first_name) + " " + atob(currentUser.middle_name) + " " + atob(currentUser.last_name)
							this.selectedproid = usermasterid
							config.data = {
								selectedproid: this.selectedproid,
								firstname: atob(currentUser.first_name),
								middle_name: atob(currentUser.middle_name),
								lastname: atob(currentUser.last_name),
							};

						} else {
							config.data = {
								selectedproid: this.selectedproid,
								firstname: this.assignnew_firstname,
								middle_name: this.assignnew_midlename,
								lastname: this.assignnew_lastname,
							};

						}

						this.dialogRef = this.dialog.open(AssignnewtokendialogComponent, config);
						this.dialogRef.afterClosed().subscribe(value => {
							if (value != null) {
								this.fullnamecheck = value.fullname
								var mobilenumber = value.mobilenumber
								this.selectedproid = this.selectedproid
								this.familymemeberidfordetail = value.familymemberid
								this.mobilesearchmasterid = value.mobilesearchmasterid
								this.profileimg = value.profileimg
								this.regnonew = value.reg_no
								this.agenew = value.age
								this.gender = value.gender
								this.last_visited = value.last_visited
								this.memberid = value.familymemberid;
								if (value.bookingtypeselectedtype == undefined) {
									this.bookingtypeselectedtype = "non_member"
								} else {
									this.bookingtypeselectedtype = value.bookingtypeselectedtype
								}


								let currentdate = new Date().getMonth();
								let send_date = new Date();
								send_date.setMonth(send_date.getMonth() + 3);
								let formatedcu = this.pipe.transform(send_date, 'dd-MM-yyyy');
								if (this.last_visited != null) {
									var dateex = this.last_visited.split("-");
									var daten = new Date("" + dateex[1] + "/" + dateex[0] + "/" + dateex[2] + "");
									//var d1 = new Date(""+this.last_visited+"");
									let convert = new Date(this.last_visited)
									this.pipe.transform(this.currentdate, );
									var diff = send_date.getTime() - daten.getTime()
									var diff2 = Math.round(diff / (1000 * 60 * 60 * 24))
									if (diff2 > 90) {
										this.newvisit = true
										this.followup = false
									} else if (diff2 < 90) {
										this.newvisit = false
										this.followup = true
									} else {
										this.newvisit = false
										this.followup = false
									}
								}


								if (this.bookingtypeselectedtype != 'non_member') {
									this.cancelsubmitdata(mobilenumber)
								}
								if (this.fullnamecheck == '0') {
									if (value.serby != "") {
										this.serby = value.serby
										this.textvalue = value.textvalue
										this.mobilenum = value.mobilenum
									} else {
										this.serby = ""
										this.textvalue = ""
										this.mobilenum = ""
									}
								}
								this.serby = value.serby
								this.textvalue = value.textvalue
								this.mobilenum = value.mobilenum
								this.assignnow(mobilenumber)
							}
						});
					})
				}
			})
	}



	onSubmit(data) {
		data.gender = (data.gender != undefined)?data.gender:this.usergendermale;
		if (data.firstname != undefined) {
			// alert("if");return false;
			const formData = new FormData();
			if (this.customfield != undefined) {
				let counte = this.customfield.length
				if (this.customfield != null) {
					let counte = this.customfield.length
					for (let i = 0; i < this.customfield.length; i++) {
						var datalast = this.customfield[i]['custom_field_id'];
						var datalastlist = (data[this.customfield[i]['custom_field_id']]);
						var newStr;
						if ((this.customfield[i]['custom_field_type'] == "List Multi Select") && (this.customfield[i]['is_mandatory'] == true)) {
							if (datalastlist == "") {
								this.snackbar.open("please select multiselect field  " + this.customfield[i]['custom_label'], '', {
									duration: 2000,
								});
								return false;
							}
						}

						// if (datalastlist != undefined) {
							// if (datalastlist) {
								var multileat="";
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
									if(datalastlist != undefined){
										for(let j=0;j<datalastlist.length;j++){
										multileat+=datalastlist[j].custom_field_value_id+","
										}
									}
									newStr = multileat.substring(0, multileat.length-1);
								}
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
									if(newStr!=undefined){
										formData.append('custom_field_text_value['+i+']',newStr);
									}
								}else if(this.customfield[i]['custom_field_type']=="Date"){
								var formattedVal:any = "";
									if(datalastlist != undefined){
										formattedVal = datalastlist.formatted;
									}
									formData.append('custom_field_text_value['+i+']',formattedVal);
								}else{
									if(datalastlist!=undefined){
										formData.append('custom_field_text_value['+i+']',datalastlist);
									}
								}

								if(this.customfield[i]['custom_field_type']=="Date"){
									if(datalastlist != undefined){
										formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
										formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
									}
								}else if(this.customfield[i]['custom_field_type']=="Amount"){
									if(datalastlist!=undefined){
										formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
										formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
										formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
									}
								}else{
									if(datalastlist!=undefined){
										formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
										formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
									}
								}
							// }
						// }
					}
				}
			}

			var usermaster = '';
			if (this.usermasterid == undefined) {
				usermaster = "";
			}
			var no = "";
			if (data.phonenumber != undefined) {
				no = data.phonenumber;
			} else {
				no = "";
			}
			formData.append('tokenproviderid', this.selectedproid);
			formData.append('tokenrequestorid', this.mobilesearchmasterid);
			formData.append('tokenrequestorno', no);
			formData.append('tokenrequestorname', data.firstname);
			formData.append('tokenno', this.token_no);
			formData.append('tokentime', this.token_apitime);
			formData.append('tokendate', this.queselecteddate);
			formData.append('bookingfor', "non_member");
			formData.append('bookingtype', this.bookingtype);
			formData.append('tokenmsg', data.tokenmsg);
			formData.append('userage', data.age);
			formData.append('usergender', data.gender);
			formData.append('bookingusername', data.firstname);
			formData.append('bookinguserid', usermaster);
			formData.append('iseditable', this.iseditable);
			if (this.queue_id != undefined) {
				//conformation box start
				if (this.token_status == "BUFFER") {
					// this.confirmationDialogService.confirm('Confirmation Alert', 'MAX no. of tokens already assigned. Do wish to assign Tokens from Buffer Range ?').then((confirmed) => {
						// if (confirmed) {
							// this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.queselecteddate + ' to ' + data.firstname + ' ?').then((confirmed) => {
							this.confirmationDialogService.confirm('BUFFER TOKEN',  'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.queselecteddate + ' to ' + data.firstname + ' ?','Yes - Book as Confirmed','No','Yes - Book as RAC','md').then((confirmed:any) => {
									if(confirmed == "rac"){
										formData.append('apt_status', "RAC");
										confirmed = true;
									}
									if (confirmed) {
										//conformation box end
										this.assignnewtoken.submitassignquecustomforunknown(
											formData,
											this.queue_id,
											this.selectedproid,
											this.mobilesearchmasterid,
											data.firstname,
											this.tokenrequestorno,
											this.token_no,
											this.token_apitime,
											this.queselecteddate,
											this.bookedfor,
											data.firstname,
											data.regno,
											this.usermasterid,
											data.gender,
											data.age,
											this.bookingtype,
											data.tokenmsg,
											this.bookingtypeselectedtype
										).subscribe((response) => {
											this.queue_id = event;
											if (response.status_code == "200") {
												//window.history.back();;
												if (this.screenname1 == 'Ongoing Queues') {
													this.assignsearchdiv = false;
													this.hiddenall = true;
													this.ongoingtab = false;
													this.queuelist = false;
													this.selfimage = null;
													this.transactionqueuelist = false;
													this.getongoingqueuetoken(this.queue_transaction_id);
												} else if (this.screenname1 == 'Todays Queues') {
													if (this.ongoingtab == false) {
														this.assignsearchdiv = false;
														this.hiddenall = true;
														this.ongoingtab = false;
														this.queuelist = false;
														this.selfimage = null;
														this.transactionqueuelist = false;
														this.getongoingqueuetoken(this.queue_transaction_id);
													} else {
														this.assignsearchdiv = false;
														this.hiddenall = true;
														this.ongoingtab = false;
														this.queuelist = false;
														this.selfimage = null;
														this.transactionqueuelist = false;
														this.gettransactionqueuetoken(this.queue_transaction_id);
													}
												} else if (this.screenname1 == 'Future Past Token') {
													if (this.todaystoken == true) {
														this.assignsearchdiv = false
														this.hiddenall = true
														this.ongoingtab = false
														this.queuelist = true
														this.transactionqueuelist = false
														this.gettransactionqueuetoken(this.queue_transaction_id);
													} else {
														this.assignsearchdiv = false;
														this.hiddenall = true;
														this.ongoingtab = false;
														this.queuelist = false;
														this.selfimage = null;
														this.transactionqueuelist = false;
														this.getongoingqueuetoken(this.queue_transaction_id);
													}
												} else {
													window.history.back();
												}
												this.snackbar.open(response.Metadata.Message, '', {
													duration: 2000,
												});
											} else if (response.status_code == '402') {
												localStorage.clear();
												this.router.navigate(['/login']);
											} else if (response.status_code == '432') {
												this.snackbar.open(response.Metadata.Message, '', {
													duration: 2000,
												});
											} else {
												this.snackbar.open(response.Metadata.Message, '', {
													duration: 2000,
												});
											}
										})
									}
								})
							// }
						// })
				} else {
					this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.queselecteddate + ' to ' + data.firstname + ' ?').then((confirmed) => {
						if (confirmed) {
							this.assignnewtoken.submitassignquecustomforunknown(
									formData,
									this.queue_id,
									this.selectedproid,
									this.mobilesearchmasterid,
									data.firstname,
									this.tokenrequestorno,
									this.token_no,
									this.token_apitime,
									this.queselecteddate,
									this.bookedfor,
									data.firstname,
									data.regno,
									this.usermasterid,
									data.gender,
									data.age,
									this.bookingtype,
									data.tokenmsg,
									this.bookingtypeselectedtype
								).subscribe((response) => {
									if (response.status_code == "200") {
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										});
										if (this.screenname1 == 'Ongoing Queues') {
											this.assignsearchdiv = false;
											this.hiddenall = true;
											this.ongoingtab = false;
											this.queuelist = false;
											this.selfimage = null;
											this.transactionqueuelist = false;
											this.getongoingqueuetoken(this.queue_transaction_id);
										} else if (this.screenname1 == 'Todays Queues') {
											if (this.todaystoken == true) {
												this.assignsearchdiv = false;
												this.hiddenall = true;
												this.ongoingtab = false;
												this.queuelist = true;
												this.transactionqueuelist = false;
												this.gettransactionqueuetoken(this.queue_transaction_id);
											} else {
												this.assignsearchdiv = false;
												this.hiddenall = true;
												this.ongoingtab = false;
												this.queuelist = false;
												this.selfimage = null;
												this.transactionqueuelist = false;
												this.getongoingqueuetoken(this.queue_transaction_id);
											}
										} else if (this.screenname1 == 'Future Past Token') {
											if (this.todaystoken == true) {
												this.assignsearchdiv = false;
												this.hiddenall = true;
												this.ongoingtab = false;
												this.queuelist = true;
												this.transactionqueuelist = false;
												this.gettransactionqueuetoken(this.queue_transaction_id);
											} else {
												this.assignsearchdiv = false;
												this.hiddenall = true;
												this.ongoingtab = false;
												this.queuelist = false;
												this.selfimage = null;
												this.transactionqueuelist = false;
												this.getongoingqueuetoken(this.queue_transaction_id);
											}
										} else {
											window.history.back();;
										}
									} else if (response.status_code == '402') {
										localStorage.clear();
										this.router.navigate(['/login']);
									} else if (response.status_code == '432') {
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										});
									} else {
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										});
									}
								})
						}
					})
				}
			} else {
				this.snackbar.open("please select queue", '', {
					duration: 2000,
				})
			}
		} else {
			// alert("else");return false;
			const formData = new FormData();
			if (this.customfield != undefined) {
				let counte = this.customfield.length
				if (this.customfield != null) {
					let counte = this.customfield.length
					for (let i = 0; i < this.customfield.length; i++) {
						var datalast = this.customfield[i]['custom_field_id'];
						var datalastlist = (data[this.customfield[i]['custom_field_id']]);
						var newStr;
						if ((this.customfield[i]['custom_field_type'] == "List Multi Select") && (this.customfield[i]['is_mandatory'] == true)) {
							if (datalastlist == "") {
								this.snackbar.open("please select multiselect field  " + this.customfield[i]['custom_label'], '', {
									duration: 2000,
								});
								return false;
							}
						}
						// if (datalastlist != undefined) {
							// if (datalastlist) {
								var multileat="";
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
								if(datalastlist != undefined){
									for(let j=0;j<datalastlist.length;j++){
									multileat+=datalastlist[j].custom_field_value_id+","
									}
								}
								newStr = multileat.substring(0, multileat.length-1);
								}
								if(this.customfield[i]['custom_field_type']=="List Multi Select"){
								if(newStr!=undefined){
									formData.append('custom_field_text_value['+i+']',newStr);
								}
								}else if(this.customfield[i]['custom_field_type']=="Date"){
								var formattedVal:any = "";
								if(datalastlist != undefined){
									formattedVal = datalastlist.formatted;
								}
								formData.append('custom_field_text_value['+i+']',formattedVal);
								}else{
								if(datalastlist!=undefined){
									formData.append('custom_field_text_value['+i+']',datalastlist);
								}
								}

								if(this.customfield[i]['custom_field_type']=="Date"){
								if(datalastlist != undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
								}
								}else if(this.customfield[i]['custom_field_type']=="Amount"){
								if(datalastlist!=undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
									formData.append('amount_type['+i+']',data["amount_type_"+this.customfield[i]['custom_field_id']]);
								}
								}else{
								if(datalastlist!=undefined){
									formData.append('custom_field_id['+i+']',this.customfield[i]['custom_field_id']);
									formData.append('custom_field_type['+i+']',this.customfield[i]['custom_field_type']);
								}
								}
							// }
						// }
					}
				}
			}
			var no = "";
			if (this.tokenrequestorno != undefined) {
				no = this.tokenrequestorno;
			} else {
				no = "";
			}
			var age;
			formData.append('tokenproviderid', this.selectedproid);
			formData.append('tokenrequestorid', this.mobilesearchmasterid);
			formData.append('tokenrequestorname', this.selfname);
			formData.append('tokenrequestorno', no);
			formData.append('tokenno', this.token_no);
			formData.append('tokentime', this.token_apitime);
			formData.append('tokendate', this.queselecteddate);
			formData.append('bookingfor', this.bookedfor);
			formData.append('bookingtype', this.bookingtype);
			formData.append('tokenmsg', data.tokenmsg);
			formData.append('userage', data.age);
			formData.append('usergender', data.gender);
			formData.append('bookingusername', this.searchfirst_name);
			formData.append('bookinguserid', this.usermasterid);
			formData.append('iseditable', this.iseditable);
			if (this.queue_id != undefined) {
				// if (this.is_buffer_token_started == true) {
				if (this.token_status == "BUFFER") {
					// this.confirmationDialogService.confirm('Confirmation Alert', 'MAX no. of tokens already assigned. Do wish to assign Tokens from Buffer Range ?').then((confirmed) => {
						// if (confirmed) {
							this.confirmationDialogService.confirm('BUFFER TOKEN', 'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.queselecteddate + ' to ' + this.searchfirst_name + ' ?','Yes - Book as Confirmed','No','Yes - Book as RAC','md').then((confirmed:any) => {
								if(confirmed == "rac"){
									formData.append('apt_status', "RAC");
									confirmed = true;
								}
								if (confirmed) {
									this.assignnewtoken.submitassignquecustom(
										formData,
										this.queue_id,
										this.selectedproid,
										this.mobilesearchmasterid,
										this.selfname,
										this.tokenrequestorno,
										this.token_no,
										this.token_apitime,
										this.queselecteddate,
										this.bookedfor,
										this.searchfirst_name,
										data.regno,
										this.usermasterid,
										this.usergendermale,
										data.age,
										this.bookingtype,
										data.tokenmsg,
										this.bookingtypeselectedtype
									).subscribe((response) => {
										if (response.status_code == "200") {
											if (this.screenname1 == 'Ongoing Queues') {
												this.assignsearchdiv = false;
												this.hiddenall = true;
												this.ongoingtab = false;
												this.queuelist = false;
												this.selfimage = null;
												this.transactionqueuelist = false;
												this.getongoingqueuetoken(this.queue_transaction_id)
											} else if (this.screenname1 == 'Todays Queues') {
												if (this.todaystoken == true) {
													this.assignsearchdiv = false;
													this.hiddenall = true;
													this.ongoingtab = false;
													this.queuelist = true;
													this.transactionqueuelist = false;
													this.gettransactionqueuetoken(this.queue_transaction_id);
												} else {
													this.assignsearchdiv = false
													this.hiddenall = true
													this.ongoingtab = false
													this.queuelist = false
													this.selfimage = null
													this.transactionqueuelist = false
													this.getongoingqueuetoken(this.queue_transaction_id)
												}
											} else if (this.screenname1 == 'Future Past Token') {
												if (this.todaystoken == true) {
													this.assignsearchdiv = false;
													this.hiddenall = true;
													this.ongoingtab = false;
													this.queuelist = true;
													this.transactionqueuelist = false;
													this.gettransactionqueuetoken(this.queue_transaction_id);
												} else {
													this.assignsearchdiv = false;
													this.hiddenall = true;
													this.ongoingtab = false;
													this.queuelist = false;
													this.selfimage = null;
													this.transactionqueuelist = false;
													this.getongoingqueuetoken(this.queue_transaction_id);
												}
											} else {
												window.history.back();;
											}
											this.snackbar.open(response.Metadata.Message, '', {
												duration: 2000,
											});
										} else if (response.status_code == '432') {
											this.snackbar.open(response.Metadata.Message, '', {
												duration: 2000,
											});
										} else if (response.status_code == '402') {
											localStorage.clear();
											this.router.navigate(['/login']);
										} else if (response.status_code == "400") {
											this.assignsearchdiv = true
											this.snackbar.open(response.Metadata.Message, '', {
												duration: 2000,
											})
										} else {
											this.snackbar.open(response.Metadata.Message, '', {
												duration: 2000,
											})
										}
									})
								}
							})

						// }
					// })
				} else {
					this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you wish to assign Token no. ' + this.token_no + ' for ' + this.queselecteddate + ' to ' + this.searchfirst_name + ' ?').then((confirmed) => {
							if (confirmed) {
								this.assignnewtoken.submitassignquecustom(
									formData,
									this.queue_id,
									this.selectedproid,
									this.mobilesearchmasterid,
									this.selfname,
									this.tokenrequestorno,
									this.token_no,
									this.token_apitime,
									this.queselecteddate,
									this.bookedfor,
									this.searchfirst_name,
									data.regno,
									this.usermasterid,
									data.gender,
									data.age,
									this.bookingtype,
									data.tokenmsg,
									this.bookingtypeselectedtype
								).subscribe((response) => {
									if (response.status_code == "200") {
										if (this.screenname1 == 'Ongoing Queues') {
											this.assignsearchdiv = false
											this.hiddenall = true
											this.ongoingtab = false
											this.queuelist = false
											this.selfimage = null
											this.transactionqueuelist = false
											this.getongoingqueuetoken(this.queue_transaction_id)
										} else if (this.screenname1 == 'Todays Queues') {
											if (this.todaystoken == true) {
												this.assignsearchdiv = false
												this.hiddenall = true
												this.ongoingtab = false
												this.queuelist = true
												this.transactionqueuelist = false
												this.gettransactionqueuetoken(this.queue_transaction_id)
											} else {
												this.assignsearchdiv = false
												this.hiddenall = true
												this.ongoingtab = false
												this.queuelist = false
												this.selfimage = null
												this.transactionqueuelist = false
												this.getongoingqueuetoken(this.queue_transaction_id)
											}
										} else if (this.screenname1 == 'Future Past Token') {
											if (this.todaystoken == true) {
												this.assignsearchdiv = false
												this.hiddenall = true
												this.ongoingtab = false
												this.queuelist = true
												this.transactionqueuelist = false
												this.gettransactionqueuetoken(this.queue_transaction_id)
											} else {
												this.assignsearchdiv = false
												this.hiddenall = true
												this.ongoingtab = false
												this.queuelist = false
												this.selfimage = null
												this.transactionqueuelist = false
												this.getongoingqueuetoken(this.queue_transaction_id)
											}
										} else {
											window.history.back();;
										}
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										});
									} else if (response.status_code == '432') {
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										});
									} else if (response.status_code == '402') {
										localStorage.clear();
										this.router.navigate(['/login']);
									} else if (response.status_code == "400") {
										this.assignsearchdiv = true
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										})
									} else {
										this.snackbar.open(response.Metadata.Message, '', {
											duration: 2000,
										})
									}
								})
							}
						})
				}
			} else {
				this.snackbar.open("please select queue", '', {
					duration: 2000,
				})
			}
		}
	}

	onqueselect(event:any,token_no:any = "") {
		this.assignnewtoken.getmonitorqueQueues(this.myFormattedDate, event).subscribe((response) => {
			this.queue_id = event
			if (response.status_code == 200) {
				if (response.Data.last_token == null) {
					this.token_no = '0'
					this.snackbar.open("This Queue is Full for the day. Select another queue.", '', {duration: 1000,});
				} else {
					if(token_no == ""){
						this.token_no = response.Data.last_token.token_no
						this.token_timeres = response.Data.last_token.token_conveted_time
						this.token_time = response.Data.last_token.token_conveted_time
						this.token_apitime = response.Data.last_token.token_time
						this.input_token_time =  response.Data.last_token.token_time
						this.token_status = response.Data.last_token.token_status
					}
				}
				this.token_date = this.queselecteddate
				this.is_buffer_token_started = response.Data.queue_info.is_buffer_token_started;
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000,});
			}
		})
		this.providerlist = false
		this.assignnewtokenform = true
		this.assignnewtoken.getcustomlist(event).subscribe((response) => {
			if (response.status_code == "200") {
				this.customfield = response.Data
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.customfield = null
			}
		});
	}

	onqueselectprv(event) {
		this.assignnewtoken.getmonitorqueQueues(this.myFormattedDate, event)
			.subscribe(
				(response) => {
					this.queue_id = event
					if (response.status_code == "200") {
						if (response.Data.last_token == null) {
							this.token_no = '0'
							this.snackbar.open("This Queue is Full for the day. Select another queue.", '', {
								duration: 1000,
							});
						} else {
							if(this.token_no == 0 || this.token_no == ""){
								this.token_time = response.Data.last_token.token_conveted_time
								this.token_apitime = response.Data.last_token.token_time
							}
							//this.token_no=response.Data.last_token.token_no
						}
						this.is_buffer_token_started = response.Data.queue_info.is_buffer_token_started
						// this.token_status = response.Data.queue_info.token_status
						this.token_date = this.queselecteddate
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {

						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						});

					}
				})
		this.providerlist = false
		this.assignnewtokenform = true
		this.assignnewtoken.getcustomlist(event).subscribe((response) => {
			if (response.status_code == "200") {
				this.customfield = response.Data
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.customfield = null
			}
		});
	}

	onAssignDateChanged(event) {
		if (event.formatted != '') {
			this.queselecteddate = event.formatted
			this.myFormattedDate = event.formatted

			this.assignnewtoken.getProviderQueues(event.formatted, this.selectedproid)
				.subscribe(
					(response) => {


						if (response.status_code == "200") {
							// this.quelistdata=response.Data
							this.queue_reqque = ""
							this.quelistdata = ""
							this.token_no = null
							this.token_time = null
							this.token_apitime = null
							this.token_date = null
							this.queue_id = ""
							this.customfield = null
							this.quelistdata = response.Data

							//this.userlogin.resetForm();


						} else if (response.status_code == '402') {
							localStorage.clear();
							this.router.navigate(['/login']);
						} else {

							this.quelistdata = []
							this.token_time = ""
							this.token_no = ""
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							});
						}
					})
		} else {
			this.snackbar.open("Please select Date", '', {
				duration: 1000,
			});
		}
	}

	onqueclick() {
		const config = new MatDialogConfig();
		this.assignnewtoken.getProviderQueues(this.queselecteddate, this.selectedproid).subscribe((response) => {
			if (response.status_code == "200") {
				this.quelistdata = response.Data;
				config.data = {
					quelistdata: this.quelistdata,
					tokendate: this.queselecteddate,
					queue_id: this.queue_id,
					token_noexist: this.token_no,
					selectedproid: this.selectedproid
				};
				this.dialogRef = this.dialog.open(DialogmonitorqueComponent, config);
				this.dialogRef.afterClosed().subscribe((value:any) => {
					if(value.token_no != 0){
						if (value.token_no == 0) {
							this.token_no = '0';
							this.snackbar.open("This Queue is Full for the day. Select another queue.", '', {
								duration: 1000,
							});
						} else {
							this.token_no = value.token_no;
							this.queue_id = value.queue_id;
						}
						this.myFormattedDate = value.selecteddate;
						this.queue_reqque = value.selectedqueue;
						this.queselecteddate = value.selecteddate;
						this.token_time = value.token_conveted_time;
						this.token_timeres = value.token_conveted_time;
						this.token_apitime = value.token_time;
						this.is_buffer_token_started = value.is_buffer_token_started;
						this.token_status = value.token_status;
						this.reshaduledate = value.selecteddate
						this.onqueselect(this.queue_id,this.token_no)
					}
				});
			}
		})
	}

	assignnow(mobilenumber) {
		this.searchtoken.getEnumValues().subscribe((response) => {
			if (response.status_code == "200") {
				this.getenumvalues = response.Data
				this.getenumdefaultvalues = response.Data[0]
			} else {
				this.getenumdefaultvalues = ""
			}
		})
		this.bookingusername = mobilenumber;
		this.tokenrequestorno = mobilenumber;
		this.userprofileimg = "";
		if(this.bookingtypeselectedtype == undefined && this.bookingtypeselectedtype != null){
			this.bookingtypeselectedtype = "non_member";
		}
		if (this.bookingtypeselectedtype == "self") {
			this.bookedfor = "self";
			this.bookedforself = true;
			this.assignnewtoken.getQTRAQUserDetails(mobilenumber, this.selectedproid).subscribe((response) => {
				if (response.status_code == '200') {
					this.iseditable = "No"
					this.searchfield = true
					this.assignsearchdiv = true
					this.bookedother = false;
					this.hiddenall = false
					this.providerlist = false
					this.assignsearchdiv = true
					this.regno = response.Data[0]['reg_no']
					this.usermasterid = response.Data[0]['user_master_id']
					this.searchmobileage = (response.Data[0]['age'])?response.Data[0]['age']:0;
					this.searchfirst_name = response.Data[0]['first_name'] + " " + response.Data[0]['last_name']
					this.usergendermale = response.Data[0]['gender']
					this.userprofileimg = response.Data[0]['profile_pic_path']
					this.datedata(this.myFormattedDate)
					this.onqueselect(this.queue_id,this.token_no)
				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.searchmobileage = 0
					this.searchfirst_name = ""
					this.usergendermale = false
					this.usergenderfemale = false
					this.usergenderOther = false
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					});
				}
			});
		} else if ((this.bookingtypeselectedtype == "family_member_with_qtraqid") || (this.bookingtypeselectedtype == "family_member_without_qtraqid")) {
			this.assignnewtoken.getQTRAQfamilymemberuserdetail(this.mobilesearchmasterid, this.familymemeberidfordetail).subscribe((response) => {
				if (response.status_code == '200') {
					if(response.Data != null){
						this.bookedfor = response.Data[0]['member_type']
						this.regno = response.Data[0]['reg_no']
						if (response.Data[0]['member_type'] == "family_member_without_qtraqid") {
							this.usermasterid = response.Data[0]['family_member_id']
						} else {
							this.usermasterid = response.Data[0]['family_member_user_id']
						}
						this.searchmobileage = (response.Data[0]['member_age'])?response.Data[0]['member_age']:0;
						this.searchfirst_name = response.Data[0]['member_first_name'] + " " + response.Data[0]['member_last_name']
						this.usergendermale = response.Data[0]['member_gender']
						this.userprofileimg = response.Data[0]['member_pic_path']
						this.searchbookinguserid = response.Data[0]['family_member_id']
						this.iseditable = "No"
					}

					this.bookedother = false;
					this.hiddenall = false
					this.assignsearchdiv = true
					this.providerlist = false
					this.assignsearchdiv = true
					this.searchfield = true
					this.datedata(this.myFormattedDate)
					if (this.token_prv_page != "0") {
						this.onqueselectprv(this.queue_id)
					} else {
						this.onqueselect(this.queue_id,this.token_no);
					}
				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.bookingusername = ""
					this.searchmobileage = 0
					this.searchfirst_name = ""
					this.usergendermale = false
					this.usergenderfemale = false
					this.usergenderOther = false
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					});
				}
			});
		} else if (this.bookingtypeselectedtype == "non_member") {
			if ((this.fullnamecheck == "0") || (this.fullnamecheck == "")) {
				this.iseditable = "Yes";
				this.bookedother = true;
				this.bookedfor = "non_member";
				this.bookedforself = true;
				if (this.serby == 'name') {
					this.searchfirst_name = this.textvalue;
					this.bookingusername = "";
				} else if (this.serby == 'both') {
					this.searchfirst_name = this.textvalue;
					this.bookingusername = this.mobilenum;
				} else {
					this.searchfirst_name = ""
					this.bookingusername = this.textvalue
				}
				this.assignsearchdiv = true;
				this.assignsearchdiv = true;
				this.searchfield = true;
				this.hiddenall = false;
			} else {
				this.bookedfor = this.bookingtypeselectedtype;
				this.regno = this.regnonew;
				this.bookedother = false;
				this.selfname = this.fullnamecheck;
				this.usermasterid = this.selectedproid;
				this.hiddenall = false;
				this.searchmobileage = (this.agenew)?this.agenew:0;
				this.searchfirst_name = this.fullnamecheck;
				this.usergendermale = this.gender;
				this.userprofileimg = this.profileimg;
				this.searchbookinguserid = this.familymemeberidfordetail;
				this.assignsearchdiv = true;
				this.providerlist = false;
				this.assignsearchdiv = true;
				this.searchfield = true;
			}
			this.datedata(this.myFormattedDate);
			this.onqueselect(this.queue_id,this.token_no);
			this.bookingfordropdownvis = true;
		} else if (this.bookingtypeselectedtype == "other") {
			this.iseditable = "No";
			this.bookedother = true;
			this.bookedfor = "self";
			this.bookedforself = true;
			this.assignsearchdiv = true;
			this.assignsearchdiv = true;
			this.searchfield = true;
			this.hiddenall = false;
			this.datedata(this.myFormattedDate);
			this.onqueselect(this.queue_id,this.token_no);
		}else{
			this.bookedfor = this.bookingtypeselectedtype;
			this.regno = this.regnonew;
			this.iseditable = "Yes";
			this.bookedother = false;
			this.selfname = this.fullnamecheck;
			this.usermasterid = this.selectedproid;
			this.hiddenall = false;
			this.searchmobileage = (this.agenew)?this.agenew:0;
			this.searchfirst_name = this.fullnamecheck;
			this.usergendermale = this.gender;
			this.userprofileimg = this.profileimg;
			this.searchbookinguserid = this.familymemeberidfordetail;
			this.assignsearchdiv = true;
			this.providerlist = false;
			this.assignsearchdiv = true;
			this.searchfield = true;
			this.datedata(this.myFormattedDate);
			this.onqueselect(this.queue_id,this.token_no);
		}
		if(this.memberid != "" && this.memberid != undefined){
			this.iseditable = "No";
		}else{
			this.iseditable = "Yes";
		}

	}

	datedata(event) {
		this.assignnewtoken.getProviderQueues(event, this.selectedproid).subscribe((response) => {
			if (response.status_code == "200") {
				this.quelistdata = response.Data

			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.quelistdata = [];
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				});
			}
		})
	}


	cancelsubmitdata(mobilenumber) {
		this.assignnewtoken.getQTRAQUserDetails(mobilenumber, this.selectedproid).subscribe((response) => {
			if (response.status_code == '200') {
				this.assignbuttonshow = true;
				this.tokenrequestorno = mobilenumber;
				this.selfname = response.Data[0].first_name + " " + response.Data[0].middle_name + " " + response.Data[0].last_name;
				this.selfnumber = response.Data[0].user_name;
				this.selfimage = response.Data[0].profile_pic_path;
				this.bookingfordropdownvis = true;
			} else if (response.status_code == '400') {
				this.tokenrequestorno = mobilenumber;
				this.bookingtypeselectedtype = "other";
			}
		})
	}

	onbookingchange(event) {
		if (event == "Family") {
			this.assignnewtoken.getQTRAQfamilymember(this.mobilesearchmasterid).subscribe((response) => {
				this.bookingtypeselectedtype = "Family"
				if (response.status_code == '200') {
					this.familymember = response.Data

					this.familymemberselfshow = false

					this.familymembershow = true

				} else if (response.status_code == '402') {
					localStorage.clear();
					this.router.navigate(['/login']);
				} else {
					this.snackbar.open(response.Metadata.Message, '', {
						duration: 2000,
					});
				}
			})
		} else if (event == "Self") {
			this.familymembershow = false;
			this.familymemberselfshow = true;
			this.bookingtypeselectedtype = "Self";
		}
	}

	familymemberchackboxcheck(masterid) {
		this.familymemeberidfordetail = masterid;
	}

	onbookingchangedata(event) {
		this.bookingtype = event
	}

	public uploadprescription(formValue: any, token_id) {
		let main_form: FormData = new FormData();
		for (let j = 0; j < this.items.length; j++) {
			main_form.append("image_title[" + j + "]", formValue.items[j]['preascription_title'])
			if ( < File > this.totalfiles[j]) {
				main_form.append("precription_report_image[" + j + "]", < File > this.totalfiles[j])
			}
		}
		this.patientservice.getuploadprescription(main_form, token_id).subscribe((response) => {
			if (response.status_code == 200) {
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				});
			} else if (response.status_code == 402) {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				});
			}
		});
	}

	backtodetail() {
		this.patienthistoryaddmore = false;
		this.hiddenall = true;
		this.dignosispriscription = false;
	}

	backtoprescription() {
		this.patienthistoryaddmore = false;
		this.dignosispriscription = true;
		this.diegnosisprescription(this.token_id);
	}

	backtoassign() {
		this.router.navigate(['/assignnewtoken/']);
	}

	backtoassignpage() {
		let roletype = JSON.parse(localStorage.getItem('activerole'));
		if (this.screenname == "Weekly Dashboard") {
			this.router.navigate(['/weeklydashboard/']);
		} else {
			this.assignsearchdiv = true;
			this.transactionqueuelist = false;
		}
	}


	getongoingqueuetokenfrontdesk() {
		//this.queue_transaction_id=queue_transaction_id
		this.ongoingqueues.getproviderongoingtransactionqueue()
			.subscribe(
				(response) => {
					if (response.status_code == '200') {
						//queue_transaction_id
						this.queue_transaction_id = response.Data.queue_details[0].queue_transaction_id

						this.latertoday = false

						this.ongoingqueuelistdatashow = false
						this.ongoingtab = true
						this.queue_details = response.Data.queue_details
						this.is_doctor_arrived = response.Data.queue_details[0].is_doctor_arrived

						this.queue_id = response.Data.queue_details[0].queue_id


						this.ongoingestimated_end_time = response.Data.queue_details[0]['ongoingestimated_end_tim']

						this.ongoingqueue_date = response.Data.queue_details[0]['queue_date']
						this.ongoingprovider_name = response.Data.queue_details[0]['provider_name']
						this.ongoingqueue_pic_url = response.Data.queue_details[0]['profileimage_path']
						this.ongoingqueue_name = response.Data.queue_details[0]['queue_name']

						this.queue_reqque = response.Data.queue_details[0].queue_id

						this.onqueselect(response.Data.queue_details[0].queue_id)

						this.avg_consulting_time = response.Data.avg_consulting_time
						this.avg_waiting_time = response.Data.avg_waiting_time
						this.checkup_total_amount = response.Data.checkup_total_amount


						//new code start
						this.completedTokensCount = response.Data.completedTokensCount
						this.cancelledTokensCount = response.Data.cancelledTokensCount
						this.notArrivedCount = response.Data.notArrivedCount
						this.arrivedCount = response.Data.arrivedCount
						this.allTokensCount = response.Data.allTokensCount

						//new code end


						this.onoganisationclick(4)
					} else if (response.status_code == '402') {
						localStorage.clear();
						this.router.navigate(['/login']);
					} else {


						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})


					}
				});
	}


	backbuttontabbedforfuturepast() {
		this.router.navigate(['/futurepastqueues/'], {
			queryParams: {
				"futurepasttokenback": "futurepasttokenback",
				"futurepastmasterid": this.futurepastmasterid,
				"futurepastfromdate": this.futurepastfromdate,
				"futurepasttodate": this.futurepasttodate,
				"futurepastlastname": this.futurepastlastname,
				"futurepastfirstname": this.futurepastfirstname,
				"futurepastmiddle_name": this.futurepastmiddle_name,
				"futurepastprofileimage_path": this.futurepastprofileimage_path,
				"futurepastorg_location": this.futurepastorg_location,
			},
			skipLocationChange: true
		})
	}

	backtohome() {
		this.router.navigate(['/'])
	}

	startTransactionQueue(tranactionid:any) {
		this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you want to Start this queue?').then((confirmed) => {
			if (confirmed) {
				this.ongoingqueues.startTransactionQueue(tranactionid).subscribe((response)=>{
					if (response.status_code == '200') {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
						var excelcurrentdate = new Date();
						let excelfromdate = excelcurrentdate.getFullYear()+"-"+(excelcurrentdate.getMonth()+1)+"-"+excelcurrentdate.getDate();
						excelcurrentdate.setDate(excelcurrentdate.getDate()+5);
						let exceltodate = excelcurrentdate.getFullYear()+"-"+(excelcurrentdate.getMonth()+1)+"-"+excelcurrentdate.getDate();
						this.ongoingqueues.exporttoexcelforrange(this.selectedproid,excelfromdate,exceltodate).subscribe((response)=>{
							if(response.status_code == 200){
								window.location.assign(response.Data.file_url);
								this.ongoingqueues.deletedownloadedexcelfile(response.Data.file_name).subscribe((deletRes)=>{});
							}else{
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})
							}
							setTimeout(() => {
								window.history.back();
							}, 3000);
						})
					} else {
						this.snackbar.open(response.Metadata.Message, '', {
							duration: 2000,
						})
					}
				})
			}
		})
	}


	//stopTransactionQueue()

	stopTransactionQueue(tranactionid) {
		this.confirmationDialogService.confirm("<span style='color:red !important'>STOP QUEUE</span>", "Are you sure you want to Stop this queue?<br><br>This will be a HARD STOP and you won't be able book tokens any further.","STOP QUEUE","Not Now").then((confirmed) => {
				if (confirmed) {
					this.ongoingqueues.stopTransactionQueue(tranactionid).subscribe((response) => {
						if (response.status_code == '200') {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
							var excelcurrentdate = new Date();
							var excelfromdate = excelcurrentdate.getFullYear()+"-"+excelcurrentdate.getMonth()+"-"+excelcurrentdate.getDate();
							excelcurrentdate.setDate(excelcurrentdate.getDate()+5);
							var exceltodate = excelcurrentdate.getFullYear()+"-"+excelcurrentdate.getMonth()+"-"+excelcurrentdate.getDate();
							this.ongoingqueues.exporttoexcelforrange(this.selectedproid,excelfromdate,exceltodate).subscribe((response)=>{
								if(response.status_code == 200){
									window.location.assign(response.Data.file_url);
									this.ongoingqueues.deletedownloadedexcelfile(response.Data.file_name).subscribe((deletRes)=>{});
								}else{
									this.snackbar.open(response.Metadata.Message, '', {
										duration: 2000,
									})
								}
								setTimeout(() => {
									window.history.back();
								}, 3000);
							})
						} else {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
						}
					})
				}
			})

	}

	searchbacktodoctor() {
		this.router.navigate(['/futurepastqueues'])

	}

	backtoongoing() {


		this.ongoingtab = false

		this.searchtokendatadetailshowdata = false
		this.transactionqueuelist = false
		this.ongoingqueuelistdatashow = true
		this.hiddenall = true

		this.cancelledpage = 0
		this.completedpage = 0
		this.notarrivedpage = 0
		this.arrivedpage = 0
		this.allpage = 0


		this.getongoingqueue(this.profileimage_path, this.first_name, this.user_master_id)
		this.router.navigate(['/ongoingqueues'])
	}


	backtoongoingassign() {
		this.ongoingtab = false
		this.assignsearchdiv = false
		this.ongoingqueuelistdatashow = true

		this.hiddenall = true
		// this.getongoingqueue(this.profileimage_path,this.first_name,this.user_master_id)
		this.router.navigate(['/ongoingqueues'])

		this.cancelledpage = 0
		this.completedpage = 0
		this.notarrivedpage = 0
		this.arrivedpage = 0
		this.allpage = 0
	}


	backtoongoingbroadcast() {
		this.broadcast = false
		this.hiddenall = true
		this.ongoingqueuelistdatashow = true
		this.getongoingqueue(this.profileimage_path, this.first_name, this.user_master_id)
		this.router.navigate(['/ongoingqueues'])
	}


	backtoongoingtoday() {
		this.ongoingtab = false
		this.getongoingqueue(this.profileimage_path, this.first_name, this.user_master_id)

		this.ongoingqueuelistdatashow = true
		this.searchtokendatadetailshowdata = false
		this.broadcasttoday = false
		this.transactionqueuelist = false
		this.hiddenall = true
		this.latertoday = true

		this.cancelledpage = 0
		this.completedpage = 0
		this.notarrivedpage = 0
		this.arrivedpage = 0
		this.allpage = 0
		this.router.navigate(['/todaysqueue'])
	}


	backtoongoingassigntoday() {
		this.ongoingtab = false
		this.broadcasttoday = false
		this.assignsearchdiv = false
		this.ongoingqueuelistdatashow = true
		this.queuelist = false
		this.transactionqueuelist = false
		this.latertoday = true
		this.hiddenall = true
		this.getongoingqueue(this.profileimage_path, this.first_name, this.user_master_id)
		this.router.navigate(['/todaysqueue'])
	}


	backtoongoingbroadcasttoday() {
		this.broadcast = false
		this.hiddenall = true
		this.broadcasttoday = false
		this.ongoingqueuelistdatashow = true
		this.latertoday = true
		this.getongoingqueue(this.profileimage_path, this.first_name, this.user_master_id)
		this.router.navigate(['/todaysqueue'])
	}

	todayqueuebroadcastback() {
		this.broadcasttoday = false
		this.queuelist = true
		this.todayongoingmsg = false
	}


	blockUnblockTransactionQueue(transaction_id, blockqueue,queue_name:any = "") {

		if (blockqueue == "Yes") {
			this.confirmationDialogService.confirm('BLOCK BOOKINGS', "Are you sure you want to block bookings for "+queue_name+" ?<br><br>You won't be able to book tokens any further but you can unblock the bookings if required.","Block Booking","Cancel").then((confirmed) => {
				if (confirmed) {
						this.ongoingqueues.blockUnblockTransactionQueue(transaction_id, blockqueue).subscribe((response) => {
							if (response.status_code == '200') {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})
								this.gettransactionqueuetoken(transaction_id)
								//window.history.back();
							} else {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})
							}
						})
					}
				})
		} else {
			this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you want to unblock these bookings?')
				.then((confirmed) => {
					if (confirmed) {
						this.ongoingqueues.blockUnblockTransactionQueue(transaction_id, blockqueue).subscribe((response) => {
							if (response.status_code == '200') {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})
								this.gettransactionqueuetoken(transaction_id)
								//window.history.back();
							} else {
								this.snackbar.open(response.Metadata.Message, '', {
									duration: 2000,
								})
							}
						})
					}
				})
		}
	}

	blockUnblockTransactionQueueongoing(transaction_id, blockqueue,queue_name:any = "") {
		if (blockqueue == "Yes") {
			this.confirmationDialogService.confirm('BLOCK BOOKINGS', "Are you sure you want to block bookings for "+queue_name+" ?<br><br>You won't be able to book tokens any further but you can unblock the bookings if required.","Block Booking","Cancel").then((confirmed) => {
				if (confirmed) {
					this.ongoingqueues.blockUnblockTransactionQueue(transaction_id, blockqueue).subscribe((response) => {
						if (response.status_code == '200') {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
							//window.history.back();
							this.getongoingqueuetoken(transaction_id)
						} else {
							this.snackbar.open(response.Metadata.Message, '', {
								duration: 2000,
							})
						}

					})
				}
			})
		} else {
			this.confirmationDialogService.confirm('Confirmation Alert', 'Are you sure you want to unblock these bookings?').then((confirmed) => {
				if (confirmed) {
					this.ongoingqueues.blockUnblockTransactionQueue(transaction_id, blockqueue).subscribe((response) => {
						if (response.status_code == '200') {
							this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
							this.getongoingqueuetoken(transaction_id)
						} else {
							this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
						}
					});
				}
			})
		}
	}

	backtorequestreceived() {
		this.router.navigate(['/requestsreceived/'], {
			queryParams: {
				"todaysnewrequestsreceived": "todaysnewrequestsreceived",
				"queue_id": this.queue_id,
				"queselecteddate": this.queselecteddate,
				"requesttype": this.requesttype,
				"selectedproid": this.selectedproid,
				"tokenidfortransactionback": this.tokenidfortransactionback
			},
			skipLocationChange: true
		})
	}

	backtorequestreceivedfirstscreen() {
		this.router.navigate(['/requestsreceived/'], {
			skipLocationChange: true
		})
	}

	backtoassigntransactionqueue() {
		this.transactionqueuelist = false
		if (this.bookingtypeselectedtype != 'non_member') {
			this.cancelsubmitdata(this.tokenrequestorno)
		}
		this.assignnow(this.tokenrequestorno)
	}

	predefined(event) {
		document.getElementById("reasonforchanging").innerHTML = event;
	}

	tranactionqueuetokendetail(data) {
		if (data == 'today') {
			this.gettransactionqueuetoken(this.queue_transaction_id)
			this.tokendetail(this.token_idres)
			let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
			let user_master_id = atob(currentUser.user_master_id);
			this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
			this.callfromreshadule = "yes"
			this.callfromprovider = "no"
			this.screenname = "Todays Queues"
			this.screenname1 = "Todays Queues"
		} else if (data == 'ongoing') {
			this.getongoingqueuetoken(this.queue_transaction_id)
			this.tokendetail(this.token_idres)
			let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
			let user_master_id = atob(currentUser.user_master_id);
			this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
			this.callfromreshadule = "yes"
			this.callfromprovider = "no"
			this.screenname = "Ongoing Queues"
			this.screenname1 = "Ongoing Queues"
		} else if (data == 'futurepast') {
			if (this.checktoday == 1) {
				this.getongoingqueuetoken(this.queue_transaction_id)
				this.tokendetail(this.token_idres)
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.callfromreshadule = "yes"
				this.callfromprovider = "no"
				this.screenname = "Future Past Token"
				this.screenname1 = "Future Past Token"
			} else {
				this.gettransactionqueuetoken(this.queue_transaction_id)
				this.tokendetail(this.token_idres)
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.callfromreshadule = "yes"
				this.callfromprovider = "no"
				this.screenname = "Future Past Token"
				this.screenname1 = "Future Past Token"
			}
		}
	}

	transactionqueproviderdetail(data) {
		if (data == 'ongoing') {
			let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
			let user_master_id = atob(currentUser.user_master_id);
			this.getongoingqueuetoken(this.queue_transaction_id)
			this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
			this.screenname = "Ongoing Queues"
			this.screenname1 = "Ongoing Queues"
			this.callfromreshadule = "no"
			this.callfromprovider = "yes"
		} else if (data == 'today') {
			if (this.checktoday == 1) {
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.getongoingqueuetoken(this.queue_transaction_id)
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.screenname = "Todays Queues"
				this.screenname1 = "Todays Queues"
				this.callfromreshadule = "no"
				this.callfromprovider = "yes"
			} else {
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.gettransactionqueuetoken(this.queue_transaction_id)
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.screenname = "Todays Queues"
				this.screenname1 = "Todays Queues"
				this.callfromreshadule = "no"
				this.callfromprovider = "yes"
			}
		} else if (data == 'futurepast') {
			if (this.checktoday == 1) {
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.getongoingqueuetoken(this.queue_transaction_id)
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.screenname = "Future Past Token"
				this.screenname1 = "Future Past Token"
				this.callfromreshadule = "no"
				this.callfromprovider = "yes"
			} else {
				let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
				let user_master_id = atob(currentUser.user_master_id);
				this.gettransactionqueuetoken(this.queue_transaction_id)
				this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
				this.screenname = "Future Past Token"
				this.screenname1 = "Future Past Token"
				this.callfromreshadule = "no"
				this.callfromprovider = "yes"
			}
		}
	}

	homepage(screen) {
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let user_master_id = atob(currentUser.user_master_id);
		if (screen == 'ongoing') {
			this.screenname = "Ongoing Queues"
			this.screenname1 = "Ongoing Queues"
			this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
			this.ongoingqueuelistdatashow = true
			this.searchtokendatadetailshowdata = false
			this.broadcasttoday = false
			this.transactionqueuelist = false
			this.hiddenall = true
			this.latertoday = false
			this.router.navigate(['/ongoingqueues'])
		} else {
			this.screenname = "Today's Queues"
			this.screenname1 = "Todays Queues"
			this.getongoingqueue(this.profileimage_path, this.first_name, user_master_id)
			this.ongoingqueuelistdatashow = true
			this.searchtokendatadetailshowdata = false
			this.broadcasttoday = false
			this.transactionqueuelist = false
			this.hiddenall = true
			this.latertoday = true
			this.router.navigate(['/todaysqueue'])
		}
	}

	refreshscreen() {
		this.getongoingqueuetoken(this.queue_transaction_id)
		this.snackbar.open("Data Refreshed", '', {duration: 2000})
	}

	sortbyarrival(event, tokentypedata) {
		var sort = '';
		if (event.target.checked == true) {
			sort = 'Yes';
		} else {
			sort = 'No';
		}
		this.ongoingqueues.getongoingtransactiontypesortbyarrival(sort, tokentypedata, this.queue_transaction_id, this.page - 1).subscribe((response) => {
			if (response.status_code == '200') {
				if (tokentypedata == "cancelled") {
					this.noshow = response.Data.queue_tokens
				} else if (tokentypedata == "notarrived") {
					this.ongoing = response.Data.queue_tokens
				} else if (tokentypedata == "arrived") {
					this.selectedindex = 3
					this.pending = response.Data.queue_tokens
				} else if (tokentypedata == "completed") {
					this.completedorcancelled = response.Data.queue_tokens
				} else if (tokentypedata == "all") {
					this.alldata = response.Data.queue_tokens
				}
			} else if (response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			} else {
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}
		});
	}

	editdetail() {
		this.editdetailmodelToggle("open")
	}

	editdetailmodelToggle(action: string) {
		if (action == "open") {
			this.edittokendetail = true;
		} else {
			this.edittokendetail = false;
		}
	}

	editdetailclose() {
		this.editdetailmodelToggle('close')
	}

	genderChanged(event) {

		this.editdetailgender = event
		//editdetailgender

	}

	tokendetailsubmit(bookingfor, token_id, user_age, user_gender, queue_transaction_id, phoneno,booking_for_user_id, booking,tokenrequestorname){
		var gendervalue;
		if(this.editdetailgender != "") {
			gendervalue = this.editdetailgender
		}else{
			gendervalue = user_gender
		}
		const formData = new FormData();
		formData.append('token_id', token_id);
		formData.append('queue_transaction_id', queue_transaction_id);
		formData.append('booking_for_name', bookingfor);
		formData.append('user_gender', gendervalue);
		formData.append('user_age', user_age);
		formData.append('token_requestor_no', phoneno);
		formData.append('booking_for_user_id', booking_for_user_id);
		formData.append('tokenrequestorname', tokenrequestorname);
		formData.append('booking_for', booking);
		this.ongoingqueues.editTokenInfo(formData).subscribe((response) => {
			if (response.status_code == '200') {
				this.tokendetail(token_id)
				this.editdetailmodelToggle('false')
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}else if(response.status_code == '402') {
				localStorage.clear();
				this.router.navigate(['/login']);
			}else{
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}
		})
	}

	searchtextchange(eventvalue) {
		if (eventvalue == "nextforconsultation") {
			this.aheadof = true
			this.nextforconsultation = this.getTransactionTokenList[0].token_sequence
			this.changeseqtokenid = this.getTransactionTokenList[0].token_sequence
			this.checkboxselectcheck = "nextforconsultation";
			this.selectShowHide = false;
		} else if (eventvalue == "afterlastarrived") {
			for (let i = 0; i < this.getTransactionTokenList.length; i++) {
				if (this.getTransactionTokenList[i]['is_max_arrival'] == true) {
					this.nextforconsultation = this.getTransactionTokenList[i].token_sequence
				}
			}
			this.changeseqtokenid = this.getTransactionTokenList[0].token_sequence
			this.checkboxselectcheck = "afterlastarrived"
			this.aheadof = true
			this.selectShowHide = false;
		} else if (eventvalue == "last") {
			this.aheadof = false
			this.nextforconsultation = ""
			this.checkboxselectcheck = "last"
			this.selectShowHide = true;
		} else {
			this.aheadof = false
			this.nextforconsultation = ""
			this.checkboxselectcheck = ""
			this.selectShowHide = true;
		}
	}

	Transactionqueuesummary(queue_transaction_id:any,queue_name:any,queue_date:any) {
		const config = new MatDialogConfig();
		config.data = {
			callfrom:"singlequeue",
			queue_transaction_id: queue_transaction_id,
			queuename: queue_name,
			queuedate: queue_date
		}

		config.width = "450px";
		this.dialogRef = this.dialog.open(TransactionqueuesummaryComponent, config);
		this.dialogRef.afterClosed().subscribe(value => {})
	}

	onholdprocess(transaction_id) {
		const config = new MatDialogConfig();
		config.data = {
			transactionid: transaction_id,
		};
		this.dialogRef = this.dialog.open(OnholdprocessdialogComponent, config);
		this.dialogRef.afterClosed().subscribe((value:any) => {
			this.getongoingqueuetoken(transaction_id)
		})
	}

	searchToken(user_data:any){
		this.ongoingqueues.setData(user_data);
   		this.router.navigateByUrl('/searchtokens');//as per router
	}

	getPrescriptionInvoicePrintDataHtml(tokenid:any,printtype:any){
		this.patientservice.getPrescriptionInvoicePrintDataHtml(tokenid,printtype).subscribe((response)=>{
			if(response.status_code == "400"){
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}else{
				var printWin = window.open("","Print","_self");
				printWin.moveTo(0,0);
				printWin.resizeTo(screen.availWidth, screen.availHeight);
				printWin.document.open();
				printWin.document.write(response.Data);
				printWin.print();
				printWin.close();
			}
		})
	}

	getFollowUpTokens(provider_id:any,queue_date:any){
		const dialogRef = this.dialog.open(FollowupdialogComponent,{
			height:'500px',
			width:'550px',
			disableClose: true,
			data:{
				providerid:provider_id,
				followupdate:queue_date
			}
		});
		dialogRef.afterClosed().subscribe((response:any) => {
			if(response.action == "view"){
				this.tokendetail(response.token_id);
			}else if(response.action == "add"){
				this.fullnamecheck = response.fullname
				var mobilenumber = response.mobilenumber
				this.familymemeberidfordetail = response.familymemberid;
				this.mobilesearchmasterid = response.mobilesearchmasterid
				this.profileimg = response.profileimg
				this.agenew = response.age
				this.gender = response.gender
				this.last_visited = response.last_visited
				if (response.bookingtypeselectedtype == undefined) {
					this.bookingtypeselectedtype = "non_member"
				} else {
					this.bookingtypeselectedtype = response.bookingtypeselectedtype
				}
				this.newvisit = false;
				this.followup = true;
				if (this.bookingtypeselectedtype != 'non_member') {
					this.cancelsubmitdata(mobilenumber)
				}
				this.mobilenum = response.mobilenumber
				this.assignnow(mobilenumber)
			}else{
				return false;
			}
		});
	}

	downloadQueueDetails(queuetransactionid:any){
		this.ongoingqueues.exporttoexcel(queuetransactionid).subscribe((response)=>{
			if(response.status_code == 200){
				window.location.assign(response.Data.file_url);
				this.ongoingqueues.deletedownloadedexcelfile(response.Data.file_name).subscribe((deletRes)=>{});
				this.snackbar.open(response.Metadata.Message, '', {duration: 2000})
			}else{
				this.snackbar.open(response.Metadata.Message, '', {
					duration: 2000,
				})
			}
		})
	}

	overRideQueue(queuetransactionid:any){
		this.router.navigate(['/overridemasterqueues/'], {
			queryParams: {
				"queuetransactionid":queuetransactionid
			},
			skipLocationChange: true
		});
	}

	onTimeChange(changedTime:any){
		this.token_apitime = changedTime;
	}
}