import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TodaysqueueComponent } from './todaysqueue.component'
import {MatTabsModule} from '@angular/material/tabs';
import {TodaysqueueRoutingModule  } from "./todaysqueue-routing.module";
//import { HomeComponent } from './home/home.component';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DatanewComponent } from './datanew/datanew.component';
import { NgProgressModule } from 'ngx-progressbar';
//import{ TimeFormatPipe } from './pipes/time.pipe'
import{ TimetodayFormatPipe } from '../../pipes/timetoday.pipe'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from '@pluritech/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NumberDirectivequeue } from './../../directive/number-only.directivequeue';
import { UiSwitchModule } from 'ngx-ui-switch';
import {MatRadioModule} from '@angular/material/radio';
//import { SidebarComponent } from '../sidebar/sidebar.component'
@NgModule({
  imports: [CommonModule, MatRadioModule,
    TodaysqueueRoutingModule,
     MatTabsModule,PaginationModule, 
     NgxPaginationModule,NgProgressModule,
     NgxMyDatePickerModule.forRoot(),
     NgMultiSelectDropDownModule.forRoot(),
     FormsModule,ReactiveFormsModule,UiSwitchModule
     ],
  declarations: [TodaysqueueComponent,NumberDirectivequeue,TimetodayFormatPipe]
})
export class TodaysqueueModule {}