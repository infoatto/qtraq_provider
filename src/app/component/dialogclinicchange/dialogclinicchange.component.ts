import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CommonService } from './../../_services';
import { ActivatedRoute, Router } from '@angular/router';
import{ WeeklydashboardComponent } from './../../component/weeklydashboard/weeklydashboard.component'




@Component({
selector: 'app-dialogclinicchange',
templateUrl: './dialogclinicchange.component.html',
styleUrls: ['./dialogclinicchange.component.scss']
})
export class DialogclinicchangeComponent implements OnInit {
organisationlist: any
constructor(
private router:Router,
private orgservice: CommonService,
public dialogRef: MatDialogRef<WeeklydashboardComponent>,
@Inject(MAT_DIALOG_DATA) public data: any,
//public dialogRef: MatDialogRef<DialogclinicchangeComponent>,
) {}

  ngOnInit() 
  {
  this.orgservice.getorganisationlist()
  .subscribe(
  data => {
  
  if(data.Data)
  {
  this.organisationlist=data.Data
  }}
  )
  }




onNoClick(orgid)  
{
/*  


*/
/*
var actas='admin'
localStorage.setItem('activerole',JSON.stringify(actas));
this.dialogRef.close();

this.router.navigate(['/']); 
*/  
let data={'orgid':orgid}

this.dialogRef.close(data);     
      
}


}
