import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogclinicchangeComponent } from './dialogclinicchange.component';

describe('DialogclinicchangeComponent', () => {
  let component: DialogclinicchangeComponent;
  let fixture: ComponentFixture<DialogclinicchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogclinicchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogclinicchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
