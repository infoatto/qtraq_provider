import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from '../../_services/notification.service';
import { Router, ActivatedRoute,Params  } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import{ LoginComponent } from '../../pages/login/login.component'
import {MatSnackBar} from '@angular/material';
@Component({
  selector: 'dialognewtoqrm',
  templateUrl: './dialognewtoqrm.component.html',
  styleUrls: ['./dialognewtoqrm.component.scss']
})
export class DialognewtoqrmComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackbar:MatSnackBar,private router: Router
  ) {
  }

  ngOnInit(){
  }

  close(){
    this.dialogRef.close()
  }
}
