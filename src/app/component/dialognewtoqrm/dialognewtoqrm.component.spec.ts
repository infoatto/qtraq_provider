import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialognewtoqrmComponent } from './dialognewtoqrm.component';

describe('DialognewtoqrmComponent', () => {
  let component: DialognewtoqrmComponent;
  let fixture: ComponentFixture<DialognewtoqrmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialognewtoqrmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialognewtoqrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
