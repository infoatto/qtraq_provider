// import { UtilsService } from './providers/utils/utils.service';
// Angular Modules
// import { Component } from '@angular/core';
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
// Other Modules
//import { AssignnewtokenComponent } from './component/assignnewtoken/assignnewtoken.component'
import { AuthServiceModule } from '@pluritech/auth-service';
import { ServerServiceModule } from '@pluritech/server-service';
import { PaginationModule } from '@pluritech/pagination';
import { Ng2TableModule } from '@pluritech/ng2-responsive-table';
import { DialogServiceModule } from '@pluritech/dialog-service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { FileUploadModule } from 'ng2-file-upload/file-upload/file-upload.module';
import { ToasterModule } from 'angular2-toaster';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';

// Component
import { AppComponent } from './app.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ContentLoaderComponent } from './components/content-loader/content-loader.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
// Parts
import { DashComponent } from './parts/dash/dash.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService } from './_services/authentication.service';
import { LoginService } from './providers/login/login.service';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { configuration } from './configuration';
import { NgDatepickerModule } from 'ng2-datepicker';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { JwtInterceptor } from './_helpers';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import {NgxPaginationModule} from 'ngx-pagination';
import { AddeditorganisationComponent } from './component/addeditorganisation/addeditorganisation.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {OrglistComponent} from './component/orglist/orglist.component'
import {AuthGuard} from'./auth.guard'
import {NotificationService} from './_services/notification.service'
import {ProvidersService} from './_services/providers.service'
import {CommonService} from './_services/common.service'
import {OrganisationsService} from './_services/organisations.service';
import {MatTabsModule} from '@angular/material/tabs';
import { ReversePipe } from './pipes/reverse.pipe';
import { MatDialogModule, MatDialog, MatIconModule, MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteModule, MatFormFieldModule, MatInputModule, } from '@angular/material'
import { AddeditproviderComponent } from './component/addeditprovider/addeditprovider.component'
import { QuesetupService } from './_services/quesetup.service'
import {AddeditQueMasterComponent} from './component/addedit-que-master/addedit-que-master.component'
import {SystemroleService} from './_services/systemrole.service'
import {AddSystemroleMasterComponent} from './component/add-systemrole-master/add-systemrole-master.component'
import {QueueaccessprolistService} from './_services/queueaccessprolist.service'
import { QuesetupMasterComponent } from './component/quesetup/quesetup-master.component'
//import { QtraqrenewalsMasterComponent } from './component/qtraqrenewals/qtraqrenewals-master.component'
import {QueuerenewalsService} from './_services/queuerenewals.service'
//import { ChangePasswordComponent } from './component/changePassword/changePassword.component'
import { ChangePasswordService } from './_services/changePassword.service'
import { RolelistComponent } from './component/rolelist/rolelist.component'
//front desk start
import { RequestreceivedService } from './_services/requestsreceived.service';
import { ToggleDirective } from './directive/toggle.directive'
//from desk end
import { FamilymemberComponent } from './component/familymember/familymember.component'
//sidebar start
import { SidebarComponent } from './component/sidebar/sidebar.component'
//sidebar end
import {MatSnackBarModule} from '@angular/material';  
import { SidebarToggleService } from './_services/sidebar-toggle.service';
import { HeaderComponent } from './component/header/header.component';
import { DialogmonitorqueComponent } from './component/dialogmonitorque/dialogmonitorque.component'
import { AssignnewtokendialogComponent } from './component/assignnewtokendialog/assignnewtokendialog.component'
import { TransactionqueuesummaryComponent } from './component/transactionqueuesummary/transactionqueuesummary.component'
import { DialogclinicchangeComponent } from './component/dialogclinicchange/dialogclinicchange.component'
import { ViewimpactdialogComponent } from './component/viewimpactdialog/viewimpactdialog.component'
import { SharetokenComponent } from './component/sharetoken/sharetoken.component'
//import { NgProgressModule } from 'ngx-progressbar';
import { BroadcastmessegeComponent } from './component/broadcastmessege/broadcastmessege.component'
import { WeeklydashboardService } from './_services/weeklydashboard.service'
import { PatienthistoryService } from './_services/patienthistory.service'
import { OverridemasterqueuesService } from './_services/overridemasterqueues.service'
import { KeysPipe } from './pipes/keys.pipe';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { SearchtokensService } from './_services/searchtokens.service'
import { FuturepastqueuesService } from './_services/futurepastqueues.service'
import { DoctoravailabilityService } from './_services/doctoravailability.service'
import { OngoingqueuesService } from './_services/ongoingqueues.service'
import { TodaysqueueService } from './_services/todaysqueue.service'
import { AssignnewtokenService } from './_services/assignnewtoken.service'
import { BroadcastmessegeService } from './_services/broadcastmessege.service'
import { QueueaccessprodetailComponent } from './component/queueaccessprodetail/queueaccessprodetail.component'
import { FrontdeskeditComponent } from './component/frontdeskedit/frontdeskedit.component'
import{ MessagequelistComponent } from './component/messagequelist/messagequelist.component'
import{ AvailabilityComponent } from './component/availability/availability.component'
import{ OrglistchangeComponent } from './component/orglistchange/orglistchange.component'
import { ErrorsnackbarComponent } from './pages/errorsnackbar/errorsnackbar.component'
//fir start
//import { AngularFireModule } from '@angular/fire';
//import { AngularFirestoreModule } from '@angular/fire/firestore';
//import { AngularFireAuthModule } from '@angular/fire/auth';
import { CoreModule } from './core/core.module';
//fir end
import {TestService} from './_services/test.service'
//import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { WindowRef } from './_services/WindowRef.service'
import { SiteComponent } from './component/sitelayout/site.component'
import { NumberDirective } from './directive/number-only.directive';
import { QueuetamplateComponent } from './component/addedit-que-master/queuetamplate/queuetamplate.component';
import { PlatformLocation } from '@angular/common';
import{ DescriptionComponent } from './component/description/description.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from './component/confirmation-dialog/confirmation-dialog.service';
import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';
import { OnholdprocessdialogComponent } from './component/onholdprocessdialog/onholdprocessdialog.component';
import { TimeonholdFormatPipe } from './pipes/timeonhold.pipe'
import { DialognewtoqrmComponent } from './component/dialognewtoqrm/dialognewtoqrm.component';
import { PatientSearchComponent } from './component/diagnosisandprescription/patient-search/patient-search.component';
// import { NgSelect2Module } from 'ng-select2';
import { SelectSuggestionDialogComponent } from './component/diagnosisandprescription/select-suggestion-dialog/select-suggestion-dialog.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { FocusDirective } from './directive/myFocus.directive';
import { GalleryPreviewComponent } from './component/diagnosisandprescription/gallery-preview/gallery-preview.component';
// import { ImageViewerModule } from 'ng2-image-viewer';
import { NotificationComponent } from './component/notification/notification.component';
// import { FileUploadModule } from 'ng2-file-upload';
//OnholdprocessdialogComponent
import {MatRadioModule} from '@angular/material/radio';
import { DiagnosisandprescriptionComponent } from './component/diagnosisandprescription/diagnosisandprescription.component';
import { ImportCustomFieldsComponent } from './component/addedit-que-master/import-custom-fields/import-custom-fields.component';
import { TimerFormatPipe } from './pipes/timer-format.pipe';
import { HtmlviewDialogComponent } from './component/htmlview-dialog/htmlview-dialog.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { FollowupdialogComponent } from './component/followupdialog/followupdialog.component';
import { HtmlviewDialogService } from './component/htmlview-dialog/htmlview-dialog.service';
import { commonInterceptor } from './interceptors/common-interceptors';
import { responseInterceptors } from './interceptors/response-interceptors';
// import { RequestsreceivedComponent } from './component/requestsreceived/requestsreceived.component';
import { NgProgressModule } from "ngx-progressbar";
import { ShareHistoryComponent } from './component/diagnosisandprescription/share-history/share-history.component';
import { FileUploadComponent } from './component/diagnosisandprescription/file-upload/file-upload.component';
import { ParseHtmlPipe } from './pipes/parse-html.pipe';
@NgModule({
  declarations: [
    ConfirmationDialogComponent,
    ErrorsnackbarComponent,
    NumberDirective,
    AvailabilityComponent,
    KeysPipe,
    ViewimpactdialogComponent,
    DialogclinicchangeComponent,
    DialogmonitorqueComponent,
    AssignnewtokendialogComponent,
    TransactionqueuesummaryComponent,
    BroadcastmessegeComponent,
    QuesetupMasterComponent,
    AppComponent,
    LoginComponent,
    DashComponent,
    ForgotPasswordComponent,
    LoaderComponent,
    MainComponent,
    ContentLoaderComponent,
    AddeditorganisationComponent,
    MessagequelistComponent,
    ReversePipe,
    OrglistComponent,
    OrglistchangeComponent,
    RolelistComponent,
    AddeditproviderComponent,
    AddeditQueMasterComponent,
    AddSystemroleMasterComponent,
    ToggleDirective,
    SidebarComponent,
    HeaderComponent,
    FrontdeskeditComponent,
    FamilymemberComponent,
    SharetokenComponent,
    SiteComponent,
    QueueaccessprodetailComponent,
    QueuetamplateComponent,
    DescriptionComponent,
    OnholdprocessdialogComponent,
    TimeonholdFormatPipe,
    DialognewtoqrmComponent,
    ImportCustomFieldsComponent,
    TimerFormatPipe,
    HtmlviewDialogComponent,
    SafeHtmlPipe,
    FollowupdialogComponent,
    DiagnosisandprescriptionComponent,
    PatientSearchComponent,
    SelectSuggestionDialogComponent,
    FocusDirective,
    GalleryPreviewComponent,
    NotificationComponent,
    ShareHistoryComponent,
    FileUploadComponent,
    ParseHtmlPipe,
    // RequestsreceivedComponent
    // BrowserModule
  ],
  imports: [
    MatRadioModule,
    HttpClientModule,
    NgxPasswordToggleModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CoreModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([]),
    AppRoutingModule,
    AuthServiceModule.forRoot(configuration.localStorageKey),
    ServerServiceModule.forRoot(),
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    PaginationModule,
    BootstrapModalModule,
    FileUploadModule,
    ToasterModule.forRoot(),
    QuillModule,
    Ng2TableModule,
    DialogServiceModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgProgressModule,
    NgxMyDatePickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    MatTabsModule,  
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
    NgDatepickerModule,
    // NgSelect2Module,
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    // ImageViewerModule,
  ],
  entryComponents: [
    OrglistComponent,
    OrglistchangeComponent,
    RolelistComponent,
    DialogmonitorqueComponent,
    AssignnewtokendialogComponent,
    QueuetamplateComponent,
    TransactionqueuesummaryComponent,
    FamilymemberComponent,
    DialogclinicchangeComponent,
    ViewimpactdialogComponent,
    SharetokenComponent,
    MessagequelistComponent,
    ErrorsnackbarComponent,
    ConfirmationDialogComponent,
    OnholdprocessdialogComponent,
    DialognewtoqrmComponent,
    ImportCustomFieldsComponent,
    HtmlviewDialogComponent,
    FollowupdialogComponent,
    PatientSearchComponent,
    SelectSuggestionDialogComponent,
    GalleryPreviewComponent, 
    ShareHistoryComponent,
    FileUploadComponent
  ],
  providers: [
    ConfirmationDialogService,
    WindowRef,
    BroadcastmessegeService,
    AssignnewtokenService,
    OngoingqueuesService,
    DoctoravailabilityService,
    FuturepastqueuesService,
    SearchtokensService,
    OverridemasterqueuesService,
    PatienthistoryService,
    WeeklydashboardService,
    RequestreceivedService,
    ChangePasswordService,
    QueuerenewalsService,
    QueueaccessprolistService,
    SystemroleService,
    QuesetupService,
    CommonService,
    ProvidersService,
    NotificationService,
    OrganisationsService,
    LoginService,
    AuthGuard,
    TestService,
    TodaysqueueService,
    AuthenticationService,
    SidebarToggleService,
    HtmlviewDialogService,
    { provide: HTTP_INTERCEPTORS, useClass: commonInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: responseInterceptors, multi: true },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(applicationRef: ApplicationRef,location: PlatformLocation) {
    Object.defineProperty(applicationRef, '_rootComponents', {get: () => applicationRef['components']});
    SidebarComponent

    function disableBack() { window.history.forward() }

     
        window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
        location.onPopState(() => {
          console.log("pressed back in add!!!!!");
          history.forward();
        })
  }


}
