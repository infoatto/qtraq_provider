import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'TimeFormatonhold'})

export class TimeonholdFormatPipe implements PipeTransform {
     transform(time: any): any {


         let hour = (time.split(':'))[0]
         let min = (time.split(':'))[1]



         let part = hour >= 12 ? 'PM' : 'AM';
         min = (min+'').length == 1 ? `0${min}` : min;
         hour = hour > 12 ? hour - 12 : hour;

if(hour==0)
{
  hour = 12
}else
{
  hour = (hour+'').length == 1 ? `0${hour}` : hour;
}
         
         return `${hour}:${min} ${part}`
       }
   }