﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//QuesetupService
//@Injectable({ providedIn: 'root' })
@Injectable()
export class FuturepastqueuesService {
    constructor(private http: HttpClient) { }


    
getorgque(page: number, pageSize: number) 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(environment.url+`getOrgProviders`
,'',httpOptions)
}




getfuturepast(providerid,fromdate,todate)
{
    
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));


    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'orgid':orgid,
        'loginuserid':atob(usermasterid),
        'providerid':""+providerid+"",
        'roletype':roletype.activerole,
        'fromdate':fromdate,
        'enddate':todate
        })
        }

return this.http.post<any>(environment.url+`providerPastFutureQueues`
,'',httpOptions)

}

transactionQueueTokens(queue_transaction_id)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'orgid':orgid,
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'queuetransactionid':queue_transaction_id
        })
        }
return this.http.post<any>(environment.url+`transactionQueueTokens`
,'',httpOptions)

}

private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}

}