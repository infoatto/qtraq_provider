﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class QuesetupService {
	constructor(private http: HttpClient) {}
	getorgque(page: number, pageSize: number) {
		const httpOptions = {
			headers: new HttpHeaders({
				'page': "" + page + "",
			})
		}
		return this.http.post < any > (environment.url + `getOrgProviders`, '', httpOptions)
	}

	getproviderqueues(providerid:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'providerid': "" + providerid + "",
			})
		}
		return this.http.post < any > (environment.url + `getOrgProviderQueues`, '', httpOptions)
	}

	getproviderqueuesforprovider() {
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		const httpOptions = {
			headers: new HttpHeaders({
				'providerid': atob(usermasterid),
			})
		}
		return this.http.post < any > (environment.url + `getOrgProviderQueues`, '', httpOptions)
	}


	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof Error) {
			const errMessage = error.error.message;
			return Observable.throw(errMessage);
		}
		return Observable.throw(error || 'Node.js server error');
	}

	quecustom(queid:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'queueid': "" + queid + "",
			})
		}
		return this.http.post < any > (environment.url + `listCustomFields`, '', httpOptions)
    }
    
	addeditque(queid:any, queue_pic:any, formvalue:any, providerid:any, hiddenqueuepic:any, templateimagename:any, queuetemplateid:any) {
		let queuetemplateiddata = "";
		if (queuetemplateid == "0") {
			queuetemplateiddata = "";
		} else {
			queuetemplateiddata = queuetemplateid;
		}
		let templateimagenamedata = "";
		if (templateimagename != undefined) {
			templateimagenamedata = templateimagename;
		} else {
			templateimagenamedata = "";
		}
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		let advancedbooking:any;
		if (formvalue.no_days_allowed > 0) {
			advancedbooking = "Yes"
		} else {
			advancedbooking = "No"
		}
		const formData = new FormData();
		formData.append('queue_pic', queue_pic);
		formData.append('queuename', formvalue.quename);
		formData.append('ispublic', formvalue.ispublic);
		formData.append('status', formvalue.status);
		formData.append('maxtoken', formvalue.maxtoken);
		formData.append('buffertokens', formvalue.buffertokens);
		formData.append('bookingmode', formvalue.booking_mode);
		formData.append('advancedbookingdays', formvalue.no_days_allowed);
		formData.append('starttime', formvalue.starttime);
		formData.append('endtime', formvalue.endtime);
		formData.append('advancedbookingallowed', advancedbooking);
		formData.append('monavailable', formvalue.monworking);
		formData.append('tueavailable', formvalue.tueworking);
		formData.append('wedavailable', formvalue.wedworking);
		formData.append('thuavailable', formvalue.thuworking);
		formData.append('friavailable', formvalue.friworking);
		formData.append('satavailable', formvalue.satworking);
		formData.append('avgconsultingtime', formvalue.avgconsultingtime);
		formData.append('sunavailable', formvalue.sunworking);
		formData.append('templateimagename', templateimagenamedata);
		formData.append('queuetemplateid', queuetemplateiddata);
		if (queid == 0) {
			const httpOptions = {
				headers: new HttpHeaders({
					'providerid': providerid,
					'createdby': atob(usermasterid),
				})
			}
			return this.http.post < any > (environment.url + `addEditMasterQueue`, formData, httpOptions)
		} else {
			const httpOptions = {
				headers: new HttpHeaders({
					'providerid': providerid,
					'createdby': atob(usermasterid),
					'queueid': "" + queid + "",
				})
			}
			const formData = new FormData();
			formData.append('queue_pic', queue_pic);
			formData.append('queuename', formvalue.quename);
			formData.append('ispublic', formvalue.ispublic);
			formData.append('status', formvalue.status);
			formData.append('maxtoken', formvalue.maxtoken);
			formData.append('buffertokens', formvalue.buffertokens);
			formData.append('bookingmode', formvalue.booking_mode);
			formData.append('advancedbookingdays', formvalue.no_days_allowed);
			formData.append('starttime', formvalue.starttime);
			formData.append('endtime', formvalue.endtime);
			formData.append('advancedbookingallowed', advancedbooking);
			formData.append('monavailable', formvalue.monworking);
			formData.append('tueavailable', formvalue.tueworking);
			formData.append('wedavailable', formvalue.wedworking);
			formData.append('thuavailable', formvalue.thuworking);
			formData.append('friavailable', formvalue.friworking);
			formData.append('satavailable', formvalue.satworking);
			formData.append('avgconsultingtime', formvalue.avgconsultingtime);
			formData.append('sunavailable', formvalue.sunworking);
			formData.append('templateimagename', templateimagenamedata);
			formData.append('queuetemplateid', queuetemplateiddata);
			formData.append('hiddenqueuepic', hiddenqueuepic);
			return this.http.post < any > (environment.url + `addEditMasterQueue`, formData, httpOptions);
		}
	}


	addMasterQueueNotice(queueid:any, formValue:any, providerid:any, fromdate:any, todate:any, showdatefield:any) {
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		if (formValue.status == null) {
			formValue.status = "Active"
		} else {
			formValue.status = formValue.status
		}
		if (showdatefield == true) {
			const httpOptions = {
				headers: new HttpHeaders({
					'createdby': atob(usermasterid),
					'providerid': providerid,
					'queueid': "" + queueid + "",
					'noticemsg': formValue.noticemsg,
					'fromdate': fromdate,
					'todate': todate,
					'status': formValue.status,
					'applytoall': formValue.allQueue
				})
			}
			return this.http.post < any > (environment.url + `addMasterQueueNotice`, '', httpOptions)
		} else {
			const httpOptions = {
				headers: new HttpHeaders({
					'createdby': atob(usermasterid),
					'providerid': providerid,
					'queueid': "" + queueid + "",
					'noticemsg': formValue.noticemsg,
					'status': formValue.status,
					'applytoall': formValue.allQueue
				})
			}
			return this.http.post < any > (environment.url + `addMasterQueueNotice`, '', httpOptions)
		}
	}


	getaddeditque(queid:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'queueid': "" + queid + "",
			})
		}
		return this.http.post < any > (environment.url + `getMasterQueueDetails`, '', httpOptions)
	}

	deletequeuedata(noticeid:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'noticeid': "" + noticeid + ""
			})
		}
		return this.http.post < any > (environment.url + `deleteQueueNotice`, '', httpOptions)
	}

	addeditqueue_assigned(queid:any, mainform:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'queueid': "" + queid + "",
			})
		}
		return this.http.post < any > (environment.url + `addEditMasterQueueAccess`, mainform, httpOptions)
	}


	editcustomEdit(queid:any, mainform:any, formvalue:any) {
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		var notifytokenonchange = ""
		if (formvalue.notifytokenonchange != "notset") {
			notifytokenonchange = formvalue.notifytokenonchange
		}
		const httpOptions = {
			headers: new HttpHeaders({
				'createdby': atob(usermasterid),
				'queueid': "" + queid + "",
				"fieldlabel": formvalue.fieldlabel,
				"fieldtype": formvalue.fieldtype,
				"onassign": formvalue.onassign,
				"oncomplet": formvalue.oncomplet,
				"status": 'Active',
				"patientvisible": formvalue.visibletopatient,
				'notifyuser': notifytokenonchange,
				"ismandatory": formvalue.ismandatory
			})
		}
		return this.http.post < any > (environment.url + `addEditQueueCustomField`, mainform, httpOptions)
	}


	updatestatus(testid:string, status:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'tblname': 'tbl_queue_notices',
				'comparecolumnname': 'queue_notice_id',
				'comparecolumndata': testid,
				'statuscolumnname': 'status',
				'statusdata': status,
			})
		}
		return this.http.post < any > (environment.url + `changeStatus`, '', httpOptions).pipe(catchError(this.handleError));
	}


	updatequestatus(testid: string, status:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'tblname': 'tbl_queue_custom_fields',
				'comparecolumnname': 'custom_field_id',
				'comparecolumndata': testid,
				'statuscolumnname': 'status',
				'statusdata': status,
			})
		}
		return this.http.post < any > (environment.url + `changeStatus`, '', httpOptions).pipe(catchError(this.handleError));
	}


	getexistingcustom(customid: string, status:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'customfieldid': "" + customid + "",
			})
		}
		return this.http.post < any > (environment.url + `getCustomFieldDetails`, '', httpOptions).pipe(catchError(this.handleError));
	}

	customfieldlabelupdate(formValue, custom_field_id, custom_label, queueid, mainform) {
		var notifytokenonchange = ""
		if (formValue.notifytokenonchange != "notset") {
			notifytokenonchange = formValue.notifytokenonchange
		}
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		const httpOptions = {
			headers: new HttpHeaders({
				'createdby': atob(usermasterid),
				"onassign": formValue.onassign,
				"oncomplet": formValue.oncomplet,
				"fieldtype": formValue.custom_field_type,
				'queueid': "" + queueid + "",
				"patientvisible": formValue.visibletopatient,
				"ismandatory": formValue.is_mandatory,
				'customfieldid': "" + custom_field_id + "",
				'fieldlabel': custom_label,
				'notifyuser': notifytokenonchange,
				'status': "Active"
			})
		}
		return this.http.post < any > (environment.url + `addEditQueueCustomField`, mainform, httpOptions).pipe(catchError(this.handleError));
    }
    
	getQueueTemplates() {
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid = currentUser.user_master_id;
		let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
		let org_type = orguserlist.org_type
		const httpOptions = {
			headers: new HttpHeaders({
				'providerid': atob(usermasterid),
				'orgtype': org_type
			})
		}
		return this.http.post < any > (environment.url + `getQueueTemplates`, '', httpOptions).pipe(catchError(this.handleError));
	}

	getproviderdetail(providerid:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'providerid': providerid,
			})
		}
		return this.http.post < any > (environment.url + `getProvidersList`, '', httpOptions)
	}

	getLoginUserDetails(providerid:any) {
		return this.http.post < any > (environment.url + `getLoginUserDetails`, '');
	}

	getupdateMasterQueue(queid:any, dielogstatus:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'queueid': "" + queid + "",
				'Status': "" + dielogstatus + ""
			})
		}
		return this.http.post < any > (environment.url + `updateMasterQueueStatus`, '', httpOptions)
	}

	changeQueueCustomeFieldStatus(ids:any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'statusdata': 'In-active',
				'customfieldvalueid': "" + ids + "",
			})
		}
		return this.http.post < any > (environment.url + `changeQueueCustomeFieldStatus`, '', httpOptions)
	}
}