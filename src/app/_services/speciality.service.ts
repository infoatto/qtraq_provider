﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class SpecialityService {
    constructor(private http: HttpClient) { }

getspecialitylist(page: number, pageSize: number,speciality) 
{

const formData = new FormData();
formData.append('page',page.toString());   
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'specialityname':speciality.specialityname,
'status':speciality.specialitystatus,
})
}
return this.http.post<any>(environment.url+`listSpeciality`
,formData,httpOptions)
}



createspeciality(speciality)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
   
    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
        'specialityname': speciality.speciality_name,
       'specialityfor': speciality.speciality_for,
        'status': speciality.status,
        'specialityid':speciality.speciality_id
       })
      };

return this.http.post<any>(environment.url+`addEditSpeciality`
,'',httpOptions)
    
}


geteditspecialitydata(specialityid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'tblname':'tbl_speciality_master',
        'comparecolumnname':'speciality_id',
        'comparecolumndata':specialityid
         })
         }

    return this.http.post<any>(environment.url+`getMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}


updatestatus(specialityid:string,status)
{
  
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_speciality_master',
'comparecolumnname':'speciality_id',
'comparecolumndata':specialityid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}





private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}