﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class PatienthistoryService {
    constructor(private http: HttpClient) { }


/*
getorgque(page: number, pageSize: number)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getOrgProviders`
,'',httpOptions)
}



getongoingqueue(user_master_provider_id)
{



    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype,
    'providerid':'6',

})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/providerTodaysQueues`
,'',httpOptions)
}

getongoingtransactionqueue(transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype,
    'queuetransactionid':transactionid,
    'tokentypefilter':'all'
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/monitorOngoingQueue`
,'',httpOptions)
   }





getongoingtransactiontype(tokentype,transactionid)
{



    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype,
    'queuetransactionid':transactionid,
    'tokentypefilter':tokentype
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/monitorOngoingQueue`
,'',httpOptions)
}



markDrArrived(transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype,
    'transactionqueueid':transactionid,
    })
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/markDrArrived`
,'',httpOptions)
}








transactionQueueMessages(page: number,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype,
    })
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/transactionQueueMessages`
,'',httpOptions)
}

addmessagesms(messege,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype,
    'queuemsg':messege
    })
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addTransactionQueueMessage`
,'',httpOptions)
}

*/
getShareHistoryProviderAppProviders()
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'providerid':atob(usermasterid),

    })
}
return this.http.post<any>(`https://demo.qtraq.in/qtraqservices/qtraqconsumerapi/getShareHistoryProviderAppProvidersnew`
,'',httpOptions)
}



getShareHistoryProviderAppConsumers(getconsumerfor,rolefor)
{




    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'providerid':rolefor,
    })
}
return this.http.post<any>(`https://demo.qtraq.in/qtraqservices/qtraqconsumerapi/getShareHistoryProviderAppConsumersnew`
,'',httpOptions)
}







getShareHistoryProviderAppConsumersself(fromdate,todate,providerchange,consumer,consumertype)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;

var fromdatedata="";
var todatedate="";
var consumerdata="";

var consumertypedata="";


var providerchangedata="";
if(((fromdate!="")&&(fromdate!=undefined)))
{
fromdatedata=fromdate
}

if(((todate!="")&&(todate!=undefined)))
{
    todatedate=todate
}

if((providerchange!="")&&(providerchange!=undefined))
{
    providerchangedata=providerchange
}


if((consumer!="")&&(consumer!=undefined))
{
    consumerdata=providerchange
}


if((consumertype!="")&&(consumertype!=undefined))
{
    consumertypedata=consumertype
}




    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'providerid':providerchangedata,
    'consumerid':consumerdata,
'fromdate':fromdatedata,
'todate':todatedate,
'consumertype':consumertypedata
})
}
return this.http.post<any>(`https://demo.qtraq.in/qtraqservices/qtraqconsumerapi/getProviderTokenHistorynew`
,'',httpOptions)
}


//other last data start 3-4-2019

getShareHistoryProviderAppConsumersother(providerchange,consumer)
{

let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let roletype = JSON.parse(localStorage.getItem('activerole'));
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let usermasterid=currentUser.user_master_id;
 let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'getdetailsfor':'Other',
    'providerid':atob(usermasterid),
    'otherproviderid':providerchange,
    'consumerid':consumer
})
}
return this.http.post<any>(environment.url+`getProviderTokenHistory`
,'',httpOptions)
}
//other last data end 3-4-2019


//token detail 3-4-2019 start

getTokenDPRDetails(token_id)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'tokenid':""+token_id+""
    })
}

return this.http.post<any>(environment.url+`getTokenDPRDetails`
,'',httpOptions)

}
//token detail 3-4-2019 end


//new code 3-6-2019



getuploadprescription(formvalue,token_id)
{


    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'tokenid':""+token_id+"",
    'uploadedby':atob(usermasterid)
    })
}

return this.http.post<any>(environment.url+`tokenUploadPrescription`
,formvalue,httpOptions)

}





uploadreport(formvalue,token_id,ontestchangeid,testname)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'testnames':""+testname+"",
    'tokenid':""+token_id+"",
     "testmasterid":""+ontestchangeid+"",
    "uploadedby":"provider",
    })
}
return this.http.post<any>(environment.url+`tokenUploadTestReports`
,formvalue,httpOptions)
}

getTokenTestList(token_id)
{

let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let roletype = JSON.parse(localStorage.getItem('activerole'));
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken;
const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'tokenid':""+token_id+""
    })
}

return this.http.post<any>(environment.url+`getTokenTestList`
,'',httpOptions)

}

deleteImageprescription(report_id,report_image)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'roletype':roletype.activerole,
        'recordid':""+report_id+"",
        'imagename':""+report_image+"",
        })
}
return this.http.post<any>(environment.url+`deletePrescriptionReportImage`
,'',httpOptions)


}
//new code 3-6-2019

//new code 3-7-2019 start


getQTRAQUserDetails(mobilenumber){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'mobileno':mobilenumber,
            'roletype':roletype.activerole,
        })
    }
    return this.http.post<any>(environment.url+`getQTRAQUserinfo`,'',httpOptions);
}


shartoken(providerchange,consumertypeid,main_form,diagnosis,fromdate,todate,prescription,report,user_master_id,bookingtype,bookingforid)
{




    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':orgid,
        'sharinguserid':atob(usermasterid),
        'sharetouserid':user_master_id,
        'sharefromdate':fromdate,
         'tokenproviderid':""+providerchange+"",
        'sharetodate':todate,
        'sharediagnosis':diagnosis,
        'shareprescription':prescription,
        'sharereport':report,
        'sharingpersontype':'provider',
        'bookingfor':bookingtype,
        'bookingforid':bookingforid
        })
        }
        return this.http.post<any>(`https://demo.qtraq.in/qtraqservices/qtraqconsumerapi/shareMultipleTokenHistorynew`
        ,main_form,httpOptions)

}


//new code 3-7-2019 end



//new code 5-3-2019 start


unsharehistory(page)
{
   // let roletype = JSON.parse(localStorage.getItem('activerole'));



    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let roletype = JSON.parse(localStorage.getItem('activerole'));

    const httpOptions = {
    headers: new HttpHeaders({
    'loginuserid':"6",
    'utoken':atob(utoken),
   // 'loginuserid':atob(usermasterid),
    'page':""+page+"",
    'roletype':roletype.activerole,
    'sharedby':'Consumer'
    })
    }
    return this.http.post<any>(environment.url+`listSharedHistories`
    ,'',httpOptions)
}


unsharehistorydata(historyid)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    const httpOptions = {
        headers: new HttpHeaders({
        //'loginuserid':"6",
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':orgid,
        'historyid':""+historyid+""
        })
    }
    return this.http.post<any>(environment.url+`unshareTokenHistory`
    ,'',httpOptions)


}



//new code end 5-3-2019 end

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof Error) {
            const errMessage = error.error.message;
            return Observable.throw(errMessage);
        }
        return Observable.throw(error || 'Node.js server error');
    }

    getPrescriptionInvoicePrintDataHtml(tokenid:any,printtype:any){
        const httpOptions = {
            headers: new HttpHeaders({
                "tokenid":tokenid,
                "printtype":printtype
            })
        }
        return this.http.post<any>(environment.url+`getPrescriptionInvoicePrintDataHtml`
        ,'',httpOptions)
    }

    shareMultipleTokenHistory(tousermasterid:any,sharefromdate:any,sharetodate:any,sharediagnosis:any,shareprescription:any,sharereport:any,bookingforid:any,bookingforname:any,bookingfor:any,tokenproviderid:any,tokenid:any){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'sharinguserid':atob(usermasterid),
                'sharetouserid':tousermasterid,
                'sharefromdate':sharefromdate,
                'sharetodate':sharetodate,
                'sharediagnosis':sharediagnosis,
                'shareprescription':shareprescription,
                'sharereport':sharereport,
                'sharingpersontype':"provider",
                'bookingforid':bookingforid,
                'bookingfor':bookingfor,
                'bookingforname':bookingforname,
                'tokenproviderid':tokenproviderid,
                'tokenid':tokenid,
            })
        }
        return this.http.post<any>(environment.url+`shareMultipleTokenHistory`,'',httpOptions);
    }

}