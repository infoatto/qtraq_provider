﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class QueueaccessprolistService {
    constructor(private http: HttpClient) { }




    getOrgProviders(page: number, pageSize: number){
        const formData = new FormData();
        formData.append('page',page.toString());   
        const httpOptions = {
            headers: new HttpHeaders({
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`getOrgProviders`,formData,httpOptions)
    }
    


getAllQueueAccessDetails(user_master_id)
{
   
    let roletype = JSON.parse(localStorage.getItem('activerole'));

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let utoken=currentUser.utoken; 
    const httpOptions = {
   headers: new HttpHeaders({
       'Access-Control-Allow-Methods':'GET, POST',
       'Access-Control-Allow-Origin':'*',
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'providerid':""+user_master_id+"",
    'roletype':roletype.activerole,
//    'providerid':""+user_master_id+""

})
}
    return this.http.post<any>(environment.url+`getAllQueueAccessDetails`
    ,'',httpOptions)


}


addqueassess(formdata)
{

   let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
         'utoken':atob(utoken),
         'orgid':orgid,
     })
     }
     return this.http.post<any>(environment.url+`addEditQueueAccess`
     ,formdata,httpOptions)


}




/*  
getsystemrole()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
   'orgid':orgid
    })
    }
    return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getSystemRoles`
    ,'',httpOptions)

}

submitsystemrole(body)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'usermasterid':atob(usermasterid),
       'orgid':orgid
        })
        }
        return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addEditSystemRoles`
        ,body,httpOptions)
}







getCountries()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getCountries`
,'',httpOptions).pipe(catchError(this.handleError));
}


getcity(val)
{
const httpOptions = {
headers: new HttpHeaders({
'stateid':  val,
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getCities`
,'',httpOptions).pipe(catchError(this.handleError));
}


getState(val)
{
const httpOptions = {
headers: new HttpHeaders({
'countryid':  val,
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getState`
,'',httpOptions).pipe(catchError(this.handleError));
}


getdatabymobile(mobileno)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'usermasterid':atob(usermasterid),    
        'mobileno':mobileno,
        })
        }

        return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getQTRAQUserDetails`
        ,'',httpOptions).pipe(catchError(this.handleError));

}





addQTRAQSystemRoleUser(formValue,dob_date)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    var isl1
    var isl2
    var isorgadmin


    if((formValue.isl1==true)||(formValue.isl1=='Yes'))
    {
    isl1="Yes"
    }
    else
    {
   isl1="No"
    }

    if((formValue.isl2==true)||(formValue.isl1=='Yes'))
    {
    isl2="Yes"
    }
    else
    {
    isl2="No"
    }


    if((formValue.isorgadmin==true)||(formValue.isorgadmin=='Yes'))
    {
    isorgadmin="Yes"
    }
    else
    {
    isorgadmin="No"
    }


    if(formValue.user_master_id!="")
    {


        

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'usermasterid':atob(usermasterid),    
        'isl1':isl1,
        'isl2':isl2,
        'isorgadmin':isorgadmin,
        'qtraquserid':""+formValue.user_master_id+"",
        'orgid':orgid
        })
        }
    
        return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addQTRAQSystemRoleUser`
        ,'',httpOptions).pipe(catchError(this.handleError));
    }
    else
    {
        const httpOptions = {
 headers: new HttpHeaders({
 'utoken':atob(utoken),
 'usermasterid':atob(usermasterid),    

 'isl1':isl1,
'isl2':isl2,
'isorgadmin':isorgadmin,
'cityid':formValue.city_id,
'stateid':formValue.state_id,
'countryid':formValue.countryid,
'firstname':formValue.firstname,
'lastname':formValue.lastname,
'middlename':formValue.middlename,
'mobileno':formValue.mobilenumber,
'gender':formValue.gender,
'emailid':formValue.emailid,
'pincode':formValue.pincode,
'orgid':orgid,
'dob':dob_date

})
            }     
            return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addQTRAQSystemRoleUser`
            ,'',httpOptions).pipe(catchError(this.handleError));
    
    }
}
*/


















private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}