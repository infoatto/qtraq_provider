﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { environment } from '../../environments/environment';

//import { Injectable } from '@angular/core';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class ProvidersService {
    constructor(private http: HttpClient,private router: Router,) { }
  getOrganisationslist(page: number, pageSize: number,organisationfilter){
    const formData = new FormData();
    formData.append('page',page.toString());   
    const httpOptions = {
      headers: new HttpHeaders({
        'page':""+page+""
      })
    }
    return this.http.post<any>(environment.url+`getOrgProviders`,formData,httpOptions)
  }


//new code check for one 5-22-2019 start



  checkOrganisationslist(){
    const formData = new FormData();
    formData.append('page',"0");   
    const httpOptions = {
      headers: new HttpHeaders({
        'page':"0"
      })
    }
    return this.http.post<any>(environment.url+`getOrgProviders`,formData,httpOptions)
  }





//new code check for one 5-22-2019 end

updatestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_organisation_master',
'comparecolumnname':'org_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}
//new code 1-3-2019



updateimagestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_organisation_images',
'comparecolumnname':'org_image_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}



updatedatetimestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_org_working_hours',
'comparecolumnname':'org_working_hour_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}









//new code end 1-3-2019

getallorganisation()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
})
}
return this.http.post<any>(environment.url+`qtraqapi/getOrganisations`
,'',httpOptions).pipe(catchError(this.handleError));
}


getSpecialities()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'orgid':""+orgid+""
})
}
return this.http.post<any>(environment.url+`getSpecialities`
,'',httpOptions).pipe(catchError(this.handleError));
}


  personaladd(profilepicname,providerid,providerdateogbarth,providerpractisingstarteddate,body,formValues){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    if(providerid!=0){
      const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
          'loginuserid':atob(usermasterid),
          'userid':providerid,
          'roletype':roletype.activerole,
          'profilepicname':profilepicname,
          'mobileno':formValues.mobileno,
          'isorgdefault':"no",
          'orgid':orgid,
        })
      }
      return this.http.post<any>(environment.url+`editQTRAQUser`,body,httpOptions).pipe(catchError(this.handleError));
    }else{  
      const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
          'loginuserid':atob(usermasterid),
          'userid':providerid,
          'roletype':roletype.activerole,
          'mobileno':formValues.mobileno,
          'firstname':formValues.firstname,
          'middlename':formValues.middlename,
          'lastname':formValues.lastname,
          'dob':providerdateogbarth,
          'autodob':formValues.autodob,
          'gender':formValues.gender,
          'countryid':formValues.countryid,
          'stateid':formValues.state_id,
          'cityid':formValues.city_id,
          'pincode':formValues.pincode,
          'emailid':formValues.emailid,
          'isorgdefault':"no",
          'orgid':orgid,
          'otp':formValues.otp,
          'qualification':formValues.qualification,
          'otherspeciality':formValues.otherspeciality,
          'practisingstarteddate':providerpractisingstarteddate,
          'consultationfees':formValues.consultationfees,
        })
      }
      return this.http.post<any>(environment.url+`addQTRAQProviderUser`,body,httpOptions).pipe(catchError(this.handleError));
    }
  }



addeditavailability(providerid,formvalue)
{


    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));


    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
          'userid':""+providerid+"",
          'roletype':roletype.activerole,
          'loginuserid':atob(usermasterid),
          'orgid':orgid,
        })
    }

return this.http.post<any>(environment.url+`addEditProviderAvailability`,formvalue,httpOptions).pipe(catchError(this.handleError));


}






addeditProviderContactDetails(providerid,formvalue)
{









    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));


    const httpOptions = {
        headers: new HttpHeaders({
     'utoken':atob(utoken),
     'createdby':atob(usermasterid),
     'userid':""+atob(usermasterid)+"",
     'roletype':roletype.activerole,
     'loginuserid':atob(usermasterid),
    // 'mobile1':formvalue.mobile1,
    // 'mobile1visibility':""+formvalue.mobile1public+"",
   //  'mobile2':formvalue.mobile2,
    // 'mobile2visibility':""+formvalue.mobile2public+"",
   //  'phone1':formvalue.phone1,
  //   'phone1visibility':""+formvalue.phone1public+"",
   //  'phone2':formvalue.phone2,
   //  'phone2visibility':""+formvalue.phone2public+"",
    // 'email1':formvalue.emailid1,
   //  'email1visibility':""+formvalue.email1public+"",
    //  'email2':formvalue.emailid2,
  //   'email2visibility':""+formvalue.email2public+"",
   //  'website':formvalue.website,
      })
    }



    const formData = new FormData();
    formData.append('mobile1',formvalue.mobile1);  
    formData.append('mobile1visibility',formvalue.mobile1public);  
    formData.append('mobile2',formvalue.mobile2);  
    formData.append('mobile2visibility',formvalue.mobile2public);  
    formData.append('phone1',formvalue.phone1);  


    formData.append('phone1visibility',formvalue.phone1public);  
    formData.append('phone2',formvalue.phone2);  
    formData.append('phone2visibility',formvalue.phone2public);  
    formData.append('email1',formvalue.emailid1);  
    formData.append('email1visibility',formvalue.email1public);  

    formData.append('email1',formvalue.emailid1);  
    formData.append('email1visibility',formvalue.email1public);  


    formData.append('email2',formvalue.emailid2);  
    formData.append('email2visibility',formvalue.email2public); 

    formData.append('website',formvalue.website); 





return this.http.post<any>(environment.url+`addEditProviderContactDetails`,formData,httpOptions).pipe(catchError(this.handleError));


}







addeditProviderContactDetailscontact(providerid,formvalue)
{






    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));


    const httpOptions = {
        headers: new HttpHeaders({
     'utoken':atob(utoken),
     'createdby':atob(usermasterid),
     'userid':""+atob(usermasterid)+"",
     'roletype':roletype.activerole,
     'loginuserid':atob(usermasterid),
   //  'mobile1':formvalue.mobile1,
   //  'mobile1visibility':""+formvalue.mobile1public+"",
    // 'mobile2':formvalue.mobile2,
    // 'mobile2visibility':""+formvalue.mobile2public+"",
    // 'phone1':formvalue.phone1,
    // 'phone1visibility':""+formvalue.phone1public+"",
    // 'phone2':formvalue.phone2,
    // 'phone2visibility':""+formvalue.phone2public+"",
    // 'email1':formvalue.emailid1,
    // 'email1visibility':""+formvalue.emailid1public+"",
     // 'email2':formvalue.emailid2,
    // 'email2visibility':""+formvalue.emailid2public+"",
    // 'website':formvalue.website,
      })
    }


    const formData = new FormData();
    formData.append('mobile1',formvalue.mobile1);  
    formData.append('mobile1visibility',formvalue.mobile1public);  
    formData.append('mobile2',formvalue.mobile2);  
    formData.append('mobile2visibility',formvalue.mobile2public);  
    formData.append('phone1',formvalue.phone1);  


    formData.append('phone1visibility',formvalue.phone1public);  
    formData.append('phone2',formvalue.phone2);  
    formData.append('phone2visibility',formvalue.phone2public);  
    formData.append('email1',formvalue.emailid1);  
    formData.append('email1visibility',formvalue.email1public);  

    formData.append('email1',formvalue.emailid1);  
    formData.append('email1visibility',formvalue.email1public);  


    formData.append('email2',formvalue.emailid2);  
    formData.append('email2visibility',formvalue.email2public); 

    formData.append('website',formvalue.website); 





return this.http.post<any>(environment.url+`addEditProviderContactDetails`,formData,httpOptions).pipe(catchError(this.handleError));


}





editorganisationlocation(organisationid,formValue)
{


    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgid':""+organisationid+"",
//'address1':formValue.address1,
//'address2':formValue.address2,
//'street':formValue.street,
//'area':formValue.area,
//'landmark':formValue.landmark,
//'countryid':formValue.countryid,
//'stateid':formValue.state_id,
//'cityid':formValue.city_id,
//'pincode':formValue.pincode
})
}




const formData = new FormData();
formData.append('address1',formValue.address1);  
formData.append('address2',formValue.address2);  
formData.append('street',formValue.street);  
formData.append('area',formValue.area);  
formData.append('landmark',formValue.landmark);  
formData.append('countryid',formValue.countryid);  
formData.append('stateid',formValue.state_id);  
formData.append('cityid',formValue.city_id);  
formData.append('pincode',formValue.pincode);  
formData.append('gpscords',formValue.gpscords);  






return this.http.post<any>(environment.url+`editOrgLocationDetails`
,formData,httpOptions).pipe(catchError(this.handleError));
}


//organisation location

editorganisationcontact(organisationid,formValue,editorganisationcontact)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgid':""+organisationid+"",
//'mobile1':formValue.mobile1,
//'mobile1public':editorganisationcontact.mobile1public,
//'mobile2':formValue.mobile2,
//'mobile2public':editorganisationcontact.mobile2public,
//'phone1':formValue.phone1,
//'phone1public':editorganisationcontact.phone1public,
//'phone2':formValue.phone2,
//'phone2public':editorganisationcontact.phone2public,
//'emailid1':formValue.emailid1,
//'emailid1public':editorganisationcontact.emailid1public,
//'emailid2':formValue.emailid2,
//'emailid2public':editorganisationcontact.emailid2public,
//'website':formValue.website,
})
}


const formData = new FormData();
    formData.append('mobile1',formValue.mobile1);  
    formData.append('mobile1public',editorganisationcontact.mobile1public);  
    formData.append('mobile2',formValue.mobile2);  
    formData.append('mobile2public',editorganisationcontact.mobile2public);  
    formData.append('phone1',formValue.phone1);  

    formData.append('phone1public',editorganisationcontact.phone1public);  
    formData.append('phone2',formValue.phone2);  
    formData.append('phone2public',editorganisationcontact.phone2public);  
    formData.append('emailid1',formValue.emailid1);  
    formData.append('emailid1public',editorganisationcontact.emailid1public);  

    formData.append('emailid2',formValue.emailid2);  
    formData.append('emailid2public',editorganisationcontact.emailid2public);  
    formData.append('website',formValue.website);  



return this.http.post<any>(environment.url+`editOrgContactDetails`
,formData,httpOptions).pipe(catchError(this.handleError));
}




//organisation end


//location service start

getCountries()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
})
}
return this.http.post<any>(environment.url+`getCountries`
,'',httpOptions).pipe(catchError(this.handleError));
}


getcity(val)
{

const httpOptions = {
headers: new HttpHeaders({
'stateid':  val,
})
}
return this.http.post<any>(environment.url+`getCities`
,'',httpOptions).pipe(catchError(this.handleError));

}


getState(val)
{

const httpOptions = {
headers: new HttpHeaders({
'countryid':  val,
})
}
return this.http.post<any>(environment.url+`getState`
,'',httpOptions).pipe(catchError(this.handleError));

}



//location service end





private handleError(error: HttpErrorResponse) 
{
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}









  geteditprovider(providerid):Observable<any>{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    const httpOptions = {
      headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'userid':""+providerid+"",
      })
    }
    return this.http.post<any>(environment.url+`getQTRAQUserAllDetails`,'',httpOptions)
  }





  getprovideronlyweb(providerid):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'providerid':providerid
      })
    }
    return this.http.post<any>(environment.url+`getOrgProvidersweb`,'',httpOptions)
  }


getOrganisationLocContactDetails(orgid):Observable<any> 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgid':orgid
})
}
return this.http.post<any>(environment.url+`getOrganisationLocContactDetails`
,'',httpOptions)
}





getplandescription(orgid):Observable<any> 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgid':orgid
})
}
return this.http.post<any>(environment.url+`getPlans`
,'',httpOptions)
}



SubscribedPlans(orgid,planid,startdate,enddate):Observable<any> 
{




let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const formData = new FormData();
formData.append('utoken',atob(utoken));  
formData.append('usermasterid',atob(usermasterid));  
formData.append('orgid',orgid);  
formData.append('planid',planid);  
//formData.append('subscriptionstartdate',startdate);  
//formData.append('subscriptionenddate',enddate);  

formData.append('paymentmode',"cheque");  
formData.append('bankname',"HDFC");  
formData.append('branchname',"HDFC Kalyan");  
formData.append('chequeno',"chequeno");  
formData.append('transactionid',"123456789");  


return this.http.post<any>(environment.url+`addEditPlanSubscription`
,formData)
}





listSubscribedPlans(orgid):Observable<any> 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgid':orgid
})
}
return this.http.post<any>(environment.url+`listSubscribedPlans`
,'',httpOptions)
}

getQTRAQUserDetails(mobilenumber)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'mobileno':mobilenumber
    })
    }
    return this.http.post<any>(environment.url+`getQTRAQUserDetails`
    ,'',httpOptions)
}






addQTRAQUserDetails(formValue,dob_date,org_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    
    headers: new HttpHeaders({
        /*
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    "orgid":"1",
    "mobileno":formValue.mobileno,
    "firstname":formValue.firstname,
    "middlename":formValue.middlename, 
    "lastname":formValue.lastname,
    "dob":dob_date,
    "gender":formValue.gender,
    "countryid":formValue.countryid,
    "stateid":formValue.stateid,
    "cityid":formValue.cityid,
    "pincode":formValue.pincode,
    "emailid":formValue.emailid,
    "isorgdefault":formValue.isorgdefault,
    "otp":formValue.otp
*/    
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
"orgid":""+org_id+"",
"mobileno":formValue.mobilenumber,
"firstname":formValue.firstname,
"middlename":formValue.middlename, 
"lastname":formValue.lastname,
"dob":dob_date,
"gender":formValue.gender,
"countryid":formValue.countryid,
"stateid":formValue.stateid,
"cityid":formValue.cityid,
"pincode":formValue.pincode,
"emailid":formValue.emailid,
"isorgdefault":formValue.isorgdefault,
"qtraquserid":formValue.qtraquserid,
"otp":""+formValue.otp+""
})
    }
    return this.http.post<any>(environment.url+`addQTRAQUser`
    ,'',httpOptions)


}





generateOtp(onconvalueget,submobile,otp_type:any=""){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=(currentUser != undefined)?currentUser.user_master_id:"";
    let utoken=(currentUser != undefined)?currentUser.utoken:"";
    const httpOptions = {
      headers: new HttpHeaders({
        'utoken':atob(utoken),
        'usermasterid':atob(usermasterid),
        'mobileno':""+submobile+"",
        "countryid":onconvalueget,
        "otptype":otp_type,
        "usertype":"P"
      })
    }
    return this.http.post<any>(environment.url+`generateOtp`,'',httpOptions)
}


makedefaultuser(user_master_id,org_id)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'qtraquserid':user_master_id,
    })
    }
    return this.http.post<any>(environment.url+`generateOtp`
    ,'',httpOptions)


}




    personaladdprofile(profilepicname,providerid,providerdateogbarth,providerpractisingstarteddate,body:any,formValues:any){
      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      let usermasterid=currentUser.user_master_id;
      const httpOptions = {
        headers: new HttpHeaders({
          'withprovider':"Yes",
          'otherspeciality':""+formValues.otherspeciality+"",
          'userid':atob(usermasterid),
          'qualification':""+formValues.qualification+"",
        })
      }
      return this.http.post<any>(environment.url+`editQTRAQUser`,body,httpOptions).pipe(catchError(this.handleError));
    }   


checkMobileNumberExist(formvalue,countryid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
     let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid

    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'userid':atob(usermasterid),
            'roletype':roletype.activerole,
          'mobilenumber':formvalue.mobilenumber
          })
        }

return this.http.post<any>(environment.url+`checkMobileNumberExist`,"",httpOptions).pipe
(catchError(this.handleError));
}


//changeMobileNumber




changeMobileNumber(formvalue,countryid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
     let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid

    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'userid':atob(usermasterid),
            'roletype':roletype.activerole,
          'mobilenumber':formvalue.mobilenumber,
          'countryid':countryid,
          'otp':formvalue.otp,
          'password':formvalue.password
          })
        }

return this.http.post<any>(environment.url+`changeMobileNumber`,"",httpOptions).pipe
(catchError(this.handleError));
}


verifyemailid(emailid){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    const httpOptions = {
      headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'userid':atob(usermasterid),
        'roletype':roletype.activerole,
        'email':emailid,
      })
    }
    return this.http.post<any>(environment.url+`getEmailVerification`,"",httpOptions).pipe(catchError(this.handleError));
  }

  getShareHistoryProviderAppProviders(providerid:any = "",getdetailsfor:any=""){
    if(providerid == ""){
      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      providerid = atob(currentUser.user_master_id);
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'providerid':providerid,
        'getdetailsfor':"Other",
      })
    }
    return this.http.post<any>(environment.url+`getShareHistoryProviderAppProviders`,"",httpOptions);
  }

  getShareHistoryProviderAppConsumers(page:any = 0,providerid:any="",otherproviderid:any="",getconsumerfor="Other",searchkey:any = ""){
    const formData = new FormData();
    formData.append('searchkey',searchkey);   
    if(providerid == ""){
      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      providerid = atob(currentUser.user_master_id);
    }else{
      let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
      let current_providerid = atob(currentUser.user_master_id);
      if(providerid == current_providerid){
        getconsumerfor = "Self";
      }
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'page':page,
        'providerid':providerid,
        'otherproviderid':otherproviderid,
        'getconsumerfor':getconsumerfor,
      })
    }
    return this.http.post<any>(environment.url+`getShareHistoryProviderAppConsumers`,formData,httpOptions);
  }

  getProviderTokenHistory(consumerid:any,providerid:any,fromdate:any,todate:any,consumertype:any,page :any=0,tokenid:any = ""){
    const httpOptions = {
      headers: new HttpHeaders({
        'providerid':providerid,
        'consumerid':consumerid,
        'fromdate':fromdate,
        'todate':todate,
        'consumertype':consumertype,
        'page':''+page+'',
        'calledbyconsumer':"No",
        'tokenid':tokenid
      })
    }
    return this.http.post<any>(environment.url+`getProviderTokenHistory`,"",httpOptions);
  }

  getSuggestion(providerid:any){
    const httpOptions = {
      headers: new HttpHeaders({
        'providerid':providerid,
      })
    }
    return this.http.post<any>(environment.url+`getSuggestion`,"",httpOptions);
  }

  getTestTypes(){
    const httpOptions = {
      headers: new HttpHeaders({})
    }
    return this.http.post<any>(environment.url+`getTestTypes`,"",httpOptions);
  }

  getTokenDPRDetails(tokenid:any){
    const httpOptions = {
      headers: new HttpHeaders({
         'tokenid':tokenid,
      })
    }
    return this.http.post<any>(environment.url+`getTokenDPRDetails`,"",httpOptions);
  }

  getTokenDetails(token_id:any){
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'roletype':roletype.activerole, 
            'orgid':orgid,
            'tokenid':token_id
        })
    }
    return this.http.post<any>(environment.url+`getTokenDetails`,'',httpOptions);
  }

  tokenAddEditDiagnosis(formData:any) {
    let providerid = atob(JSON.parse(localStorage.getItem('Adminuser')).user_master_id);
    const httpOptions = {
      headers: new HttpHeaders({
        'requestedby':providerid,
      })
    }
    return this.http.post<any>(environment.url+`tokenAddEditDiagnosis`,formData,httpOptions);
  }

  tokenAddEditPrescription(formData:any) {
    let providerid = atob(JSON.parse(localStorage.getItem('Adminuser')).user_master_id);
    const httpOptions = {
      headers: new HttpHeaders({
        'requestedby':providerid,
      })
    }
    return this.http.post<any>(environment.url+`tokenAddEditPrescription`,formData,httpOptions);
  }

  tokenAddEditDiagnosisPrescription(formData:any) {
    let providerid = atob(JSON.parse(localStorage.getItem('Adminuser')).user_master_id);
    const httpOptions = {
      headers: new HttpHeaders({
        'requestedby':providerid,
      })
    }
    return this.http.post<any>(environment.url+`tokenAddEditDiagnosisPrescription`,formData,httpOptions);
  }

  deletePrescriptionReportImage(id:any,name:any){
    const httpOptions = {
      headers: new HttpHeaders({
        'imagename':name,
        'recordid':id,
        'usertype':'P',
      })
    }
    return this.http.post<any>(environment.url+`deletePrescriptionReportImage`,"",httpOptions);
  }

  tokenUploadPrescription(token_id:any,formdata:any){
    let providerid = atob(JSON.parse(localStorage.getItem('Adminuser')).user_master_id);
    const httpOptions = {
      headers: new HttpHeaders({
        'tokenid':token_id,
        'uploadedby':providerid
      })
    }
    return this.http.post<any>(environment.url+`tokenUploadPrescription`,formdata,httpOptions);
  }

  tokenUploadTestReports(token_id:any,formdata:any){
    let providerid = atob(JSON.parse(localStorage.getItem('Adminuser')).user_master_id);
    const httpOptions = {
      headers: new HttpHeaders({
        'tokenid':token_id,
        'uploadedby':providerid
      })
    }
    return this.http.post<any>(environment.url+`tokenUploadTestReports`,formdata,httpOptions);
  }

}



