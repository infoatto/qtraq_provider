﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class TestService {
    constructor(private http: HttpClient) { }

   


getTestlist(page: number, pageSize: number,testfilter) 
{
const formData = new FormData();
formData.append('page',page.toString());   
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'testname':testfilter.testname,
'status':testfilter.teststatus,
})
}
return this.http.post<any>(environment.url+`listTest`
,formData,httpOptions)
}



createtest(test)
{

    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
   
    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
          'testname': test.testname,
          'status': test.status,
          'testmasterid':test.testid
       })
      };

return this.http.post<any>(environment.url+`addEditTest`
,'',httpOptions)
    
}


getedittestdata(testid)
{
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'tblname':'tbl_test_types_master',
        'comparecolumnname':'test_master_id',
        'comparecolumndata':testid
         })
         }



    return this.http.post<any>(environment.url+`getMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}
updatestatus(testid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_test_types_master',
'comparecolumnname':'test_master_id',
'comparecolumndata':testid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}





private handleError(error: HttpErrorResponse) 
{
console.error('server error:', error);
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}