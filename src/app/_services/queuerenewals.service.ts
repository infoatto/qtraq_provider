﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class QueuerenewalsService {
    constructor(private http: HttpClient) { }

   

getsubplanlist()
{

   
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let utoken=currentUser.utoken; 
    const httpOptions = {
 
headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
   
})
}
    return this.http.post<any>(environment.url+`listSubscribedPlans`
    ,'',httpOptions)


}

getplanlist()
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let utoken=currentUser.utoken; 
    const httpOptions = {
 headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
})
}
    return this.http.post<any>(environment.url+`getPlans`
    ,'',httpOptions)
}


subplan(razorpay_payment_id,org_id,plan_id)
{


    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    const formData = new FormData();
    formData.append('utoken',atob(utoken));  
    formData.append('loginuserid',atob(usermasterid));  
    formData.append('orgid',orgid);  
    formData.append('planid',plan_id);  
    formData.append('paymentmode','cheque');  
    formData.append('bankname','bankname');  
    formData.append('branchname','branchname');  
    formData.append('chequeno','chequeno');  
    formData.append('transactionid',razorpay_payment_id);  
/*   
let utoken=currentUser.utoken; 
const httpOptions = {
 headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'orgid':orgid,
})
}
*/

    return this.http.post<any>(environment.url+`addEditPlanSubscription`
    ,formData)
}




/*
getnotificationlist(page: number, pageSize: number,notificationfilter) 
{
const formData = new FormData();
formData.append('page',page.toString());   
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'notificationtitle':notificationfilter.notificationtitle,
'status':notificationfilter.notificationstatus,
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/listNotificationMaster`
,formData,httpOptions)
}



createnotification(notification)
{
const formData = new FormData();
formData.append('notificationtitle',notification.notificationtitle);  
formData.append('content',notification.content);  
formData.append('status',notification.status);  
formData.append('nid',notification.nid);  
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
 let utoken=currentUser.utoken; 
   const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
       })
      };
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addEditNotificationMaster`
,formData,httpOptions)
}


geteditnotificationdata(nid)
{
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'nid':nid,
    })
    }

    return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getNotificationMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}


updatestatus(testid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_test_types_master',
'comparecolumnname':'test_master_id',
'comparecolumndata':testid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/
changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}
*/




private handleError(error: HttpErrorResponse) 
{
console.error('server error:', error);
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}