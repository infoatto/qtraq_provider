﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class SystemroleService {
    constructor(private http: HttpClient) { }

   
getsystemrole()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let roletype = JSON.parse(localStorage.getItem('activerole'));
let orgid=orguserlist.orgid
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 

const httpOptions = {

    headers: new HttpHeaders({
    'utoken':atob(utoken),
   'orgid':orgid,
   'roletype':roletype.activerole,
   'loginuserid':atob(usermasterid)
    })
    }
    return this.http.post<any>(environment.url+`getSystemRoles`
    ,'',httpOptions)

}

submitsystemrole(body)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));

    const httpOptions = {

        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
       'orgid':orgid
        })
        }
        return this.http.post<any>(environment.url+`addEditSystemRoles`
        ,body,httpOptions)
}







getCountries()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
})
}
return this.http.post<any>(environment.url+`getCountries`
,'',httpOptions).pipe(catchError(this.handleError));
}


getcity(val)
{
const httpOptions = {
headers: new HttpHeaders({
'stateid':  val,
})
}
return this.http.post<any>(environment.url+`getCities`
,'',httpOptions).pipe(catchError(this.handleError));
}


getState(val)
{
const httpOptions = {
headers: new HttpHeaders({
'countryid':  val,
})
}
return this.http.post<any>(environment.url+`getState`
,'',httpOptions).pipe(catchError(this.handleError));
}


getdatabymobile(mobileno,countrycode)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'countryid':countrycode,
        'mobileno':mobileno,
        })
        }

        return this.http.post<any>(environment.url+`getQTRAQUserDetails`
        ,'',httpOptions).pipe(catchError(this.handleError));

}





addQTRAQSystemRoleUser(formValue:any, dob_date:any, conid:any, mobilenumber:any) {
	let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
	let usermasterid = currentUser.user_master_id;
	let utoken = currentUser.utoken;
	let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
	let orgid = orguserlist.orgid;
	var isl1:any;
	var isl2:any;
	var isorgadmin:any;
	if ((formValue.isl1 == true) || (formValue.isl1 == 'Yes')) {
		isl1 = "Yes";
	} else {
		isl1 = "No";
	}
	if ((formValue.isl2 == true) || (formValue.isl1 == 'Yes')) {
		isl2 = "Yes";
	} else {
		isl2 = "No";
	}
	if ((formValue.isorgadmin == true) || (formValue.isorgadmin == 'Yes')) {
		isorgadmin = "Yes";
	} else {
		isorgadmin = "No";
	}
	if (formValue.user_master_id) {
		const httpOptions = {
			headers: new HttpHeaders({
				'utoken': atob(utoken),
				'loginuserid': atob(usermasterid),
				'qtraquserid': "" + formValue.user_master_id + "",
				'orgid': orgid,
				'otp': "" + formValue.otp + "",
			})
		}
		const formData = new FormData();
		formData.append('countryid', conid);
		formData.append('mobileno', mobilenumber);
		formData.append('isl1', isl1);
		formData.append('isl2', isl2);
		formData.append('isorgadmin', isorgadmin);
		return this.http.post < any > (environment.url + `addQTRAQSystemRoleUser`, formData, httpOptions).pipe(catchError(this.handleError));
	} else {
		const formData = new FormData();
		formData.append('countryid', formValue.countryid);
		formData.append('mobileno', formValue.mobilenumber);
		formData.append('isl1', isl1);
		formData.append('isl2', isl2);
		formData.append('isorgadmin', isorgadmin);
		formData.append('cityid', formValue.city_id);
		formData.append('stateid', formValue.state_id);
		formData.append('firstname', formValue.firstname);
		formData.append('middlename', formValue.middlename);
		formData.append('lastname', formValue.lastname);
		formData.append('gender', formValue.gender);
		formData.append('email', formValue.emailid);
		formData.append('dob', dob_date);
		formData.append('autodob', formValue.autodob);
		formData.append('pincode', formValue.pincode);
		const httpOptions = {
			headers: new HttpHeaders({
				'utoken': atob(utoken),
				'loginuserid': atob(usermasterid),
				'otp': "" + formValue.otp + "",
				'orgid': orgid,
			})
		}
		return this.http.post < any > (environment.url + `addQTRAQSystemRoleUser`, formData, httpOptions).pipe(catchError(this.handleError));
	}
}







	generateOtp(onconvalueget,submobile){
		let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
		let usermasterid=currentUser.user_master_id;
		let utoken=currentUser.utoken; 
		let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
		let orgid=orguserlist.orgid
		const httpOptions = {
			headers: new HttpHeaders({
				'utoken':atob(utoken),
				'usermasterid':atob(usermasterid),
				'mobileno':submobile,
				'otptype':'registration',
				'usertype':'P',
				"countryid":onconvalueget,
				"orgid":orgid
			})
		}
		return this.http.post<any>(environment.url+`generateOtp`
		,'',httpOptions)
	}













private handleError(error: HttpErrorResponse) 
{
console.error('server error:', error);
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}