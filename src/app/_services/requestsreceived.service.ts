﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class RequestreceivedService {
    constructor(private http: HttpClient) { }


/*

getnotificationlist(page: number, pageSize: number,notificationfilter)
{
const formData = new FormData();
formData.append('page',page.toString());
let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'notificationtitle':notificationfilter.notificationtitle,
'status':notificationfilter.notificationstatus,
})
}
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/listNotificationMaster`
,formData,httpOptions)
}



createnotification(notification)
{

const formData = new FormData();
formData.append('notificationtitle',notification.notificationtitle);
formData.append('content',notification.content);
formData.append('status',notification.status);
formData.append('nid',notification.nid);

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
 let utoken=Adminuser.utoken;

    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
       })
      };

return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/addEditNotificationMaster`
,formData,httpOptions)

}


geteditnotificationdata(nid)
{
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'nid':nid,
    })
    }

    return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/getNotificationMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}


updatestatus(testid:string,status)
{
let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_test_types_master',
'comparecolumnname':'test_master_id',
'comparecolumndata':testid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(`http://attoinfotech.in/demo/qtraqservices/qtraqapi/
changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}
*/


private handleError(error: HttpErrorResponse)
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}





getrequestnewtokenlist(page,size,tokenproviderid)
{



    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'page':""+page+"",
    'orgid':orgid,
    'serviceproviderid':atob(usermasterid),
    'tokenproviderid':""+tokenproviderid+""
      })
    }
    return this.http.post<any>(environment.url+`listNewTokenRequested`
,'',httpOptions)

}






rescheduletokens(page,size,tokenproviderid)
{

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let roletype = JSON.parse(localStorage.getItem('activerole'));
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'page':""+page+"",
'orgid':orgid,
'serviceproviderid':atob(usermasterid),
'tokenproviderid':tokenproviderid
  })
}
return this.http.post<any>(environment.url+`listTokenRescheduleRequestes`
,'',httpOptions)

}





getProvidersList()
{

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let roletype = JSON.parse(localStorage.getItem('activerole'));
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'orgid':orgid,
    })
}
return this.http.post<any>(environment.url+`getProvidersList`
,'',httpOptions)
}

getrequestcancelledllist(page,size,tokenproviderid)
{

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let roletype = JSON.parse(localStorage.getItem('activerole'));
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'page':""+page+"",
'orgid':orgid,
'serviceproviderid':atob(usermasterid),
'tokenproviderid':tokenproviderid
  })
}
return this.http.post<any>(environment.url+`listCancelledTokens`
,'',httpOptions)

}


//new services code 2-4-2019




getTokenDetails(tokenid)
{

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let roletype = JSON.parse(localStorage.getItem('activerole'));
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'orgid':orgid,
'tokenid':tokenid
  })
}
return this.http.post<any>(environment.url+`getTokenDetails`
,'',httpOptions)

}








gettokenlist(date,providerid)
{

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
let roletype = JSON.parse(localStorage.getItem('activerole'));
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'orgid':orgid,
'providerid':providerid,
'requesteddate':date,
'usertype':'provider'
  })
}
return this.http.post<any>(environment.url+`getProviderAssignedQueues`
,'',httpOptions)

}



getmonitorqueQueues(queid,providerid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'queueid':queid,
    'requesteddate':providerid
      })
    }
    return this.http.post<any>(environment.url+`checkQueue`
    ,'',httpOptions)


}








getmonitorqueQueuesassign(providerid,date){
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
      headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':orgid,
        'providerid':providerid,
        'requesteddate':date,
      })
    }
    return this.http.post<any>(environment.url+`getProviderAssignedQueues`,'',httpOptions)
}



















getcustomlist(queueid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'orgid':orgid,
    'queueid':queueid,
    'fieldtype':'onassign'
      })
    }

    return this.http.post<any>(environment.url+`listCustomFields`
    ,'',httpOptions)


}


providerReturnToken(tokenmessage,tokentype,token_id)
{




    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));

    const formData = new FormData();
    formData.append('tokenmsg',tokenmessage);
    formData.append('requesttype',tokentype);

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'orgid':orgid,
    'tokenid':token_id,
     'requesttype':tokentype
      })
    }

    return this.http.post<any>(environment.url+`providerReturnToken`
    ,formData,httpOptions)

}


confirmtoken(formData,token_no,token_time,token_date,token_id,queue_id,tokenmessage)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'orgid':orgid,
    'tokenid':""+token_id+"",
    'queueid':""+queue_id+"",
    'tokenno':""+token_no+"",
    'tokentime':""+token_time+"",
    'tokendate':""+token_date+"",
    'requesttype':"newtoken"

      })
    }








    return this.http.post<any>(environment.url+`providerConfirmToken`
    ,formData,httpOptions)
}
//new service end

confirmtokenres(formData,token_no,token_time,token_date,token_id,queue_id,tokenmessage)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'orgid':orgid,
    'tokenid':""+token_id+"",
    'queueid':""+queue_id+"",
    'tokenno':""+token_no+"",
    'tokentime':""+token_time+"",
    'tokendate':""+token_date+"",
    'requesttype':"reschedule"

      })
    }

    return this.http.post<any>(environment.url+`providerConfirmToken`
    ,formData,httpOptions)
}


getTokenDPRDetails(tokenid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'orgid':orgid,
    'tokenid':tokenid,
 })
 }

    return this.http.post<any>(environment.url+`getTokenDPRDetails`
    ,'',httpOptions)

}






getTestTypes()
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
   })
 }
    return this.http.post<any>(environment.url+`getTestTypes`
    ,'',httpOptions)

}




getEnumValues()
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole,
    'getenumfor':'tokenamt'
    })
    }

    return this.http.post<any>(environment.url+`getEnumValues`
    ,'',httpOptions)



}




}