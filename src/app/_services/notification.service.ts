﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class NotificationService {
    constructor(private http: HttpClient) { }




getnotificationlist(page: number, pageSize: number,notificationfilter)
{
const formData = new FormData();
formData.append('page',page.toString());
let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
const httpOptions = {

headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'notificationtitle':notificationfilter.notificationtitle,
    'status':notificationfilter.notificationstatus,
    'appname':"QRM"
})
}
return this.http.post<any>(environment.url+`listNotificationMaster`
,formData,httpOptions)
}



createnotification(notification)
{

const formData = new FormData();
formData.append('notificationtitle',notification.notificationtitle);
formData.append('content',notification.content);
formData.append('status',notification.status);
formData.append('nid',notification.nid);

let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
 let utoken=Adminuser.utoken;

    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
       })
      };

return this.http.post<any>(environment.url+`addEditNotificationMaster`
,formData,httpOptions)

}


geteditnotificationdata(nid)
{
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'nid':nid,
    })
    }

    return this.http.post<any>(environment.url+`getNotificationMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}


updatestatus(testid:string,status)
{
let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=Adminuser.user_master_id;
let utoken=Adminuser.utoken;
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_test_types_master',
'comparecolumnname':'test_master_id',
'comparecolumndata':testid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}





private handleError(error: HttpErrorResponse)
{
console.error('server error:', error);
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}