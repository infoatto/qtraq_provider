﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class OverridemasterqueuesService {
    constructor(private http: HttpClient) { }


    

getTransactionQueuesDetails(providerid,queid,providerdate)
{



    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 



    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'roletype':roletype.activerole,
        'providerid':providerid,
        'queueid':queid,
        'queuedate':providerdate
       })
    }
        return this.http.post<any>(environment.url+`getTransactionQueuesDetails`
        ,'',httpOptions)
  
}





providerTransactionQueuesDetails()
{



    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

 
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'roletype':roletype.activerole,
        'providerid':atob(usermasterid)
       })
    }
        return this.http.post<any>(environment.url+`getTransactionQueuesDetails`
        ,'',httpOptions)
  
}












getorgque(page: number, pageSize: number) 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(environment.url+`getOrgProviders`
,'',httpOptions)
}





submitoverrideque(formdata,queue_id,queue_transaction_id_data)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'roletype':roletype.activerole,
        'providerid':atob(usermasterid)
      
       })
    }
    return this.http.post<any>(environment.url+`overrideMasterQueues`,formdata,httpOptions)
}



private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



}