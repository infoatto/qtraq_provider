import { Injectable, Output,EventEmitter } from '@angular/core';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class SidebarToggleService {
  @Output() toggleVal: EventEmitter<any> = new EventEmitter();
  constructor() { }
  gettoggleVal(){
    return this.toggleVal;
  }

  assignToggleVal(passed:any = true){
    this.toggleVal.emit(passed);
  }
}
