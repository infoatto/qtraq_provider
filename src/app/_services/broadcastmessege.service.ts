﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { User } from "../_models";
import { HttpHeaders } from "@angular/common/http";
import { ListTest } from "../interface/testinterface";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";

//@Injectable({ providedIn: 'root' })
@Injectable()
export class BroadcastmessegeService {
  constructor(private http: HttpClient) {}
 
  private handleError(error: HttpErrorResponse) {
    console.error("server error:", error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || "Node.js server error");
  }
}
