﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class TodaysqueueService {
    constructor(
        private http: HttpClient
    ) { 

    }

    private user_data:any;
    setData(data:any){
        this.user_data = data;
    }

    getSharedData(){
        let temp = this.user_data;
        this.clearData();
        return temp;
    }

    clearData(){
        this.user_data = undefined;
    }
   
    getorgque(page: number, pageSize: number){
        const httpOptions = {
            headers: new HttpHeaders({
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`getOrgProviders`,'',httpOptions)
    }

    getongoingqueue(user_master_provider_id){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':user_master_provider_id,
            })
        }
        return this.http.post<any>(environment.url+`providerTodaysQueues`,'',httpOptions)
    }

    getongoingtransactionqueue(transactionid){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':transactionid,
                'tokentypefilter':'all'
            })
        }
        return this.http.post<any>(environment.url+`monitorOngoingQueue`,'',httpOptions)
    }
        
    getproviderongoingtransactionqueue(){
        const httpOptions = {
            headers: new HttpHeaders({
                'tokentypefilter':'all'
            })
        }
        return this.http.post<any>(environment.url+`monitorOngoingQueue`,'',httpOptions)
    }

    getongoingtodayslater(transactionid){
       const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':transactionid
            })
        }
        return this.http.post<any>(environment.url+`transactionQueueTokens`,'',httpOptions)
    }
       
    getongoingtransactiontype(tokentype,transactionid,page){ 
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':transactionid,
                'tokentypefilter':tokentype,
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`monitorOngoingQueue`,'',httpOptions)
    }

    getongoingtransactiontypesortbyarrival(sortby,tokentype,transactionid,page){ 
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':transactionid,
                'tokentypefilter':tokentype,
                'sortbyarrival':sortby,
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`monitorOngoingQueue`,'',httpOptions)
    }

    getproviderongoingtransactiontype(tokentype,page){
        const httpOptions = {
            headers: new HttpHeaders({
                'tokentypefilter':tokentype,
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`monitorOngoingQueue`,'',httpOptions)
    }

    markDrArrived(transactionid){
        const httpOptions = {
            headers: new HttpHeaders({
                'transactionqueueid':transactionid,
            })
        }
        return this.http.post<any>(environment.url+`markDrArrived`,'',httpOptions)
    }
    
    transactionQueueMessages(page: number,queuetransactionid){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queuetransactionid,
            })
        }
        return this.http.post<any>(environment.url+`transactionQueueMessages`,'',httpOptions)
    }

    addmessagesms(messege,queuetransactionid){
        const formData = new FormData();
        formData.append('queuemsg',messege);  
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queuetransactionid,
            })
        }
        return this.http.post<any>(environment.url+`addTransactionQueueMessage`,formData,httpOptions)
    }

    private handleError(error: HttpErrorResponse){
        if (error.error instanceof Error) {
            const errMessage = error.error.message;
            return Observable.throw(errMessage);
        }
        return Observable.throw(error || 'Node.js server error');
    }

    transactionQueueTokens(queue_transaction_id){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queue_transaction_id
            })
        }
        return this.http.post<any>(environment.url+`transactionQueueTokens`,'',httpOptions)
    }

    startTransactionQueue(queue_transaction_id){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queue_transaction_id
            })
        }
        return this.http.post<any>(environment.url+`startTransactionQueue`,'',httpOptions)
    }

    stopTransactionQueue(queue_transaction_id){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queue_transaction_id
            })
        }
        return this.http.post<any>(environment.url+`stopTransactionQueue`,'',httpOptions)
    }

    blockUnblockTransactionQueue(transaction_id,blockqueue){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':transaction_id,
                'blockqueue':blockqueue,
            })
        }
        return this.http.post<any>(environment.url+`blockUnblockTransactionQueue`,'',httpOptions)
    }

    editTokenInfo(formdata){
        return this.http.post<any>(environment.url+`editTokenInfo`,formdata)
    }

    getFollowUpTokens(providerid:any,providerorgid:any,followupdate:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':providerid,
                'providerorgid':providerorgid,
                'followupdate':followupdate
            })
        }
        return this.http.post<any>(environment.url+`getFollowUpTokens`,'',httpOptions)
    }

    exporttoexcel(queuetransactionid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':queuetransactionid
            })
        }
        return this.http.post<any>(environment.url+`exporttoexcel`,'',httpOptions)
    }

    deletedownloadedexcelfile(filename:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'filename':filename
            })
        }
        return this.http.post<any>(environment.url+`deletedownloadedexcelfile`,'',httpOptions)
    }

    exporttoexcelforrange(providerid:any,fromdate:any,todate:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':providerid,
                'fromdate':fromdate,
                'todate':todate
            })
        }
        return this.http.post<any>(environment.url+`exporttoexcelforrange`,'',httpOptions)
    }
}