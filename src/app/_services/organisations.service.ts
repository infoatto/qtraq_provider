﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//import { Injectable } from '@angular/core';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class OrganisationsService {
    constructor(private http: HttpClient) { }

   


getOrganisationslist(page: number, pageSize: number,organisationfilter) 
{
const formData = new FormData();
formData.append('page',page.toString());   
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'usermasterid':atob(usermasterid),
'orgname':organisationfilter.orgname,
'status':organisationfilter.orgstatus,
})
}
return this.http.post<any>(environment.url+`listOrganisations`
,formData,httpOptions)
}














createtest(test)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
   
    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':  atob(utoken),
          'usermasterid': atob(usermasterid),
          'testname': test.testname,
          'status': test.status,
          'testmasterid':test.testid
       })
      };

return this.http.post<any>(environment.url+`addEditTest`
,'',httpOptions)
    
}


getedittestdata(testid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':  atob(utoken),
        'usermasterid':atob(usermasterid),
        'tblname':'tbl_test_types_master',
        'comparecolumnname':'test_master_id',
        'comparecolumndata':testid
         })
         }

    return this.http.post<any>(environment.url+`getMasterDetails`
    ,'',httpOptions).pipe(catchError(this.handleError));

}



updatestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_organisation_master',
'comparecolumnname':'org_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}
//new code 1-3-2019



updateimagestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_organisation_images',
'comparecolumnname':'org_image_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}



updatedatetimestatus(orgid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_org_working_hours',
'comparecolumnname':'org_working_hour_id',
'comparecolumndata':orgid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }
return this.http.post<any>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}









//new code end 1-3-2019

getallorganisation()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
})
}
return this.http.post<any>(environment.url+`getOrganisations`
,'',httpOptions).pipe(catchError(this.handleError));
}


getSpecialities(orgid)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'orgid':""+orgid+"",
'usermasterid':  atob(usermasterid),
})
}
return this.http.post<any>(environment.url+`getSpecialities`
,'',httpOptions).pipe(catchError(this.handleError));
}


organisation(orgid,formValues,formValue,custom,orgtype)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 




  


    if(orgid!=0)     
{

    const httpOptions = {
        headers: new HttpHeaders({
          'utoken':atob(utoken),
    //      'usermasterid':atob(usermasterid),
          'loginuserid':atob(usermasterid),
          'orgtype':orgtype,
          'orgid':""+orgid+"",
           //'orgname':formValue.org_name,
          'parentid':formValue.ordanisationid,
          'monworking':custom.mon,
          'tueworking':custom.tue,
          'wedworking':custom.wen,
          'thuworking':custom.thu,
          'friworking':custom.fri,
          'satworking':custom.sat,
          'sunworking':custom.sun,
           })
          }


          

        return this.http.post<any>(environment.url+`editOrganisationProfile`
        ,formValues,httpOptions).pipe(catchError(this.handleError));
        
}   
     else
     {  


        const httpOptions = {
            headers: new HttpHeaders({
              'utoken':atob(utoken),
              'loginuserid':atob(usermasterid),
              'orgtype':formValue.orgtype,
          //    'orgname':formValue.org_name,
              'parentid':formValue.ordanisationid,
              'monworking':custom.mon,
              'tueworking':custom.tue,
              'wedworking':custom.wen,
              'thuworking':custom.thu,
              'friworking':custom.fri,
              'satworking':custom.sat,
              'sunworking':custom.sun,
               })
              }
    


return this.http.post<any>(environment.url+`addOrganisationProfile`
,formValues,httpOptions).pipe(catchError(this.handleError));
     }
}


editorganisationlocation(organisationid,formValue)
{

let roletype = JSON.parse(localStorage.getItem('activerole'));

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'orgid':""+organisationid+""
//'address1':formValue.address1,
//'address2':formValue.address2,
//'street':formValue.street,
//'area':formValue.area,
//'landmark':formValue.landmark,
//'countryid':formValue.countryid,
//'stateid':formValue.state_id,
//'stateid':formValue.stateid,
//'gpscords':formValue.gpscords,
//'cityid':formValue.city_id,
//'pincode':formValue.pincode
})
}


const formData = new FormData();
formData.append('address1',formValue.address1);  
formData.append('address2',formValue.address2);  
formData.append('street',formValue.street);  
formData.append('area',formValue.area);  
formData.append('landmark',formValue.landmark);  
formData.append('countryid',formValue.countryid);  
formData.append('stateid',formValue.state_id);  
//formData.append('stateid',formValue.stateid); 
formData.append('cityid',formValue.city_id);  
formData.append('pincode',formValue.pincode);  
formData.append('gpscords',formValue.gpscords);  



return this.http.post<any>(environment.url+`editOrgLocationDetails`
,formData,httpOptions).pipe(catchError(this.handleError));
}


//organisation location

   
editorganisationcontact(organisationid:any,formValue:any,editorganisationcontact:any){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 








    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':""+organisationid+"",
           // 'mobile1':formValue.mobile1,
           // 'mobile1public':editorganisationcontact.mobile1public,
           // 'mobile2':formValue.mobile2,
           // 'mobile2public':editorganisationcontact.mobile2public,
            //  'phone1':""+formValue.phone1+"",
           // 'phone1public':editorganisationcontact.phone1public,
           // 'phone2':formValue.phone2,
           // 'phone2public':editorganisationcontact.phone2public,
           // 'emailid1':formValue.emailid1,
         //   'emailid1public':editorganisationcontact.emailid1public,
          //  'emailid2':formValue.emailid2,
           // 'emailid2public':editorganisationcontact.emailid2public,
           // 'website':""+formValue.website+"",
        })
    }

    const formData = new FormData();
    formData.append('mobile1',formValue.mobile1);  
    formData.append('mobile1public',editorganisationcontact.mobile1public);  
    formData.append('mobile2',formValue.mobile2);  
    formData.append('mobile2public',editorganisationcontact.mobile2public);  
    formData.append('phone1',formValue.phone1);  

    formData.append('phone1public',editorganisationcontact.phone1public);  
    formData.append('phone2',formValue.phone2);  
    formData.append('phone2public',editorganisationcontact.phone2public);  
    formData.append('emailid1',formValue.emailid1);  
    formData.append('emailid1public',editorganisationcontact.emailid1public);  

    formData.append('emailid2',formValue.emailid2);  
    formData.append('emailid2public',editorganisationcontact.emailid2public);  
    formData.append('website',formValue.website);  





    return this.http.post<any>(environment.url+`editOrgContactDetails`,formData,httpOptions).pipe(catchError(this.handleError));
}









//organisation end


//location service start

getCountries()
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
})
}
return this.http.post<any>(environment.url+`getLiveCountries`
,'',httpOptions).pipe(catchError(this.handleError));
}


getcity(val)
{

const httpOptions = {
headers: new HttpHeaders({
'stateid':  val,
})
}
return this.http.post<any>(environment.url+`getLiveCities`
,'',httpOptions).pipe(catchError(this.handleError));

}


getState(val)
{

const httpOptions = {
headers: new HttpHeaders({
'countryid':  val,
})
}
return this.http.post<any>(environment.url+`getLiveState`
,'',httpOptions).pipe(catchError(this.handleError));

}



//location service end





private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}









geteditorganisation(orgid):Observable<any> 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 

let roletype = JSON.parse(localStorage.getItem('activerole'));


const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'orgid':orgid,
'roletype':roletype.activerole
})
}
return this.http.post<any>(environment.url+`getOrganisationProfile`
,'',httpOptions)
}




getOrganisationLocContactDetails(orgid):Observable<any> 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 

let roletype = JSON.parse(localStorage.getItem('activerole'));


const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'orgid':orgid,
'roletype':roletype.activerole
})
}
return this.http.post<any>(environment.url+`getOrganisationLocContactDetails`
,'',httpOptions)
}





getplandescription(orgid):Observable<any> 
{
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {

headers: new HttpHeaders({
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
'roletype':roletype.activerole,
'orgid':orgid
})
}
return this.http.post<any>(environment.url+`getPlans`
,'',httpOptions)
}



SubscribedPlans(orgid,planid,startdate,enddate):Observable<any> 
{




let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
let roletype = JSON.parse(localStorage.getItem('activerole'));



const formData = new FormData();
formData.append('utoken',atob(utoken));  
formData.append('loginuserid',atob(usermasterid));  
formData.append('orgid',orgid);  
formData.append('planid',planid);  
formData.append('roletype',roletype.activerole);  

//formData.append('subscriptionstartdate',startdate);  
//formData.append('subscriptionenddate',enddate);  

formData.append('paymentmode',"cheque");  
formData.append('bankname',"HDFC");  
formData.append('branchname',"HDFC Kalyan");  
formData.append('chequeno',"chequeno");  
formData.append('transactionid',"123456789");  


return this.http.post<any>(environment.url+`addEditPlanSubscription`
,formData)
}





listSubscribedPlans(orgid):Observable<any> 
{

    let roletype = JSON.parse(localStorage.getItem('activerole'));   
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'roletype':roletype.activerole,
'loginuserid':atob(usermasterid),
'orgid':orgid
})
}
return this.http.post<any>(environment.url+`listSubscribedPlans`
,'',httpOptions)
}

getQTRAQUserDetails(mobilenumber)
{
   // let roletype = JSON.parse(localStorage.getItem('activerole'));

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
    
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'mobileno':mobilenumber,
    'roletype':roletype.activerole
    })
    }
    return this.http.post<any>(environment.url+`getQTRAQUserDetails`
    ,'',httpOptions)
}






addQTRAQUserDetails(formValue,dob_date,org_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));



    const httpOptions = {
    
    headers: new HttpHeaders({
        /*
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    "orgid":"1",
    "mobileno":formValue.mobileno,
    "firstname":formValue.firstname,
    "middlename":formValue.middlename, 
    "lastname":formValue.lastname,
    "dob":dob_date,
    "gender":formValue.gender,
    "countryid":formValue.countryid,
    "stateid":formValue.stateid,
    "cityid":formValue.cityid,
    "pincode":formValue.pincode,
    "emailid":formValue.emailid,
    "isorgdefault":formValue.isorgdefault,
    "otp":formValue.otp
*/    
'utoken':atob(utoken),
'loginuserid':atob(usermasterid),
"orgid":""+org_id+"",
'roletype':roletype.activerole,
"mobileno":formValue.mobilenumber,
"firstname":formValue.firstname,
"middlename":formValue.middlename, 
"lastname":formValue.lastname,
"dob":dob_date,
"gender":formValue.gender,
"countryid":formValue.countryid,
"stateid":formValue.stateid,
"cityid":formValue.cityid,
"pincode":formValue.pincode,
"emailid":formValue.emailid,
"isorgdefault":formValue.isorgdefault,
"qtraquserid":formValue.qtraquserid,
"otp":""+formValue.otp+""
})
    }
    return this.http.post<any>(environment.url+`addQTRAQUser`
    ,'',httpOptions)


}





generateOtp(onconvalueget,submobile)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'mobileno':submobile,
    "countryid":onconvalueget
    })
    }
    return this.http.post<any>(environment.url+`generateOtp`
    ,'',httpOptions)


}


makedefaultuser(user_master_id,org_id)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'usermasterid':atob(usermasterid),
    'qtraquserid':user_master_id,
    })
    }
    return this.http.post<any>(environment.url+`generateOtp`
    ,'',httpOptions)


}



deleteimage(org_id,org_image_id)
{

    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':org_id,
         'imageid':org_image_id,
        })
        }
        return this.http.post<any>(environment.url+`deleteOrgImage`
        ,'',httpOptions)
}




saveworkinghoures(org_id,formdata)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    let roletype = JSON.parse(localStorage.getItem('activerole'));


    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype,
        'orgid':""+org_id+"",
        })
        }
        return this.http.post<any>(environment.url+`addEditOrganisationWorkingHrs`
        ,formdata,httpOptions)
}





getOrganizationType(){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken)
        })
    }
    return this.http.post<any>(environment.url+`getServiceTypes`,'',httpOptions)
}



//new code 5-22-2019 start



deleteorglogo(org_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':  atob(utoken),
    'loginuserid':  atob(usermasterid),
     'orgid':""+org_id+""
    })
    }
    return this.http.post<any>(environment.url+`deleteorglogo`
    ,'',httpOptions).pipe(catchError(this.handleError));


}



//new code 5-22-2019 end


}



