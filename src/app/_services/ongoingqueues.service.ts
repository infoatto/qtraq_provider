﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class OngoingqueuesService {
    constructor(private http: HttpClient) { }
   



      
getorgque(page: number, pageSize: number) 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(environment.url+`getOrgProviders`
,'',httpOptions)
}



getongoingqueue(user_master_provider_id)
{
    
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'providerid':user_master_provider_id,
    
})
}
return this.http.post<any>(environment.url+`providerTodaysQueues`
,'',httpOptions)
}

getongoingtransactionqueue(transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'queuetransactionid':transactionid,
    'tokentypefilter':'all'
})
}
return this.http.post<any>(environment.url+`monitorOngoingQueue`
,'',httpOptions)
   }

getongoingtransactiontype(tokentype,transactionid,page)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'queuetransactionid':transactionid,
    'tokentypefilter':tokentype,
    'page':""+page+""
})
}
return this.http.post<any>(environment.url+`monitorOngoingQueue`
,'',httpOptions)
}

markDrArrived(transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'transactionqueueid':transactionid,
    })
}
return this.http.post<any>(environment.url+`markDrArrived`
,'',httpOptions)
}

transactionQueueMessages(page: number,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype.activerole,
    })
}
return this.http.post<any>(environment.url+`transactionQueueMessages`
,'',httpOptions)
}

//add message start

addmessagesms(messege,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 


    const formData = new FormData();
    formData.append('queuemsg',messege);  


    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype.activerole
    })
}
return this.http.post<any>(environment.url+`addTransactionQueueMessage`
,formData,httpOptions)
}

//add message end







private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}
}