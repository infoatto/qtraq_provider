﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { commonInterceptor } from 'app/interceptors/common-interceptors';

@Injectable()
export class AssignnewtokenService {
    constructor(private http: HttpClient) { }

    getorgque(page: number, pageSize: number){
        const httpOptions = {
            headers: new HttpHeaders({
                'page':""+page+"",
            })
        }
        return this.http.post<any>(environment.url+`getOrgProviders`,'',httpOptions)
    }

    submitassignquecustom(formdata:any,queue_id:any,selectedproid:any,
        mobilesearchmasterid:any,selfname:any,tokenrequestorno:any,
        token_no:any,token_apitime:any,queselecteddate:any,self:any,
        bookingusername:any,regno:any,userid:any,usergendermale:any,
        searchmobileage:any,bookingtype:any,tokenmsg:any,bookingtypeselectedtype:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queueid':""+queue_id+"",
            })
        }
        return this.http.post<any>(environment.url+`providerAsignNewToken`,formdata,httpOptions)
    }

    submitassignquecustomforunknown(formdata,queue_id,selectedproid,mobilesearchmasterid,selfname,tokenrequestorno,token_no,token_apitime,queselecteddate,self,bookingusername,regno,usermaster,usergendermale,searchmobileage,bookingtype,tokenmsg,bookingtypeselectedtype){
        const httpOptions = {
            headers: new HttpHeaders({
                'queueid':queue_id,
            })
        }
        return this.http.post<any>(environment.url+`providerAsignNewToken`,formdata,httpOptions)
    }

    getcustomlist(event){
        const httpOptions = {
            headers: new HttpHeaders({
                'queueid':event,
                'fieldtype':'onassign',
            })
        }
        return this.http.post<any>(environment.url+`listCustomFields`,'',httpOptions)
    }

    getProviderQueues(date:any,selectedproid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':selectedproid,
                'requesteddate':date
            })
        }
        return this.http.post<any>(environment.url+`getProviderAssignedQueues`,'',httpOptions)
    }

    getmonitorqueQueues(date:any,queueid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queueid':queueid,
                'requesteddate':date,
            })
        }
        return this.http.post<any>(environment.url+`checkQueue`,'',httpOptions)
    }

    familymemberdata(bookingfor:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'listtype':bookingfor,
            })
        }
        return this.http.post<any>(environment.url+`getFamilyMembers`,'',httpOptions)
    }

    providergetQTRAQUserDetails(mobilenumber:any){
        let currentUser  = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid = currentUser.user_master_id;
        //common header end
        const httpOptions = {
            headers: new HttpHeaders({
                'mobileno':mobilenumber,
                'providerid':atob(usermasterid)
            })
        }
        return this.http.post<any>(environment.url+`getQTRAQUserDetails`,'',httpOptions)
    }


    getQTRAQUserDetails(mobilenumber:any,selectedproid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'mobileno':mobilenumber,
                'providerid':""+selectedproid+""
            })
        }
        return this.http.post<any>(environment.url+`getQTRAQUserDetails`,'',httpOptions)
    }

    getQTRAQfamilymember(userid){
        const httpOptions = {
            headers: new HttpHeaders({
                'userid':userid,
            })
        }
        return this.http.post<any>(environment.url+`getFamilyMembersForProvider`,'',httpOptions)
    }

    getQTRAQfamilymemberuserdetail(selectedproid:any,userid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'userid':selectedproid,
                'familymemberid':userid
            })
        }
        return this.http.post<any>(environment.url+`getFamilyMembersForProvider`,'',httpOptions)
    }

    gettokenmessage(page:any,tokenid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'tokenid':tokenid,
                'page':""+page+""
            })
        }
        return this.http.post<any>(environment.url+`getTokenMessages`,'',httpOptions)
    }

    dignosissubmit(mainform:any,formValue,tokenid,token_datefordig,token_timefordig,booking_for_user_id){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'requestedby':atob(usermasterid)
            })
        }
        return this.http.post<any>(environment.url+`tokenAddEditDiagnosis`,mainform,httpOptions);
    }

    prescriptionsubmit(mainform:any,formValue:any,tokenid:any,token_datefordig:any,token_timefordig:any,booking_for_user_id:any){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'tokenid':tokenid,
                'consumerid':booking_for_user_id,
                'tokendate':token_datefordig,
                'tokentime':token_timefordig,
                'remarks':formValue.complaints_text,
                'requestedby':atob(usermasterid)
            })
        }
        return this.http.post<any>(environment.url+`tokenAddEditPrescription`,mainform,httpOptions)
    }

    getTransactionQueueDatas(queue_id:any,token_date:any,assigntokenprovider:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queueid':queue_id,
                'providerid':assigntokenprovider,
                'queuedate':token_date
            })
        }
        return this.http.post<any>(environment.url+`getTransactionQueueDatas`,'',httpOptions)
    }

    transactionQueueTokens(transaction_id:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':""+transaction_id+""
            })
        }
        return this.http.post<any>(environment.url+`transactionQueueTokens`,'',httpOptions)
    }

    getQTRAQUserDetailsnew(formdata:any,selectedproid:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':""+selectedproid+""
            })
        }
        return this.http.post<any>(environment.url+`searchQTRAQUsersDetails`,formdata,httpOptions)
    }

    TransactionqueuesummaryComponent(queue_transaction_id:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'queuetransactionid':""+queue_transaction_id+""
            })
        }
        return this.http.post<any>(environment.url+`getTransactionQueueSummary`,'',httpOptions)
    }

    daterangeQueueSummary(providerid:any,providerorgid:any,queue_start_date:any,queue_end_date:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':providerid,
                'providerorgid':""+providerorgid+"",
                'startdate':""+queue_start_date+"",
                'enddate':""+queue_end_date+"",
            })
        }
        return this.http.post<any>(environment.url+`getTransactionQueueSummary`,'',httpOptions)
    }

    getHoldQueueDetails(transaction_id){
        const httpOptions = {
            headers: new HttpHeaders({
                'transactionqueueid':""+transaction_id+""
            })
        }
        return this.http.post<any>(environment.url+`getHoldQueueDetails`,'',httpOptions)
    }

    private handleError(error: HttpErrorResponse){
        if (error.error instanceof Error) {
            const errMessage = error.error.message;
            return Observable.throw(errMessage);
        }
        return Observable.throw(error || 'Node.js server error');
    }

    putonhold(transaction_id:any,nextmin:any,reson:any,hold:any){
        const formData = new FormData();
        formData.append('apiaction',hold);
        formData.append('transactionqueueid',transaction_id);
        formData.append('holdtime',nextmin);
        formData.append('holdreason',reson);
        return this.http.post<any>(environment.url+`holdQueue`,formData)
    }
}