﻿import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { getCountries,Plan,responsedata,ListPlan,editplanresponsedata } from '../interface'
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router, ActivatedRoute, Params } from "@angular/router";
//@Injectable({ providedIn: 'root' })
@Injectable()
export class CommonService {
    constructor(private http: HttpClient,private router: Router) { }   
    private notificationObj = new BehaviorSubject<any>({
        notification_count: '0',
    });
    
    setNotificationCount(user: any) {
        this.notificationObj.next(user);
    }

    getNotificationCount() {
        return this.notificationObj.asObservable();
    }
    
    getCountries(){
        return this.http.get<any>(environment.url+`api/getCountries`);
    }

    userLogOut(){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        const httpOptions = {
            headers: new HttpHeaders({
            'loginuserid':atob(utoken),
            'userid':""+atob(usermasterid)+"",
            'usertype':"P"
            })
        }
        return this.http.post<any>(environment.url+`userLogOut`,'',httpOptions);
    }

    getQrmDashboardDetails(){


        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid


        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':""+orgid+"",
            'serviceproviderid':atob(usermasterid)
            })
        }
        return this.http.post<any>(environment.url+`getQrmDashboardDetails`,'',httpOptions);


    }

    getLoginUserDetails()
    {

        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid


        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':""+orgid+""
            })
        }
        return this.http.post<any>(environment.url+`getLoginUserDetails`,'',httpOptions);


    }

    getorgrole()
    {
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid


        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':""+orgid+""
            })
        }
        return this.http.post<any>(environment.url+`getProviderOrgsRoles`,'',httpOptions);
    }

    getOrganisation()
    {

        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 

        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'usermasterid':atob(usermasterid),
            })
        }


        return this.http.post<any>(environment.url+`getOrganisations`,'',httpOptions);
    }

    getPlanlist(page: number, pageSize: number,planfilter):Observable<ListPlan> {
        const formData = new FormData();
        formData.append('page',page.toString());   
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':atob(utoken),
                'usermasterid':atob(usermasterid),
                'planname':planfilter.planname,
                'plantype':planfilter.plantype,
                'status':planfilter.status,
                'planstartdate':planfilter.planstartdate,
                'planenddate':planfilter.planenddate,
            })
        }
        return this.http.post<ListPlan>(environment.url+`qtraqapi/listPlans`,formData,httpOptions).pipe(catchError(this.handleError));
    }

    createplan(plan:Plan): Observable<responsedata> {
        if(plan.planid==0){
        const formData = new FormData();
        formData.append('plan_image',plan.plan_image_path);    
    const httpOptions = {
            headers: new HttpHeaders({
            'utoken':  atob(plan.utoken),
            'usermasterid': atob(plan.usermasterid),
            'planname':plan.planname,
            'plandescription':plan.plandescription,
            'planstartdate':plan.planstartdate,
            'planenddate':plan.planenddate,
            'plantokencount':plan.plantokencount,
            'planamount':plan.planamount,
            'plancurrency':plan.plancurrency,
            'plancountryid':plan.plancountryid,
            'plantype':plan.plantype,
            'status':plan.planstatus,
            'planimagename':plan.plan_image,
            'orgid':plan.Organisation
            })
        };
        return this.http.post<responsedata>(environment.url+`addEditPlans`
        ,formData,httpOptions).pipe(catchError(this.handleError));
        }
    }

    updateplan(plan){
        let planimage=""
        const formData = new FormData();
        formData.append('plan_image',plan.plan_image_path);   
            const httpOptions = {
                headers: new HttpHeaders({
                'utoken':  atob(plan.utoken),
                'usermasterid': atob(plan.usermasterid),
                'planname':plan.planname,
                'plandescription':plan.plandescription,
                'planstartdate':plan.planstartdate,
                'planenddate':plan.planenddate,
                'plantokencount':plan.plantokencount,
                'planamount':plan.planamount,
                'plancurrency':plan.plancurrency,
                'plancountryid':plan.plancountryid,
                'plantype':plan.plantype,
                'status':plan.planstatus,
                'planid':plan.plan_id,
                'planimagename':plan.plan_image,
                'orgid':plan.Organisation
                })
            };
        return this.http.post<any>(environment.url+`addEditPlans`,formData,httpOptions).pipe(catchError(this.handleError));
    }

    geteditplandata(planid){
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'usermasterid':  atob(usermasterid),
                'getsingle':'Yes',
                'planid':planid
            })
        }
        return this.http.post<editplanresponsedata>(environment.url+`qtraqapi/getPlans`,'',httpOptions).pipe(catchError(this.handleError));
    }


updatestatus(planid:string,status)
{
let currentUser = JSON.parse(localStorage.getItem('currentUser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 

//let panid=planid;



const httpOptions = {
headers: new HttpHeaders({
'utoken':  atob(utoken),
'usermasterid':  atob(usermasterid),
'tblname':'tbl_plan_master',
'comparecolumnname':'plan_id',
'comparecolumndata':planid,
'statuscolumnname':'status',
'statusdata':status,
 })
 }

 return this.http.post<responsedata>(environment.url+`changeStatus`
,'',httpOptions).pipe(catchError(this.handleError));
}











private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}



//Admin services start 1-7-2019


getorganisationlist(){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':  atob(utoken),
            'loginuserid':  atob(usermasterid),
            'usertype':"P",
            'apptype':"W"
        })
    }
    return this.http.post<any>(environment.url+`getProviderOrgs`,'',httpOptions).pipe(catchError(this.handleError));
}




saveLoginInfo(logintype)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
const httpOptions = {
    headers: new HttpHeaders({
        'firsttimelogin':  "No",
        'loginuserid':  atob(usermasterid),
    })
 }
 return this.http.post<any>(environment.url+`saveLoginInfo`
    ,'',httpOptions).pipe(catchError(this.handleError));
}



    backtohome()
    {
    this.router.navigate(['/'])
    }
//admin services ens 1-7-2018


    getProvidersList(){
        return this.http.post<any>(environment.url+`getProvidersList`,'').pipe(catchError(this.handleError));
    }

    getOrgProviderQueues(provider_id:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'providerid':  provider_id
            })
        };
        return this.http.post<any>(environment.url+`getOrgProviderQueues`,'',httpOptions).pipe(catchError(this.handleError));
    }

    importCustomFieldsForm(formValues:any){
        const httpOptions = {
            headers: new HttpHeaders({
                'fromqueueid':  formValues.current_queue_id,
                'toqueueid':  formValues.queue_id
            })
        };
        return this.http.post<any>(environment.url+`importCustomFields`,'',httpOptions).pipe(catchError(this.handleError));
    }

    
    getTokenNotifications(page:any){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':""+orgid+"",
            'serviceproviderid':atob(usermasterid),
            "page":page.toString()
            })
        }
        return this.http.post<any>(environment.url+`getTokenNotifications`,'',httpOptions);
    }


}