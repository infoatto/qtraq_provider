﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class WeeklydashboardService {
constructor(private http: HttpClient) { }
     
getorgque(page: number, pageSize: number) 
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken; 
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(environment.url+`getOrgProviders`
,'',httpOptions)
}



getongoingqueue(user_master_provider_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'providerid':'6',
    
})
}
return this.http.post<any>(environment.url+`providerTodaysQueues`
,'',httpOptions)
}

getongoingtransactionqueue(transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'queuetransactionid':transactionid,
    'tokentypefilter':'all'
})
}
return this.http.post<any>(environment.url+`monitorOngoingQueue`
,'',httpOptions)
   }
    




getongoingtransactiontype(tokentype,transactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,
    'queuetransactionid':transactionid,
    'tokentypefilter':tokentype
})
}
return this.http.post<any>(environment.url+`monitorOngoingQueue`
,'',httpOptions)

}


getProviderOrgs(loginuserid:any = ""){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid;
    let usermasterid :any;
    if(loginuserid != ""){
        usermasterid = loginuserid;
    }else{
        usermasterid=atob(currentUser.user_master_id);
    }
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
        'utoken':atob(utoken),
        'providerid':usermasterid,
        'orgid':orgid,
        'roletype':roletype.activerole,
    })
}
return this.http.post<any>(environment.url+`getProviderOrgs`
,'',httpOptions)

}




getProviderOrgMinMaxBookingDates(providerorgid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
        'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'providerorgid':providerorgid,
    })
}
return this.http.post<any>(environment.url+`getProviderOrgMinMaxBookingDates`
,'',httpOptions)

}







getProviderWorkingCalender(month,year,calendertype,orgid,loginuserid:any=""){
    if(calendertype=='Monthly'){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let roletype = JSON.parse(localStorage.getItem('activerole'));
        let utoken=currentUser.utoken; 
        var usermasterid :any;
        if(loginuserid != ""){
            usermasterid = loginuserid;
        }else{
            usermasterid= atob(currentUser.user_master_id);
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':atob(utoken),
                'providerid':usermasterid,
                'orgid':""+orgid+"",
                'searchorgid':""+orgid+"",
                'calendertype':"monthly",
                'monthname':""+month+"",
                'yearname':""+year+"",
            })
        }
        return this.http.post<any>(environment.url+`getProviderWorkingCalender`,'',httpOptions)
    }else{
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let roletype = JSON.parse(localStorage.getItem('activerole'));
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        let utoken=currentUser.utoken; 
        var usermasterid :any;
        if(loginuserid != ""){
            usermasterid = loginuserid;
        }else{
            usermasterid=atob(currentUser.user_master_id);
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':atob(utoken),
                'providerid':usermasterid,
                'orgid':orgid,
                'searchorgid':""+orgid+"",
                'calendertype':"yearly",
                'yearname':""+year+"",
            })
        }
        return this.http.post<any>(environment.url+`getProviderWorkingCalender`,'',httpOptions)
    }
}
    
transactionQueueMessages(page: number,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype.activerole,
    })
}
return this.http.post<any>(environment.url+`transactionQueueMessages`
,'',httpOptions)
}

//add message start

addmessagesms(messege,queuetransactionid)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken; 

    const formData = new FormData();
    formData.append('queuemsg',messege);  

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'queuetransactionid':queuetransactionid,
    'roletype':roletype.activerole
    })
}
return this.http.post<any>(environment.url+`addTransactionQueueMessage`
,formData,httpOptions)
}





//add message end

private handleError(error: HttpErrorResponse) 
{
if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}

}