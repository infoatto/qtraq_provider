﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


function _window() : any {
    return window;
 }
 

@Injectable()
export class WindowRef {
    get nativeWindow() : any {
        return _window();
     }
}