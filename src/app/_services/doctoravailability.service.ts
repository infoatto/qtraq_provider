﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

//@Injectable({ providedIn: 'root' })
@Injectable()
export class DoctoravailabilityService {
    constructor(private http: HttpClient) { }

    doctoravailability(page,size,formdata)
{




    const formData = new FormData();

    formData.append('page',page.toString());
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;


var monthdate
var yeaardate

if(formdata.value==undefined)
{

    monthdate=formdata.month
    yeaardate=formdata.year

}
else{

    monthdate=formdata.value.month
    yeaardate=formdata.value.year

}







let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole
    if(activerolel1=="l2")
    {

        const httpOptions = {
            headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':orgid,
            'roletype':roletype.activerole,
            'page':""+page+"",
            'providerid':atob(usermasterid),
            'listfor':'dr',
            'monthfilter':""+monthdate+"",
            'yearfilter':""+yeaardate+"",
        })
        }

        return this.http.post<any>(environment.url+`listUnavailability`
        ,formData,httpOptions)

    }
else{


//if(formdata.value.month)
//{



    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
       'orgid':orgid,
    'roletype':roletype.activerole,
    'page':""+page+"",
    'listfor':'manager',
    'monthfilter':""+monthdate+"",
    'yearfilter':""+yeaardate+"",
})
}



return this.http.post<any>(environment.url+`listUnavailability`
,formData,httpOptions)

//}

//}

}






}


    adddoctoravailability(formdata,providerid){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let roletype = JSON.parse(localStorage.getItem('activerole'));
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken;
        let broadcasttoallqueue=""
        if(formdata.broadcasttoallqueue==true){
            broadcasttoallqueue="Yes"
        }else{
            broadcasttoallqueue="No"
        }
        let canceltokens
        if(formdata.canceltokens==true){
            canceltokens="Yes"
        }else{
            canceltokens="No"
        }
        let applaytoall
        if(formdata.applytoall==true){
            applaytoall="Yes"
        }else{
            applaytoall="No"
        }
        if(formdata.todate==''){
            const httpOptions = {
            headers: new HttpHeaders({
                'utoken':atob(utoken),
                'loginuserid':atob(usermasterid),
                'orgid':orgid,
                'roletype':roletype.activerole,
                'queueid':formdata.queue_id,
                // 'providerid':providerid,
                //'unavailabilitydaytype':formdata.unavailabilitydaytype,
                //'fromdate':formdata.fromdata.formatted,
                // 'fullday':formdata.fullday,
                //'leavetimetype':'until',
                //'leavetime':formdata.leavetime,
                //'unavailabilitytype':formdata.unavailabilitytype,
                //'unavailabilityreason':formdata.unavailabilityreason,
                //'canceltokens':canceltokens,
                //'broadcasttoallqueue':broadcasttoallqueue,
                //'broadcastedmessage':formdata.broadcastedmessage,

                })
            }

            const formData = new FormData();
            formData.append('unavailabilitydaytype',formdata.unavailabilitydaytype);
            formData.append('fromdate',formdata.fromdata.formatted,);
            formData.append('fullday',formdata.fullday,);
            formData.append('leavetimetype','until');
            formData.append('leavetime',formdata.leavetime);
            formData.append('unavailabilitytype',formdata.unavailabilitytype);
            formData.append('unavailabilityreason',formdata.unavailabilityreason);
            formData.append('canceltokens',canceltokens);
            formData.append('broadcasttoallqueue',broadcasttoallqueue);
            formData.append('broadcastedmessage',formdata.broadcastedmessage);
            formData.append('providerid',providerid);
            formData.append('applytoallorg',applaytoall);
            formData.append('block_further_appointments',formdata.block_further_appointments)
            formData.append('queueid',formdata.queue_id)
            return this.http.post<any>(environment.url+`setUnavailability`,formData,httpOptions)
        }else{
            var todate
            if(formdata.todate.formatted==undefined){
                todate=formdata.todate
            }else{
                todate=formdata.todate.formatted
            }
            const httpOptions = {
                headers: new HttpHeaders({
                    'utoken':atob(utoken),
                    'loginuserid':atob(usermasterid),
                    'orgid':orgid,
                    'roletype':roletype.activerole,
                    'queueid':formdata.queue_id,
                    //'providerid':providerid,
                    //'unavailabilitydaytype':formdata.unavailabilitydaytype,
                    //'fromdate':formdata.fromdata.formatted,
                    //'todate':formdata.todate.formatted,
                    //'fullday':formdata.fullday,
                    //'leavetimetype':"until",
                    //'leavetime':formdata.leavetime,
                    //'unavailabilitytype':formdata.unavailabilitytype,
                    //'unavailabilityreason':formdata.unavailabilityreason,
                    // 'canceltokens':canceltokens,
                    // 'broadcasttoallqueue':broadcasttoallqueue,
                    // 'broadcastedmessage':formdata.broadcastedmessage,
                })
            }
            const formData = new FormData();
            formData.append('unavailabilitydaytype',formdata.unavailabilitydaytype);
            formData.append('fromdate',formdata.fromdata.formatted);
            formData.append('todate',todate);
            formData.append('fullday',formdata.fullday);
            formData.append('leavetimetype','until');
            formData.append('leavetime',formdata.leavetime);
            formData.append('unavailabilitytype',formdata.unavailabilitytype);
            formData.append('unavailabilityreason',formdata.unavailabilityreason);
            formData.append('canceltokens',canceltokens);
            formData.append('broadcasttoallqueue',broadcasttoallqueue);
            formData.append('broadcastedmessage',formdata.broadcastedmessage);
            formData.append('providerid',providerid);
            formData.append('applytoallorg',applaytoall);
            formData.append('block_further_appointments',formdata.block_further_appointments)
            formData.append('queueid',formdata.queue_id)
            return this.http.post<any>(environment.url+`setUnavailability`,formData,httpOptions)
        }
    }







addproviderdoctoravailability(formdata)
{
   let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
if(formdata.todate=='')
{

     const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'orgid':orgid,
    'roletype':roletype.activerole,

 //   'providerid':atob(usermasterid),
 //  'unavailabilitydaytype':formdata.unavailabilitydaytype,
 //  'fromdate':formdata.fromdata.formatted,
 //   'fullday':formdata.fullday,
  // 'leavetimetype':'until',
  // 'leavetime':formdata.leavetime,
  // 'unavailabilitytype':formdata.unavailabilitydaytype,
  // 'unavailabilityreason':formdata.unavailabilityreason,
  // 'canceltokens':formdata.canceltokens,
 //  'broadcasttoallqueue':formdata.broadcasttoallqueue,
  // 'broadcastedmessage':formdata.broadcastedmessage,
   'applytoall':formdata.applytoall


})
}



const formData = new FormData();
formData.append('unavailabilitydaytype',formdata.unavailabilitydaytype);
formData.append('fromdate',formdata.fromdata.formatted);
formData.append('fullday',formdata.fullday);
formData.append('leavetimetype','until');
formData.append('leavetime',formdata.leavetime);
formData.append('unavailabilitytype',formdata.unavailabilitydaytype);
formData.append('unavailabilityreason',formdata.unavailabilityreason);
formData.append('canceltokens',formdata.canceltokens);
formData.append('broadcasttoallqueue',formdata.broadcasttoallqueue);
formData.append('broadcastedmessage',formdata.broadcastedmessage);
formData.append('providerid',atob(usermasterid));
formData.append('applytoallorg',formdata.applytoall);








return this.http.post<any>(environment.url+`setUnavailability`
,formData,httpOptions)
}
else
{

    const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'orgid':orgid,
        'roletype':roletype.activerole,
      //  'providerid':atob(usermasterid),
   //    'unavailabilitydaytype':formdata.unavailabilitydaytype,
      // 'fromdate':formdata.fromdata.formatted,
    //   'todate':formdata.todate.formatted,
     //  'fullday':formdata.fullday,
   //    'leavetimetype':"until",
     //  'leavetime':formdata.leavetime,
    //   'unavailabilitytype':formdata.unavailabilitydaytype,
    //   'unavailabilityreason':formdata.unavailabilityreason,
    //   'canceltokens':formdata.canceltokens,
    //   'broadcasttoallqueue':formdata.broadcasttoallqueue,
    //   'broadcastedmessage':formdata.broadcastedmessage,
      'applytoallorg':formdata.applytoall
    })
    }


    const formData = new FormData();
    formData.append('unavailabilitydaytype',formdata.unavailabilitydaytype);
    formData.append('fromdate',formdata.fromdata.formatted);
    formData.append('todate',formdata.todate.formatted);
    formData.append('fullday',formdata.fullday);
    formData.append('leavetimetype','until');
    formData.append('leavetime',formdata.leavetime);
    formData.append('unavailabilitytype',formdata.unavailabilitydaytype);
    formData.append('unavailabilityreason',formdata.unavailabilityreason);
    formData.append('canceltokens',formdata.canceltokens);
    formData.append('broadcasttoallqueue',formdata.broadcasttoallqueue);
    formData.append('broadcastedmessage',formdata.broadcastedmessage);
    formData.append('providerid',atob(usermasterid));
    formData.append('applytoallorg',formdata.applytoall);

    return this.http.post<any>(environment.url+`setUnavailability`
    ,formData,httpOptions)

}
}
















private handleError(error: HttpErrorResponse)
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}


getorgque(page: number, pageSize: number)
{
let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
let usermasterid=currentUser.user_master_id;
let utoken=currentUser.utoken;
let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
let orgid=orguserlist.orgid
const httpOptions = {
headers: new HttpHeaders({
'utoken':atob(utoken),
'orgid':orgid,
'page':""+page+""
})
}
return this.http.post<any>(environment.url+`getOrgProviders`
,'',httpOptions)
}








    viewimpact(formDetailData:any,providerid){
        let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken;
        let roletype = JSON.parse(localStorage.getItem('activerole'));
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
        let applaytoall
        if(formDetailData.applytoall==true){
            applaytoall="Yes"
        }else{
            applaytoall="No"
        }
        if((formDetailData.fromdata!="")&&(formDetailData.todate!="")){
            const httpOptions = {
                headers: new HttpHeaders({
                    'utoken':atob(utoken),
                    'loginuserid':atob(usermasterid),
                    'orgid':orgid,
                    'roletype':roletype.activerole,
                    'providerid':providerid,
                    'unavailabilitydaytype':formDetailData.unavailabilitydaytype,
                    'fromdate':formDetailData.fromdata.formatted,
                    'todate':formDetailData.todate.formatted,
                    'fullday':formDetailData.fullday,
                    'leavetimetype':formDetailData.leavetimetype,
                    'leavetime':formDetailData.leavetime,
                    'applytoallorg':applaytoall
                })
            }
            return this.http.post<any>(environment.url+`getUnavailabilityImpact`,'',httpOptions)
        }else if(formDetailData.fromdata!=""){
            const httpOptions = {
                headers: new HttpHeaders({
                    'utoken':atob(utoken),
                    'loginuserid':atob(usermasterid),
                    'orgid':orgid,
                    'roletype':roletype.activerole,
                    'providerid':providerid,
                    'unavailabilitydaytype':formDetailData.unavailabilitydaytype,
                    'fromdate':formDetailData.fromdata.formatted,
                    'fullday':formDetailData.fullday,
                    'leavetimetype':formDetailData.leavetimetype,
                    'leavetime':formDetailData.leavetime,
                    'applytoallorg':applaytoall
                })
            }
            return this.http.post<any>(environment.url+`getUnavailabilityImpact`,'',httpOptions)
        }else if(formDetailData.todate!=""){
            const httpOptions = {
                headers: new HttpHeaders({
                    'utoken':atob(utoken),
                    'loginuserid':atob(usermasterid),
                    'orgid':orgid,
                    'roletype':roletype.activerole,
                    'providerid':providerid,
                    'unavailabilitydaytype':formDetailData.unavailabilitydaytype,
                    'fromdate':formDetailData.fromdata.formatted,
                    'fullday':formDetailData.fullday,
                    'leavetimetype':formDetailData.leavetimetype,
                    'leavetime':formDetailData.leavetime,
                    'applytoallorg':applaytoall
                })
            }
            return this.http.post<any>(environment.url+`getUnavailabilityImpact`,'',httpOptions)
        }else{
            const httpOptions = {
                headers: new HttpHeaders({
                    'utoken':atob(utoken),
                    'loginuserid':atob(usermasterid),
                    'orgid':orgid,
                    'roletype':roletype.activerole,
                    'providerid':providerid,
                    'unavailabilitydaytype':formDetailData.unavailabilitydaytype,
                    'fullday':formDetailData.fullday,
                    'leavetimetype':formDetailData.leavetimetype,
                    'leavetime':formDetailData.leavetime,
                    'applytoallorg':applaytoall
                })
            }
            return this.http.post<any>(environment.url+`getUnavailabilityImpact`,'',httpOptions)
        }

    }












doctor_unavailability(doctor_unavailability_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
       'orgid':orgid,
    'roletype':roletype.activerole,
   'unavailabilityid':doctor_unavailability_id
})

    }
return this.http.post<any>(environment.url+`listUnavailability`
,'',httpOptions)

}







changeUnavailability(doctor_unavailability_id:any){
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole
    const formData = new FormData();
    formData.append('status',"In-active");
    formData.append('unavailabilityid',doctor_unavailability_id);
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'orgid':orgid,
            'roletype':roletype.activerole,
            'unavailabilityid':""+doctor_unavailability_id+"",
        })
    }
    return this.http.post<any>(environment.url+`changeUnavailability`,formData,httpOptions)
}





getUnavailabilityDetails(doctor_unavailability_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
       'orgid':orgid,
    'roletype':roletype.activerole,
   'unavailabilityid':doctor_unavailability_id

})

    }
return this.http.post<any>(environment.url+`getUnavailabilityDetails`
,'',httpOptions)

}





changeUnavailabilityupdate(doctor_unavailability_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole

    const formData = new FormData();
    formData.append('status',"In-active");

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
       'orgid':orgid,
    'roletype':roletype.activerole,
   'unavailabilityid':doctor_unavailability_id,

})

    }
return this.http.post<any>(environment.url+`changeUnavailability`
,formData,httpOptions)

}





changeUnavailabilityupdatedata(formdata,doctor_unavailability_id)
{
    let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let usermasterid=currentUser.user_master_id;
    let utoken=currentUser.utoken;
    let activerole = JSON.parse(localStorage.getItem("activerole"));
    var activerolel1 = activerole.activerole



/*

  const formData = new FormData();
    formData.append('unavailabilitydaytype',formdata.unavailabilitydaytype);
    formData.append('fromdate',formdata.fromdata.formatted);
    formData.append('todate',formdata.todate.formatted);
    formData.append('fullday',formdata.fullday);
    formData.append('leavetimetype','until');
    formData.append('leavetime',formdata.leavetime);
    formData.append('unavailabilitytype',formdata.unavailabilitydaytype);
    formData.append('unavailabilityreason',formdata.unavailabilityreason);
    formData.append('canceltokens',formdata.canceltokens);
    formData.append('broadcasttoallqueue',formdata.broadcasttoallqueue);
    formData.append('broadcastedmessage',formdata.broadcastedmessage);
    formData.append('providerid',atob(usermasterid));
    formData.append('applytoallorg',formdata.applytoall);


*/



    const formData = new FormData();
    formData.append('unavailabilityid',doctor_unavailability_id);
    formData.append('status',"Active");
    formData.append('unavailabilitytype',formdata.unavailabilitytype);
    formData.append('unavailabilityreason',formdata.unavailabilityreason);
    formData.append('blockfurtherappointment',formdata.block_further_appointments);


    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
       'orgid':orgid,
    'roletype':roletype.activerole,
   'unavailabilityid':doctor_unavailability_id,

})

    }
return this.http.post<any>(environment.url+`changeUnavailability`
,formData,httpOptions)

}

getProviderAssignedQueuesAll(providerid:any){
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken;
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
      headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':orgid,
        'providerid':providerid,
      })
    }
    return this.http.post<any>(environment.url+`getProviderAssignedQueuesAll`,'',httpOptions)
}


}