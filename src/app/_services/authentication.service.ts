﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { Loginresponse } from '../interface'
import { _throw } from 'rxjs/observable/throw';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()

export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        //let currentUser = JSON.parse(localStorage.getItem('currentTokens'));
        let currentUser:any;
        let currentUser1 = localStorage.getItem('currentTokens');
        if (currentUser1 != "") {
            currentUser = 0;
        }else {
            currentUser = currentUser1
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'username': username,
                'password': password,
                'fcmtoken': currentUser,
                'usertype': 'P',
                'apptype': 'W'
            })
        };
        return this.http.post<any>(environment.url + "userLogin","", httpOptions).pipe(map(Loginresponse => {
            if (Loginresponse.status_code = 200) {
                if (Loginresponse.Data) {
                    if (Loginresponse) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        var plan ={
                            "utoken": btoa(Loginresponse.Data.utoken),
                            "user_name": btoa(Loginresponse.Data.user_name),
                            "user_master_id": btoa(Loginresponse.Data.user_master_id),
                            "org_id": btoa(Loginresponse.Data.org_id),
                            "first_name": btoa(Loginresponse.Data.first_name),
                            "middle_name": btoa(Loginresponse.Data.middle_name),
                            "last_name": btoa(Loginresponse.Data.last_name),
                            "profile_pic": btoa(Loginresponse.Data.profile_pic),
                            "email_id": btoa(Loginresponse.Data.email_id),
                            "system_user": btoa(Loginresponse.Data.system_user),
                            "system_role_id": btoa(Loginresponse.Data.system_role_id),
                            "gender": btoa(Loginresponse.Data.gender),
                            "system_roles": btoa(Loginresponse.Data.system_roles),
                            "profileimage_path": btoa(Loginresponse.Data.profileimage_path),
                            "provider_specialities": btoa(Loginresponse.Data.provider_specialities),
                            "country_id": Loginresponse.Data.country_id,
                        }
                        localStorage.setItem('Adminuser', JSON.stringify(plan));
                    }
                }
                return Loginresponse;
            }else{
                localStorage.clear();
                return Loginresponse
            }
        }));
    }



    forgotPasswordgetotp(mobilenumber) {
        const httpOptions = {
            headers: new HttpHeaders({
                'mobileno': ""+mobilenumber+"",
                'action': 'generateOtp',
                'otptype':'resetpassword',
                'usertype':'P'
            })
        }
        return this.http.post<any>(environment.url + `forgotPassword`, '', httpOptions)
    }

    forgotPasswordsave(newpassword:any,confirmpassword:any,otp:any,mobile:any) {
        console.log("called");
        const httpOptions = {
            headers: new HttpHeaders({
                'newpassword': newpassword,
                'confirmpassword': confirmpassword,
                'otp': otp,
                'mobileno': ""+mobile+"",
                'action': 'saveNewPassword',
            })
        }
        return this.http.post<any>(environment.url + `forgotPassword`, '', httpOptions)
    }





    /*
    login(username: string, password: string) {
        return this.http.get<any>("http://attoinfotech.in/demo/mitashiservices/mitashiapi/getHomeSettings");
    }
    */


    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('Adminuser');
        localStorage.removeItem('orguserrole');
    }
}