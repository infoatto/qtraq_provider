﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

import { User } from '../_models';
import { HttpHeaders } from '@angular/common/http';
import { ListTest } from '../interface/testinterface'
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
//@Injectable({ providedIn: 'root' })
@Injectable()
export class SearchtokensService {
    constructor(private http: HttpClient) { }



tokensearchlist(page,size,formvalue) 
{

var fromdate=""
var todate=""

/*
if(formvalue.fromdate!="")
{
    fromdate=formvalue.fromdate.formatted
}
else
{
    fromdate=""
}


if(formvalue.todate!="")
{
    todate=formvalue.todate.formatted
}
else
{
    todate=""
}
*/


        let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=Adminuser.user_master_id;

const httpOptions = {
    headers: new HttpHeaders({
        'providerid':atob(usermasterid),
        'serviceproviderid':atob(usermasterid),
        'consumername':formvalue.patient,
        'page':""+page+"",
        'tokenhistorytype':formvalue.tokentype,
        'fromdate':fromdate,
        'todate':todate
    })
}
        
return this.http.post<any>(environment.url+`providerSearchUserTokens`
,'',httpOptions)
    


    }


private handleError(error: HttpErrorResponse) 
{

if (error.error instanceof Error) {
const errMessage = error.error.message;
return Observable.throw(errMessage);
}
    return Observable.throw(error || 'Node.js server error');
}


gettokendetail(tokentype,token_id){
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'roletype':roletype.activerole, 
            'orgid':orgid,
            'tokenid':token_id
        })
    }
    return this.http.post<any>(environment.url+`getTokenDetails`,'',httpOptions);
}


getEnumValues()
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype.activerole, 
    'getenumfor':'tokenamt'
    })
    }
        
    return this.http.post<any>(environment.url+`getEnumValues`
    ,'',httpOptions)



}




    getcustomlist(event)
    {
     let currentUser = JSON.parse(localStorage.getItem('Adminuser'));
        let usermasterid=currentUser.user_master_id;
        let utoken=currentUser.utoken; 
    
        let roletype = JSON.parse(localStorage.getItem('activerole'));
        let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
        let orgid=orguserlist.orgid
     

        
     
        const httpOptions = {
        headers: new HttpHeaders({
        'utoken':atob(utoken),
        'loginuserid':atob(usermasterid),
        'roletype':roletype.activerole,
        'orgid':orgid,
        'queueid':event,
        'fieldtype':'all',
         })
        }
return this.http.post<any>(environment.url+`listCustomFields`
        ,'',httpOptions)
       
    }







markasnoshow(tokenid,queue_transaction_id)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
    'transactionqueueid':queue_transaction_id,
    'tokenid':tokenid,
    })
    }
        
    return this.http.post<any>(environment.url+`tokenMarkNoShow`
    ,'',httpOptions)
    

}

tokenRACMarkConfirm(tokenid,queue_transaction_id){
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'roletype':roletype, 
            'orgid':orgid,
            'transactionqueueid':queue_transaction_id,
            'tokenid':tokenid,
        })
    }
    return this.http.post<any>(environment.url+`tokenRACMarkConfirm`
    ,'',httpOptions)
}


markasarrived(tokenid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
     'tokenid':tokenid,
    })
    }
        
    return this.http.post<any>(environment.url+`tokenMarkArrived`
    ,'',httpOptions)
    

}

getChangeSequenceReasons()
{
    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
   
    })
    }
        
    return this.http.post<any>(environment.url+`getChangeSequenceReasons`
    ,'',httpOptions)

}

getTransactionTokenList(transactionqueueid,currenttokenid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
     'transactionqueueid':transactionqueueid,
     'currenttokenid':currenttokenid,
    })
    }
        
    return this.http.post<any>(environment.url+`getTransactionTokenList`
    ,'',httpOptions)
    

}


    changesequencesubmit(reson:any,token_id:any,transactionid:any,changeseqtokenid:any,position:string){
        const formData = new FormData();
        formData.append('reason',reson);
        const httpOptions = {
            headers: new HttpHeaders({
                'transactionqueueid':transactionid,
                'tokenid':token_id,
                'aftertokensequence':""+changeseqtokenid+"",
                'position':position
            })
        }
        return this.http.post<any>(environment.url+`tokenChangeSequence`,formData,httpOptions)
    }




cancelsubmit(reson,token_id,transactionid,changeseqtokenid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const formData = new FormData();
    formData.append('reason',reson);

    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
     'transactionqueueid':transactionid,
     'tokenid':token_id,
     'requestedby':"provider",
   
     
    })
    }
        
    return this.http.post<any>(environment.url+`cancelToken`
    ,formData,httpOptions)

}







tokenStartProcessing(token_id,transactionid,token_no,tokensequenceno)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
     'transactionqueueid':transactionid,
     'tokenid':token_id,
     'tokenno':token_no
     
     
    })
    }
        
    return this.http.post<any>(environment.url+`tokenStartProcessing`
    ,'',httpOptions)

}





tokenProcessingCompleted(queue_id,token_id,transactionid)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
    headers: new HttpHeaders({
    'utoken':atob(utoken),
    'loginuserid':atob(usermasterid),
    'roletype':roletype, 
    'orgid':orgid,
     'transactionqueueid':transactionid,
     'tokenid':token_id,
 'queueid':queue_id
    })
    }
        
    return this.http.post<any>(environment.url+`tokenProcessingCompleted`
    ,'',httpOptions)

}






tokenSaveOnCompleteDetails(token_id,transactionid,formdata)
{

    let Adminuser = JSON.parse(localStorage.getItem('Adminuser'));
    let usermasterid=Adminuser.user_master_id;
    let utoken=Adminuser.utoken; 
    let orguserlist = JSON.parse(localStorage.getItem('orguserrole'));
    let orgid=orguserlist.orgid
    let roletype = JSON.parse(localStorage.getItem('activerole'));
    
    const httpOptions = {
        headers: new HttpHeaders({
            'utoken':atob(utoken),
            'loginuserid':atob(usermasterid),
            'roletype':roletype, 
            'orgid':orgid,
            'transactionqueueid':transactionid,
            'tokenid':token_id
        })
    }
        
    return this.http.post<any>(environment.url+`tokenSaveOnCompleteDetails`,formdata,httpOptions)

}





}