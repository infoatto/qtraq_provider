import { ModuleWithProviders } from '@angular/core';
import { Routes } from '@angular/router';

export interface Loginresponse {
    status_code: number;
    Metadata: Message;
    Data: Userdata;
   }

export interface getCountries
{
    status_code:number;
    Metadata:Message;
    Data:Cdata[]
}


export interface Message
{
   Message:string;
}

export interface Userdata
{
    user_master_id:string;
    org_id:string;
    user_name:string
    first_name:string;
    middle_name?:string;
    last_name?:string;
    profile_pic?:string;
    email_id:string;
    system_user:string;
    system_role_id:string;
    gender:string;
    profileimage_path:string;
    utoken?:string;
    system_roles:string;
    provider_specialities:string
   
}


export interface Cdata
{
    country_id:number;
    country_code:string;
    country_name:string;
    country_phonecode:string;
    currency_code?:string;
}

export interface Plan
{
    utoken:string;
    usermasterid:string;
    planid:any
    planname:string;
    plandescription:string;
    planstartdate:string;
    planenddate:string;
    plantokencount:string;
    planamount:string;
    plancurrency:string;
    plancountryid:string;
    plantype:string;
    Organisation:string;
    plan_image:string;
    planstatus:string;
    plan_image_path:string;
}

export interface responsedata
{
status_code:number;
Metadata:Message;
Data:any
}

export interface editplanresponsedata
{
status_code:number;
Metadata:Message;
Data:Plan
}



export interface ListPlan 
{
    status_code: number;
    Metadata: Message;
    Data: Plandata;
    total_count:string;
}

export interface Plandata
{
    utoken:string;
    usermasterid:string;
    planid:any
    planname:string;
    plandescription:string;
    planstartdate:string;
    planenddate:string;
    plantokencount:string;
    planamount:string;
    plancurrency:string;
    plancountryid:string;
    plantype:string;
    plan_image:string;
    planstatus:string;
    plan_image_path:string;
}








