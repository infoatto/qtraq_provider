import { Utils } from './../../utils/utils';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from './../../_services';
import { LoginService } from './../../providers/login/login.service';
import { configuration } from './../../configuration';
import {MatSnackBar} from '@angular/material';
import { RegistrationValidator } from './register.validator';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { timer, Subscription } from 'rxjs';
import { TimerFormatPipe } from 'app/pipes/timer-format.pipe';
import { ProvidersService } from 'app/_services/providers.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  private classes = ['hold-transition', 'login-page'];
  public emailToRecovery: string;
  public isSubmitting = false;
  public loginform:Boolean=false
  public loginotpform:Boolean=false
  public getotp:any
  public mobilenumber:any
  public msgFeedback: string;
  public projectName = configuration.projectName;
  registrationFormGroup: FormGroup;
  passwordFormGroup: FormGroup;
  otpFlag:any = false;
  otpResendTime:any;
  counter:any = 10;
  tick = 1000;
  constructor(
    private loginService: LoginService,
    private snackbar:MatSnackBar,  
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router:Router,
    private providerservice:ProvidersService
  ){
    // localStorage.removeItem('remainingTime');
    // localStorage.removeItem('otpFlag');
    this.passwordFormGroup = this.formBuilder.group({
      otp:['', Validators.required],
      mobilenumber:['', Validators.required],
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    },{
      validator: RegistrationValidator.validate.bind(this)
    });
    this.registrationFormGroup = this.formBuilder.group({
      passwordFormGroup: this.passwordFormGroup
    });
    this.counter = (localStorage.getItem("remainingTime") != undefined)?localStorage.getItem("remainingTime"):180;
    var otpTimerFlag:any = (localStorage.getItem("otpFlag") != undefined)?localStorage.getItem("otpFlag"):false;
    if(otpTimerFlag == 1){
      this.startTimer();
    }
  }
  
  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    for (const cl of this.classes) {
      body.classList.add(cl);
    }
   
   
  }

  genotpclick(){
    if(this.passwordFormGroup.controls['mobilenumber'].value !=""){
      this.mobilenumber = this.passwordFormGroup.controls['mobilenumber'].value
      this.providerservice.generateOtp("", this.mobilenumber,"resetpassword").subscribe((response) => {
        if(response.status_code=="200"){
          this.getotp=response.otp
          this.loginotpform=true
          this.loginform=false
          this.snackbar.open(response.Metadata.Message,'', {
            duration: 2000,
          });
          localStorage.setItem("remainingTime","180");
          this.startTimer();
        }else{
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
          this.otpFlag = false;
          localStorage.setItem("otpFlag","0");
          localStorage.setItem("remainingTime","180")
          return false;
        }
      });
    }else{
      this.snackbar.open("Please Enter Mobile Number",'', {duration: 2000,});
    }
  }

 

  startTimer(){
    this.counter = (localStorage.getItem("remainingTime") != undefined)?localStorage.getItem("remainingTime"):180;
    if(this.counter > 0){
      this.otpFlag = true;
      localStorage.setItem("otpFlag","1");
      var timersub = timer(0, this.tick).subscribe(() =>{
        --this.counter;
        this.otpResendTime = this.counter;
        localStorage.setItem("remainingTime",this.otpResendTime);
        if(this.counter == 0){
          this.otpFlag = false;
          localStorage.setItem("otpFlag","0");
          localStorage.setItem("remainingTime","180")
          timersub.unsubscribe();
          return false;
        }
      });
    }else{
      this.otpFlag = false;
      localStorage.setItem("otpFlag","0");
      localStorage.setItem("remainingTime","180")
      return false;
    }
  }

  onClickRegister(formvalue:any){
    if(formvalue != undefined){
      this.authService.forgotPasswordsave(
        formvalue.passwordFormGroup.password,
        formvalue.passwordFormGroup.repeatPassword,
        formvalue.passwordFormGroup.otp,
        formvalue.passwordFormGroup.mobilenumber,
      ).subscribe((response:any) => {
        if(response.status_code=="200"){
          this.router.navigate(["/login"]);
          this.getotp=response.otp;
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }else{
          this.snackbar.open(response.Metadata.Message,'', {duration: 2000,});
        }
      })
    }
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    for (const cl of this.classes) {
      body.classList.remove(cl);
    }
  }

  public async recoveryPassword(): Promise<void> {
    this.isSubmitting = true;
    this.msgFeedback = '';
    try {
      this.msgFeedback = 'Email de recuperação enviado com sucesso.';
    } catch (e) {
      switch (e.status) {
        case 'InvalidEmail':
          this.msgFeedback = 'Email inválido ou não encontrado entre os alunos cadastrados.';
          break;
        default:
          this.msgFeedback = 'Um erro inesperado aconteceu';
          break;
      }
    }
    this.isSubmitting = false;
  }
}
