import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { UtilsService } from './../../providers/utils/utils.service';
import { CustomError } from './../../models/custom-error';
import { ToasterService } from 'angular2-toaster';
import { configuration } from './../../configuration';
import { AuthService } from '@pluritech/auth-service';
import { Credential } from './../../models/credential';
import { LoginService } from './../../providers/login/login.service';
import { AuthenticationService } from './../../_services';
import { OrglistComponent } from './../../component/orglist/orglist.component'
import { ErrorsnackbarComponent } from './../../pages/errorsnackbar/errorsnackbar.component'
import { MatDialogConfig } from '@angular/material'
import { MatDialog } from '@angular/material';
import { CommonService } from './../../_services';
import { MatSnackBar } from '@angular/material';

import{ DialognewtoqrmComponent } from './../../component/dialognewtoqrm/dialognewtoqrm.component'



export class DialogContentExample {
  constructor(public dialog: MatDialog, 
    //private afs: AngularFirestore
    ) { }

}



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})



export class LoginComponent implements OnInit, OnDestroy {

  private classes = ['hold-transition', 'login-page'];
  public projectName = configuration.projectName;
  public isFormSubmitted: boolean;
  public loginForm: FormGroup;
  public errorMsg: string;
  returnUrl: string;
  loading = false;
  submitted = false;
  error = '';
  role = '';
  massage = "";
  checkboxcheck:any
  userData:any;
  responsedatatext:any
  dialogRef:any
  modelcatlogo:Boolean=false
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orgservice: CommonService,
    private loginService: LoginService,
    private authService: AuthService,
    private toasterService: ToasterService,
    // private utilsService: UtilsService,
    private authservice: AuthenticationService,
    public dialog: MatDialog,
    //public msg: MessagingService,
    private snackbar: MatSnackBar,
   
  ) { }

  private initForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  get f() { return this.loginForm.controls; }

  onSubmit() {


    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.authservice.login(this.f.username.value, this.f.password.value)
      .subscribe(
        data => {
          if (data.Data != null && data.Data != []) {
            // this.router.navigate(["/"]);
            // return;

            if (data.status_code == "200") {



if(data.Data.first_time_login==true)
{

this.logofun('open')

}else{

              this.orgservice.getorganisationlist()
              .subscribe(
                (response) => {
                  if (response.status_code == 200) {
                    if (response.Data.length > 1) {
                      this.dialog.open(OrglistComponent, { disableClose: true })
                    }
                    else {
                      this.setorganisation()
                    }
                  }
                  else {
this.responsedatatext = "You do not have access to any QRM registered organisations.Please contact your Organisation Admin for access"

                    this.snackbar.openFromComponent(ErrorsnackbarComponent, {
                      duration: 2000,
                      } 
                    )
                  }
                  
                  
                })
                
}
                
              }
              else {
                
              }
              
              // this.dialog.open(OrglistComponent, {disableClose: true })
              // const dialogRef = this.dialog.open(DialogContentExampleDialog);
              /*
              dialogRef.afterClosed().subscribe(result => {

              });
              */
            }
            else {
              
              
              this.massage = data.Metadata.Message
              
            }
          },
          error => {

          });
          
        }
        ngOnInit() {


/*
          const data = {
            uid: "dfdfdfdf",
            email: "test@gmailfddf.com",
            displayName: "i dontfdfd know",
            photoURL: "i don,tfdf know"
          }
          
          this.msg.getPermission(data)
      
          this.msg.monitorRefresh(data)
          this.msg.receiveMessages()
          var constdata = this.msg.gettokendata()
  */        
          
          this.initForm();
          
          
          const body = document.getElementsByTagName('body')[0];
          for (const cl of this.classes) {
            body.classList.add(cl);
          }
        }
        ngOnDestroy() {
          const body = document.getElementsByTagName('body')[0];
          for (const cl of this.classes) {
            body.classList.remove(cl);
          }
        }
        
        
           
        //new code for singal organisation start
        
        setorganisation() {
          
          
          this.orgservice.getorganisationlist()
          .subscribe(
            (response) => {
              
              if (response.status_code == "200") {
            localStorage.setItem('orguserrole', "")
            var orguserrole =
              {
                "adminrole": response.Data[0].roles[0].isAssigned,
                "frontdesk": response.Data[0].roles[1].isAssigned,
                "doctor": response.Data[0].roles[2].isAssigned,
                "orgname": response.Data[0].org_name,
                "orgid": response.Data[0].org_id,
                "orgimg": response.Data[0].org_logo,
                "org_type":response.Data[0].org_type
              }
            localStorage.setItem('orguserrole', JSON.stringify(orguserrole));
            localStorage.setItem('actorganistion', "")
            var orgactive =
              {
                "org_name": response.Data[0].org_name,
                "org_image": response.Data[0].org_logo,
              }
            localStorage.setItem('actorganistion', JSON.stringify(orgactive));



            var actas = null

            if (response.Data[0].roles[2].isAssigned == true) {
              actas = {
                "activerole": 'l2'
              }
            }

            else if (response.Data[0].roles[1].isAssigned == true) {
              actas = {
                "activerole": "l1"
              }
            }
            else if (response.Data[0].roles[0].isAssigned == true) {
              actas = {
                "activerole": "admin"
              }
            }
            //this.dialogRef.close();
            localStorage.removeItem('activerole');
            localStorage.setItem('activerole', JSON.stringify(actas));
            this.router.navigate(["/"]);

            //window.location.reload()
            if (actas == null) {
              localStorage.clear();
              this.router.navigate(["/login"]);

              /*
              this.snackbar.open("System role not available", '', {
                duration: 2000,
              });
*/
this.snackbar.openFromComponent(ErrorsnackbarComponent, {
  duration: 2000,
  } 
)
              // window.location.reload()
            }
            else {
              
            }
          }
          else {





            this.snackbar.open(response.Metadata.Message, '', {
              duration: 2000,
            })
          }
          /*
          
            localStorage.setItem('orguserrole',"")
              var orguserrole=
              {
              "adminrole":rolesadmin,
              "frontdesk":frontdesk,
              "doctor":doctor,
              "orgname":orgname,
              "orgid":orgid,
              "orgimg": org_logo
            }
          localStorage.setItem('orguserrole', JSON.stringify(orguserrole));
          localStorage.setItem('actorganistion',"")
          var orgactive=
          {
            "org_name":orgname,
            "org_image":org_logo,
          }
          localStorage.setItem('actorganistion', JSON.stringify(orgactive));
          
          
          
          var actas=null
          
          if(doctor==true)
          {
             actas={
              "activerole":'l2'
                }
          }
          
          else if(frontdesk==true)
          {
             actas={
              "activerole":"l1"
                }
          }
          else if(rolesadmin==true)
          {
             actas={
              "activerole":"admin"
                }
          }
          //this.dialogRef.close();
          localStorage.removeItem('activerole');
          localStorage.setItem('activerole',JSON.stringify(actas));
          window.location.reload()
          if(actas==null)
          {
          localStorage.clear();
          this.router.navigate(["/login"]); 
          
          this.snackbar.open("System role not available",'', {
            duration: 2000,
            });
            window.location.reload()
          }
          else
          {
            
          }
          */

        })

  }


  logofun(action:string)
  {
  
      if(action == "open"){
          this.modelcatlogo = true;
              }else{
                  this.modelcatlogo = false;
                  }
  
  }
  
  logincancel(){
    this.logofun("cancel")
  }

  loginconditionsubmit(){
    if(this.checkboxcheck=="Yes"){
      this.orgservice.saveLoginInfo(this.checkboxcheck).subscribe((response) => {
        if (response.status_code == "200") {
          this.logofun("cancel")
          this.orgservice.getorganisationlist().subscribe((response) => {
              if (response.status_code == 200) {
                if (response.Data.length > 1) {
                  this.dialog.open(OrglistComponent, { disableClose: true })
                }
                else {
                  this.setorganisation()
                }
              }else {
                this.responsedatatext = "You do not have access to any QRM registered organisations.Please contact your Organisation Admin for access"
                this.snackbar.openFromComponent(ErrorsnackbarComponent, {duration: 2000,});
              }
            })
          }else{
            this.snackbar.open(response.Metadata.Message, '', {duration: 2000,});
          }
      })
    }
  }

  oncheckboxchange(event){
    if(event.target.checked==false){
      this.checkboxcheck="No"
    }else{
      this.checkboxcheck="Yes"
    }
  }

  opendielog(){
    this.dialogRef=this.dialog.open(DialognewtoqrmComponent,{height:'auto',width:'300px'});
  }

}

