import { Router, NavigationEnd, RouterOutlet } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Event as RouterEvent,
  NavigationStart,
  NavigationCancel,
  NavigationError
} from '@angular/router'
import {trigger,state,style,animate,transition} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':leave', [   // :leave is alias to '* => void'
        animate('1000ms ease-out', style({opacity:2})) 
      ])
    ]),
    trigger('enterAnimation', [
      transition(':enter', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate('500ms ease-in', style({transform: 'translateY(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateY(0)', opacity: 1}),
        animate('500ms ease-in', style({transform: 'translateY(100%)', opacity: 0}))
      ])
    ])
  ]
})
export class AppComponent implements OnInit {

constructor(private router: Router){
  // router.events.subscribe((event: RouterEvent) => {
  //   this.navigationInterceptor(event)
  // })

  this.router.events.subscribe((event: any) => {
    switch (true) {
      case event instanceof NavigationStart: {
        this.loading = true;
        break;
      }
      case event instanceof NavigationEnd:{
        this.loading = true;
      }
      case event instanceof NavigationCancel:
      case event instanceof NavigationError: {
      // case event instanceof NavigationEnd: {
        setTimeout(() => {
          this.loading = false;
        }, 1000);
        break;
      }
      default: {
        break;
      }
    }
  });
  
}

haderany:Boolean=false
loading = true;
orgerrolelist=""
  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }

      $('html,body').animate({ scrollTop: 0 }, 'slow', () => {
        $('body').removeClass('sidebar-open');
      });


      const collapse = $('.navbar-collapse');
      if (collapse && collapse[0] && collapse[0].classList.contains('in')) {
        collapse[0].classList.remove('in');
      }



    });
  
  
  }
}
