import { ModuleWithProviders } from '@angular/core';
import { Routes } from '@angular/router';




export interface ListTest 
{
    status_code: number;
    Metadata: Message;
    Data: Testdata;
    total_count:string;
}

export interface Message
{
   Message:string;
}

export interface Testdata
{
"test_master_id":string,
"test_name":string,
"status":string,
"created_on":string,
"created_by":string,
"updated_on":string,
"updated_by":string
}


