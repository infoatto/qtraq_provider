import { Directive, ElementRef, HostListener, Input,Renderer,OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
@Directive({ selector: '[myFocus]' })
export class FocusDirective implements OnInit {
 
  @Input('myFocus') isFocused: boolean;
 
  constructor(private hostElement: ElementRef, private renderer: Renderer) {}
 
  ngOnInit() {
    if (this.isFocused) {
      this.renderer.invokeElementMethod(this.hostElement.nativeElement, 'focus');
    }
  }
}