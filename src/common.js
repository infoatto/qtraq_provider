function test(){
  //jQuery('.two-input-right.com-checkbox').addClass('test');
}
jQuery(document).ready(function() {
  $(document).on("change", ".upload-btn-wrapper input[type='file']" , function() {
    var i = $(this).parent().find('.btn-upload').clone();
    var file = $(this)[0].files[0].name;
    var exts = ['jpg','png','jpeg'];
    // first check if file field has any value
    if ( file ) {
      // split file name at dot
      var get_ext = file.split('.');
      // reverse name to check extension
      get_ext = get_ext.reverse();
      // check file type is valid as given in 'exts' array
      if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
        $(this).next('.image-name').text(file);
      } else {
        $(this).next('.image-name').text('');
      }
    }
  });

  /* $('.upload-btn-wrapper input').change(function() {
    var i = $(this).prev('.btn-upload').clone();
    var file = $(this)[0].files[0].name;
    $(this).prev('.btn-upload').text(file);
  }); */

   //jQuery(".quepro-list-table").clone(true).appendTo('#table-scroll').addClass('clone');
  //  alert("called");

   jQuery(document).on("click",".sidebar-dropdown > a",function() {
    jQuery(".sidebar-submenu").slideUp(200);
    if (
      jQuery(this)
        .parent()
        .hasClass("active")
    ) {
      jQuery(".sidebar-dropdown").removeClass("active");
      jQuery(this)
        .parent()
        .removeClass("active");
    } else {
      jQuery(".sidebar-dropdown").removeClass("active");
      jQuery(this)
        .next(".sidebar-submenu")
        .slideDown(200);
      jQuery(this)
        .parent()
        .addClass("active");
    }
  });

  jQuery("#close-sidebar").click(function() {
    jQuery(".page-wrapper").removeClass("toggled");
  });
  jQuery("#show-sidebar").click(function() {
    jQuery(".page-wrapper").addClass("toggled");
  });



/**crop image while uploading */
var crop_max_width = 400;
var crop_max_height = 400;
var jcrop_api;
var canvas;
var context;
var image;

var prefsize;

jQuery(".file").change(function() {
  loadImage(this);
});

function loadImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    canvas = null;
    reader.onload = function(e) {
      image = new Image();
      image.onload = validateImage;
      image.src = e.target.result;
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function dataURLtoBlob(dataURL) {
  var BASE64_MARKER = ';base64,';
  if (dataURL.indexOf(BASE64_MARKER) == -1) {
    var parts = dataURL.split(',');
    var contentType = parts[0].split(':')[1];
    var raw = decodeURIComponent(parts[1]);

    return new Blob([raw], {
      type: contentType
    });
  }
  var parts = dataURL.split(BASE64_MARKER);
  var contentType = parts[0].split(':')[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);
  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], {
    type: contentType
  });
}

function validateImage() {
  if (canvas != null) {
    image = new Image();
    image.onload = restartJcrop;
    image.src = canvas.toDataURL('image/png');
  } else restartJcrop();
}

function restartJcrop() {
  if (jcrop_api != null) {
    jcrop_api.destroy();
  }
  jQuery(".views").empty();
  jQuery(".views").append("<canvas id=\"canvas\">");
  canvas = jQuery("#canvas")[0];
  context = canvas.getContext("2d");
  canvas.width = image.width;
  canvas.height = image.height;
  context.drawImage(image, 0, 0);
  jQuery("#canvas").Jcrop({
    onSelect: selectcanvas,
    onRelease: clearcanvas,
    boxWidth: crop_max_width,
    boxHeight: crop_max_height
  }, function() {
    jcrop_api = this;
  });
  clearcanvas();
}

function clearcanvas() {
  prefsize = {
    x: 0,
    y: 0,
    w: canvas.width,
    h: canvas.height,
  };
}

function selectcanvas(coords) {
  prefsize = {
    x: Math.round(coords.x),
    y: Math.round(coords.y),
    w: Math.round(coords.w),
    h: Math.round(coords.h)
  };
}

function applyCrop() {
  canvas.width = prefsize.w;
  canvas.height = prefsize.h;
  context.drawImage(image, prefsize.x, prefsize.y, prefsize.w, prefsize.h, 0, 0, canvas.width, canvas.height);
  validateImage();
}

function applyScale(scale) {
  if (scale == 1) return;
  canvas.width = canvas.width * scale;
  canvas.height = canvas.height * scale;
  context.drawImage(image, 0, 0, canvas.width, canvas.height);
  validateImage();
}

function applyRotate() {
  canvas.width = image.height;
  canvas.height = image.width;
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.translate(image.height / 2, image.width / 2);
  context.rotate(Math.PI / 2);
  context.drawImage(image, -image.width / 2, -image.height / 2);
  validateImage();
}

function applyHflip() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.translate(image.width, 0);
  context.scale(-1, 1);
  context.drawImage(image, 0, 0);
  validateImage();
}

function applyVflip() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.translate(0, image.height);
  context.scale(1, -1);
  context.drawImage(image, 0, 0);
  validateImage();
}

jQuery("#cropbutton").click(function(e) {
  applyCrop();
});
jQuery("#scalebutton").click(function(e) {
  var scale = prompt("Scale Factor:", "1");
  applyScale(scale);
});
jQuery("#rotatebutton").click(function(e) {
  applyRotate();
});
jQuery("#hflipbutton").click(function(e) {
  applyHflip();
});
jQuery("#vflipbutton").click(function(e) {
  applyVflip();
});

jQuery("#form").submit(function(e) {
  e.preventDefault();
  formData = new FormData(jQuery(this)[0]);
  var blob = dataURLtoBlob(canvas.toDataURL('image/png'));
  //---Add file blob to the form data
  formData.append("cropped_image[]", blob);
  jQuery.ajax({
    url: "whatever.php",
    type: "POST",
    data: formData,
    contentType: false,
    cache: false,
    processData: false,
    success: function(data) {
      alert("Success");
    },
    error: function(data) {
      alert("Error");
    },
    complete: function(data) {}
  });
});
});
$(document).mouseup(function(e)
{
    var container = $("#sidebar");

    // if the target of the click isn't the container nor a descendant of the container
    //if (!container.is(e.target) && container.has(e.target).length === 0)
    //{
      if (!$(event.target).closest('.home-bar-btn').length) {
        // ... clicked on the 'body', but not inside of .home-bar-btn
        container.fadeOut(100);
        //alert('test');
      }
      else{
        //alert('test2');
        container.fadeIn(100);
      }
        //container.fadeOut(100);
        //$('.page-wrapper.chiller-theme').removeClass('toggled');
    //}
});
$(document).on("click", "#tnc-check" , function() {
  if ($(this).is(':checked')) {
    $('#proceedbtn').attr("disabled", false);
  }else{
    $('#proceedbtn').attr("disabled", true);
  }
  //$('#proceedbtn').attr("disabled", true);
});
$(document).on('change', ".override-time", function(){
  $(this).addClass('override-time-change');
  $(this).parent().siblings().find('.override-time').addClass('override-time-change');
});
$(document).on("click", ".checkbox-theme input[type='checkbox']" , function() {
  if($(this).prop('checked')){
    $(this).next('label').removeClass('text_grey');
    $(this).next('label').addClass('text_pink')
  }else{
    $(this).next('label').removeClass('text_pink');
    $(this).next('label').addClass('text_grey')
  }
});

$(document).on('change', ".customfield-select", function(){
  if($(this).val() != ''){
    $('.customfield-select').addClass('customselect-selected');
  }
  else{
    $('.customfield-select').removeClass('customselect-selected')
  }
});
$(document).on("click", ".radio-theme input[type='radio']" , function() {
  if($(this).is(':checked')){
    $(this).parent('label').removeClass('text_grey');
    $('.radio-theme label').removeClass('text_pink');
    $(this).parent('label').addClass('text_pink');
  }/* else{

    $(this).parent('label').removeClass('text_pink');
    $(this).parent('label').addClass('text_grey')
  } */
});
$(document).ready(function(){
  if($(".radio-theme input[type='radio']").is(':checked')){
  //  alert('test');
    $(this).parent('label').addClass('text_pink');
  }
});
$(window).on("load resize ", function() {
  var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
  $('.tbl-header').css({'padding-right':scrollWidth});
}).resize();

$(document).on("change", ".upload-btn-wrapper input[type='file']" , function() {
  var i = $(this).parent().find('.btn-upload').clone();
  var file = $(this)[0].files[0].name;
  var exts = ['jpg','png','jpeg'];
  // first check if file field has any value
  if ( file ) {
    // split file name at dot
    var get_ext = file.split('.');
    // reverse name to check extension
    get_ext = get_ext.reverse();
    // check file type is valid as given in 'exts' array
    if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
      $(this).next('.image-name').text(file);
    } else {
      $(this).next('.image-name').text('');
    }
  }
  //$(this).parent().find('.btn-upload').text(file);
  //$(this).next('.image-name').text(file);
});
$(document).on("click", ".sidebar-menu ul li a" , function() {
  $('.sidebar-menu ul li').removeClass('active');
    $(this).parent('li').addClass('active')
  });

$('table.queueacess-table tbody').scroll(function(e) { //detect a scroll event on the tbody
  /*
  Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
  of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
  */
  $('table.queueacess-table thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
  $('table.queueacess-table thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
  $('table.queueacess-table tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
});

// function EZView(){
// }
